# Install base image fronm nodeJS v14
FROM node:14-alpine
ARG UID
# Install dependencies
RUN apk --no-cache add git
# Set user to the current host user
USER $UID
# Set current directory to /app
WORKDIR /app
# Copy packages files
COPY --chown=$UID:$UID package.json .
COPY --chown=$UID:$UID package-lock.json .

ENTRYPOINT [ "/app/entrypoint.sh" ]
# Expose port
EXPOSE 3000
CMD [ "npm", "start" ]