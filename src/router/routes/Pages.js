import { lazy } from "react";

const PagesRoutes = [
  {
    path: "/login",
    component: lazy(() =>
      import("../../views/pages/authentication/Login/index")
    ),
    layout: "BlankLayout",
    meta: {
      authRoute: true,
    },
  },
  {
    path: "/myhome",
    component: lazy(() => import("../../views/pages/testHome")),
  },
  {
    path: "/PrepairedType",
    component: lazy(() => import("../../views/pages/UsgPrepairedType")),
  },
  {
    path: "/TypeMain",
    component: lazy(() => import("../../views/pages/system_data/UsgTypeMain")),
  },
  {
    path: "/TypeSub",
    component: lazy(() => import("../../views/pages/system_data/UsgTypeSub")),
  },
  {
    path: "/TypeSubject",
    component: lazy(() =>
      import("../../views/pages/system_data/UsgTypeSubject")
    ),
  },
  {
    path: "/AssignedType",
    component: lazy(() => import("../../views/pages/UsgAssignedType")),
  },
  {
    path: "/ItemModel",
    component: lazy(() => import("../../views/pages/UsgItemModel")),
  },

  {
    path: "/UsgMonth",
    component: lazy(() => import("../../views/pages/system_data/UsgMonth")),
  },
  {
    path: "/UsgAsnafModelsUnit",
    component: lazy(() => import("../../views/pages/UsgAsnafModelunit")),
  },
  {
    path: "/UsgAsnafSetup",
    component: lazy(() => import("../../views/pages/UsgAsnafSetup/Main")),
  },
  {
    path: "/UsgOhda7sabSanf",
    component: lazy(() => import("../../views/pages/UsgOhda7sabSanf/main")),
  },
  {
    path: "/UsgOhda7sabSanff/:id/edit",
    component: lazy(() => import("../../views/pages/UsgOhda7sabSanf/edit")),
  },
  {
    path: "/UsgAsnafModels",
    component: lazy(() => import("../../views/pages/UsgAsnafModel/Main")),
  },

  {
    path: "/UsgAsnafModel/:id/edit",
    component: lazy(() => import("../../views/pages/UsgAsnafModel/Edit")),
  },

  {
    path: "/UsgYearPeriod",
    component: lazy(() =>
      import("../../views/pages/system_data/UsgYearPeriod")
    ),
  },
  {
    path: "/UsgTrainingPlan",
    component: lazy(() =>
      import("../../views/pages/system_data/UsgTrainingPlan")
    ),
  },
  {
    path: "/UsgTrainingYear",
    component: lazy(() =>
      import("../../views/pages/system_data/UsgTrainingYear")
    ),
  },

  {
    path: "/misc/not-authorized",
    component: lazy(() => import("../../views/pages/misc/NotAuthorized")),
    layout: "BlankLayout",
    meta: {
      publicRoute: true,
    },
  },
  {
    path: "/misc/error",
    component: lazy(() => import("../../views/pages/misc/Error")),
    layout: "BlankLayout",
    meta: {
      publicRoute: true,
    },
  },
  {
    path: "/profile",
    component: lazy(() => import("../../views/pages/personal_account/Profile")),
    // meta: {
    //   publicRoute: true,
    // },
  },
  {
    path: "/UsgType/training",
    component: lazy(() => import("../../views/pages/UsgType/training/index")),
  },
  {
    path: "/UsgType/training_/:id/serials",
    // component: lazy(() => import("../../views/pages/UsgType/subject/index")),
    component: lazy(() => import("../../views/pages/UsgType/training/Edit")),
  },
  {
    path: "/UsgType/combat",
    component: lazy(() => import("../../views/pages/UsgType/combat/index")),
  },
  {
    path: "/UsgType/plain",
    component: lazy(() => import("../../views/pages/UsgType/plain/index")),
  },
  {
    path: "/UsgType/combat_/:id/serials",
    component: lazy(() => import("../../views/pages/UsgType/combat/Edit.js")),
  },
  {
    path: "/UsgType/training_subject/:id/edit",
    component: lazy(() => import("../../views/pages/UsgType/subject/index")),
  },
  {
    path: "/UsgType/combat_subject/:id/edit",
    component: lazy(() => import("../../views/pages/UsgType/subject/index")),
  },

  // {
  //   path: "/resetPassword",
  //   component: lazy(() => import("../../views/pages/resetPassword")),
  // },
  {
    path: "/notifications",
    component: lazy(() => import("../../views/pages/Notifications")),
  },
];

export default PagesRoutes;
