// ** Routes Imports
 
import PagesRoutes from './Pages' 
// ** Document title
const TemplateTitle = 'ABo Baker'

// ** Default Route
const DefaultRoute = '/myhome'

// ** Merge Routes
const Routes = [
    
    ...PagesRoutes, 

]
export { DefaultRoute, TemplateTitle, Routes }
