 import { createRequestAsync, methods } from '../../generic_redux'


export const STATE_ID = "UsgAsnafSetupReducer"

export const postUsgAsnafSetupRequestAsync
    = createRequestAsync({
        stateId: STATE_ID,
        method: methods.POST,
        endPoint: "UsgAsnafSetup/Post/"
    })
 