import { createReducer } from "../generic_redux"
import { STATE_ID } from "./thunk"

const UsgAsnafSetupReducer = createReducer({ stateId: STATE_ID })

export default UsgAsnafSetupReducer