import {
  ENABLE_SAVE_BUTTON,
  DISABLE_SAVE_BUTTON,
  ENABLE_NAVBAR_BUTTONS,
  DISABLE_NAVBAR_BUTTONS,
  ENABLE_UPLOAD_BUTTON,
  DISABLE_UPLOAD_BUTTON,
  ENABLE_DELETE_BUTTON,
  DISABLE_DELETE_BUTTON,
  ENABLE_PRINT_BUTTON,
  DISABLE_PRINT_BUTTON,
  ENABLE_LOG_BUTTON,
  DISABLE_LOG_BUTTON,
  UPDATE_NUMBER_UPLOAD_BUTTON
} from './actions'

/**
 *
 * @param {Function} hanlder the callback of the handler
 * @param {String} title the title which shown in the toolip
 * @returns {Object}
 */
export const enableSaveButton = (handler, title = "حفظ") => {
  return {
    type: ENABLE_SAVE_BUTTON,
    title,
    handler,
    show: true
  }
}

export const disableSaveButton = () => {
  return {
    type: DISABLE_SAVE_BUTTON,
    title: "",
    handler: () => { },
    show: false
  }
}

export const enableUploadButton = (handler, title = "مرفقات") => {
  return {
    type: ENABLE_UPLOAD_BUTTON,
    title,
    handler,
    show: true
  }
}

export const disableUploadButton = () => {
  return {
    type: DISABLE_UPLOAD_BUTTON,
    title: "",
    handler: () => {},
    show: false
  }
}

export const updateNumberUploadButton = (newNumber) => {
  return {
    type: UPDATE_NUMBER_UPLOAD_BUTTON,
    number: newNumber
  }
}

export const enableDeleteButton = (handler, title = "حذف") => {
  return {
    type: ENABLE_DELETE_BUTTON,
    title,
    handler,
    show: true
  }
}

export const disableDeleteButton = () => {
  return {
    type: DISABLE_DELETE_BUTTON,
    title: "",
    handler: () => {},
    show: false
  }
}


export const enablePrintButton = (handler, title = "طباعة") => {
  return {
    type: ENABLE_PRINT_BUTTON,
    title,
    handler,
    show: true
  }
}

export const disablePrintButton = () => {
  return {
    type: DISABLE_PRINT_BUTTON,
    title: "",
    handler: () => {},
    show: false
  }
}

export const enableLogButton = (handler, title = "تتبع") => {
  return {
    type: ENABLE_LOG_BUTTON,
    title,
    handler,
    show: true
  }
}

export const disableLogButton = () => {
  return {
    type: DISABLE_LOG_BUTTON,
    title: "",
    handler: () => {},
    show: false
  }
}

export const enableNavbarButtons = (buttons) => {
  return {
    type: ENABLE_NAVBAR_BUTTONS,
    arrayOfButtons: buttons,
    enableArrayOfButtons: true
  }
}






export const disableNavbarButtons = () => {
  return {
    type: DISABLE_NAVBAR_BUTTONS,
    arrayOfButtons: [],
    enableArrayOfButtons: false
  }
}

