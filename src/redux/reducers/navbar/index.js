import {
  ENABLE_SAVE_BUTTON,
  DISABLE_SAVE_BUTTON,
  ENABLE_UPLOAD_BUTTON,
  DISABLE_UPLOAD_BUTTON,
  ENABLE_NAVBAR_BUTTONS,
  DISABLE_NAVBAR_BUTTONS,
  ENABLE_DELETE_BUTTON,
  DISABLE_DELETE_BUTTON,
  UPDATE_NUMBER_UPLOAD_BUTTON,
  ENABLE_PRINT_BUTTON,
  DISABLE_PRINT_BUTTON,
  ENABLE_LOG_BUTTON,
  DISABLE_LOG_BUTTON,
} from "./actions";

// ** Initial State
const initialState = {
  suggestions: [],
  bookmarks: [],
  query: "",

  // saveButtonParameters
  buttonParameters: {
    title: "حفظ",
    handler: () => {},
    show: false,
  },
  uploadButtonParameters: {
    title: "مرفقات",
    handler: () => {},
    show: false,
    number: 0,
  },

  deleteButtonParameters: {
    title: "حذف",
    handler: () => {},
    show: false,
  },

  printButtonParameters: {
    title: "طباعة",
    handler: () => {},
    show: false,
  },

  logButtonParameters: {
    title: "تتبع",
    handler: () => {},
    show: false,
  },

  navbarButtons: {
    // each element will be an object consists of {title, handler}
    arrayOfButtons: [],
    enableArrayOfButtons: false,
  },
};

const navbarReducer = (state = initialState, action) => {
  switch (action.type) {
    case "HANDLE_SEARCH_QUERY":
      return { ...state, query: action.val };
    case "GET_BOOKMARKS":
      return {
        ...state,
        suggestions: action.data,
        bookmarks: action.bookmarks,
      };
    case "UPDATE_BOOKMARKED":
      let objectToUpdate;

      // ** find & update object
      state.suggestions.find((item) => {
        if (item.id === action.id) {
          item.isBookmarked = !item.isBookmarked;
          objectToUpdate = item;
        }
      });

      // ** Get index to add or remove bookmark from array
      const bookmarkIndex = state.bookmarks.findIndex(
        (x) => x.id === action.id
      );

      if (bookmarkIndex === -1) {
        state.bookmarks.push(objectToUpdate);
      } else {
        state.bookmarks.splice(bookmarkIndex, 1);
      }

      return { ...state };

    case ENABLE_SAVE_BUTTON:
    case DISABLE_SAVE_BUTTON:
      return {
        ...state,
        buttonParameters: {
          title: action.title,
          handler: action.handler,
          show: action.show,
        },
      };

    case ENABLE_UPLOAD_BUTTON:
    case DISABLE_UPLOAD_BUTTON:
      return {
        ...state,
        uploadButtonParameters: {
          title: action.title,
          handler: action.handler,
          show: action.show,
        },
      };

    case UPDATE_NUMBER_UPLOAD_BUTTON:
      state.uploadButtonParameters.number = action.number;
      return { ...state };

    case ENABLE_DELETE_BUTTON:
    case DISABLE_DELETE_BUTTON:
      return {
        ...state,
        deleteButtonParameters: {
          title: action.title,
          handler: action.handler,
          show: action.show,
        },
      };

    case ENABLE_NAVBAR_BUTTONS:
    case DISABLE_NAVBAR_BUTTONS:
      return {
        ...state,
        navbarButtons: {
          arrayOfButtons: action.arrayOfButtons,
          enableArrayOfButtons: action.enableArrayOfButtons,
        },
      };

    case ENABLE_PRINT_BUTTON:
    case DISABLE_PRINT_BUTTON:
      return {
        ...state,
        printButtonParameters: {
          title: action.title,
          handler: action.handler,
          show: action.show,
        },
      };

    case ENABLE_LOG_BUTTON:
    case DISABLE_LOG_BUTTON:
      return {
        ...state,
        logButtonParameters: {
          title: action.title,
          handler: action.handler,
          show: action.show,
        },
      };

    default:
      return state;
  }
};

export default navbarReducer;
