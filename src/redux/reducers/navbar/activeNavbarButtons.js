import { useDispatch } from "react-redux";
import {
  disableDeleteButton,
  disableLogButton,
  disablePrintButton,
  disableSaveButton,
  disableUploadButton,
  enableDeleteButton,
  enableLogButton,
  enablePrintButton,
  enableSaveButton,
  enableUploadButton,
} from "./actionCreators";
import { useEffect } from "react";

export const ActiveNavbarButtons = ({ save, remove, print, logs, upload }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    save && dispatch(enableSaveButton(save));
    remove && dispatch(enableDeleteButton(remove));
    logs && dispatch(enableLogButton(logs));
    print && dispatch(enablePrintButton(print));
    upload && dispatch(enableUploadButton(upload));
    return (_) => {
      save && dispatch(disableSaveButton());
      remove && dispatch(disableDeleteButton());
      logs && dispatch(disableLogButton());
      print && dispatch(disablePrintButton());
      upload && dispatch(disableUploadButton());
    };
  }, []);
  return <></>;
};
