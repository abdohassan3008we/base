// ** Redux Imports
import { combineReducers } from "redux";
// ** Reducers Imports
import auth from "./auth";
import navbar from "./navbar";
import layout from "./layout";
import navbarReducer from "./navbar"
import UsgAsnafSetupReducer from "../UsgAsnafSetup/reducer"
import Ohda7sabSanfSetupReducer from "../Ohda7sabSanfSetup/reducer"
import usersReducer from "../auther/users/reducer" 
const rootReducer = combineReducers({
    auth,
    navbar,
    layout,
    navbarReducer,
    usersReducer,
    UsgAsnafSetupReducer,
    Ohda7sabSanfSetupReducer,
});

export default rootReducer;