 
import { httpRequestAsync } from ".";
import apiClient from "../../../utility/services/apiClient";
import { requestAction, requestErrorAction, requestSuccessAction } from "../actions";
import { methods } from "../methods";

export const createRequestAsync = (
    {
        stateId = "",
        api = apiClient,
        endPoint = endPoint,

        method = methods.GET,

        onRequest = requestAction,
        onSuccessRequest = requestSuccessAction,
        onFailureRequest = requestErrorAction,
        dataFields = { data: _ => _ },
    } = {}) =>
    (
        {

            id = "",
            params = null,

            onSuccess = (_) => { },
            onFailure = (_) => { },
        } = {}
    ) =>

        httpRequestAsync(
            {
                stateId: stateId,
                api: api,
                endPoint: endPoint + id,
                params: params,
                method: method,
                onRequest: onRequest,
                onSuccessRequest: onSuccessRequest,
                onFailureRequest: onFailureRequest,
                dataFields: dataFields,
                onSuccess: onSuccess,
                onFailure: onFailure
            }
        )

