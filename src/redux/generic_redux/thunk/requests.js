 

import apiClient from '../../../utility/services/apiClient'
import { requestAction, requestErrorAction, requestSuccessAction } from '../actions'
import { methods } from '../methods'

export const httpRequest = (
    {
        stateId = "",
        method = methods.GET,
        api = apiClient,
        endPoint,
        params = null,

        onRequest = requestAction,
        onSuccessRequest = requestSuccessAction,
        onFailureRequest = requestErrorAction,
        dataFields = { data: _ => _ },

        onSuccess = (_) => { },
        onFailure = (_) => { },
    } = {}
) => {

    let args =
    {
        stateId: stateId,
        api: api,
        endPoint: endPoint,
        params: params,

        onRequest: onRequest,
        onSuccessRequest: onSuccessRequest,
        onFailureRequest: onFailureRequest,
        dataFields: dataFields,

        onSuccess: onSuccess,
        onFailure: onFailure,
    }
    switch (method.toUpperCase()) {
        case methods.GET:
            return getRequest(args)
        case methods.LIST:
            return listRequest(args)
        case methods.POST:
            return postRequest(args)
        case methods.PUT:
            return putRequest(args)
        case methods.DELETE:
            return deleteRequest(args)
        default: return null
    }
}


export const postRequest = (
    {
        stateId = "",
        api = apiClient,
        endPoint,
        params = null,

        onRequest = requestAction,
        onSuccessRequest = requestSuccessAction,
        onFailureRequest = requestErrorAction,
        dataFields = { data: _ => _ },

        onSuccess = (_) => { },
        onFailure = (_) => { },
    } = {}) =>
    (dispatch) => {
        dispatch(onRequest({ stateId: stateId }));
        api
            .post(endPoint, params)
            .then((response) => {
                dispatch(onSuccessRequest({ stateId: stateId, dataFields: dataFields }));
                onSuccess(response.data)
            })
            .catch((error) => {
                dispatch(onFailureRequest({ stateId: stateId, error: error }));
                onFailure(error)
            });
    };

export const putRequest = (
    {
        stateId = "",
        api = apiClient,
        endPoint,
        params = null,

        onRequest = requestAction,
        onSuccessRequest = requestSuccessAction,
        onFailureRequest = requestErrorAction,
        dataFields = { data: _ => _ },

        onSuccess = (_) => { },
        onFailure = (_) => { },
    } = {}) =>
    (dispatch) => {
        dispatch(onRequest({ stateId: stateId }));
        api
            .put(endPoint, params)
            .then((response) => {
                dispatch(onSuccessRequest({ stateId: stateId, dataFields: dataFields }));
                onSuccess(response.data)
            })
            .catch((error) => {
                dispatch(onFailureRequest({ stateId: stateId, error: error }));
                onFailure(error)
            });
    };

export const deleteRequest = (
    {
        stateId = "",
        api = apiClient,
        endPoint,
        params = null,

        onRequest = requestAction,
        onSuccessRequest = requestSuccessAction,
        onFailureRequest = requestErrorAction,
        dataFields = { data: _ => _ },

        onSuccess = (_) => { },
        onFailure = (_) => { },
    } = {}) => {

    return (dispatch) => {
        dispatch(onRequest({ stateId: stateId }));
        api
            .delete(endPoint)
            .then((response) => {
                dispatch(onSuccessRequest({ stateId: stateId, dataFields: dataFields }));
                onSuccess(response.data)
            })
            .catch((error) => {
                dispatch(onFailureRequest({ stateId: stateId, error: error }));
                onFailure(error)
            });
    };
}


export const getRequest = (
    {
        stateId = "",
        api = apiClient,
        endPoint,
        params = null,

        onRequest = requestAction,
        onSuccessRequest = requestSuccessAction,
        onFailureRequest = requestErrorAction,
        dataFields = { data: _ => _ },

        onSuccess = (_) => { },
        onFailure = (_) => { },
    } = {}) =>
    (dispatch) => {
        dispatch(onRequest({ stateId: stateId }));
        api
            .get(endPoint, params)
            .then((response) => {
                dispatch(onSuccessRequest({ stateId: stateId, data: response.data, dataFields: dataFields }))
                onSuccess(response.data)
            })
            .catch((error) => {
                dispatch(onFailureRequest({ stateId: stateId, error: error }));
                onFailure(error)
            });
    };




export const listRequest = (
    {
        stateId = "",
        api = apiClient,
        endPoint,
        params = null,

        onRequest = requestAction,
        onSuccessRequest = requestSuccessAction,
        onFailureRequest = requestErrorAction,
        dataFields = { data: _ => _ },

        onSuccess = (_) => { },
        onFailure = (_) => { },
    } = {}) =>
    getRequest(
        {
            stateId: stateId,
            api: api,
            endPoint: endPoint,
            params: params,
            onRequest: onRequest,
            onSuccessRequest: onSuccessRequest,
            onFailureRequest: onFailureRequest,
            dataFields: dataFields,
            onSuccess: onSuccess,
            onFailure: onFailure,
        }
    )

