import {
  REQUEST_ACTION,
  REQUEST_SUCCESS_ACTION,
  REQUEST_ERROR_ACTION,
} from "./actions_types";

export const requestAction = ({ stateId = "" }) => ({
  type: REQUEST_ACTION(stateId),
});

export const requestSuccessAction = ({
  stateId = "",
  data,
  dataFields,
} = {}) => {
  return {
    type: REQUEST_SUCCESS_ACTION(stateId),
    payload: data,
    fields: dataFields,
  };
};

export const requestErrorAction = ({ stateId = "", error }) => ({
  type: REQUEST_ERROR_ACTION(stateId),
  payload: error,
});

