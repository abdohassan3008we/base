 
export const REQUEST_ACTION = (stateId) => stateId + "_REQUEST_ACTION_"
export const REQUEST_SUCCESS_ACTION = (stateId) => stateId + "_REQUEST_SUCCESS_ACTION_"
export const REQUEST_ERROR_ACTION = (stateId) => stateId + "_REQUEST_ERROR_ACTION_"