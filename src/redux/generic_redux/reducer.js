 

import {
    REQUEST_ACTION,
    REQUEST_SUCCESS_ACTION,
    REQUEST_ERROR_ACTION,
} from "./actions_types";


const commonInitialState = {
    isLoading: false,
    dataFields: { data: [], item: {} },
    errorMsg: '',
}

export const createReducer = (
    { stateId = "", initialState = {} } = {}
) =>
    (state = { ...commonInitialState, ...initialState }, action) => {
        switch (action.type) {
            case REQUEST_ACTION(stateId):
                return { ...state, isLoading: true, }

            case REQUEST_SUCCESS_ACTION(stateId):
                if (typeof action.payload == "undefined") {
                    return { ...state, isLoading: false }
                }
                // create object of dataFields with there data
                let dataFieldsObj = {}
                let data = action.payload
                let dataFields = action.fields
                let fields = Object.keys(dataFields)
                for (let i = 0; i < fields.length; i++) {
                    const field = fields[i];
                    const func = dataFields[field];
                    let fieldData = func(data, { state: state, action: action })
                    dataFieldsObj[field] = fieldData
                }
                return { ...state, isLoading: false, dataFields: { ...state.dataFields, ...dataFieldsObj } }
            case REQUEST_ERROR_ACTION(stateId):
                return { ...state, isLoading: false, errorMsg: action.payload }
            default: return state
        }

    }
