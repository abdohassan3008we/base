import { listRequestAsync } from "../../generic_redux";
import { STATE_ID } from '.'

const listOhda7sabSanfSetupRequestAsync = (
    {
    params,

        onSuccess = (_) => { },
        onFailure = (_) => { },
    } = {}
    ) => listRequestAsync(
        {
            stateId: STATE_ID,
            endPoint: "/Ohda7sabSanfSetup/GetAll/",
            params: { params: { ...params } },
            onSuccess: onSuccess,
            onFailure: onFailure
        }
    )
export default listOhda7sabSanfSetupRequestAsync