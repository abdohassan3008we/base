import { createReducer } from "../../generic_redux"
import { STATE_ID } from "./thunk"

const usersReducer = createReducer({ stateId: STATE_ID })

export default usersReducer