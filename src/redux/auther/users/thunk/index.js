import { createRequestAsync, methods } from '../../../generic_redux'
export const STATE_ID = "UsersReducer"
  

export const postUsersRequestAsync
    = createRequestAsync({
        stateId: STATE_ID,
        method: methods.POST,

        endPoint: `Users/authenticate`
    })
 