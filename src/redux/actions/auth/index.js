// ** UseJWT import to get config
import useJwt from "../../../auth/jwt/useJwt";
import apiClient from "../../../utility/services/apiClient";

const config = useJwt.jwtConfig;

// ** Handle User Login
export const handleLogin = (data) => {
  let str = "";
  for (const [p, val] of Object.entries(data)) {
    str += val;
  }
  return (dispatch) => {
    dispatch({
      type: "LOGIN",
      data,
      config,
      [config.storageTokenKeyName]: data[config.storageTokenKeyName],
      [config.storageRefreshTokenKeyName]:
        data[config.storageRefreshTokenKeyName],
    });
    // ** Add to user, accessToken & refreshToken to localStorage
    localStorage.setItem("loginData", JSON.stringify(data));
    // Re-read the new authentication keys from local storage
    // apiClient.reconfigure()
    localStorage.setItem(config.storageTokenKeyName, JSON.stringify(data.data));
    // localStorage.setItem(config.storageRefreshTokenKeyName, JSON.stringify(data.refreshToken))

    // localStorage.setItem(config.storageTokenKeyName, str)
    localStorage.setItem(config.storageRefreshTokenKeyName, str);
  };
};

// ** Handle User Logout
export const handleLogout = () => {
  return (dispatch) => {
    dispatch({
      type: "LOGOUT",
      [config.storageTokenKeyName]: null,
      [config.storageRefreshTokenKeyName]: null,
    });

    // ** Remove user, accessToken & refreshToken from localStorage
    apiClient.removeSessionData([config.storageTokenKeyName,config.storageRefreshTokenKeyName]);
  };
};
