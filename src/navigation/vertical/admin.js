import { GrCatalog, GrSystem } from "react-icons/gr";
import { TbCircleDotted } from "react-icons/tb";
import { HiOutlineDatabase } from "react-icons/hi";
import { FiType } from "react-icons/fi";
import { useTranslation } from "react-i18next";
const { t } = useTranslation();
export default [
  {
    id: "Cataloge",
    title: "كاتالوج الاصناف",
    //icon: <TbCircleDotted size={12} />,
    //icon:<img src={tanklogo}/>,
    icon: <GrCatalog size={20} style={{ color: "#111111" }} />,
    children: [
      // {
      //   id: "UsgOhda7sabSanf",
      //   title: "UsgOhda7sabSanf",
      //   icon: <TbCircleDotted size={12} />,
      //   navLink: "/UsgOhda7sabSanf",
      // },
      {
        id: "UsgAsnafSetup",
        title: t("UsgAsnafSetup"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgAsnafSetup",
      },
      {
        id: "UsgAsnafModel",
        title: t("UsgAsnafModel"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgAsnafModels",
      },
    ],
  },

  {
    id: "System_Managment",
    title: "إدارة بيانات النظام",
    //icon: <TbCircleDotted size={12} />,
    //icon:<img src={tanklogo}/>,
    icon: <HiOutlineDatabase size={20} />,
    children: [
      {
        id: "Month",
        title: t("Month"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgMonth",
      },
      {
        id: "PrepairedType",
        title: t("PrepairedType"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/PrepairedType",
      },
      {
        id: "ItemModel",
        title: t("ItemModel"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/ItemModel",
      },
      {
        id: "AssignedType",
        title: t("AssignedType"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/AssignedType",
      },
      {
        id: "TypeSubject",
        title: t("TypeSubject"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/TypeSubject",
      },
      {
        id: "TypeMain",
        title: t("TypeMain"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/TypeMain",
      },
      {
        id: "TypeSub",
        title: t("TypeSub"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/TypeSub",
      },
      {
        id: "UsgYearPeriod",
        title: t("Year Period"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgYearPeriod",
      },
      {
        id: "UsgTrainingPlan",
        title: t("Training Plan"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgTrainingPlan",
      },
      {
        id: "UsgTrainingYear",
        title: t("Training Year"),
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgTrainingYear",
      },
    ],
  },

  {
    id: "Usage_Type",
    title: "نوع اﻹستخدام",
    //icon: <TbCircleDotted size={12} />,
    //icon:<img src={tanklogo}/>,
    icon: <FiType size={20} />,
    children: [
      {
        id: "تدريب",
        title: "تدريب",
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgType/training",
      },
      {
        id: "قتال",
        title: "قتال",
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgType/combat",
      },
    ],
  },
];
