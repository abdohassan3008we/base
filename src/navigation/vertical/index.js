import { ImBooks } from "react-icons/im";
import { SiBookstack } from "react-icons/si";
import { TbCircleDotted } from "react-icons/tb";
import { HiOutlineDatabase } from "react-icons/hi";
import { FiType } from "react-icons/fi";
import { userRoles } from "../../utility/constants";
import navigation from ".";

export const getNavigation = (user) => {
  return navigation.filter(
    (n) => n.users == undefined ||  n.users.includes(user)
  );
};

export default [
  {
    id: "Cataloge",
    title: "CatalogeItem",
    users: [userRoles.ADMIN],
    //icon:<img src={tanklogo}/>,
    icon: <ImBooks size={20} />,
    children: [
      {
        id: "UsgAsnafSetup",
        title: "UsgAsnafSetup",
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgAsnafSetup",
      },
      {
        id: "UsgAsnafModel",
        title: "UsgAsnafModel",
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgAsnafModels",
      },
    ],
  },
  {
    id: "System_Managment",
    title: "System_Managment",
    users: [userRoles.ADMIN],
    //icon: <TbCircleDotted size={12} />,
    //icon:<img src={tanklogo}/>,
    icon: <HiOutlineDatabase size={20} />,
    children: [
      {
        id: "Month",
        title: "Month",
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgMonth",
      },
      {
        id: "PrepairedType",
        title: "PrepairedType",
        icon: <TbCircleDotted size={12} />,
        navLink: "/PrepairedType",
      },
      {
        id: "ItemModel",
        title: "ItemModel",
        icon: <TbCircleDotted size={12} />,
        navLink: "/ItemModel",
      },
      {
        id: "AssignedType",
        title: "AssignedType",
        icon: <TbCircleDotted size={12} />,
        navLink: "/AssignedType",
      },
      {
        id: "TypeSubject",
        title: "TypeSubject",
        icon: <TbCircleDotted size={12} />,
        navLink: "/TypeSubject",
      },
      {
        id: "TypeMain",
        title: "TypeMain",
        icon: <TbCircleDotted size={12} />,
        navLink: "/TypeMain",
      },
      {
        id: "TypeSub",
        title: "TypeSub",
        icon: <TbCircleDotted size={12} />,
        navLink: "/TypeSub",
      },
      {
        id: "UsgYearPeriod",
        title: "Year Period",
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgYearPeriod",
      },
      {
        id: "UsgTrainingPlan",
        title: "Training Plan",
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgTrainingPlan",
      },
      {
        id: "UsgTrainingYear",
        title: "Training Year",
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgTrainingYear",
      },
    ],
  },
  {
    id: "ohda_sanf",
    title: "ohda_sanf",
    users: [userRoles.USER],
    icon: <SiBookstack size={20} />,
    children: [
      {
        id: "UsgOhda7sabSanf",
        title: "UsgOhda7sabSanf",
        icon: <TbCircleDotted />,
        navLink: "/UsgOhda7sabSanf",
      },
      // {
      //   id: "UsgAsnafModel",
      //   title: "UsgAsnafModel",
      //   icon: <TbCircleDotted size={12} />,
      //   navLink: "/UsgAsnafModels",
      // },
    ],
  },
  {
    id: "Usage_Type",
    title: "Usage_Type",
    //icon: <TbCircleDotted size={12} />,
    //icon:<img src={tanklogo}/>,
    icon: <FiType size={20} />,
    users: [userRoles.ADMIN, userRoles.USER],

    children: [
      // {
      //   id: "Training",
      //   title: "Training",
      //   icon: <TbCircleDotted size={12} />,
      //   navLink: "/UsgType/training",
      // },
      // {
      //   id: "combat",
      //   title: "combat",
      //   icon: <TbCircleDotted size={12} />,
      //   navLink: "/UsgType/combat",
      // },
      {
        id: "plain",
        title: "Equipment",
        icon: <TbCircleDotted size={12} />,
        navLink: "/UsgType/plain",
      },
    ],
  },
];
