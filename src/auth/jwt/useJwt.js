// ** Core JWT Import
import getJwt from "@src/@core/auth/jwt/useJwt";
const { jwt } = getJwt({});
export default jwt;
