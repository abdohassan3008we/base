export default {
  short_name: "UPS", // variable
  default_language: "ar", // variable
  active_translation: false, // variable

  default_theme: "dark", // variable
  active_theme: true, // variable
};
