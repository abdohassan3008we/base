// You can customize the template with the help of this file
import { Circle, Settings } from "react-feather";
import { FaLinode } from "react-icons/fa";
import app_icon from "@src/assets/images/icons/icons8-tank-30.png";
import app from "@src/configs/app_data";
import app_data from "./app_data";

//Template config options
const themeConfig = {
  app: {
    appName: "app_name",
    detail: "app_detail",
    icon: app_icon,
  },
  layout: {
    isRTL:
      (!localStorage.getItem("lang") && app.default_language === "ar") ||
      localStorage.getItem("lang") === "ar"
        ? true
        : false,
    skin: app_data.default_theme ?? "dark", // light, dark, bordered, semi-dark
    routerTransition: "zoomIn", // fadeIn, fadeInLeft, zoomIn, none or check this for more transition https://animate.style/
    type: "vertical", // vertical, horizontal
    contentWidth: "full", // full, boxed
    menu: {
      isHidden: false,
      isCollapsed: false,
    },
    navbar: {
      // ? For horizontal menu, navbar type will work for navMenu type
      type: "floating", // static , sticky , floating, hidden
      backgroundColor: "white", // BS color options [primary, success, etc]
    },
    footer: {
      type: "static", // static, sticky, hidden
    },
    customizer: true,
    scrollTop: true, // Enable scroll to top button
  },
};

export default themeConfig;
