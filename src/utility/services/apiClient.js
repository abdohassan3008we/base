import axios from "axios";

const TIMEOUT = 1000 * 60;

/**
 * Configurable HTTP dispatcher to send and fetch data back and forth from the configured backend API.
 *
 * @class ApiClient
 * @author Mohamed Abdul-Fattah
 */
class ApiClient {
  constructor() {
    // const express = require("express");
    // const cors = require("cors");
    // const app = express();
    // //use cors as middleware
    // const allowedDomains = `[“http://localhost:7043, “http://localhost:3000”, “http://localhost:6000”]`
    // app.use(
    //   cors({
    //     origin: allowedDomains,
    //     credentials: true
    //   })
    // )
    this.loginData = undefined;
    this.userData = undefined;
    this.client = axios.create({
      baseURL: process.env.REACT_APP_API_HOST,
      withCredentials: true,
      headers: {
        ...this._defaultHeaders(),
        ...this._authHeaders(),
      },
      params: {},
      timeout: TIMEOUT,
    });
  }

  /**
   * Performs a GET request
   *
   * @returns {Promise}
   */
  get() {
    const response = this.client.get(...arguments);
    // Reconfigure to defaults in case query params are configured for this request
    this.reconfigure();
    return response;
  }

  /**
   * Performs a POST request
   *
   * @returns {Promise}
   */
  post() {
    const response = this.client.post(...arguments);
    // Reconfigure to defaults in case query params are configured for this request
    this.reconfigure();
    return response;
  }

  /**
   * Performs a PUT request
   *
   * @returns {Object}
   */
  put() {
    const response = this.client.put(...arguments);
    // Reconfigure to defaults in case query params are configured for this request
    this.reconfigure();
    return response;
  }

  /**
   * Performs a DELETE request
   *
   * @returns {Object}
   */
  delete() {
    const response = this.client.delete(...arguments);
    // Reconfigure to defaults in case query params are configured for this request
    this.reconfigure();
    return response;
  }

  /**
   * Sets authentication values in query params instead of headers
   *
   * @returns {ApiClient}
   */
  authParams() {
    const userData = this.getUserData();

    if (this._validUserData(userData)) {
      this.client.defaults.params["appSiteId"] =
        userData.admUserItem.userSiteId;
      this.client.defaults.params["appUserId"] = userData.admUserItem.userId;
    }
    this.client.defaults.headers = this._defaultHeaders();
    return this;
  }

  /**
   * Remove all authentication headers
   *
   * @returns {ApiClient}
   */
  noAuth() {
    this.client.defaults.headers = this._defaultHeaders();
    return this;
  }
  /**
   * Add custom headers
   *
   * @param {Object} headers
   * @returns {ApiClient}
   */
  headers(headers) {
    this.client.defaults.headers = {
      ...this._defaultHeaders(),
      ...this._authHeaders(),
      ...headers,
    };
    return this;
  }
  /**
   * Configures the API client to send auth headers in camelCase instead of kebab-case
   *
   * @returns {ApiClient}
   */
  camelCaseAuthHeaders() {
    const userData = this.getUserData();
    let authHeaders = {};

    if (this._validUserData(userData)) {
      authHeaders = {
        appUserId: userData.admUserItem.userId,
        appSiteId: userData.admUserItem.userSiteId,
      };
    }

    this.client.defaults.headers = {
      ...this._defaultHeaders(),
      ...authHeaders,
    };
    return this;
  }

  /**
   * Adds userRoleCode to query params
   *
   * @returns {ApiClient}
   */
  addUserRoleCode() {
    const userData = this.getUserData();

    if (this._validUserData(userData)) {
      this.client.defaults.params["userRoleCode"] =
        userData.admUserItem.userRoleCode;
    }
    return this;
  }

  /**
   * Adds personSiteId to query params
   *
   * @returns {ApiClient}
   */
  addPersonSiteId() {
    const userData = this.getUserData();

    if (this._validUserData(userData)) {
      this.client.defaults.params["personSiteId"] =
        userData.admUserItem?.jobSiteId;
    }
    return this;
  }

  /**
   * Reconfigure the axios client with the default and auth headers
   */
  reconfigure() {
    this.userData = undefined;
    this.client.defaults.headers = {
      ...this._defaultHeaders(),
      ...this._authHeaders(),
    };
    this.client.defaults.params = {};
    this.client.defaults.timeout = TIMEOUT;
  }

  /**
   * Checks whether the user is authenticated or not
   *
   * @returns {Boolean}
   */
  isAuthenticated() {
    return this._validUserData(this.getUserData());
  }

  /**
   * Gets user login data from local storage set on login
   *
   * @returns {Object}
   */
  getLoginData() {
    // Cache loginData to avoid reading headache
    if (this.loginData) return this.loginData;
    this.loginData = JSON.parse(localStorage.getItem("loginData"));
    return this.loginData || {};
  }
  /**
   * Gets user data from local storage set on login
   *
   * @returns {Object}
   */
  getUserData() {
    // Cache userData to avoid reading headache
    if (this.userData) return this.userData;
    this.userData = JSON.parse(localStorage.getItem("userData"));
    return this.userData || {};
  }

  // Private

  /**
   * Add CORS and JSON headers to all requests
   *
   * @returns {Object}
   */
  _defaultHeaders() {
    const accessToken = localStorage.getItem("accessToken");
    const tokenNew =
      accessToken != null
        ? accessToken.substring(1, accessToken.length - 1)
        : accessToken;
    return {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers":
        "Origin, X-Requested-With, Content-Type, Accept",
      "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
      "Content-Type": "application/json",
      Authorization: `Bearer ${tokenNew}`,
    };
  }

  /**
   * Add auth tokens/values to headers if any
   *
   * @returns {Object}
   */
  _authHeaders() {
    const userData = this.getUserData();

    // if (this._validUserData(userData)) {
    //   return {
    //     'app-user-id': userData?.admUserItem?.userId,
    //     'app-site-id': userData?.admUserItem?.userSiteId,
    //   }
    // }

    return {};
  }

  /**
   * Checks whether the user data are valid or not
   *
   * @param {Object|null} userData
   * @returns {Boolean}
   */
  _validUserData(userData) {
    return userData && Object.keys(userData).length > 0;
  }
  removeSessionData(items) {
    items = [
      ...(items ?? []),
      "loginData",
      "userRole",
      "siteId",
      "siteName",
      "userData",
      "showCardPlan",
      "showCreateCard",
    ];
    items.map((item) => {
      localStorage.removeItem(item);
    });
  }
}

export default new ApiClient();
