import apiClient from "../apiClient";
import { Local } from "@src/utility/page_storage";

const defaultBreaker = "BreAkeR";
const isBreakerFound = ({ breaker = defaultBreaker, params = {} }) => {
  // for (const ele of params) {
  //   if (ele === breaker) return true;
  // }
  // return false;
  for (const key in params) {
    if (Object.hasOwnProperty.call(params, key)) {
      if (params[key] === breaker) return true;
    }
  }
  return false;
  // }
  // Object.keys(params).map(key=>
  //   params[key].
  //   )
};
export const createListRequest =
  ({
    breaker = defaultBreaker,
    api = apiClient,
    endPoint,
    action = (_) => _,
    ifSuccess,
    ifFailure,
  } = {}) =>
  ({
    id = "",
    params = {},
    onSuccess = (_) => _,
    onFailure = (_) => _,
    setData = (_) => _,
    setIsLoading = (_) => _,
    setError = (_) => _,
    dataAction = (_) => _,
  } = {}) => {
    if (!(isBreakerFound({ breaker, params }) || breaker === id)) {
      setIsLoading(true);
      api
        .get(endPoint + id.toString(), { params })
        .then((response) => {
          let data = dataAction(response.data);
          data = action(data);
          setData(data);

          setIsLoading(false);

          if (ifSuccess) {
            ifSuccess(data);
          } else {
            onSuccess(data);
          }
        })
        .catch((err) => {
          console.log(err.response?.status);
          if (err.response?.status == "401") {
            apiClient.removeSessionData();
            Local.refresh();
          }
          if (ifFailure) {
            ifFailure(err.response);
          } else {
            onFailure(err.response);
          }

          setIsLoading(false);
          setError(err.response);
        });
    }
  };

export const createGetRequest =
  ({
    breaker = defaultBreaker,
    api = apiClient,
    endPoint,
    action = (_) => _,
    ifSuccess,
    ifFailure,
  } = {}) =>
  ({
    id = "",
    params = {},
    onSuccess = (_) => _,
    onFailure = (_) => _,
    setData = (_) => _,
    setIsLoading = (_) => _,
    setError = (_) => _,
    dataAction = (_) => _,
  } = {}) => {
    if (!(isBreakerFound({ breaker, params }) || breaker === id)) {
      setIsLoading(true);
      api
        .get(endPoint + id.toString(), { params })
        .then((response) => {
          let data = dataAction(response.data);
          data = action(data);
          setData(data);

          setIsLoading(false);

          if (ifSuccess) {
            ifSuccess(data);
          } else {
            onSuccess(data);
          }
        })
        .catch((err) => {
          if (ifFailure) {
            ifFailure(err.response);
          } else {
            onFailure(err.response);
          }
          setIsLoading(false);
          setError(err.response);
          onFailure(err.response);
        });
    }
  };

export const createPostRequest =
  ({
    breaker = defaultBreaker,
    api = apiClient,
    endPoint,
    action = (_) => _,
    ifSuccess,
    ifFailure,
  } = {}) =>
  ({
    id = "",
    body = {},
    params = {},
    onSuccess = (_) => _,
    onFailure = (_) => _,
    setData = (_) => _,
    setIsLoading = (_) => _,
    setError = (_) => _,
    dataAction = (_) => _,
  } = {}) => {
    if (!(isBreakerFound({ breaker, params }) || breaker === id)) {
      setIsLoading(true);
      api
        .post(endPoint + id.toString(), body, { params })
        .then((response) => {
          let data = dataAction(response.data);
          data = action(data);
          setData(data);

          setIsLoading(false);

          if (ifSuccess) ifSuccess(data);
          onSuccess(data);
        })
        .catch((err) => {
          if (ifFailure) ifFailure(err.response);
          onFailure(err.response);
          setIsLoading(false);
          setError(err.response);
          onFailure(err.response);
        });
    }
  };

export const createUpdateRequest =
  ({
    breaker = defaultBreaker,
    api = apiClient,
    endPoint,
    action = (_) => _,
    ifSuccess,
    ifFailure,
  } = {}) =>
  ({
    id = "",
    body = {},
    params = {},
    onSuccess = (_) => _,
    onFailure = (_) => _,
    setData = (_) => _,
    setIsLoading = (_) => _,
    setError = (_) => _,
    dataAction = (_) => _,
  } = {}) => {
    if (!(isBreakerFound({ breaker, params }) || breaker === id)) {
      setIsLoading(true);
      api
        .put(endPoint + id.toString(), body, { params })
        .then((response) => {
          let data = dataAction(response.data);
          data = action(data);
          setData(data);
          setIsLoading(false);

          if (ifSuccess) {
            ifSuccess(data);
          } else {
            onSuccess(data);
          }
        })
        .catch((err) => {
          setIsLoading(false);
          setError(err.response);

          if (ifFailure) {
            ifFailure(err.response);
          } else {
            onFailure(err.response);
          }
        });
    }
  };

export const createDeleteRequest =
  ({
    breaker = defaultBreaker,
    api = apiClient,
    endPoint,
    action = (_) => _,
    ifSuccess,
    ifFailure,
  } = {}) =>
  ({
    id = "",
    params = {},
    onSuccess = (_) => _,
    onFailure = (_) => _,
    setData = (_) => _,
    setIsLoading = (_) => _,
    setError = (_) => _,
    dataAction = (_) => _,
  } = {}) => {
    if (!(isBreakerFound({ breaker, params }) || breaker === id)) {
      setIsLoading(true);
      api
        .delete(endPoint + id.toString(), { params })
        .then((response) => {
          let data = dataAction(response.data);
          data = action(data);
          setData(data);

          setIsLoading(false);

          if (ifSuccess) {
            ifSuccess(data);
          } else {
            onSuccess(data);
          }
        })
        .catch((err) => {
          setIsLoading(false);
          setError(err.response);

          if (ifFailure) {
            ifFailure(err.response);
          } else {
            onFailure(err.response);
          }
        });
    }
  };
