import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { getAvailableNextStatusRequestAsync } from "../../redux/admin/grantedFormAction/thunk";
import { disableDeleteButton, disableNavbarButtons, enableDeleteButton, enableNavbarButtons } from "../../redux/reducers/navbar/actionCreators";
import ErrorPopup from "../../views/customizedComponents/ErrorPopup";
import { executeProcessForm } from '../processes/formActions'

/**
 * Adds form actions to the navbar action buttons based on the given form attributes
 *
 * @param {String} formTypeCode
 * @param {String} currentStatusCode
 * @param {Number} formId
 * @param {Function} handleDelete
 * @param {Number} key
 * @param {Function} onSuccess
 * @param {Function} onFailure
 * @author Mohamed Abdul-Fattah
 */
export default function useFormActions(
  formTypeCode,
  currentStatusCode,
  formId,
  handleDelete,
  key = 1,
  {
    onSuccess = (_) => { },
    onFailure = (_) => { ErrorPopup('تعذر اتخاذ الإجراء!') }
  } = {}
) {
  const dispatch = useDispatch()
  const [actions, setActions] = useState([])

  useEffect(() => {
    if (formTypeCode && currentStatusCode) {
      dispatch(getAvailableNextStatusRequestAsync(
        {
          currentStatusCode,
          formTypeCode
        },
        {
          onSuccess: data => { setActions(data) },
          onFailure: _ => { ErrorPopup('تعذر عرض الإجراءات!') }
        }
      ))
    }
  }, [formTypeCode, currentStatusCode, key])

  useEffect(() => {
    actions.forEach(action => {
      if (action.formActionCode === 'DELETE') { dispatch(enableDeleteButton(handleDelete)) }
    })

    return _ => { dispatch(disableDeleteButton()) }
  }, [actions, handleDelete])

  useEffect(() => {
    const btns = executeProcessForm(
      actions.filter(a => a.formActionName && a.formActionCode !== "DELETE"),
      formId,
      dispatch,
      {
        onSuccess,
        onFailure
      }
    )
    dispatch(enableNavbarButtons(btns))

    return _ => { dispatch(disableNavbarButtons()) }
  }, [actions, formId, dispatch])
}
