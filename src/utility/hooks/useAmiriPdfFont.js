import { Font } from "@react-pdf/renderer";
import AmiriRegular from "../../assets/fonts/Amiri/Amiri-Regular.ttf";
import AmiriItalic from "../../assets/fonts/Amiri/Amiri-Italic.ttf";
import AmiriBold from "../../assets/fonts/Amiri/Amiri-Bold.ttf";
import AmiriBoldItalic from '../../assets/fonts/Amiri/Amiri-BoldItalic.ttf'

/**
 * React hook to register Amiri font typeface in React PDF package
 *
 * @author Mohamed Abdul-Fattah
 * @deprecated React PDF package is not used any more and should be removed
 */
export default function () {
  Font.register({
    family: 'Amiri',
    fonts: [
      { src: AmiriRegular },
      { src: AmiriItalic, fontStyle: 'italic' },
      { src: AmiriBoldItalic, fontStyle: 'italic', fontWeight: 700 },
      { src: AmiriBold, fontStyle: 'oblique' },
    ],
  })
}
