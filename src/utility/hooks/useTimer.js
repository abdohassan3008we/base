import { useState, useEffect } from "react";
import { timeDiff } from "@src/utility/Utils";

/**
 * Generates a timer object ticking every second
 *
 * @param {Date} datetime
 * @returns {Object}
 * @example const { seconds, minutes, hours, days } = useTimer()
 * @author Mohamed Abdul-Fattah
 */
export default function useTimer(datetime = new Date) {
  let error = false
  if (!datetime) {
    error = true
    datetime = new Date()
  }
  const timestamp = datetime.getTime();
  const [timer, setTimer] = useState(timeDiff(timestamp));

  useEffect(() => {
    const timerId = setInterval(() => { setTimer(timeDiff(timestamp)) }, 1000);
    return () => { clearInterval(timerId) }
  }, [])

  return {
    seconds: timer.s,
    minutes: timer.m,
    hours: timer.h,
    days: timer.d,
    error
  }
}
