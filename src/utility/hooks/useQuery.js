import React from 'react'
import { useLocation } from "react-router-dom";

/**
 * React hook to get URL query parameters
 *
 * @returns {URLSearchParams}
 * @example const query = useQuery(); query.get('contractTypeCode')
 * @author Mohamed Abdul-Fattah
 */
export default function useQuery() {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}
