import { userRoles } from "./constants";

const storage = {
  userData: localStorage.getItem("userData"),
};

export const Local = {
  // window action
  refresh: () => window.location.reload(false),
  // local items action
  setItem: (item, value) => localStorage.setItem(item, value),
  getItem: (item) => localStorage.getItem(item),
  isItemEquals: (item, value) => localStorage.getItem(item) == value,
  removeItem: (item) => localStorage.removeItem(item),
  // user check
  isAdmin: () =>
    JSON.parse(localStorage.getItem("userData"))?.userRoleId == userRoles.ADMIN,
  isUser: (userId) =>
    JSON.parse(localStorage.getItem("userData"))?.userRoleId == userId,
  // local storage items
  userData: JSON.parse(storage.userData),
  userRole: JSON.parse(storage.userData)?.userRoleId,
  siteId: JSON.parse(storage.userData)?.siteId,
  /////*****///// another /////*****/////
  getShowPlan: () => {
    return JSON.parse(localStorage.getItem("showCardPlan"));
  },
  getShowCard: () => {
    return JSON.parse(localStorage.getItem("showCreateCard"));
  },
  getSiteID: () => {
    return JSON.parse(localStorage.getItem("siteId"));
  },
};
