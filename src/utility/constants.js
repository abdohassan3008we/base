export const userRoles = {
  SUPERADMIN: "SUPERADMIN", // مدير النظام الرئيسي
  ADMIN: "1", // 
  USER: "71",
  // WHWSCOM: "WHWSCOM", // قائد وحدة مستودع/ورشة
  // WHWSUSR: "WHWSUSR", // مستخدم اجراءات مستودع/ورشة
  // REPGRPCOM: "REPGRPCOM", // قائد مجموعة إصلاح
  // REPGRPUSR: "REPGRPUSR", // اجراءات مجموعة إصلاح
  // REPSECINS: "REPSECINS", // مفتش فني
  // REPSECTECH: "REPSECTECH", // فني قسم إصلاح
  // QASECUSR: "QASECUSR", // مراجع جودة إصلاح
  // STRSECUSR: "STRSECUSR", // أمين عهدة مخزن
  // SCROFFUSR: "SCROFFUSR", // فرد امن
  // SCROFFCOM: "SCROFFCOM", // قائد الامن
  // STRGRPUSR: "STRGRPUSR", // اجراءات مجموعة مخزنية
  // STRGRPCOM: "STRGRPCOM", // قائد مجموعة مخزنية
};

export const userTitleCodes = {
  "Dr.": 1,
  "MR.": 2,
  "Eng.": 3,
  "Pv.": 4,
  "Cpl.": 5,
  "Sgt.": 6,
  "SSg.": 7,
  "MSg.": 8,
  "SgM.": 9,
  "Lt.": 10,
  "1st Lt.": 11,
  "Cpt.": 12,
  "Maj.": 13,
  "LtC.": 14,
  "Col.": 15,
  "Brg.": 16,
  "MG.": 17,
  "LtG.": 18,
  "Gen.": 19,
  "GA.": 20,
};
