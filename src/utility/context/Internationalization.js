// ** React Imports
import { useState, createContext } from "react";

// ** Intl Provider Import
import { IntlProvider } from "react-intl";

// ** Core Language Data
import messagesEn from "../../assets/data/locales/en.json";
import messagesDe from "../../assets/data/locales/de.json";
import messagesFr from "../../assets/data/locales/fr.json";
import messagesAr from "../../assets/data/locales/ar.json";

// ** User Language Data
import userMessagesEn from "@src/assets/data/locales/en.json";
import userMessagesDe from "@src/assets/data/locales/de.json";
import userMessagesFr from "@src/assets/data/locales/fr.json";
import userMessagesAr from "@src/assets/data/locales/ar.json";
import { useTranslation } from "react-i18next";
import { useRTL } from "../hooks/useRTL";
import i18n from "../../configs/i18n";
import app_data from "../../configs/app_data";

// ** Menu msg obj
const menuMessages = {
  en: { ...messagesEn, ...userMessagesEn },
  de: { ...messagesDe, ...userMessagesDe },
  fr: { ...messagesFr, ...userMessagesFr },
  ar: { ...messagesAr, ...userMessagesAr },
};

// ** Create Context
const Context = createContext();

const IntlProviderWrapper = ({ children }) => {
  // ** States

  const [locale, setLocale] = useState(localStorage.getItem("lang"));
  const [messages, setMessages] = useState(
    menuMessages[localStorage.getItem("lang")]
  );
  // const { i18n } = useTranslation();

  // ** Switches Language
  const switchLanguage = (lang) => {
    setLocale(lang);
    setMessages(menuMessages[lang]);
    i18n.changeLanguage(lang);
  };
  return (
    <Context.Provider value={{ locale, switchLanguage }}>
      <IntlProvider
        key={locale}
        locale={locale}
        messages={messages}
        defaultLocale={app_data.default_language}
      >
        {children}
      </IntlProvider>
    </Context.Provider>
  );
};

export { IntlProviderWrapper, Context as IntlContext };
