import apiClient from "./services/apiClient";
import { DefaultRoute, Routes } from "../router/routes";

import { userTitleCodes } from "./constants";
import moment from "moment-timezone";
moment.locale("ar_EG");

// ** Checks if an object is empty (returns boolean)
export const isObjEmpty = (obj) => Object.keys(obj).length === 0;

// ** Returns K format from a number
export const kFormatter = (num) =>
  num > 999 ? `${(num / 1000).toFixed(1)}k` : num;

// ** Converts HTML to string
export const htmlToString = (html) => html.replace(/<\/?[^>]+(>|$)/g, "");

// ** Checks if the passed date is today
const isToday = (date) => {
  const today = new Date();
  return (
    /* eslint-disable operator-linebreak */
    date.getDate() === today.getDate() &&
    date.getMonth() === today.getMonth() &&
    date.getFullYear() === today.getFullYear()
    /* eslint-enable */
  );
};

/**
 ** Format and return date in Humanize format
 ** Intl docs: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/format
 ** Intl Constructor: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/DateTimeFormat
 * @param {String} value date to format
 * @param {Object} formatting Intl object to format with
 */
export const formatDate = (
  value,
  formatting = { month: "short", day: "numeric", year: "numeric" }
) => {
  if (!value) return value;
  return new Intl.DateTimeFormat("en-US", formatting).format(new Date(value));
};

// ** Returns short month of passed date
export const formatDateToMonthShort = (value, toTimeForCurrentDay = true) => {
  const date = new Date(value);
  let formatting = { month: "short", day: "numeric" };

  if (toTimeForCurrentDay && isToday(date)) {
    formatting = { hour: "numeric", minute: "numeric" };
  }

  return new Intl.DateTimeFormat("en-US", formatting).format(new Date(value));
};

/**
 ** Return if user is logged in
 ** This is completely up to you and how you want to store the token in your frontend application
 *  ? e.g. If you are using cookies to store the application please update this function
 */
export const isUserLoggedIn = () => localStorage.getItem("loginData");
// export const isUserLoggedIn = () => true
export const getUserData = () => JSON.parse(localStorage.getItem("userData"));

/**
 ** This function is used for demo purpose route navigation
 ** In real app you won't need this function because your app will navigate to same route for each users regardless of ability
 ** Please note role field is just for showing purpose it's not used by anything in frontend
 ** We are checking role just for ease
 * ? NOTE: If you have different pages to navigate based on user ability then this function can be useful. However, you need to update it.
 * @param {String} userRole Role of user
 */
export const getHomeRouteForLoggedInUser = (userRole) => {
  if (userRole === "admin") return "/";
  if (userRole === "client") return "/access-control";
  return "/login";
};

// ** React Select Theme Colors
export const selectThemeColors = (theme) => ({
  ...theme,
  colors: {
    ...theme.colors,
    primary25: "#7367f01a", // for option hover bg-color
    primary: "#7367f0", // for selected option bg-color
    neutral10: "#7367f0", // for tags bg-color
    neutral20: "#ededed", // for input border-color
    neutral30: "#ededed", // for input hover border-color
  },
});

/**
 * Generate a random string
 *
 * @param {Number} length random string length
 * @returns {String}
 */
export function randomString(length = 12) {
  let random = "";
  let characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    random += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return random;
}

// let timestamp = Date.parse('11/23/2021 17:30:00');
export function timeDiff(timestamp) {
  let current = new Date().getTime();

  let diff = current - timestamp;

  let days = Math.floor(diff / (24 * 60 * 60 * 1000));
  diff %= 24 * 60 * 60 * 1000;
  let hours = Math.floor(diff / (60 * 60 * 1000));
  diff %= 60 * 60 * 1000;
  let minutes = Math.floor(diff / (60 * 1000));
  diff %= 60 * 1000;
  let seconds = Math.floor(diff / 1000);

  return {
    d: days,
    h: hours,
    m: minutes,
    s: seconds,
  };
}

/**
 * Checks whether the rendering screen is allowed for current user or not
 *
 * @param {Object} screens
 * @param {Number} appScreenId
 * @returns {Boolean}
 */
export const isScreenNotAllowed = (screens, appScreenId) => {
  return false;
  return (
    screens.isFulfilled && // make sure to authorize pages if the getMenuByRole request has been fulfilled
    !screens.data[appScreenId]
  );
};

/**
 * Get application default route
 *
 * @returns {string}
 */
export const getDefaultRoute = () => {
  const homeScreenId = apiClient.getUserData()?.admUserItem?.homeScreenId;
  if (homeScreenId) {
    return Routes.find((r) => r.id === homeScreenId)?.path ?? DefaultRoute;
  }
  return DefaultRoute;
};

/**
 *
 * @param {Array} elements list of object of elements you want to sort
 * @param {[string]} criteriaList list of criteria you want to sort the elements according to
 */
export const sortByManyCriteria = (elements, criteriaList) => {
  let criteriaTmp = [];
  for (let criteria of criteriaList) {
    let mainCriteria;
    criteriaTmp.push(criteria);
    elements.sort((elementOne, elementTwo) => {
      let otherCriteria = true;
      for (let i = 0; i < criteriaTmp.length; i++) {
        if (i === criteriaTmp.length - 1)
          mainCriteria =
            elementOne[criteriaTmp[i]] > elementTwo[criteriaTmp[i]] ? -1 : 1;
        else
          otherCriteria &=
            elementOne[criteriaTmp[i]] === elementTwo[criteriaTmp[i]];
      }
      return otherCriteria && mainCriteria;
    });
  }
};

/**
 * Convert the date to arabic
 *
 * @param {string} date
 * @param {Boolean} day
 * @param {Boolean} month
 * @param {Boolean} year
 * @param {Boolean} rtl
 * @returns {string} The date in arabic language with format D-M-YYYY
 */
export const arabicDate = (
  date,
  { day = true, month = true, year = true, rtl = true } = {}
) => {
  if (!date) return "";
  let dateFormat = "";
  if (rtl) {
    dateFormat += day ? "YYYY" : "   /YYYY";
    dateFormat += month ? "/M" : "/  ";
    dateFormat += day ? "/D" : "";
  } else {
    dateFormat += year ? "YYYY/" : "    /";
    dateFormat += month ? "M/" : "  /";
    dateFormat += day ? "D" : "  ";
  }

  return moment(new Date(date)).format(dateFormat);
};

export const posOfTitleCode = (titleCode) => {
  if (!userTitleCodes.hasOwnProperty(titleCode)) return -1;
  return userTitleCodes[titleCode];
};

