// ** Logo
import logo from "@src/assets/images/logo/logo.png";
import logo1 from "@src/assets/images/icons/icons8-tank-30.png";

const SpinnerComponent = () => {
  return (
    <div style={{ position: "relative" }}>
      <div style={{ left: "48%", top: "30em", position: "absolute" }}>
        <img className="fallback-logo" src={logo1} alt="logo" />
      </div>
    </div>
  );
};

export default SpinnerComponent;
