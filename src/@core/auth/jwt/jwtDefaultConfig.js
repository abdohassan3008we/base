const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;
// ** Auth Endpoints
export default {
  loginEndpoint: `${REACT_APP_API_HOST}/Users/authenticate`,
  userRoleEndpoint: `${REACT_APP_API_HOST}Users/GetUsrData`,

  // registerEndpoint: "/jwt/register",
  // refreshEndpoint: "/jwt/refresh-token",
  logoutEndpoint: "/jwt/logout",

  // ** This will be prefixed in authorization header with token
  // ? e.g. Authorization: Bearer <token>
  tokenKey: "Authorization",
  tokenType: "Bearer",
  userRole:"userRole",

  // ** Value of this property will be used as key to store JWT token in storage
  storageTokenKeyName: "accessToken",
  storageRefreshTokenKeyName: "refreshToken",
};
