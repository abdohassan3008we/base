// ** React Imports
import { useEffect } from "react";
import { NavLink } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// ** Third Party Components
import { Disc, X, Circle } from "react-feather";

// ** Config
import themeConfig from "@configs/themeConfig";
import { useTranslation } from "react-i18next";

const VerticalMenuHeader = (props) => {
  // ** Props
  const {
    menuCollapsed,
    setMenuCollapsed,
    setMenuVisibility,
    setGroupOpen,
    menuHover,
  } = props;

  const { t } = useTranslation();
  // ** Reset open group
  useEffect(() => {
    if (!menuHover && menuCollapsed) setGroupOpen([]);
  }, [menuHover, menuCollapsed]);

  // ** Menu toggler component
  const Toggler = () => {
    if (!menuCollapsed) {
      return (
        <Disc
          size={20}
          data-tour="toggle-icon"
          className="text-primary toggle-icon d-none d-xl-block"
          onClick={() => setMenuCollapsed(true)}
        />
      );
    } else {
      return (
        <Circle
          size={20}
          data-tour="toggle-icon"
          className="text-primary toggle-icon d-none d-xl-block"
          onClick={() => setMenuCollapsed(false)}
        />
      );
    }
  };

  return (
    <Container className="navbar-header">
      <Row className="nav navbar-nav flex-row">
        <Col className="nav-item mr-auto" style={{ maxWidth: "81%" }}>
          <NavLink to="/" className="navbar-brand">
            <h2>
              {menuCollapsed ? (
                <span className="brand-text2 mb-0">
                  <img
                    className="vertical-menu-header-icon"
                    src={themeConfig.app.icon}
                    height={"21px"}
                  ></img>
                </span>
              ) : (
                <> </>
              )}
              {menuCollapsed ? (
                <span className="vertical-menu-header-title brand-text mb-0">
                  {t(themeConfig.app.appName)}
                </span>
              ) : (
                <span className="brand-text mb-0">
                  {t(themeConfig.app.appName)}
                </span>
              )}
              <br />
              {!menuCollapsed ? (
                <span className="brand-text2 mb-0">
                  {t(themeConfig.app.detail)}
                </span>
              ) : (
                <></>
              )}
            </h2>
          </NavLink>
        </Col>
        <Col className="nav-item nav-toggle" style={{ maxWidth: "19%" }}>
          <div className="nav-link modern-nav-toggle cursor-pointer">
            <Toggler />
            <X
              onClick={() => setMenuVisibility(false)}
              className="toggle-icon icon-x d-block d-xl-none"
              size={20}
            />
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default VerticalMenuHeader;
