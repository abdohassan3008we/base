// ** Icons Import
import { Heart } from "react-feather";
import app_data from "../../../../configs/app_data";
import { useTranslation } from "react-i18next";

const Footer = () => {
  const { t } = useTranslation();
  return (
    <p className="clearfix mb-0 text-center">
      <span className="float-md-center d-block d-md-inline-block mt-25">
        {t("Copyright")} © {new Date().getFullYear()}
        <a href="/" target="_blank" rel="noopener noreferrer">
          /{app_data.short_name} ,
        </a>
        <span className="d-none d-sm-inline-block">
          {t("Rights Reserved")} ,
        </span>
      </span>
    </p>
  );
};

export default Footer;
