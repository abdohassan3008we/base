// ** React Imports
import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import {
  Button,
  UncontrolledButtonDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
} from "reactstrap";
import { MoreVertical } from "react-feather";
import { Badge } from "reactstrap";
// ** Third Party Components
import * as Icon from "react-feather";
import { NavItem, NavLink, UncontrolledTooltip } from "reactstrap";

const NavbarRightIcons = (props) => {
  const store = useSelector((state) => state.navbarReducer);

  // const store = {
  //   buttonParameters: { show: false, handler: () => {} },
  //   uploadButtonParameters: { show: false, handler: () => {} },
  //   printButtonParameters: { show: false, handler: () => {} },
  //   deleteButtonParameters: { show: false, handler: () => {} },
  //   logButtonParameters: { show: false, handler: () => {} },
  //   navbarButtons: { enableArrayOfButtons: false }
  // }
  const style = {
    backgroundColor: "transparent !important",
    border: "none",
    color: "#6e6b7b !important",
  };

  // ** Loops through icons Array to return Bookmarks
  const SaveIcon = Icon["Save"];
  const UploadIcon = Icon["Paperclip"];
  const DeleteIcon = Icon["Trash2"];
  const PrintIcon = Icon["Printer"];
  const LogIcon = Icon["Info"];
  const renderIcons = () => {
    return (
      <>
        {/** Save icon */}
        <NavItem className="d-none d-lg-block">
          <NavLink>
            <SaveIcon
              id="save"
              color={store.buttonParameters.show ? "#a38226" : "#d0d2d6"}
              className="ficon"
              onClick={store.buttonParameters.handler}
            />
            {store.buttonParameters.show ? (
              <UncontrolledTooltip target="save">
                {store.buttonParameters.title}
              </UncontrolledTooltip>
            ) : null}
          </NavLink>
        </NavItem>

        {/** Upload icon */}
        <NavItem className="d-none d-lg-block">
          <NavLink>
            <UploadIcon
              id="upload"
              color={store.uploadButtonParameters.show ? "#a38226" : "#d0d2d6"}
              className="ficon"
              onClick={store.uploadButtonParameters.handler}
            />
            {store.uploadButtonParameters.number &&
            store.uploadButtonParameters.number > 0 ? (
              <Badge
                style={{ left: "0px" }}
                pill
                color="danger"
                className="badge-up"
              >
                {store.uploadButtonParameters.number}
              </Badge>
            ) : (
              <></>
            )}
            {store.uploadButtonParameters.show ? (
              <UncontrolledTooltip target="upload">
                {store.uploadButtonParameters.title}
              </UncontrolledTooltip>
            ) : null}
          </NavLink>
        </NavItem>

        {/** Delete icon */}
        <NavItem className="d-none d-lg-block">
          <NavLink>
            <DeleteIcon
              id="delete"
              color={store.deleteButtonParameters.show ? "#6C0303" : "#d0d2d6"}
              className="ficon"
              onClick={store.deleteButtonParameters.handler}
            />
            {store.deleteButtonParameters.show ? (
              <UncontrolledTooltip target="delete">
                {store.deleteButtonParameters.title}
              </UncontrolledTooltip>
            ) : null}
          </NavLink>
        </NavItem>

        <NavItem className="d-none d-lg-block">
          <NavLink>
            <PrintIcon
              id="print"
              color={store.printButtonParameters.show ? "#a38226" : "#d0d2d6"}
              className="ficon"
              onClick={store.printButtonParameters.handler}
            />
            {store.printButtonParameters.show ? (
              <UncontrolledTooltip target="print">
                {store.printButtonParameters.title}
              </UncontrolledTooltip>
            ) : null}
          </NavLink>
        </NavItem>

        <NavItem className="d-none d-lg-block">
          <NavLink>
            <LogIcon
              id="log"
              color={store.logButtonParameters.show ? "#a38226" : "#d0d2d6"}
              className="ficon"
              onClick={store.logButtonParameters.handler}
            />
            {store.logButtonParameters.show ? (
              <UncontrolledTooltip target="log">
                {store.logButtonParameters.title}
              </UncontrolledTooltip>
            ) : null}
          </NavLink>
        </NavItem>

        {store.navbarButtons.enableArrayOfButtons &&
        store.navbarButtons.arrayOfButtons.length
          ? store.navbarButtons.arrayOfButtons.map((button, index) => {
              return (
                <Button
                  key={index}
                  style={{ marginRight: "10px" }}
                  color="primary"
                  onClick={button.handler}
                  size="sm"
                >
                  {button.title}
                </Button>
              );
            })
          : null}
      </>
    );
  };

  return (
    <Fragment>
      <ul className="nav navbar-nav bookmark-icons">{renderIcons()}</ul>

      <ul className="navbar-nav d-lg-none ">
        <NavItem className="mobile-menu mr-auto w-100">
          <NavLink className="nav-menu-main menu-toggle hidden-xs is-active">
            <UncontrolledButtonDropdown
              className="dropdown-icon-wrapper"
              direction="up"
            >
              <DropdownToggle tag="span">
                <MoreVertical size={17} className="cursor-pointer" />
              </DropdownToggle>

              <DropdownMenu>
                {/** Save button */}
                <DropdownItem tag="a" className="w-100">
                  <SaveIcon
                    id="save"
                    size={20}
                    color={store.buttonParameters.show ? "green" : "#d0d2d6"}
                    className="ficon mr-50 "
                    onClick={store.buttonParameters.handler}
                  />
                  {store.buttonParameters.show ? (
                    <UncontrolledTooltip target="save">
                      {store.buttonParameters.title}
                    </UncontrolledTooltip>
                  ) : null}
                </DropdownItem>

                {/** Upload button */}
                <DropdownItem tag="a" className="w-100">
                  <UploadIcon
                    id="upload"
                    size={20}
                    color={
                      store.uploadButtonParameters.show ? "green" : "#d0d2d6"
                    }
                    className="ficon mr-50 "
                    onClick={store.uploadButtonParameters.handler}
                  />
                  {store.uploadButtonParameters.show ? (
                    <UncontrolledTooltip target="upload">
                      {store.uploadButtonParameters.title}
                    </UncontrolledTooltip>
                  ) : null}
                </DropdownItem>

                {/** Delete button */}
                <DropdownItem tag="a" className="w-100">
                  <DeleteIcon
                    id="delete"
                    size={20}
                    color={
                      store.deleteButtonParameters.show ? "green" : "#d0d2d6"
                    }
                    className="ficon mr-50 "
                    onClick={store.deleteButtonParameters.handler}
                  />
                  {store.deleteButtonParameters.show ? (
                    <UncontrolledTooltip target="delete">
                      {store.deleteButtonParameters.title}
                    </UncontrolledTooltip>
                  ) : null}
                </DropdownItem>

                {/** Print button */}
                <DropdownItem tag="a">
                  <PrintIcon
                    id="print"
                    color={
                      store.printButtonParameters.show ? "green" : "#d0d2d6"
                    }
                    className="ficon"
                    onClick={store.printButtonParameters.handler}
                  />
                  {store.printButtonParameters.show ? (
                    <UncontrolledTooltip target="print">
                      {store.printButtonParameters.title}
                    </UncontrolledTooltip>
                  ) : null}
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledButtonDropdown>
          </NavLink>
        </NavItem>
      </ul>
    </Fragment>
  );
};

export default NavbarRightIcons;
