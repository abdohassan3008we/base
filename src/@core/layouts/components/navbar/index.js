// ** React Imports
import { Fragment, useEffect, useState } from "react";
import * as Icon from "react-feather";
// ** Dropdowns Imports
import IntlDropdown from "./IntlDropdown";
// import SiteDropdown from "./SiteDropdown";
import UserDropdown from "./UserDropdown";
import NavbarSearch from "./NavbarSearch";
import NotificationDropdown from "./NotificationDropdown";
import { useHistory } from "react-router-dom";

// ** Custom Components
import NavbarBookmarks from "./NavbarBookmarks";

// ** Third Party Components
import { Sun, Moon, Home } from "react-feather";
import { NavItem, NavLink, UncontrolledTooltip } from "reactstrap";
import NavbarRightIcons from "./NavbarRightIcons";
import { getDefaultRoute } from "../../../../utility/Utils";
import app_data from "../../../../configs/app_data";
import { userRoles } from "../../../../utility/constants";
import { Local } from "../../../../utility/page_storage";
import apiClient from "../../../../utility/services/apiClient";

const ThemeNavbar = (props) => {

  // ** Props
  const { skin, setSkin, setMenuVisibility } = props;

  // ** Function to toggle Theme (Light/Dark)
  const ThemeToggler = () => {
    if (skin === "dark") {
      return <Sun className="ficon" onClick={() => setSkin("light")} />;
    } else {
      return <Moon className="ficon" onClick={() => setSkin("dark")} />;
    }
  };
  const history = useHistory();
  const redirectToHome = () => {
    history.push(getDefaultRoute());
  };

  return (
    <Fragment>
      <div className="bookmark-wrapper d-flex align-items-center">
        <NavbarBookmarks setMenuVisibility={setMenuVisibility} />
      </div>
      <div className="bookmark-wrapper d-flex align-items-center">
        <NavbarRightIcons />
      </div>
      <ul className="nav navbar-nav align-items-center ml-auto">
        {/* {Local.isAdmin() ? (
          <span style={{ paddingLeft: "20px", paddingRight: "20px" }}>
            <SiteDropdown />
          </span>
        ) : (
          <></>
        )} */}
        {app_data.active_translation ? <IntlDropdown /> : <></>}
        <NavItem className="d-none d-lg-block">
          <NavLink className="nav-link-style">
            <Home onClick={() => redirectToHome()} />
          </NavLink>
        </NavItem>
        {app_data.active_theme ? (
          <NavItem className="d-none d-lg-block">
            <NavLink className="nav-link-style">
              <ThemeToggler />
            </NavLink>
          </NavItem>
        ) : (
          <></>
        )}
        {/* <NavbarSearch /> */}
        <NotificationDropdown />
        <UserDropdown />
      </ul>
    </Fragment>
  );
};

export default ThemeNavbar;
