// ** React Imports
import { Fragment, useEffect } from 'react'
import { UncontrolledButtonDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap'


// ** Third Party Components
import * as Icon from 'react-feather'
import {
  NavItem,
  NavLink,
} from 'reactstrap'

// ** Store & Actions
import { useDispatch } from 'react-redux'
// import { getBookmarks } from '@store/actions/navbar'

const NavbarBookmarks = props => {
  // ** Props
  const { setMenuVisibility } = props


  // ** Store Vars
  const dispatch = useDispatch()

  // // ** ComponentDidMount
  // useEffect(() => {
  //   dispatch(getBookmarks())
  // }, [])

  return (
    <Fragment>
      <ul className='navbar-nav d-xl-none'>
        <NavItem className='mobile-menu mr-auto'>
          <NavLink className='nav-menu-main menu-toggle hidden-xs is-active' onClick={() => setMenuVisibility(true)}>
            <Icon.Menu className='ficon' />
          </NavLink>
        </NavItem>
      </ul>

      {/* <ul className='navbar-nav d-xl-none'>
        <NavItem className='mobile-menu mr-auto'>
          <NavLink className='nav-menu-main menu-toggle hidden-xs is-active' onClick={() => setMenuVisibility(true)}>
            <UncontrolledButtonDropdown>
              <DropdownToggle color='primary' caret>
                Primary
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem href='/' tag='a'>
                  Option 1
                </DropdownItem>
                <DropdownItem href='/' tag='a'>
                  Option 2
                </DropdownItem>
                <DropdownItem href='/' tag='a'>
                  Option 3
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledButtonDropdown>
          </NavLink>
        </NavItem>
      </ul> */}









    </Fragment>
  )
}

export default NavbarBookmarks
