// ** React Imports
import { useContext } from 'react'

// ** Third Party Components
import ReactCountryFlag from 'react-country-flag'
import { UncontrolledDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap'

// ** Internationalization Context
import { IntlContext } from '@src/utility/context/Internationalization'
import { useRTL } from '../../../../utility/hooks/useRTL'

const IntlDropdown = () => {
  // ** Context
  const intlContext = useContext(IntlContext)
  const [isRtl, setIsRtl] = useRTL(localStorage.getItem("rtl"))
  // ** Vars
  const langObj = {
    en: 'English',
    de: 'German',
    fr: 'French',
    ar: 'العربية'
  }

  // ** Function to switch Language
  const handleLangUpdate = (e, lang) => {
   
    e.preventDefault()
    localStorage.setItem("lang", lang)
    if (lang == 'ar') {
      setIsRtl(true)
    } else {
      setIsRtl(false)
    }
    intlContext.switchLanguage(lang)
    
  } 
  return (
    <UncontrolledDropdown href='/' tag='li' className='dropdown-language nav-item'>
      <DropdownToggle href='/' tag='a' className='nav-link' onClick={e => e.preventDefault()}>
        <ReactCountryFlag
          className='country-flag flag-icon'
          countryCode={intlContext.locale === 'en' ? 'us' : 'eg'}
          svg
        />
        <span className='selected-language'>{langObj[intlContext.locale]}</span>
      </DropdownToggle>
      <DropdownMenu className='mt-0' right>
        <DropdownItem href='/' tag='a' onClick={e => handleLangUpdate(e, 'en')}>
          <ReactCountryFlag className='country-flag' countryCode='us' svg />
          <span className='ml-1'>English</span>
        </DropdownItem>
        {/* <DropdownItem href='/' tag='a' onClick={e => handleLangUpdate(e, 'fr')}>
          <ReactCountryFlag className='country-flag' countryCode='fr' svg />
          <span className='ml-1'>French</span>
        </DropdownItem>
        <DropdownItem href='/' tag='a' onClick={e => handleLangUpdate(e, 'de')}>
          <ReactCountryFlag className='country-flag' countryCode='de' svg />
          <span className='ml-1'>German</span>
        </DropdownItem> */}
        <DropdownItem href='/' tag='a' onClick={e => handleLangUpdate(e, 'ar')}>
          <ReactCountryFlag className='country-flag' countryCode='eg' svg />
          <span className='ml-1'>العربية</span>
        </DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  )
}

export default IntlDropdown
