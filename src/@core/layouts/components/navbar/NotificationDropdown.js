import { Fragment, useEffect } from "react";
import { Link } from "react-router-dom";
import { FaEnvelope, FaRegEnvelopeOpen } from "react-icons/fa";
import moment from "moment";
import "moment/locale/ar-sa";

// ** Third Party Components
import classnames from "classnames";
import PerfectScrollbar from "react-perfect-scrollbar";
import { Bell } from "react-feather";
import {
  Button,
  Badge,
  Media,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  UncontrolledDropdown,
} from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
// import { listWmsNotificationRequestAsync, readWmsNotificationRequestAsync } from '../../../../redux/admin/notification/thunk/index';
import { useHistory } from "react-router-dom";
import { iacHeader, ackHeader } from "../../../../utility/strings";
moment.locale("ar-sa");

const NotificationDropdown = () => {
  let history = useHistory();
  const dispatch = useDispatch();
  const notificationStore = {
    iacArray: [],
    ackArray: [],
    numNewNotifications: [],
  };

  useEffect(() => {
    const interval = setInterval(intervalFunc, 10000);
    return () => clearInterval(interval);
  }, [notificationStore]);

  // useEffect(() => {
  //   dispatch(listWmsNotificationRequestAsync());
  // }, [])

  function intervalFunc() {
    //   dispatch(listWmsNotificationRequestAsync());
  }

  function closeNotification(e, item) {
    //   e.preventDefault();
    //   if(!item.openDate) {
    //     dispatch(readWmsNotificationRequestAsync(item, notificationStore.numNewNotifications));
    //   }
    //   if(item.editScreenUrl){
    //     history.push(item.editScreenUrl);
    //   }
  }

  function notificationItem(item, index) {
    return (
      <a
        key={index}
        className="d-flex"
        href="/"
        onClick={(e) => closeNotification(e, item)}
      >
        <Media
          className={classnames("d-flex", {
            "align-items-start": !item.switch,
            "align-items-center": item.switch,
          })}
        >
          {!item.switch ? (
            <Fragment>
              <Media left>
                {item.openDate ? (
                  <FaRegEnvelopeOpen size={20} />
                ) : (
                  <FaEnvelope size={20} />
                )}
              </Media>
              <Media body>
                {item.title}
                <small className="notification-text">
                  {item.notificationMessage}
                </small>
                {item.createTimestamp ? (
                  <div>
                    <small>
                      {moment(
                        item.createTimestamp,
                        "YYYY-MM-DD hh:mm:ss"
                      ).fromNow()}
                    </small>
                  </div>
                ) : null}
              </Media>
            </Fragment>
          ) : (
            <Fragment>
              {item.title}
              {item.switch}
            </Fragment>
          )}
        </Media>
      </a>
    );
  }

  // ** Function to render Notifications
  const renderNotificationItems = () => {
    return (
      <PerfectScrollbar
        component="li"
        className="media-list scrollable-container"
        options={{
          wheelPropagation: false,
        }}
      >
        {notificationStore.iacArray.length ? (
          <div>
            <li className="dropdown-menu-header">
              <DropdownItem className="d-flex" tag="div" header>
                <h4 className="notification-title mb-0 mr-auto">{iacHeader}</h4>
              </DropdownItem>
            </li>
            {notificationStore.iacArray.map((item, index) => {
              return notificationItem(item, index);
            })}
          </div>
        ) : null}
        {notificationStore.ackArray.length ? (
          <div>
            <li className="dropdown-menu-header">
              <DropdownItem className="d-flex" tag="div" header>
                <h4 className="notification-title mb-0 mr-auto">{ackHeader}</h4>
              </DropdownItem>
            </li>
            {notificationStore.ackArray.map((item, index) => {
              return notificationItem(item, index);
            })}
          </div>
        ) : null}
      </PerfectScrollbar>
    );
  };

  return (
    <UncontrolledDropdown
      tag="li"
      className="dropdown-notification nav-item mr-25"
    >
      <DropdownToggle
        tag="a"
        className="nav-link"
        href="/"
        onClick={(e) => e.preventDefault()}
      >
        <Bell size={21} />
        {notificationStore.numNewNotifications > 0 && (
          <Badge pill color="danger" className="badge-up">
            {notificationStore.numNewNotifications}
          </Badge>
        )}
      </DropdownToggle>
      <DropdownMenu tag="ul" right className="dropdown-menu-media mt-0">
        <li className="dropdown-menu-header">
          <DropdownItem className="d-flex" tag="div" header>
            <h4 className="notification-title mb-0 mr-auto">
              <Bell size={21} /> التنبيهات
            </h4>
            <Button.Ripple
              style={{ width: "fit-content" }}
              color="primary"
              block
              tag={Link}
              to="/notifications"
            >
              عرض كل التنبيهات
            </Button.Ripple>
          </DropdownItem>
        </li>
        {renderNotificationItems()}
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};

export default NotificationDropdown;
