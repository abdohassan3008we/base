// ** React Imports
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

// ** Custom Components
import Avatar from "@components/avatar";

// ** Utils
import { isUserLoggedIn } from "@utils";

// ** Store & Actions
import { useDispatch } from "react-redux";
import { handleLogout } from "@store/actions/auth";

// ** Third Party Components
import {
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
} from "reactstrap";
import { User, Power, CreditCard, HelpCircle } from "react-feather";
import { useTranslation } from "react-i18next";

// ** Default Avatar Image
import defaultAvatar from "@src/assets/images/profile/profile.png";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import apiClient from "../../../../utility/services/apiClient";

const UserDropdown = () => {
  // ** Store Vars
  const dispatch = useDispatch();

  // ** State
  const [username, setUsername] = useState("User Name");
  const [userRole, setUserRole] = useState("User Role");
  const [userAvatar, setUserAvatar] = useState(defaultAvatar);

  const [userData, setUserData] = useState(null);

  useEffect(() => {
    setUserData(apiClient.getUserData());
  }, []);
  useEffect(() => {
    if (userData) {
      setUsername(userData?.userName);
      setUserRole(userData?.userRole);
      setUserAvatar(
        userData?.personalPhoto
          ? "data:image/png;base64," + userData?.personalPhoto
          : null
      );
    }
  }, [userData]);

  //  if (userData && Object.keys(userData).length > 0) {
  // }

  const { t } = useTranslation();

  return (
    <UncontrolledDropdown tag="li" className="dropdown-user nav-item">
      <DropdownToggle
        href="/"
        tag="a"
        className="nav-link dropdown-user-link"
        onClick={(e) => e.preventDefault()}
      >
        <div className="user-nav d-sm-flex d-none">
          <span className="user-name font-weight-bold">{username}</span>
          <span className="user-status text-primary">{userRole}</span>
        </div>
        <Avatar img={userAvatar} imgHeight="40" imgWidth="40" status="online" />
      </DropdownToggle>
      <DropdownMenu right>
        {/* <DropdownItem tag={Link} to='/pages/profile'>
          <User size={14} className='mr-75' />
          <span className='align-middle'>Profile</span>
        </DropdownItem> */}
        {/* <DropdownItem tag={Link} to='/apps/email'>
          <Mail size={14} className='mr-75' />
          <span className='align-middle'>Inbox</span>
        </DropdownItem>
        <DropdownItem tag={Link} to='/apps/todo'>
          <CheckSquare size={14} className='mr-75' />
          <span className='align-middle'>Tasks</span>
        </DropdownItem>
        <DropdownItem tag={Link} to='/apps/chat'>
          <MessageSquare size={14} className='mr-75' />
          <span className='align-middle'>Chats</span>
        </DropdownItem> */}
        {/* <DropdownItem divider />
        <DropdownItem tag={Link} to='/pages/account-settings'>
          <Settings size={14} className='mr-75' />
          <span className='align-middle'>Settings</span>
        </DropdownItem> */}
        {/* <DropdownItem tag={Link} to='/pages/pricing'>
          <CreditCard size={14} className='mr-75' />
          <span className='align-middle'>Pricing</span>
        </DropdownItem> */}
        {/* <DropdownItem tag={Link} to='/pages/faq'>
          <HelpCircle size={14} className='mr-75' />
          <span className='align-middle'>FAQ</span>
        </DropdownItem> */}
        <DropdownItem tag={Link} to="/profile">
          <User size={14} className="mr-75" />
          <span className="align-middle">{t("Profile")}</span>
        </DropdownItem>
        <DropdownItem
          tag={Link}
          to="/login"
          onClick={() => {
            dispatch(handleLogout());
            // window.location.reload(false);
          }}
        >
          <Power size={14} className="mr-75" />
          <span className="align-middle">{t("Logout")}</span>
        </DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};

export default UserDropdown;
