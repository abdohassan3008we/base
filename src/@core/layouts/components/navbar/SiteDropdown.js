import { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { FaEnvelope, FaRegEnvelopeOpen } from "react-icons/fa";
import moment from "moment";
import "moment/locale/ar-sa";

// ** Third Party Components
import classnames from "classnames";
import PerfectScrollbar from "react-perfect-scrollbar";
import { Bell, User } from "react-feather";
import {
  Badge,
  Media,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  UncontrolledDropdown,
  Label,
} from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
// import { listWmsNotificationRequestAsync, readWmsNotificationRequestAsync } from '../../../../redux/admin/notification/thunk/index';
import { useHistory } from "react-router-dom";
import { iacHeader, ackHeader } from "../../../../utility/strings";
import { useTranslation } from "react-i18next";
import { createListRequest } from "../../../../utility/services/requests";
moment.locale("ar-sa");

const SiteDropdown = () => {
  const { t } = useTranslation();
  const forceUpdate = () => {
    window.location.reload(false);
  };
  const [data, setData] = useState([]);
  useEffect(() => {
    createListRequest({ endPoint: "UsgAsnafSerialForBranch/GetAllTBUnits" })({
      dataAction: (data) => data.data,
      setData,
    });
  }, []);

  const currentSiteId = localStorage.getItem("siteId");
  return (
    <UncontrolledDropdown tag="li" className="nav-item">
      <DropdownToggle
        tag="a"
        className="nav-link"
        href="/"
        onClick={(e) => e.preventDefault()}
      >
        <b>{localStorage.getItem("siteName") ?? t("Site Name")}</b>
      </DropdownToggle>
      <DropdownMenu right>
        {data.map((site) => {
          return (
            <>
              <DropdownItem
                tag={"Link"}
                onClick={() => {
                  forceUpdate();
                  localStorage.setItem("siteId", site.siteId);
                  localStorage.setItem("siteName", site.siteName);
                }}
              >
                <User size={14} className="mr-75" />
                {site.siteName}
              </DropdownItem>
            </>
          );
        })}
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};

export default SiteDropdown;
