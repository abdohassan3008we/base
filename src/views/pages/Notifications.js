// ** React Imports
import React, { useEffect } from "react";
import Table from "@src/views/customizedComponents/Table";
// ** Store & Actions
// import { useDispatch, useSelector } from "react-redux";
// import {
//   listWmsNotificationRequestAsync,
//   readWmsNotificationRequestAsync
// } from '../../redux/admin/notification/thunk/index';
import Tabs from "@src/views/customizedComponents/Tabs";
import { Card, Button } from "reactstrap";
import { iacHeader, ackHeader } from "../../utility/strings";
import { Link } from "react-router-dom";
import PageTitle from "../customizedComponents/PageTitle";
import BreadcrumbsDefault from "../customizedComponents/breadcrumbs";
import { string } from "prop-types";
import { useDispatch, useSelector } from "react-redux";

/**
 * Render notifications page
 *
 * @param {Boolean} isPage
 * @returns {JSX.Element}
 */
const Notifications = () => {
  // ** Store Vars
  const dispatch = useDispatch();
  const notificationStore = useSelector(
    (state) => state.WmsNotificationReducer
  );

  const searchFields = [
    "notifPriorityName",
    "notifStatusName",
    "notifTypeName",
    "formNum",
    "openDate",
    "notificationMessage",
  ];

  /**
   * Table columns configurations
   *
   * @param {[Object]}
   */
  const columns = [
    {
      name: "رقم النموذج",
      selector: "formNum",
      sortable: true,
      minWidth: "150px",
    },
    {
      name: "التاريخ",
      selector: "createTimestamp",
      sortable: true,
      minWidth: "150px",
    },
    {
      name: "الرسالة",
      selector: "notificationMessage",
      sortable: true,
      minWidth: "250px",
    },
    {
      name: "نوع التنبيه",
      selector: "notifTypeName",
      sortable: true,
      minWidth: "120px",
    },
    {
      name: "درجة الأهمية",
      selector: "notifPriorityName",
      sortable: true,
      minWidth: "120px",
    },
    {
      name: "تاريخ الإطلاع",
      selector: "openDate",
      sortable: true,
      minWidth: "100px",
    },
    {
      minWidth: "90px",
      cell: (row) => {
        let url = row.editScreenUrl?.replace(":id", row.formId);
        return (
          <div>
            {
              <Button.Ripple
                tag={Link}
                to={url}
                color="primary"
                block
                // onClick={_ => handleClick(row)}
              >
                {row.notifTypeCode == "IAC" ? "فتح" : "إطلاع"}
              </Button.Ripple>
            }
          </div>
        );
      },
    },
  ];

  // const handleClick = (item) => {
  //   if (!item.openData) {
  //     dispatch(readWmsNotificationRequestAsync(item, notificationStore.numNewNotifications))
  //   }
  // }

  const tabItems = [
    {
      num: 1,
      name: iacHeader,
      component: (
        <div className="app-user-list">
          <Table
            columns={columns}
            tableTitle={iacHeader}
            data={notificationStore?.iacArray}
            showAddBtn={false}
            showExtractBtn={false}
            searchFields={searchFields}
          />
        </div>
      ),
    },
    {
      num: 2,
      name: ackHeader,
      component: (
        <div className="app-user-list">
          <Table
            columns={columns}
            tableTitle={ackHeader}
            data={notificationStore?.ackArray}
            showAddBtn={false}
            showExtractBtn={false}
            searchFields={searchFields}
          />
        </div>
      ),
    },
  ];

  // ** Get Tasks on mount & based on dependency change
  // useEffect(() => {
  //   dispatch(listWmsNotificationRequestAsync());
  // }, []);

  const TITLE = "التنبيهات";

  return (
    <>
      <PageTitle title={TITLE} />
      <BreadcrumbsDefault list={[{ name: TITLE, link: null }]} />
      <Card>
        <Tabs items={tabItems} />
      </Card>
    </>
  );
};

export default Notifications;
