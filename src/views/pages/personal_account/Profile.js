import {
  Label,
  Row,
  Col,
  Container,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
} from "reactstrap";
import "@styles/react/libs/charts/apex-charts.scss";
import "@styles/base/pages/dashboard-ecommerce.scss";
import { AvForm, AvGroup, AvField } from "availity-reactstrap-validation-safe";
import apiClient from "@src/utility/services/apiClient";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import {
  ConfirmCancelPopup,
  FormMode,
  Forms,
  ImageField,
  SuccessPopup,
  Text,
  Types,
} from "@src/views/customizedComponents/generic_page";

const EcommerceDashboard = () => {
  const [userData, setUserData] = useState({});

  useEffect(() => {
    setUserData(apiClient.getUserData());
  }, [apiClient, setUserData]);

  return (
    <>
      <Forms
        // mode={FormMode.SHOW}
        fieldWidth={1 / 3}
        disable={false}
        parameters={{
          forms: [
            {
              size: "0.25",
              component: () => (
                <div
                  style={
                    {
                      // borderRadius: "30%",
                      // borderColor: "#451245",
                      // borderStyle: "solid",
                      // borderWidth: "2px",
                      // overflow: "hidden",
                    }
                  }
                >
                  <ImageField
                    field={{
                      // selector: "image",
                      value: userData?.personalPhoto
                        ? `data:image/png;base64,${userData?.personalPhoto}`
                        : null,
                    }}
                  />
                </div>
              ),
            },
            {
              title: "بيانات المستخدم",
              size: "0.5",
              fields: [
                {
                  fieldWidth: 0.5,
                  name: "اسم المستخدم",
                  placeholder: "اسم المستخدم",
                  selector: "userName",
                  value: userData?.userName,
                },
                {
                  fieldWidth: 0.5,
                  name: "اسم الحساب",
                  placeholder: "اسم الحساب",
                  selector: "userId",
                  value: userData?.userId,
                },
                {
                  fieldWidth: 1,
                  name: "دور المستخدم",
                  placeholder: "دور المستخدم",
                  selector: "userRoleName",
                  value: userData?.userRoleName,
                },
                {
                  fieldWidth: 1,
                  name: "اسم المجموعة",
                  placeholder: "اسم المجموعة",
                  selector: "userGroupName",
                  value: userData?.userGroupName,
                },
                {
                  fieldWidth: 1,
                  name: "اسم الوحدة",
                  placeholder: "اسم الوحدة",
                  selector: "unitName",
                  value: userData?.unitName,
                },
              ],
            },
            {
              size: "0.25",
              component: () => (
                <Link to="/resetPassword" className="btn btn-primary">
                  تغيير كلمة المرور
                </Link>
              ),
            },
            {
              title: "بيانات شخصية",
              fields: [
                {
                  name: "نوع تحقيق الشخصية",
                  placeholder: "نوع تحقيق الشخصية",
                  selector: "identityTypeName",
                  value: userData?.identityTypeName,
                },
                {
                  name: "رقم تحقيق الشخصية",
                  placeholder: "رقم تحقيق الشخصية",
                  selector: "identityNum",
                  value: userData?.identityNum,
                },
                {
                  name: "اسم المجموعة",
                  placeholder: "اسم المجموعة",
                  selector: "userGroupName",
                  value: userData?.userGroupName,
                },
                {
                  name: "اللقب/الرتبة",
                  placeholder: "اللقب/الرتبة",
                  selector: "titleName",
                  value: userData?.titleName,
                },
                {
                  name: "الوظيفة",
                  placeholder: "الوظيفة",
                  selector: "jobName",
                  value: userData?.jobName,
                },
                {},
                {
                  name: "الموبايل",
                  placeholder: "الموبايل",
                  selector: "mobileNumber",
                  value: userData?.mobileNumber,
                  type: Types.PHONE,
                },
                {
                  name: "الهاتف",
                  placeholder: "الهاتف",
                  selector: "phoneNumber",
                  value: userData?.phoneNumber,
                },
                {
                  name: "البريد الالكتروني",
                  placeholder: "البريد الالكتروني",
                  selector: "emailAddress",
                  value: userData?.emailAddress,
                  type: Types.EMAIL,
                },
              ],
            },
            {
              title: "محل الإقامة",
              fields: [
                {
                  name: "إسم الدولة",
                  placeholder: "إسم الدولة",
                  selector: "countryName",
                  value: userData?.countryName,
                },
                {
                  name: "إسم المحافظة",
                  placeholder: "إسم المحافظة",
                  selector: "governorateName",
                  value: userData?.governorateName,
                },
                {
                  name: "إسم المدينة",
                  placeholder: "إسم المدينة",
                  selector: "cityName",
                  value: userData?.cityName,
                },
                {
                  fieldWidth: 1,
                  name: "العنوان",
                  placeholder: "العنوان بالتفصيل",
                  selector: "addressDesc",
                  value: userData?.addressDesc,
                  type: Types.TEXTAREA,
                },
              ],
            },
          ],
        }}
      />

      {/* 
      <div id="dashboard-ecommerce">
        <Card>
          <CardHeader>
            <CardTitle
              tag="h4"
              style={{ margin: "auto", padding: "0", fontSize: "200%" }}
            >
              {userData?.admUserItem?.personName}
            </CardTitle>
          </CardHeader>
          <CardBody>
            <AvForm>
              <Row>
                <Col sm={12}>
                  <Container>
                    <Row>
                      <Col
                        className="d-flex"
                        style={{ justifyContent: "space-between" }}
                        md={12}
                        lg={12}
                        sm={4}
                      >
                        <h3>بيانات المستخدم</h3>
                        <Link to="/resetPassword" className="btn btn-primary">
                          تغيير كلمة المرور
                        </Link>
                      </Col>
                    </Row>
                    <hr />
                    <Row>
                      <Col sm={4}>
                        <Label for="userName">اسم المستخدم</Label>
                        <AvField
                          type="text"
                          name="userName"
                          id="userName"
                          placeholder="اسم المستخدم"
                          autoComplete="off"
                          readOnly
                          value={userData?.userName}
                        />
                      </Col>
                      <Col sm={4}>
                        <Label for="userRoleName">دور المستخدم</Label>
                        <AvField
                          type="text"
                          name="userRoleName"
                          id="userRoleName"
                          placeholder="دور المستخدم"
                          autoComplete="off"
                          readOnly
                          value={userData?.userRoleName}
                        />
                      </Col>
                      <Col sm={4}>
                        <AvGroup>
                          <Label for="userGroupName">اسم المجموعة</Label>
                          <AvField
                            type="text"
                            name="userGroupName"
                            id="userGroupName"
                            placeholder="اسم المجموعة"
                            value={userData?.userGroupName}
                            readOnly
                          />
                        </AvGroup>
                      </Col>
                    </Row>
                    <br />
                    <br />
                    <Row>
                      <Col sm={4}>
                        <h3>بيانات شخصية</h3>
                      </Col>
                    </Row>
                    <hr />
                    <Row>
                      <Col sm={4}>
                        <Label for="identityTypeName">نوع تحقيق الشخصية</Label>

                        <AvField
                          type="text"
                          name="identityTypeName"
                          id="identityTypeName"
                          placeholder="نوع تحقيق الشخصية"
                          autoComplete="off"
                          readOnly
                          value={userData?.identityTypeName}
                        />
                      </Col>
                      <Col sm={4}>
                        <AvGroup>
                          <Label for="identityNum">رقم تحقيق الشخصية</Label>
                          <AvField
                            type="text"
                            name="identityNum"
                            id="identityNum"
                            placeholder="رقم تحقيق الشخصية"
                            value={userData?.identityNum}
                            readOnly
                          />
                        </AvGroup>
                      </Col>
                      <Col sm={4}>
                        <Label for="genderName">الجنس</Label>
                        <AvField
                          type="text"
                          name="genderName"
                          id="genderName"
                          placeholder="الجنس"
                          autoComplete="off"
                          readOnly
                          value={userData?.genderName}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col sm={4}>
                        <Label for="titleName">اللقب/الرتبة</Label>
                        <AvField
                          type="text"
                          name="titleName"
                          id="titleName"
                          placeholder="اللقب/الرتبة"
                          autoComplete="off"
                          readOnly
                          value={userData?.titleName}
                        />
                      </Col>
                      <Col sm={4}>
                        <Label for="jobName">الوظيفة</Label>
                        <AvField
                          type="text"
                          name="jobName"
                          id="jobName"
                          placeholder="الوظيفة"
                          autoComplete="off"
                          readOnly
                          value={userData?.jobName}
                        />
                      </Col>
                    </Row>
                    <Row></Row>
                    <Row>
                      <Col sm={4}>
                        <AvGroup>
                          <Label for="mobileNumber">الموبايل</Label>
                          <AvField
                            name="mobileNumber"
                            id="mobileNumber"
                            placeholder="الموبايل"
                            autoComplete="off"
                            readOnly
                            value={userData?.mobileNumber}
                          />
                        </AvGroup>
                      </Col>
                      <Col sm={4}>
                        <AvGroup>
                          <Label for="phoneNumber">الهاتف</Label>
                          <AvField
                            name="phoneNumber"
                            id="phoneNumber"
                            placeholder="الهاتف"
                            autoComplete="off"
                            readOnly
                            value={userData?.phoneNumber}
                          />
                        </AvGroup>
                      </Col>
                      <Col sm={4}>
                        <AvGroup>
                          <Label for="emailAddress">البريد الالكتروني</Label>
                          <AvField
                            type="text"
                            name="emailAddress"
                            id="emailAddress"
                            placeholder="البريد الالكتروني"
                            autoComplete="off"
                            readOnly
                            value={userData?.emailAddress}
                          />
                        </AvGroup>
                      </Col>
                    </Row>
                    <Row style={{ marginRight: "0.1%" }}>محل الاقامة</Row>
                    <div
                      style={{
                        border: "1px solid rgba(255,255,255, 0.1)",
                        borderRadius: "5px",
                        marginBottom: "5px",
                        marginTop: "5px",
                      }}
                    >
                      <Row>
                        <Col sm={4}>
                          <Label for="countryName">إسم الدولة</Label>
                          <AvField
                            name="countryName"
                            id="countryName"
                            placeholder="إسم الدولة"
                            autoComplete="off"
                            readOnly
                            value={userData?.countryName}
                          />
                        </Col>
                        <Col sm={4}>
                          <Label for="governorateName">إسم المحافظة</Label>
                          <AvField
                            name="governorateName"
                            id="governorateName"
                            placeholder="إسم المحافظة"
                            autoComplete="off"
                            readOnly
                            value={userData?.governorateName}
                          />
                        </Col>
                        <Col sm={4}>
                          <Label for="cityName">إسم المدينة</Label>
                          <AvField
                            name="cityName"
                            id="cityName"
                            placeholder="إسم المدينة"
                            autoComplete="off"
                            readOnly
                            value={userData?.cityName}
                          />
                        </Col>
                        <Col sm={12}>
                          <AvGroup>
                            <Label for="addressDesc">العنوان</Label>
                            <AvField
                              type="textarea"
                              name="addressDesc"
                              id="addressDesc"
                              placeholder="العنوان"
                              style={{ minHeight: "50px", maxHeight: "50px" }}
                              autoComplete="off"
                              readOnly
                              value={userData?.addressDesc}
                            />
                          </AvGroup>
                        </Col>
                      </Row>
                    </div>
                  </Container>
                </Col>
              </Row>
            </AvForm>
          </CardBody>
        </Card>
      </div> 
      */}
    </>
  );
};

export default EcommerceDashboard;
