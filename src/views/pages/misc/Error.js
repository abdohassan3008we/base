import { Button } from 'reactstrap'
import { Link } from 'react-router-dom'
import errorImg2 from '@src/assets/images/backgrounds/soldier.png'
import errorImg2Dark from '@src/assets/images/backgrounds/soldierDarkMode.png'

import '@styles/base/pages/page-misc.scss'
import { useSkin } from '@hooks/useSkin'

const Error = () => {
  const [skin, setSkin] = useSkin()

  return (
    <div className='misc-wrapper'>
      <div className='misc-inner p-2 p-sm-3'>
        <div className='w-100 text-center'>
          <h2 className='mb-1' dir='rtl'>تعذر الوصول إلي الصفحة 🕵🏻‍♀️</h2>
          <p className='mb-2 text-primary'>الصفحة لا توجد على الخادم </p>
          <div style={{display:"grid"}}>
          <img className='img-fluid' style={{paddingBottom:"10px"}} src={skin === 'dark' ? errorImg2Dark : errorImg2} alt='Not authorized page' />
          <Button tag={Link} to='/' color='primary' className='btn-sm-block mb-2'>
            العودة إلي الصفحة الرئيسية
          </Button>
          </div>
        </div>
      </div>
    </div>
  )
}
export default Error
