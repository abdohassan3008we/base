import { Button } from 'reactstrap'
import { Link } from 'react-router-dom'
import notAuthImg from '@src/assets/images/pages/unauthorized-access.png'

import '@styles/base/pages/page-misc.scss'
import PageTitle from '../../customizedComponents/PageTitle'
import Row from 'reactstrap/lib/Row'
import Col from 'reactstrap/lib/Col'

const NotAuthorized = () => {
  return (
    <>
      <PageTitle title='غير مصرّح بالدخول' />
      <div className='misc-wrapper'>
        <div className='misc-inner p-2 p-sm-3'>
          <div className='w-100 text-center'>
            <h2 className='mb-1' dir='rtl'>غير مصرّح لك بالدخول 🔐</h2>
            <p className='mb-2' dir='rtl'>
              لا تملك الصلاحيات الكافية للولوج إلي هذه الصفحة!
            </p>
            <Row>
              <Col>
                <Button tag={Link} to='/' color='primary' className='btn-sm-block mb-1'>
                  العودة إلي الصفحة الرئيسية
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>
                <img className='img-fluid' src={notAuthImg} alt='Not authorized page' />
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </>
  )
}
export default NotAuthorized
