import "@styles/react/libs/tables/react-dataTable-component.scss";
import {
  CreateScreens,
  unEditable,
  id,
  hideList,
  popupPages,
  enableEdit,
  enableAdd,
  required,
  ltr,
  enableDelete,
  hide,
  enablePagination,
  StandardPages,
  Validator,
  Types,
} from "../../customizedComponents/generic_page";
const siteIdOfuser = JSON.parse(localStorage.getItem("userData"));
const screens = CreateScreens({
  popupPages,
  urlTitle: "admin/ItemModel",
  title: "ItemModel",
  list: { endPoint: ["/UsgItemModel/GetAll"] },
  get: { endPoint: ["/UsgItemModel/GetById"] },
  post: { endPoint: ["/UsgItemModel/Post"] },
  put: { endPoint: ["/UsgItemModel/Put"] },
  del: { endPoint: ["/UsgItemModel/Delete"] },

  //   params: { active: true },
  enableAdd,
  enablePagination,
  enableEdit,
  enableDelete,
  forms: [
    {
      fields: [
        {
          hide,
          id,
          selector: "modelId",
        },

        {
          name: "ItemModel",
          selector: "modelName",
          sortable: true,
          type: Types.TEXT,
          required,
        },
        {
          name: "modelDesc",
          selector: "modelDesc",
          sortable: true,
          type: Types.TEXTAREA,
          required,
        },
      ],
    },
  ],
});

export default screens[StandardPages.MASTER];
