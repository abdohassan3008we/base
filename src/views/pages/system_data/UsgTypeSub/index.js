import "@styles/react/libs/tables/react-dataTable-component.scss";
import {
  CreateScreens,
  unEditable,
  id,
  hideList,
  popupPages,
  enableEdit,
  enableAdd,
  required,
  enableDelete,
  ltr,
  hide,
  enablePagination,
  StandardPages,
  Validator,
  Types,
  enableAddByFile,
} from "../../../customizedComponents/generic_page";

const screens = CreateScreens({
  popupPages,
  urlTitle: "TypeSub",
  title: "TypeSub",
  list: { endPoint: ["/UsgTypeSub/GetAll"] },
  get: { endPoint: ["/UsgTypeSub/GetById"] },
  post: { endPoint: ["/UsgTypeSub/Post"] },
  put: { endPoint: ["/UsgTypeSub/Put"] },
  del: { endPoint: ["/UsgTypeSub/Delete"] },

  // enableAddByFile,
  enableAdd,
  enablePagination,
  enableDelete,
  enableEdit,
  forms: [
    {
      fields: [
        {
          hide,
          id,
          selector: "subUsageTypeId",
        },
        {
          hide,
          id,
          selector: "mainUsageTypeId",
        },

        {
          name: "subUsageTypeName",
          selector: "subUsageTypeName",
          sortable: true,
          type: Types.TEXT,
          required,
        },
      ],
    },
  ],
});

export default screens[StandardPages.MASTER];
