import "@styles/react/libs/tables/react-dataTable-component.scss";
import {
  CreateScreens,
  unEditable,
  id,
  hideList,
  popupPages,
  enableEdit,
  enableAdd,
  required,
  ltr,
  hide,
  enablePagination,
  StandardPages,
  enableDelete,
  Validator,
  Types,
} from "../../../customizedComponents/generic_page";

const screens = CreateScreens({
  popupPages,
  urlTitle: "admin/UsgTrainingPlan",
  title: "Training Plan",
  list: { endPoint: ["/UsgTrainingPlan/GetAll"] },
  get: { endPoint: ["/UsgTrainingPlan/GetById"] },
  post: { endPoint: ["/UsgTrainingPlan/Post"] },
  put: { endPoint: ["/UsgTrainingPlan/Put"] },
  del: { endPoint: ["/UsgTrainingPlan/Delete/"] },

  //   params: { active: true },
  // enableDelete,
  // enableAdd,
  enableExtract: false,
  enablePagination,
  // enableEdit,
  forms: [
    {
      fields: [
        {
          hide,
          id,
          selector: "planId",
        },
        {
          name: "Train Year",
          selector: "trainingId",
          labelSelector: "yearPeriodId",
          type: Types.LIST,

          listProps: {
            listEndPoint: ["/UsgTrainingYear/GetAll"],
            enablePagination,
            valueSelector: "trainingId",
            labelSelector: "trainYear",
          },
        },
        {
          name: "startDate",
          selector: "startDate",
          sortable: true,
          type: Types.DATE_TIME_PICKER,
          required,
        },
        {
          name: "endDate",
          selector: "endDate",
          sortable: true,
          type: Types.DATE_TIME_PICKER,
          required,
        },
      ],
    },
  ],
});

export default screens[StandardPages.MASTER];
