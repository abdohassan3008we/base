import "@styles/react/libs/tables/react-dataTable-component.scss";
import {
  CreateScreens,
  unEditable,
  id,
  hideList,
  popupPages,
  enableEdit,
  enableAdd,
  required,
  ltr,
  hide,
  enablePagination,
  StandardPages,
  enableDelete,
  Validator,
  Types,
} from "../../../customizedComponents/generic_page";
//} from "@genericComponents";

const screens = CreateScreens({
  popupPages,
  urlTitle: "admin/UsgTrainingYear",
  title: "Training Year",
  list: { endPoint: ["/UsgTrainingYear/GetAll"] },
  get: { endPoint: ["/UsgTrainingYear/GetById"] },
  post: { endPoint: ["/UsgTrainingYear/Post"] },
  put: { endPoint: ["/UsgTrainingYear/Put"] },
  del: { endPoint: ["/UsgTrainingYear/Delete/"] },

  //   params: { active: true },
  // enableDelete,
  // enableAdd,
  enableExtract: false,
  enablePagination,
  // enableEdit,
  forms: [
    {
      fields: [
        {
          hide,
          id,
          selector: "trainingId",
        },

        {
          name: "Training Year",
          selector: "trainYear",
          sortable: true,
          type: Types.TEXT,
          required,
        },
        {
          name: "startDate",
          selector: "startDate",
          sortable: true,
          type: Types.DATE_TIME_PICKER,
          required,
        },
        {
          name: "EndDate",
          selector: "endDate",
          sortable: true,
          type: Types.DATE_TIME_PICKER,
          required,
        },
        {
          name: "periodCount",
          selector: "periodCount",
          type: Types.SWITCH,
          defaultValue: true,
          activeInList: false,
        },
      ],
    },
  ],
});

export default screens[StandardPages.MASTER];
