import "@styles/react/libs/tables/react-dataTable-component.scss";
import {
  CreateScreens,
  unEditable,
  id,
  hideList,
  popupPages,
  enableEdit,
  enableAdd,
  required,
  ltr,
  hide,
  enablePagination,
  StandardPages,
  enableDelete,
  Validator,
  Types,
  PrintingModel,
} from "../../../customizedComponents/generic_page";

const screens = CreateScreens({
  popupPages,
  urlTitle: "admin/UsgMonth",
  title: "MonName",
  list: { endPoint: ["/UsgMonth/GetAll"] },
  get: { endPoint: ["/UsgMonth/GetById"] },
  post: { endPoint: ["/UsgMonth/Post"] },
  put: { endPoint: ["/UsgMonth/Put"] },
  del: { endPoint: ["/UsgMonth/Delete/"] },

  //   params: { active: true },
  // enableDelete,
  // enableAdd,
  enableExtract: true,
  enablePagination,
  // enableEdit,

  forms: [
    {
      fields: [
        {
          hide,
          id,
          selector: "monId",
        },

        {
          name: "MonName",
          selector: "monName",
          sortable: true,
          type: Types.TEXT,
          required,
        },
      ],
    },
  ],
});

export default screens[StandardPages.MASTER];
