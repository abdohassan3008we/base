import "@styles/react/libs/tables/react-dataTable-component.scss";
import {
  CreateScreens,
  unEditable,
  id,
  hideList,
  popupPages,
  enableEdit,
  enableAdd,
  required,
  ltr,
  hide,
  enableDelete,
  enablePagination,
  StandardPages,
  Validator,
  Types,
} from "../../../customizedComponents/generic_page";

const screens = CreateScreens({
  popupPages,
  urlTitle: "admin/TypeSubject",
  title: "TypeSubject",
  list: { endPoint: ["/UsgTypeSubject/GetAll"] },
  get: { endPoint: ["/UsgTypeSubject/GetById"] },
  get: { endPoint: ["/UsgTypeSubject/GetAllSubUsageTypeId"] },
  post: { endPoint: ["/UsgTypeSubject/Post"] },
  put: { endPoint: ["/UsgTypeSubject/Put"] },
  del: { endPoint: ["/UsgTypeSubject/Delete"] },

  //   params: { active: true },
  enableAdd,
  enablePagination,
  enableDelete,
  enableEdit,
  forms: [
    {
      fields: [
        {
          hide,
          id,
          selector: "subjectId",
        },
        {
          hide,
          id,
          selector: "subUsageTypeId",
        },

        {
          name: "subjectName",
          selector: "subjectName",
          sortable: true,
          type: Types.TEXT,
          required,
        },
      ],
    },
  ],
});

export default screens[StandardPages.MASTER];
