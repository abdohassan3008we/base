import "@styles/react/libs/tables/react-dataTable-component.scss";
import {
  CreateScreens,
  unEditable,
  id,
  hideList,
  popupPages,
  enableEdit,
  enableAdd,
  required,
  ltr,
  hide,
  enableDelete,
  enablePagination,
  StandardPages,
  Validator,
  Types,
} from "../../../customizedComponents/generic_page";
// } from "@genericComponents";

const screens = CreateScreens({
  popupPages,
  urlTitle: "admin/TypeMain",
  title: "TypeMain",
  list: { endPoint: ["/UsgTypeMain/GetAll"] },
  get: { endPoint: ["/UsgTypeMain/GetById"] },
  post: { endPoint: ["/UsgTypeMain/Post"] },
  put: { endPoint: ["/UsgTypeMain/Put"] },
  del: { endPoint: "/UsgTypeMain/Delete" },

  //   params: { active: true },

  enablePagination,
  enableEdit,
  enableDelete,
  enableAdd,
  forms: [
    {
      fields: [
        {
          hide,
          id,
          selector: "usageTypeId",
        },

        {
          name: "usageTypeName",
          selector: "usageTypeName",
          sortable: true,
          type: Types.TEXT,
          required,
        },
      ],
    },
  ],
});

export default screens[StandardPages.MASTER];
