import "@styles/react/libs/tables/react-dataTable-component.scss";
import {
  CreateScreens,
  unEditable,
  id,
  hideList,
  popupPages,
  enableEdit,
  enableAdd,
  required,
  ltr,
  hide,
  enablePagination,
  StandardPages,
  enableDelete,
  Validator,
  Types,
} from "../../../customizedComponents/generic_page";
//} from "@genericComponents";

const screens = CreateScreens({
  popupPages,
  urlTitle: "admin/UsgYearPeriod",
  title: "Year Period",
  list: { endPoint: ["/UsgYearPeriod/GetAll"] },
  get: { endPoint: ["/UsgYearPeriod/GetById"] },
  post: { endPoint: ["/UsgYearPeriod/Post"] },
  put: { endPoint: ["/UsgYearPeriod/Put"] },
  del: { endPoint: ["/UsgYearPeriod/Delete/"] },

  //   params: { active: true },
  // enableDelete,
  // enableAdd,
  enableExtract: false,
  enablePagination,
  // enableEdit,
  forms: [
    {
      fields: [
        {
          hide,
          id,
          selector: "yearPeriodId",
        },

        {
          name: "periodName",
          selector: "periodName",
          sortable: true,
          type: Types.TEXT,
          required,
        },
        {
          name: "startDate",
          selector: "startDate",
          sortable: true,
          type: Types.DATE_TIME_PICKER,
          required,
        },
        {
          name: "EndDate",
          selector: "endDate",
          sortable: true,
          type: Types.DATE_TIME_PICKER,
          required,
        },
        {
          name: "monthCount",
          selector: "monthCount",
          sortable: true,
          type: Types.COUNTER,
          required,
        },
      ],
    },
  ],
});

export default screens[StandardPages.MASTER];
