import { Edit2 } from "react-feather";
import {
  CreateScreens,
  StandardPages,
  SuccessMessage,
} from "../../customizedComponents/generic_page";

import SerialsTabComponent from "./SerialsTab";
import { Button } from "reactstrap";
import { createPostRequest } from "../../../utility/services/requests";
import { Local } from "../../../utility/page_storage";
import SuccessPopup from "../../customizedComponents/SuccessPopup";
const flagShowPlan = Local.getShowPlan();
const flagShowCard = Local.getShowCard();
export default CreateScreens({
  title: "UsgOhda7sabSanf",
  urlTitle: "UsgOhda7sabSanf",
  editUrlTitle: "UsgOhda7sabSanff/:id/edit",
  list: {
    endPoint: ["/UsgOhda7sabSanf/GetAll"],
  },
  get: { endPoint: ["/UsgOhda7sabSanf/GetByAsnafId/"] },

  enablePagination: true,
  enableEdit: flagShowCard !== 0,
  // enableShow: flagShowCard !== 0,
  forms: [
    {
      fields: [
        {
          hide: true,
          id: true,
          selector: "asnafId",
        },
        {
          hide: true,
          selector: "fsc",
        },
        {
          hide: true,
          selector: "niin",
        },
        {
          fieldWidth: 1 / 3,
          name: "fsc-niin",
          cell: (row) => (row.fsc ?? "") + "-" + (row.niin ?? ""),
          value: (row) => {
            return row.fsc + "-" + row.niin;
          },
          sortable: true,
          unEditable: true,
        },
        {
          fieldWidth: 1 / 3,
          name: "Nomenclature",
          selector: "nomenclature",
          sortable: true,
          unEditable: true,
        },
        {
          fieldWidth: 1 / 3,
          name: "Total Qty",
          selector: "totalQty",
          sortable: true,
          unEditable: true,
        },
      ],
    },
  ],
  tabs: [
    {
      num: 1,
      name: "Serials",
      component: (selectors) => {
        const Tab = SerialsTabComponent({ masterId: selectors.asnafId });

        return <Tab />;
      },
      listeners: ["asnafId"],
    },
  ],
  pages: [
    flagShowCard === 0 ? (
      {
        name: "Create Cards",
        btn: (params) => (
          <Button {...params} className="btn btn-primary">
            {params.title}
          </Button>
        ),
        onClick: () => {
          createPostRequest({ endPoint: "ExcuteProc/unitFillAsnaf" })({
            onSuccess: () => {
              SuccessPopup("تم انشاء الكروت بنجاح");
              localStorage.setItem("showCreateCard", 1);
              Local.refresh();
            },
          });
        },
      }
    ) : flagShowPlan === 0 ? (
      {
        name: "Create Plan",
        btn: (params) => (
          <Button {...params} className="btn btn-primary">
            {params.title}
          </Button>
        ),
        onClick: () => {
          createPostRequest({ endPoint: "ExcuteProc/unitFillUsgPlan" })({
            onSuccess: () => {
              SuccessPopup("تم انشاء الخطة بنجاح");
              localStorage.setItem("showCardPlan", 1);
              Local.refresh();
            },
          });
          // createPostRequest({ endPoint: "ExcuteProc/Training_Plan" })();
        },
      }
    ) : (
      <></>
    ),
  ],
});
