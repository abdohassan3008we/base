import { TbEditCircle as Edit } from "react-icons/tb";
import {
  CreateScreens,
  EditModel,
  StandardPages,
  Types,
  enablePagination,
} from "../../customizedComponents/generic_page";
import { Button } from "reactstrap";
import { createPostRequest } from "../../../utility/services/requests";
import { Local } from "../../../utility/page_storage";

export default ({ masterId }) =>
  CreateScreens({
    popupPages: true,
    title: "",
    list: {
      endPoint: ["UsgAsnafSerial/GetUsgAsnafSerialsByAsnafID"],
      breaker: null,
      params: { MasterID: masterId },
    },
    put: {
      endPoint: "UsgAsnafSerial/Put",
    },
    enableAdd: true,
    enablePagination: true,
    enableExtract: false,
    // enableEdit: true,

    forms: [
      {
        fields: [
          {
            hide: true,
            id: true,
            selector: "asnafSerialsId",
          },

          {
            name: "Serial Number",
            selector: "serialNum",
            sortable: true,
            // unEditable: true,
          },
          {
            name: "Army Number",
            selector: "armyNum",
            sortable: true,
          },
          {
            name: "Motor Number",
            selector: "motorNum",
            sortable: true,
          },
          {
            name: "Tiktiky Number",
            selector: "tiktikyNum",
            sortable: true,
          },
          {
            name: "chassisNum",
            selector: "chassisNum",
            sortable: true,
          },
          {
            name: "BoxGear Number",
            selector: "boxGearNum",
            sortable: true,
          },
          {
            name: "Model",
            selector: "asnafModelId",
            type: Types.TEXT,
            hideEdit: true,
            hideList: true,
          },
          // {
          //   name: "الموديل",
          //   selector: "asnafModelId",
          //   labelSelector: "asnafModelName",
          //   type: Types.LIST,
          //   listProps: {
          //     enablePagination,
          //     listEndPoint: ["UsgItemModel/GetAllForUnit"],
          //     labelSelector: "modelName",
          //     valueSelector: "modelId",
          //   },
          // },
          {
            readOnly: Local.getShowPlan() != 0 ? true : false,
            name: "نوع الإستخدام",
            selector: "mainUsageTypeId",
            labelSelector: "mainUsageTypeName",
            type: Types.LIST,
            listProps: {
              enablePagination,
              listEndPoint: ["UsgTypeMain/GetAllForUnit"],
              labelSelector: "usageTypeName",
              valueSelector: "mainUsageTypeId",
            },
          },
          {
            name: "Assigned Type",
            selector: "assignedTypeId",
            labelSelector: "assignedName",
            type: Types.LIST,
            listProps: {
              enablePagination,
              listEndPoint: ["UsgAssignedType/GetAllForUnit"],
              labelSelector: "assignedName",
              valueSelector: "assignedTypeId",
            },
          },
          {
            name: "Usage Prepaired",
            selector: "prepairedTypeId",
            labelSelector: "prepairedName",
            type: Types.LIST,
            listProps: {
              enablePagination,
              listEndPoint: ["UsgPrepairedType/GetAllForUnit"],
              labelSelector: "prepairedName",
              valueSelector: "prepairedTypeId",
            },
          },
          // {
          //   name: "ChildUnitCode",
          //   selector: "childFieldUnitId",
          //   labelSelector: "childUnitName",
          //   type: Types.LIST,
          //   listProps: {
          //     enablePagination,
          //     listEndPoint: ["UsgAsnafSerial/GetChildFiledUnitByParentUnitID"],
          //     labelSelector: "unitName",
          //     valueSelector: "fieldUnitId",
          //   },
          // },
          // {},
        ],
      },
    ],
    pages: [
      {
        name: StandardPages.EDIT_MODEL,
        disable: (params) => {
          return params.serialStatus == 2;
        },
        component: (params) => {
          console.log(params);
          return <EditModel {...params} />;
        },
        icon: (params) => {
          return <Edit {...params} color="#006d43" />;
        },
      },
      // {
      //   name: "Create Plan",
      //   btn: (params) => (
      //     <Button {...params} className="btn btn-primary">
      //       {params.title}
      //     </Button>
      //   ),
      //   onClick: () => {
      //     createPostRequest({ endPoint: "ExcuteProc/unitFillUsgPlan" })();
      //     // createPostRequest({ endPoint: "ExcuteProc/Training_Plan" })();
      //   },
      // },
    ],
  })[StandardPages.MASTER];
