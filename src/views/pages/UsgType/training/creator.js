import { Button } from "reactstrap";
import { userRoles } from "../../../../utility/constants";
import { Local } from "../../../../utility/page_storage";
import {
  CreateScreens,
  PrintingModel,
  StandardPages,
} from "../../../customizedComponents/generic_page";
import { trainingReport } from "../../../reports/reportsFiles";

import SerialsTab from "../serials/creator";
// const admin = Local.isAdmin();
export const screens = CreateScreens({
  title: "",
  urlTitle: "/UsgType/training",
  enableEdit: true,
  enableShow: false,
  editUrlTitle: "/UsgType/training_/:id/serials",
  list: {
    endPoint: Local.isAdmin()
      ? ["UsgAsnafSerialForBranch/GetUsgAsnafByMainUsageTypeAndSiteID"]
      : ["UsgAsnafMainUsage/GetUsgAsnafByMainUsageType"],
    params: Local.isAdmin()
      ? {
          MasterID: 1,
          SiteId: localStorage.getItem("siteId"),
        }
      : { MasterID: 1 },
  },
  get: {
    endPoint: ["Asnaf/GetAsnafByAsnafID?MasterID="],
  },

  enablePagination: true,
  //   enableEdit: true,
  forms: [
    {
      fields: [
        {
          hide: true,
          id: true,
          selector: "asnafId",
        },
        {
          hide: true,
          selector: "fsc",
        },
        {
          hide: true,
          selector: "niin",
        },
        {
          // fieldWidth: 1 / 3,
          name: "fsc-niin",
          cell: (row) => (row.fsc ?? "") + "-" + (row.niin ?? ""),
          value: (row) => {
            return row.fsc + "-" + row.niin;
          },
          sortable: true,
          unEditable: true,
        },

        {
          // fieldWidth: 1 / 2,

          name: "Nomenclature",
          selector: "nomenclature",
          sortable: true,
          unEditable: true,
        },
        // {
        //   fieldWidth: 1 / 3,
        //   name: "Total Qty",
        //   selector: "totalQty",
        //   sortable: true,
        //   unEditable: true,
        //   hideList: true,
        // },
      ],
    },
  ],
  tabs: [
    {
      num: 1,
      name: "Serials",
      component: (data) => {
        const Tab = SerialsTab({
          // masterId: data.asnafId,
          usgType: "training",
          usgTypeId: 1,
          masterId: data.asnafId,
          title: data.nomenclature,
        });

        return <Tab />;
      },
      listeners: ["asnafId"],
    },
  ],
  pages: [
    {
      name: "report",
      disable: false,
      component: (params) => {
        return params.open ? (
          <PrintingModel
            {...{
              ...params,
              isOpen: true,
              handleModel: params.handleModel,
              // showCard: true,
              cardSizeScale: 1.5,
              files: [
                {
                  title: "إسم التقرير الظاهر للمستخدم",
                  fileName: "إسم الملف بعد الحفظ",
                  // component: myReport({
                  //   header: {},
                  //   body: {},
                  //   footer: {},
                  // }),
                  component: trainingReport({}),
                },
              ],
            }}
          />
        ) : (
          <></>
        );
      },
      btn: (params) => <Button {...params}>{params.title}</Button>,
    },
  ],
});
