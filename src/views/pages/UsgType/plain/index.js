import Trining from "../training/index";
import Combat from "../combat/index";
import {
  Forms,
  List,
  SuccessPopup,
  Tabs,
} from "../../../customizedComponents/generic_page";
import { useTranslation } from "react-i18next";
import { Button, Card, CardBody, Col, Row } from "reactstrap";
import { useFormik } from "formik";
import {
  createListRequest,
  createPostRequest,
} from "../../../../utility/services/requests";
import { Local } from "@src/utility/page_storage";

import { useEffect, useState } from "react";
// import { Tabs } from "react-bootstrap";
export default function Index() {
  const { t } = useTranslation();
  const [key, setKey] = useState(1);
  const [data, setData] = useState([]);

  const forceUpdate = () => {
    setKey(k => k + 1)
  }
  const formik = useFormik({
    initialValues: {},
    onSubmit: () => {
      createPostRequest({
        endPoint: Local.isAdmin()
          ? `/ExcuteProc/tbApproveUsg?UnitSiteId=${Local.getSiteID()}`
          : "/ExcuteProc/unitApproveUsg",
      })({
        onSuccess: () => {
          setKey((k) => k + 1);
        },
      });
    },
  });
  useEffect(() => {
    createListRequest({
      endPoint: Local.isAdmin()
        ? `/CheckUnitConfirmPlan/GetUnitPlanStatus?UnitSiteId=${Local.getSiteID()}`
        : "/CheckUnitConfirmPlan/GetUnitPlanStatus",
    })({
      onSuccess: (data) => {
        setData(data);
      },
    });
  }, [key]);
  console.log(data);
  const tabs = [
    {
      num: 1,
      name: t("Training"),
      component: <Trining />,
    },
    {
      num: 2,
      name: t("Combat"),
      component: <Combat />,
    },
  ];
  console.log(data.unitPlanStatus);
  return (
    <>
      <Card>
        <CardBody >
          <div style={{ display: "flex", justifyContent: "space-around" }}>
            <div style={{ width: "50%" }}>
              {localStorage.getItem("siteName") ??
                <div style={{ width: "50%" }}>
                  <List
                    field={
                      {
                        placeholder: t("Site Name"),
                        selector: "",
                        listProps: {
                          listEndPoint: "UsgAsnafSerialForBranch/GetAllTBUnits",
                          valueSelector: "siteId",
                          labelSelector: "siteName",
                          dataAction: (data) => data.data
                        }
                      }
                    }
                    onChange={
                      (site) => {
                        forceUpdate();
                        localStorage.setItem("siteId", site.value);
                        localStorage.setItem("siteName", site.label);
                      }
                    }
                  />
                </div>
              }
            </div>
            <div style={{ width: "50%" }}>
              {data.unitPlanStatus == 0 ? (
                <Button.Ripple color="primary" onClick={formik.handleSubmit}>
                  {t("Confirm")}
                </Button.Ripple>
              ) : data.unitPlanStatus == 1 && Local.isAdmin() ? (
                <Button.Ripple
                  color="primary"
                  onClick={() => {
                    formik.handleSubmit();
                    SuccessPopup("تم تاكيد الخطة");
                  }}
                >
                  {t("Confirm")}
                </Button.Ripple>
              ) : data.unitPlanStatus == 1 && !Local.isAdmin() ? (
                "تم ارسال الخطه للفرع للمراجعة"
              ) : data.unitPlanStatus == 2 && !Local.isAdmin() ? (
                "تم تاكيد الخطة من الفرع"
              ) : data.unitPlanStatus == 2 && Local.isAdmin() ? (
                "تم تاكيد الخطة"
              ) : (
                ""
              )}
            </div>
          </div>
        </CardBody>
      </Card>
      <Tabs items={tabs} />
    </>
  );
}
