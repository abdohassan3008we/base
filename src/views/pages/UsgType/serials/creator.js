import { Edit, Edit2 } from "react-feather";
import {
  CreateScreens,
  EditModel,
  StandardPages,
  Types,
  enablePagination,
} from "../../../customizedComponents/generic_page";
import { userRoles } from "../../../../utility/constants";
import { Local } from "../../../../utility/page_storage";

export default ({ masterId, title, usgType, usgTypeId }) => {
  const listEndPoint = Local.isAdmin()
    ? [
        "UsgAsnafSerialForBranch/GetAllUsgAsnafSerialByMainUsageTypeIdAsnafIdAndSiteId",
      ]
    : ["UsgAsnafSerial/GetUsgAsnafSerialsByAsnafIdAndTrainingOrFight"];
  const listParams = Local.isAdmin()
    ? {
        MasterID: masterId,
        VMainUsageTypeId: usgTypeId,
        SiteId: localStorage.getItem("siteId"),
      }
    : {
        MasterID: masterId,
        VMainUsageTypeId: usgTypeId,
      };

  return CreateScreens({
    popupPages: false,
    // urlTitle: "UsgType/training/serials",
    editUrlTitle: `/UsgType/${usgType}_subject/:id/edit`,
    title: title,
    list: {
      endPoint: listEndPoint,
      breaker: null,
      params: listParams,
    },
    get: {
      endPoint: ["UsgAsnafSerial/GetUsgAsnafSerialsByAsnafID?MasterID="],
    },
    enableEdit: true,
    // enableAdd:true,
    enablePagination: true,
    enableExtract: false,

    forms: [
      {
        fields: [
          {
            hide: true,
            id: true,
            selector: "asnafSerialId",
          },

          {
            fieldWidth: 1 / 3,
            name: "Serial Number",
            selector: "serialNum",
            sortable: true,
            // unEditable: true,
          },
          {
            fieldWidth: 1 / 3,
            name: "Army Number",
            selector: "armyNum",
            sortable: true,
          },
          {
            fieldWidth: 1 / 3,
            name: "Motor Number",
            selector: "motorNum",
            sortable: true,
            hide: true,
          },
          {
            fieldWidth: 1 / 3,
            name: "Tiktiky Number",
            selector: "tiktikyNum",
            sortable: true,
            hide: true,
          },
          {
            fieldWidth: 1 / 3,
            name: "chassisNum",
            selector: "chassisNum",
            sortable: true,
          },
          {
            fieldWidth: 1 / 3,
            name: "BoxGear Number",
            selector: "boxGearNum",
            sortable: true,
            hide: true,
          },
          {
            fieldWidth: 1 / 3,
            name: "الموديل",
            selector: "asnafModelId",
            labelSelector: "asnafModelName",
            hide: true,
            type: Types.LIST,
            listProps: {
              enablePagination,
              listEndPoint: ["UsgItemModel/GetAllForUnit"],
              labelSelector: "modelName",
              valueSelector: "modelId",
            },
          },
          {
            fieldWidth: 1 / 3,
            name: "نوع الإستخدام",
            selector: "mainUsageTypeId",
            labelSelector: "mainUsageTypeName",
            hide: true,
            type: Types.LIST,
            listProps: {
              enablePagination,
              listEndPoint: ["UsgTypeMain/GetAll"],
              labelSelector: "usageTypeName",
              valueSelector: "mainUsageTypeId",
            },
          },
          {
            fieldWidth: 1 / 3,
            name: "Assigned Type",
            selector: "assignedTypeId",
            labelSelector: "assignedTypeName",
            hide: true,
            type: Types.LIST,
            listProps: {
              enablePagination,
              listEndPoint: ["UsgAssignedType/GetAll"],
              labelSelector: "assignedName",
              valueSelector: "assignedTypeId",
            },
          },
          {
            fieldWidth: 1 / 3,
            name: "Usage Prepaired",
            selector: "prepairedTypeId",
            labelSelector: "prepairedTypeName",
            hide: true,
            type: Types.LIST,
            listProps: {
              enablePagination,
              listEndPoint: ["UsgPrepairedType/GetAll"],
              labelSelector: "prepairedName",
              valueSelector: "prepairedTypeId",
            },
          },
          // {
          //   fieldWidth: 1 / 3,
          //   name: "Child Unit Code",
          //   selector: "childUnitCode",
          //   labelSelector: "childUnitName",
          //   minWidth: "150px",
          //   type: Types.LIST,
          //   listProps: {
          //     enablePagination,
          //     listEndPoint: ["UsgAsnafSerial/GetChildFiledUnitByParentUnitID"],
          //     labelSelector: "unitName",
          //     valueSelector: "fieldUnitId",
          //   },
          // },
          {
            fieldWidth: 1 / 3,
            name: "totalPlannedValue",
            selector: "totalPlannedValue",
            minWidth: "150px",
          },
          {
            fieldWidth: 1 / 3,
            name: "totalActualValue",
            selector: "totalActualValue",
            minWidth: "150px",
          },
          { fieldWidth: 1 / 3 },
        ],
      },
    ],
  })[StandardPages.MASTER];
};
