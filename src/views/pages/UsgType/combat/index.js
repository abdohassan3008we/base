import { StandardPages } from "../../../customizedComponents/generic_page";
import { screens } from "./creator";

export default screens[StandardPages.MASTER];
