import { useFormik } from "formik";
import { ErrorPopup, Forms } from "../../../customizedComponents/generic_page";
import {
  Button,
  Card,
  CardFooter,
  CardHeader,
  CardTitle,
  CardBody,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
} from "reactstrap";
import { useTranslation } from "react-i18next";
import { createUpdateRequest } from "../../../../utility/services/requests";
import SuccessPopup from "../../../customizedComponents/SuccessPopup";
import { useEffect, useState } from "react";

export default ({ data, handelSubSubjectModel, id, masterFormik }) => {
  const { t } = useTranslation();
  const formik = useFormik({
    initialValues: data,
    enableReinitialize: true,
    onSubmit: (values) => {
      masterFormik.handleSubmit();
      // SuccessPopup(t("Success Update"));
      // handelSubSubjectModel();
      // createUpdateRequest({ endPoint: "UsgSerialUsageRateDetail/Put" })({
      //   body: values,
      //   onSuccess: (_) => {
      //     SuccessPopup(t("Success Update"));
      //     handelSubSubjectModel && handelSubSubjectModel();
      //   },
      //   onFailure: () => {
      //     ErrorPopup(t("Error Update"));
      //   },
      // });
    },
  });
  useEffect(() => {
    masterFormik.setFieldValue(id, data);
  }, []);

  function handleChange(index, field, value) {
    formik.setFieldValue(`[${index}].${field}`, value);
    masterFormik.setFieldValue(`${id}.[${index}].${field}`, value);
  }
  return (
    <>
      <CardHeader>
        <Button className="btn" color="primary" onClick={formik.handleSubmit}>
          {t("Save")}
        </Button>
      </CardHeader>

      <div
        style={{
          maxHeight: "50em",
          padding: "15px",
          overflowY: "scroll",
          justifyContent: "center",
        }}
      >
        <Row style={{ justifyContent: "center" }}>
          {formik?.values?.map((item, index) => (
            // <Row key={index}>
            <Col md={4}>
              <Card>
                <CardTitle
                  style={{ textAlign: "center" }}
                >{` شهر ${item.monthName}`}</CardTitle>
                <Row>
                  <Col md={5} style={{ marginRight: "15px" }}>
                    <FormGroup>
                      <Label
                        style={{ marginLeft: "10px" }}
                        for={`plannedValue${index}`}
                      >
                        {t("plannedValue")}
                      </Label>
                      <Input
                        style={{ marginLeft: "10px" }}
                        id={`plannedValue${index}`}
                        defaultValue={item?.plannedValue || 0}
                        onChange={(e) =>
                          handleChange(index, "plannedValue", e.target.value)
                        }
                      />
                    </FormGroup>
                  </Col>

                  <Col md={5}>
                    <FormGroup>
                      <Label
                        style={{ marginLeft: "10px" }}
                        for={`actualValue${index}`}
                      >
                        {t("actualValue")}
                      </Label>

                      <Input
                        style={{ marginLeft: "10px" }}
                        id="actualValue"
                        defaultValue={item?.actualValue || 0}
                        onChange={(e) =>
                          handleChange(index, "actualValue", e.target.value)
                        }
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md={5} style={{ marginRight: "15px" }}>
                    <FormGroup>
                      <Label
                        style={{ marginLeft: "10px" }}
                        for={`tranningPersonNum${index}`}
                      >
                        {t("tranningPersonNum")}
                      </Label>
                      <Input
                        style={{ marginLeft: "10px" }}
                        id="tranningPersonNum"
                        defaultValue={item?.tranningPersonNum || 0}
                        onChange={(e) =>
                          handleChange(
                            index,
                            "tranningPersonNum",
                            e.target.value
                          )
                        }
                      />
                    </FormGroup>
                  </Col>
                  <Col md={5}>
                    <FormGroup>
                      <Label
                        style={{ marginLeft: "10px" }}
                        for={`tranningCount${index}`}
                      >
                        {t("tranningCount")}
                      </Label>
                      <Input
                        style={{ marginLeft: "10px" }}
                        id="tranningCount"
                        defaultValue={item?.tranningCount || 0}
                        onChange={(e) =>
                          handleChange(index, "tranningCount", e.target.value)
                        }
                      />
                    </FormGroup>
                  </Col>
                </Row>
              </Card>
            </Col>
            // </Row>
          ))}
        </Row>

        {/* <Forms
          mode="edit"
          formik={formik}
          parameters={{
            forms: formik?.values?.data?.map((item, index) => {
              return getMonthFormObject({
                id: item.detId,
                title: item.monthName,
                rateId: item.rateId,
                detId: item.detId,
              });
            }),
          }}
        /> */}
      </div>
    </>
    // <Tabs
    //   items={[
    //     getMonthPlanTab({ num: 0, id: "1-23", title: "JAN" }),
    //     getMonthPlanTab({ num: 1, id: "2-23", title: "JAN" }),
    //     getMonthPlanTab({ num: 2, id: "3-23", title: "JAN" }),
    //     getMonthPlanTab({ num: 3, id: "4-23", title: "JAN" }),

    //   ]}
    // />/UsgType/training_subject
  );
};
// const getMonthPlanTab = ({ num, id, title }) => ({
//   num: num,
//   name: title ?? id,
//   component: (
//     <Form fieldWidth={1 / 3} fields={getMonthFieldsObject({ id: id })} />
//   ),
// });

const getMonthFormObject = ({ id, title }) => ({
  title: title ?? id,
  size: 1 / 3,
  fields: [
    { name: "plannedValue", selector: "plannedValue" + id },
    { name: "actualValue", selector: "actualValue" + id },
    { name: "tranningPersonNum", selector: "tranningPersonNum" + id },
    { name: "tranningCount", selector: "tranningCount" + id },
  ],
});

// const getMonthFieldsObject = ({ id }) => [
//   { name: "", selector: "field1" + id },
//   { name: "", selector: "field2" + id },
//   { name: "", selector: "field3" + id },
//   { name: "", selector: "field4" + id },
//   { name: "", selector: "field5" + id },
//   { name: "", selector: "field6" + id },
// ];
