import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import {
  createGetRequest,
  createUpdateRequest,
} from "../../../../utility/services/requests";
import {
  CreateScreens,
  CustomModel,
  FormMode,
  Table,
  Tabs,
} from "../../../customizedComponents/generic_page";
import { Form } from "../../../customizedComponents/generic_page/screens/pageContent/forms/form";
import { useEffect, useState } from "react";
import { useFormik } from "formik";
// import { Edit } from "react-feather";
import { TbEditCircle as Edit } from "react-icons/tb";
import SubSubject from "../subSubject";
import { useDispatch } from "react-redux";
import {
  disableSaveButton,
  enableSaveButton,
} from "../../../../redux/reducers/navbar/actionCreators";
import { ActiveNavbarButtons } from "../../../../redux/reducers/navbar/activeNavbarButtons";
import { Local } from "../../../../utility/page_storage";
import { userRoles } from "../../../../utility/constants";
import SuccessPopup from "../../../customizedComponents/SuccessPopup";
import ErrorPopup from "../../../customizedComponents/ErrorPopup";
import { useTranslation } from "react-i18next";
// import tab1 from "./index";

export default () => {
  const { t } = useTranslation();
  const { id } = useParams();
  const [item, setItem] = useState(null);
  const [tabs, setTabs] = useState([]);
  // const [key, setKey] = useState(0);
  const [subSubjectParams, setSubSubjectParams] = useState(null);
  const [openSubSubject, setOpenSubSubject] = useState(false);
  const handelSubSubjectModel = () => {
    setOpenSubSubject((o) => !o);
  };
  const forceUpdate = () => {
    Local.refresh();

    // setKey((k) => k + 1);
  };

  useEffect(() => {
    createGetRequest({
      endPoint: Local.isAdmin()
        ? "UsgAsnafSerialForBranch/GetAllUsgSerialUsageRateByAsnafSerialID"
        : "UsgSerialUsageRate/GetAllByAsnafSerialID",
    })({
      params: Local.isAdmin()
        ? { MasterID: id, SiteId: Local.siteId }
        : { MasterID: id },
      onSuccess: ({ data }) => {
        setItem(data[0]);
      },
    });
  }, []);

  useEffect(() => {
    item &&
      createGetRequest({
        endPoint: Local.isAdmin()
          ? "UsgTypeSub/GetAllByMainUsageTypeID"
          : "UsgTypeSub/GetAllByMainUsageTypeIDForUnit",
      })({
        params: { MasterID: item.mainUsageTypeId },
        onSuccess: ({ data }) => {
          data.map((subject, index) => {
            createGetRequest({
              endPoint: "UsgTypeSubject/GetAllBySubUsageTypeID",
            })({
              params: { MasterID: subject.subUsageTypeId },
              onSuccess: ({ data }) => {
                setTabs((t) => [
                  ...t,
                  {
                    num: index + 1,
                    name: subject.subUsageTypeName,
                    component: subject.subjectFlag ? (
                      <SubjectDetails
                        subjects={data}
                        onClick={(params) => {
                          setSubSubjectParams((oldParams) => ({
                            ...params,
                            name:
                              subject.subUsageTypeName + " - " + params.name,
                          }));
                          handelSubSubjectModel();
                        }}
                      />
                    ) : (
                      <>
                        <SubSubjectDetails
                          masterId={item?.rateId}
                          subSubjectId={subject.subUsageTypeId}
                        />
                      </>
                    ),
                  },
                ]);
              },
            });
          });
        },
      });
  }, [item]);

  let sortedTabs = tabs.sort((p1, p2) =>
    p1.num > p2.num ? 1 : p1.num < p2.num ? -1 : 0
  );

  const formik = useFormik({
    initialValues: item ?? {},
    enableReinitialize: true,
    onSubmit: (values) => {
      createUpdateRequest({ endPoint: "UsgSerialUsageRate/Put" })({
        body: values,
        onSuccess: (_) => {
          SuccessPopup(t("Success Update"));
          forceUpdate();
        },
      });
    },
  });

  ActiveNavbarButtons({ save: formik.submitForm });

  return (
    <>
      <Form
        // disable={true}
        mode={FormMode.EDIT}
        formik={formik}
        fields={[
          {
            hide: true,
            id: true,
            selector: "rateId",
          },
          {
            name: "Nomenclature",
            selector: "nomenclature",
            unEditable: true,
          },
          {
            name: "Serial Number",
            selector: "serialNum",
            unEditable: true,
          },
          {
            name: "counterReading",
            selector: "counterReading",
          },
          {
            name: "MotorClock",
            selector: "motorClock",
          },
          // {
          //   fieldWidth: 1 / 3,
          //   name: "firstMaintenance",
          //   selector: "firstMaintenance",
          //   sortable: true,
          //   unEditable: true,
          // },
          // {
          //   fieldWidth: 1 / 3,
          //   name: "secondMaintenance",
          //   selector: "secondMaintenance",
          //   sortable: true,
          //   unEditable: true,
          // },
          // {
          //   fieldWidth: 1 / 3,
          //   name: "fourthMaintenance ",
          //   selector: "fourthMaintenance",
          //   sortable: true,
          //   unEditable: true,
          // },
        ]}
      />
      <Tabs items={sortedTabs} />
      {openSubSubject && (
        <CustomModel
          open={openSubSubject}
          handleModel={handelSubSubjectModel}
          title={subSubjectParams.name ?? ""}
          body={
            <SubSubjectDetails
              masterId={item?.rateId}
              subjectId={subSubjectParams?.subjectId}
              subSubjectId={subSubjectParams?.subSubjectId}
              handelSubSubjectModel={handelSubSubjectModel}
              forceUpdate={forceUpdate}
            />
          }
        />
      )}
    </>
  );
};

const SubjectDetails = ({ subjects, onClick }) => {
  const { t } = useTranslation();
  return (
    <Table
      showSearch={false}
      showAddBtn={false}
      columns={[
        { name: t("subjectName"), selector: "subjectName" },
        {
          name: t("totalPlannedValue"),
          selector: "totalValueForSubject",
          // minWidth: "150px",
        },
        {
          name: t("totalActualValue"),
          selector: "actualValueForSubject",
          // minWidth: "150px",
        },
        {
          cell: (row) => {
            return (
              <Edit
                className="active_icon"
                color="#006d43"
                // size={25}
                onClick={() =>
                  onClick({
                    subjectId: row.subjectId,
                    subSubjectId: row.subUsageTypeId,
                    name: row.subjectName,
                  })
                }
              />
            );
          },
        },
      ]}
      data={subjects}
    />
  );
};

const SubSubjectDetails = ({
  masterId = null,
  subjectId = null,
  subSubjectId = null,
  handelSubSubjectModel,
  forceUpdate,
} = {}) => {
  const [data, setdata] = useState(null);
  const [yearPeriods, setYearPeriods] = useState(null);
  const [periodsTabs, setPeriodsTabs] = useState([]);
  const { t } = useTranslation();

  const formik = useFormik({
    initialValues: {},
    enableReinitialize: true,
    onSubmit: (valuesObj) => {
      let valuesList = [];
      Object.keys(valuesObj).map((key) => {
        const subFormikValues = valuesObj[key] ?? [];
        valuesList = [...valuesList, ...subFormikValues];
      });
      createUpdateRequest({ endPoint: "UsgSerialUsageRateDetail/Put" })({
        body: valuesList,
        onSuccess: (_) => {
          SuccessPopup(t("Success Update"));
          forceUpdate && forceUpdate();
          handelSubSubjectModel && handelSubSubjectModel();
        },
        onFailure: () => {
          ErrorPopup(t("Error Update"));
        },
      });
    },
  });

  useEffect(() => {
    createGetRequest({
      endPoint: "/UsgYearPeriod/GetAllByYearID",
    })({
      onSuccess: (data) => {
        setYearPeriods((d) => data);
      },
    });
  }, []);
  useEffect(() => {
    if (yearPeriods) {
      yearPeriods.map((yearPeriod) => {
        createGetRequest({
          endPoint:
            "UsgSerialUsageRateDetail/GetSerialByRateIDAndSubjectAndSubType",
          breaker: "-1",
        })({
          params: {
            MasterID: masterId ?? "-1",
            VSubjectID: subjectId,
            VSubUsageTypeId: subSubjectId,
            VYearPeriodID: yearPeriod.yearPeriodId,
          },
          setData: (data) =>
            setdata((d) => ({
              ...d,
              [yearPeriod.periodName]: {
                data: data.sort((i1, i2) => i1.planId - i2.planId),
                id: yearPeriod.yearPeriodId,
              },
            })),
        });
      });
    }
  }, [masterId, subjectId, subSubjectId, yearPeriods]);
  useEffect(() => {
    if (data) {
      setPeriodsTabs(
        Object.keys(data)
          .map((tabName, index) => {
            console.log(data[tabName]["id"], data[tabName]["data"]);
            return {
              num: data[tabName]["id"],
              name: tabName,
              id: data[tabName]["id"],
              component: (
                <SubSubject
                  data={data[tabName]["data"]}
                  id={"id_" + data[tabName]["id"]}
                  handelSubSubjectModel={handelSubSubjectModel}
                  masterFormik={formik}
                />
              ),
            };
          })
          .sort((t1, t2) => t1.id - t2.id)
      );
    }
  }, [data]);

  return (
    <>
      <Tabs items={periodsTabs} />
    </>
  );
};
