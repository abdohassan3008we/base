import "@styles/react/libs/tables/react-dataTable-component.scss";
import {
  CreateScreens,
  unEditable,
  id,
  hideList,
  popupPages,
  enableEdit,
  enableAdd,
  required,
  enableDelete,
  ltr,
  hide,
  enablePagination,
  StandardPages,
  Validator,
  Types,
} from "../../customizedComponents/generic_page";

const screens = CreateScreens({
  popupPages,
  urlTitle: "admin/AssignedType",
  title: "AssignedType",
  list: { endPoint: ["/UsgAssignedType/GetAll", "data"] },
  get: { endPoint: ["/UsgAssignedType/GetById"] },
  post: { endPoint: ["/UsgAssignedType/Post"] },
  put: { endPoint: ["/UsgAssignedType/Put"] },
  del: { endPoint: ["/UsgAssignedType/Delete"] },

  //   params: { active: true },
  enableAdd,
  enablePagination,
  enableEdit,
  enableDelete,

  forms: [
    {
      fields: [
        {
          hide,
          id,
          selector: "assignedTypeId",
        },

        {
          name: "AssignedType",
          selector: "assignedName",
          sortable: true,
          type: Types.TEXT,
          required,
        },
      ],
    },
  ],
});

export default screens[StandardPages.MASTER];
