import "@styles/react/libs/tables/react-dataTable-component.scss";
import {
  CreateScreens,
  unEditable,
  id,
  hideList,
  popupPages,
  enableEdit,
  enableAdd,
  required,
  ltr,
  enableDelete,
  hide,
  enablePagination,
  StandardPages,
  Validator,
  Types,
} from "../../customizedComponents/generic_page";

const screens = CreateScreens({
  popupPages,
  urlTitle: "admin/PrepairedType",
  title: "PrepairedType",
  list: { endPoint: ["/UsgPrepairedType/GetAll"] },
  get: { endPoint: ["/UsgPrepairedType/GetById"] },
  post: { endPoint: ["/UsgPrepairedType/Post"] },
  put: { endPoint: ["/UsgPrepairedType/Put"] },
  del: { endPoint: ["/UsgPrepairedType/Delete/"] },

  //   params: { active: true },

  enablePagination,
  enableEdit,
  enableDelete,
  enableAdd,
  forms: [
    {
      fields: [
        {
          hide,
          id,
          selector: "prepairedTypeId",
        },

        {
          name: "PrepairedType",
          selector: "prepairedName",
          sortable: true,
          type: Types.TEXT,
          required,
        },
      ],
    },
  ],
});

export default screens[StandardPages.MASTER];
