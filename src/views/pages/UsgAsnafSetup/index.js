// ** Styles
// import "@styles/react/libs/tables/react-dataTable-component.scss";
import { param } from "jquery";
import {
  unEditable,
  readOnly,
  id,
  hide,
  enableEdit,
  enablePagination,
  // enableDelete,
  Types,
  hideList,
  enableAdd,
  CreateScreens,
  StandardPages,
} from "../../customizedComponents/generic_page";
import AddUsgAsnafSetup from "./AddItem/index";
const screens = CreateScreens({
  title:"UsgAsnafSetup",
  urlTitle: "UsgAsnafSetup",

  list: { endPoint: ["/UsgAsnafSetup/GetAll"] },
  post: { endPoint: ["/UsgAsnafSetup/Post"] },
  get: { endPoint: ["/UsgAsnafSetup/GetById/"] },
  put: {
    endPoint: ["/UsgAsnafSetup/Put"],
    enableFormListening: true,
  },
  del: { endPoint: ["/UsgAsnafSetup/Delete/"] },
  //addPopupPage: true,
  popupPages: true,
  enableAdd,
  // enableEdit,
  enableShow: true,
  enableDelete: false,
  enablePagination,
  // list: { params: { active: true } },
  forms: [
    {
      fields: [
        {
          id,
          hide,
          selector: "asnafSetupId",
        },
        {
          readOnly: true,
          name: "fsc-niin",
          value: (row) => row.fsc && row.niin && `${row.fsc}-${row.niin}`,
          cell: (row) => row.fsc && row.niin && `${row.fsc}-${row.niin}`,
          minWidth: "100px",
        },
        {
          name: "Nomenclature",
          selector: "nomenclature",
          readOnly: true,
        },
      ],
    },
  ],
  pages: [
    {
      name: StandardPages.ADD_MODEL,
      component: (params) => {
        const tab = AddUsgAsnafSetup({ ...params });
        return tab;
      },
    },
  ],
});

export default screens;
