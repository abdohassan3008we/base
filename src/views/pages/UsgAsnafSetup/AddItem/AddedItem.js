import React, { useEffect } from "react";
import { Trash2 } from "react-feather";
import { useTranslation } from "react-i18next";
import { Label, Row, Col, FormGroup, Input } from "reactstrap";
export default function AddedItem({ item, handleRemoval, setValidForm }) {
  const { t } = useTranslation();
  useEffect(() => {
    document.body.classList.add("modal-open");
  }, []);
  return (
    <>
      <Row style={{ position: "relative" }}>
        <Trash2
          dataId={item.htmlId}
          className="cursor-pointer text-danger"
          size={15}
          style={{ position: "absolute", top: 0, left: "1em", zIndex: 9999 }}
          onClick={(e) => {
            // User may click on the child element to this svg which has the data ID.
            // So, we pass the dataId from the parent if clicked on child element.
            const elem =
              e.target.tagName === "svg" ? e.target : e.target.parentElement;
            handleRemoval(elem.getAttribute("dataId"));
          }}
        />
        <Col sm={1}></Col>
        <Col md={4}>
          <FormGroup>
            <Label for="niin">{t("fsc-niin")}</Label>
            <Input
              id="niin"
              defaultValue={
                item.fsc && item.niin ? `${item.fsc}-${item.niin}` : ""
              }
              disabled
            />
          </FormGroup>
        </Col>

        <Col md={4}>
          <FormGroup>
            <Label for="nomenclature">اسم الصنف</Label>
            <Input
              id="nomenclature"
              defaultValue={item.nomenclature}
              disabled
            />
          </FormGroup>
        </Col>
      </Row>

      <hr />
    </>
  );
}
