import { PlusSquare } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Card, CardBody } from "reactstrap";
import { randomString } from "../../../../utility/Utils";
import { listOhda7sabSanfSetupRequestAsync } from "../../../../redux/Ohda7sabSanfSetup/thunk";
import ErrorPopup from "../../../customizedComponents/ErrorPopup";
import ServerSideTable from "../../../customizedComponents/ServerSideTable";
import { useEffect } from "react";
import { createGetRequest } from "../../../../utility/services/requests";
import { t } from "i18next";
export default function List({
  setSelectedItems,
  data,
  keysearch,
  setKeysearch,
  setData,
}) {
  const store = useSelector((state) => state.Ohda7sabSanfSetupReducer);
  const dispatch = useDispatch();
  /**
   * Handle equipment selection
   *
   * @param {Object} row
   */
  const handleSelection = (row) => {
    setSelectedItems((prevItems) => [
      ...prevItems,
      { ...row, htmlId: randomString() },
    ]);
  };

  /**
   * Table columns configurations
   *
   * @param {[Object]}
   */
  const columns = [
    {
      width: "5em",
      cell: (row) => (
        <div>
          <PlusSquare
            onClick={(_) => {
              handleSelection(row);
              setKeysearch((s) => s + 1);
            }}
            size={22}
            className="mr-50"
            style={{ cursor: "pointer" }}
          />
        </div>
      ),
    },
    {
      name: t("fsc-niin"),
      cell: (row) => {
        return row.fsc && row.niin ? `${row.fsc}-${row.niin}` : "";
      },
      minWidth: "100px",
    },

    {
      name: t("Nomenclature"),
      selector: "nomenclature",
      sortable: true,
      minWidth: "180px",
    },
  ];
  useEffect(() => {
    createGetRequest({ endPoint: "Asnaf/GetAllForTb" })({
      onSuccess: (data) => {
        setData(data);
      },
    });
  }, []);
  return (
    <div className="app-user-list">
      <Card>
        <CardBody>
          <ServerSideTable
            refresherKey={keysearch}
            columns={columns}
            allData={data}
            data={data?.data}
            totalRecords={data?.totalRecords}
            progress={store?.data?.isLoading}
            onPagination={(PageNumber = 1, PageSize = 5, PageSearch) => {
              createGetRequest({ endPoint: "Asnaf/GetAllForTb" })({
                params: { PageNumber, PageSize, PageSearch },
                onSuccess: (data) => {
                  setData(data);
                },
              });
            }}
            showAddBtn={false}
            showSearch={true}
          />
        </CardBody>
      </Card>
    </div>
  );
}
