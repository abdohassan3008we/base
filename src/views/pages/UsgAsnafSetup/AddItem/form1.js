import React, { useEffect } from "react"
import { useFormik } from "formik"; 
import {
  Card,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Form,
  Button,
  Label,
} from "reactstrap"; 

const Form1 = ({ setData ,setKeysearch}) => {
  const formik = useFormik({
    initialValues: {
      itemName: null,
      niin:null,
    },
    onSubmit: (values) => {
      setData(values)
      setKeysearch(2)
      
    },
  });
  useEffect(() => setData([]), []); 
  return (
    <Card>
      <CardBody>
        <Form onSubmit={formik.handleSubmit}>
          <Row>
          
             {/* <Col md="3">
              <FormGroup>
                <Label for="niin">كود الصنف</Label>
                <Input {...formik.getFieldProps('niin')} />
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label for="itemName"> اسم الصنف</Label>
                <Input {...formik.getFieldProps('itemName')} />
              </FormGroup>
            </Col> */}
          
          </Row>
          <div style={{ textAlign: "center" }}>
            <Row>
              <Col md={12}>
                <FormGroup>
                  <Button.Ripple type="submit" color="primary">
                    بحث
                  </Button.Ripple>
                </FormGroup>
              </Col>
            </Row>
          </div>
        </Form>
      </CardBody>
    </Card>
  );
};

export default Form1;
