import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useFormik } from "formik";
import { Col, Row, Button } from "reactstrap";
import List from "./list";
import Form1 from "./form1";
import Form2 from "./form2";
import CustomModal from "@src/views/customizedComponents/Modal";
import ErrorPopup from "../../../customizedComponents/ErrorPopup";
import SuccessPopup from "../../../customizedComponents/SuccessPopup";
import { postUsgAsnafSetupRequestAsync } from "../../../../redux/UsgAsnafSetup/thunk";
import { useTranslation } from "react-i18next";

/**
 * @param {Boolean} open boolean indicates whether to open the modal or not
 * @param {Function} handleModal
 * @returns {React.Component}
 */
const Index = (param) => {
  const { open, handleModel } = param;
  const forceUpdatemaster = param.forceUpdate;
  const [key, setKey] = useState(1);
  const [data, setData] = useState(null);
  const [keysearch, setKeysearch] = useState(1);
  const [selectedItems, setSelectedItems] = useState([]);
  const { t } = useTranslation();

  const dispatch = useDispatch();
  const siteIdOfuser = JSON.parse(localStorage.getItem("userData"));
  console.log(siteIdOfuser.siteId);
  const formik = useFormik({
    initialValues: {
      items: [...selectedItems],
    },
    enableReinitialize: true,
    onSubmit: (_) => {
      const inputs = selectedItems.map((item) => {
        // Construct inputs from selected array
        return {
          asnafId: item.asnafId,
          tbSiteId: siteIdOfuser?.siteId ?? null,
          deptId: null,
        };
      });
      // Submit each record alone
      for (const input of inputs) {
        dispatch(
          postUsgAsnafSetupRequestAsync({
            params: input,
            onSuccess: (_) => {
              handleModel();
              SuccessPopup(t("Success Add"));
              forceUpdatemaster();
            },
            onFailure: (_) => {
              ErrorPopup(t("Error Add"));
            },
          })
        );
      }
      // Reset selected items
      setSelectedItems([]);
    },
  });

  /**
   * Force update/re-render the component
   */
  const forceUpdate = () => {
    setKey((key) => key + 1);
    setSelectedItems([]);
  };

  const [validForm, setValidForm] = useState(true);

  const CardBody = (
    <>
      <Row>
        <Col sm="12" style={{ textAlign: "center" }}>
          <div className="d-flex" style={{ justifyContent: "space-between" }}>
            <h4 style={{ padding: "0", fontSize: "200%" }}>إضافة صنف جديد</h4>
          </div>
        </Col>
      </Row>
      <hr />
      <Row>
        {/* <Col sm='12'>
          <Form1 setKeysearch={setKeysearch} setData={setData} invAuditMaster={invAuditMaster}   />
        </Col> */}
        <Col sm="12">
          <List
            keysearch={keysearch}
            setKeysearch={setKeysearch}
            setSelectedItems={setSelectedItems}
            data={data}
            setData={setData}
          />
        </Col>
        <Col sm="12">
          <Form2
            selectedItems={selectedItems}
            setSelectedItems={setSelectedItems}
            setValidForm={setValidForm}
            refresh={forceUpdate}
          />
        </Col>
      </Row>
      <Row>
        <Col style={{ textAlign: "right" }}>
          <Button.Ripple
            color="primary"
            onClick={formik.handleSubmit}
            disabled={!validForm}
          >
            حفظ
          </Button.Ripple>
          &nbsp;
        </Col>
        <Col style={{ textAlign: "left" }}>
          &nbsp;
          <Button
            onClick={() => {
              handleModel();
              setKeysearch(1);
            }}
          >
            إلغاء
          </Button>
        </Col>
      </Row>
    </>
  );

  return (
    <CustomModal
      open={open}
      handleModal={handleModel}
      body={CardBody}
      maxWidth="80%"
    />
  );
};

export default Index;
