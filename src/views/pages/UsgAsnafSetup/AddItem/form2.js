import { useEffect, useState } from 'react';
import {
  Card,
  CardHeader,
  Container,
} from 'reactstrap'
import AddedItem from './AddedItem'; 

const Form2 = ({ selectedItems, setSelectedItems,  refresh, setValidForm }) => {
  const [openSerialModal, setOpenSerialModal] = useState(false);
  const [catalogItemId, setCatalogItemId] = useState(undefined);

  useEffect(() => setSelectedItems([]), [])

  /**
   * Remove from selected items
   *
   * @param {Number} id
   */
  const handleRemoval = (id) => setSelectedItems((items) => items.filter((item) => item.htmlId !== id))

  /**
   * Toggles add new serial modal
   *
   * @param {Number} catalogItemId
   */
  const toggleAddNewSerialModal = (catalogItemId) => {
    setCatalogItemId(catalogItemId)
    setOpenSerialModal(prevToggle => !prevToggle)
  }

  return (
    <Card>
      <CardHeader>
        <h4>الأصناف المضافة ({selectedItems.length})</h4>
      </CardHeader>
      <div style={{ maxHeight: "30em", overflowY: "scroll" }}>
        <Container>
          {selectedItems.length ? selectedItems.map(((item, index) => (
            <AddedItem
              key={item.htmlId}
              item={item}
              index={index}
              handleRemoval={handleRemoval}
              
              openAddSerialModal={toggleAddNewSerialModal}
              setValidForm={setValidForm}
            />
          ))) : (
            <p style={{ textAlign: "center" }}>لا توجد عناصر مضافة</p>
          )}
        </Container>
      </div>
      
    </Card>
  )
}
export default Form2
