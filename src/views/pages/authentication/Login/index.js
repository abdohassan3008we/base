import { useState } from "react";
import classnames from "classnames";
import useJwt from "@src/auth/jwt/useJwt";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { handleLogin } from "@store/actions/auth";
import { useHistory } from "react-router-dom";
import InputPasswordToggle from "@components/input-password-toggle";
import { getHomeRouteForLoggedInUser, isObjEmpty } from "@utils";
import backgroundSoldierDark from "@src/assets/images/backgrounds/soldierDarkMode.png";
import backgroundSoldierLight from "@src/assets/images/backgrounds/soldier.png";
import backgroundTankDark from "@src/assets/images/backgrounds/tanksbg.jpeg";
import backgroundTankLight from "@src/assets/images/backgrounds/arm.jpg";
import logo1 from "../../../../assets/images/logo/tankslogo.png";
import logo2 from "../../../../assets/images/logo/nozom-logo.png";
import { SuccessPopup } from "@src/views/customizedComponents/generic_page";
import { Local } from "@src/utility/page_storage";

import {
  CardText,
  Form,
  Input,
  FormGroup,
  Label,
  Button,
  Card,
  CardBody,
} from "reactstrap";
import { useSkin } from "@hooks/useSkin";

import "./index.css";
import "@styles/base/pages/page-auth.scss";
// import { useTranslation } from "react-i18next";
import { postUsersRequestAsync } from "../../../../redux/auther/users/thunk";
import jwtDefaultConfig from "../../../../@core/auth/jwt/jwtDefaultConfig";
import { useTranslation } from "react-i18next";
import { createGetRequest } from "../../../../utility/services/requests";
import apiClient from "../../../../utility/services/apiClient";
import {
  Form as GForm,
  Types,
} from "../../../customizedComponents/generic_page";

const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [userName, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);
  // const { t } = useTranslation()
  const { register, errors, handleSubmit } = useForm();
  const { t } = useTranslation();

  const onSubmit = (_) => {
    if (isObjEmpty(errors)) {
      useJwt
        .login({ userName: userName, password: password })
        .then((res) => {
          if (res.status == 200) {
            const data = {
              ...res.data,
              accessToken: res.data,
              refreshToken: res.data,
            };

            localStorage.setItem(
              jwtDefaultConfig.storageTokenKeyName,
              JSON.stringify(res.data)
            );
            const accessToken = `Bearer ${res.data}`;

            const tokenNew =
              accessToken != null
                ? accessToken.substring(7, accessToken.length)
                : accessToken;
            dispatch(handleLogin(res));
            const headers = {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers":
                "Origin, X-Requested-With, Content-Type, Accept",
              "Access-Control-Allow-Methods":
                "GET,PUT,POST,DELETE,PATCH,OPTIONS",
              "Content-Type": "application/json",
              Authorization: `${accessToken}`,
            };

            createGetRequest({
              api: apiClient.headers(headers),
              endPoint: "UserData/GetUserRole",
            })({
              onSuccess: (r) => {
                localStorage.setItem("userData", JSON.stringify(r.data));
                localStorage.setItem("userRole", r.data.userRoleId);
                history.push(getHomeRouteForLoggedInUser("admin"));
                createGetRequest({
                  endPoint: "/CheckUnitConfirmPlan/CheckCreateCard",
                })({
                  onSuccess: (r) => {
                    console.log(r);
                    localStorage.setItem("showCreateCard", JSON.stringify(r));
                    createGetRequest({
                      endPoint: "CheckUnitConfirmPlan/CheckCreatePlan",
                    })({
                      onSuccess: (r) => {
                        console.log(r);
                        localStorage.setItem("showCardPlan", JSON.stringify(r));
                        Local.refresh();
                      },
                    });
                  },
                });
              },
            });
            // dispatch(
            //  getUsersTypeRequestAsync({header:headers,
            //    onSuccess: (r) => {
            //      if(r.succeeded)
            //      {
            //         localStorage.setItem("userRole",JSON.stringify(r.data))
            //         localStorage.setItem("siteID",JSON.stringify(r.data.siteID.replaceAll('"','')))
            //         localStorage.setItem("userTypeName",JSON.stringify(r.data.userTypeName.replaceAll('"','')))
            //      }
            //    }
            //  })
            // )
          }
        })
        .catch(({ response }) => {
          // setErrorMessage(err.response);
          setErrorMessage(
            response ? t("Wrong Username_Pass") : t("NetworkError")
          );
        });
    }
  };
  const [skin, setSkin] = useSkin();

  return (
    <>
      <div
        className="login-wrapper-background"
        style={{
          backgroundImage:
            skin === "dark"
              ? `url(${backgroundTankLight})`
              : `url(${backgroundTankDark})`,
          // backgroundImage: `url(${backgroundTankLight})`,
        }}
      >
        <div className="login-wrapper">
          <div className="login-header ">
            {/* <img id="img1" src={logo1} /> */}
            <div
              className="login-header-text"
              style={{
                backgroundColor:   skin === "dark"?"#060d21bb":"#f5f5f5dd",
                color: skin === "dark"?"#bbb": "#6e6b7b",
                borderRadius: "50px 10px",
                padding: "15px",
              }}
            >
              {t("app_detail").toUpperCase()}
            </div>
            {/* <img id="img2" src={logo2} /> */}
          </div>
          <div className="login-body">
            <Card
              className="text-center login-card"
              // style={{ backgroundColor: "#283046 !important" }}
            >
              <CardBody>
                <CardText
                  style={{
                    textAlign: "center",
                    fontSize: "130%",
                    fontWeight: "bolder",
                    padding: "5%",
                  }}
                  className="mb-2"
                >
                  {t("LogIn")}
                </CardText>
                <Form
                  className="auth-login-form mt-2 "
                  onSubmit={handleSubmit(onSubmit)}
                >
                  <FormGroup style={{ textAlign: "right" }}>
                    <Label className="form-label" for="login-email">
                      {t("Username")}
                    </Label>
                    <Input
                      style={{ textAlign: "right" }}
                      autoFocus
                      type="text"
                      value={userName}
                      id="login-email"
                      name="login-email"
                      placeholder={t("Enter Username")}
                      autoComplete="off"
                      onChange={(e) => setEmail(e.target.value)}
                      className={classnames({
                        "is-invalid": errors["login-email"],
                      })}
                      innerRef={register({
                        required: true,
                        validate: (value) => value !== "",
                      })}
                    />
                  </FormGroup>
                  <FormGroup style={{ textAlign: "right" }}>
                    <Label className="form-label" for="login-password">
                      {t("Password")}
                    </Label>
                    <InputPasswordToggle
                      style={{ textAlign: "right" }}
                      value={password}
                      id="login-password"
                      name="login-password"
                      onChange={(e) => setPassword(e.target.value)}
                      className={classnames({
                        "is-invalid": errors["login-password"],
                      })}
                      innerRef={register({
                        required: true,
                        validate: (value) => value !== "",
                      })}
                    />
                  </FormGroup>
                  <Button.Ripple type="submit" color="primary" block>
                    {t("Login")}
                  </Button.Ripple>
                </Form>
                <br />
                <CardText
                  style={{ textAlign: "center", color: "red" }}
                  className="mb-2"
                >
                  {errorMessage}
                </CardText>
                {/* <GForm
                  formik={null}
                  fields={[
                    {
                      name: "w",
                      selector: "www",
                      type: Types.RADIAN_GROUP,
                      groupProps: {
                        children: [
                          {
                            value: "1",
                            label: "1",
                            check: false,
                            disable: false,
                          },
                          {
                            value: "2",
                            label: "2",
                            check: false,
                            disable: false,
                          },
                        ],
                      },
                    },
                  ]}
                /> */}
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
