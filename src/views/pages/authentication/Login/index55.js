import InputPasswordToggle from "@components/input-password-toggle";
import {
  Card,
  CardBody,
  CardText,
  Form,
  FormGroup,
  Label,
  Input,
  CustomInput,
  Button,
} from "reactstrap";
import "@styles/base/pages/page-auth.scss";
import "./index.css";

import logo1 from "../../../../assets/images/pages/logo1.png";
import logo2 from "../../../../assets/images/pages/logo2.png";

const LoginV1 = () => {
  return (
    <div className=" login-wrapper">
      <div className="login-header ">
        <img id="img1" src={logo1} />

        <div className="login-header-text">
          المستودع الرئيسى لذخيرة المهندسين العسكريين
        </div>

        <img id="img2" src={logo2} />
      </div>
      <div className="login-body">
        <Card className="mb-0 text-right login-card">
          <CardBody>
            <CardText className="mb-2">من فضلك قم بتسجيل الدخول</CardText>
            <Form
              className="auth-login-form mt-2"
              onSubmit={handleSubmit(onSubmit)}
            >
              <FormGroup>
                <Label className="form-label" for="login-email">
                  Email
                </Label>
                <Input
                  autoFocus
                  type="text"
                  value={email}
                  id="login-email"
                  autoComplete="off"
                  name="login-email"
                  placeholder="john@example.com"
                  onChange={(e) => setEmail(e.target.value)}
                  className={classnames({
                    "is-invalid": errors["login-email"],
                  })}
                  innerRef={register({
                    required: true,
                    validate: (value) => value !== "",
                  })}
                />
              </FormGroup>
              <FormGroup>
                <div className="d-flex justify-content-between">
                  <Label className="form-label" for="login-password">
                    Password
                  </Label>
                  <Link to="/forgot-password">
                    <small>Forgot Password?</small>
                  </Link>
                </div>
                <InputPasswordToggle
                  value={password}
                  id="login-password"
                  name="login-password"
                  classNames="input-group-merge"
                  onChange={(e) => setPassword(e.target.value)}
                  className={classnames({
                    "is-invalid": errors["login-password"],
                  })}
                  innerRef={register({
                    required: true,
                    validate: (value) => value !== "",
                  })}
                />
              </FormGroup>
              <FormGroup>
                <hr />
                <CustomInput
                  type="checkbox"
                  className="custom-control-Primary"
                  id="remember-me"
                  label="Remember Me"
                />
              </FormGroup>
              <Button.Ripple type="submit" color="primary" block>
                Sign in
              </Button.Ripple>
            </Form>
          </CardBody>
        </Card>
      </div>
    </div>
  );
};

export default LoginV1;
