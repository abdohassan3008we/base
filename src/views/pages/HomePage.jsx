import { useState, useEffect } from "react"
import Table from "../customizedComponents/Table"
import BreadCrumbs from "../customizedComponents/breadcrumbs"
import apiClient from "../../utility/services/apiClient"

const data = [
  {
    id: 1,
    name: "مكتشف معادن أرضي",
    serial: "SER00135454",
    nsn: "2344-223",
    itemConditionName: "جديد"
  },
  {
    id: 2,
    name: "سلاح أرض جو",
    serial: "SER00198563",
    nsn: "9866-635",
    itemConditionName: "كهنة"
  },
  {
    id: 3,
    name: "لغم أرضي",
    serial: "SER96300056",
    nsn: "96223-31",
    itemConditionName: "جديد"
  }
]

const columns = [
  {
    name: "كود الصنف",
    selector: "nsn",
    sortable: true
  },
  {
    name: "اسم الصنف",
    selector: "name",
    sortable: true
  },
  {
    name: "السيريال",
    selector: "serial",
    sortable: true
  },
  {
    name: "حالة الصنف",
    selector: "itemConditionName"
  }
]

const products = [
  {
    name: "اسم الصنف",
    selector: "title"
  },
  {
    name: "سعر الصنف",
    cell: ({ price }) => `${price}$`
  },
  {
    name: "التقييم",
    selector: "rating"
  }
]

const searchFields = [
  "name",
  "nsn",
  "itemConditionName"
]

const breadcrumbs = [
  {
    name: "الاطلاع على المعدات",
    link: null
  }
]

const HomePage = () => {
  const [data, setData] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    apiClient
      .get("https://dummyjson.com/products")
      .then(({ data }) => {
        setData(data.products)
        setIsLoading(false)
      })
      .catch(() => { alert("ERROR!") })
  }, [])

  return (
    <>
      <BreadCrumbs list={breadcrumbs} />
      <Table
        data={data}
        columns={products}
        showAddBtn={false}
        searchFields={["title", "rating"]}
        progress={isLoading}
      />
    </>
  )
}
 
export default HomePage
