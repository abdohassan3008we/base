// ** Styles
// import "@styles/react/libs/tables/react-dataTable-component.scss";
import { param } from "jquery";
import {
  unEditable,
  readOnly,
  id,
  hide,
  enableEdit,
  enablePagination,
  enableDelete,
  Types,
  hideList,
  enableAdd,
  CreateScreens,
  StandardPages,
} from "../../customizedComponents/generic_page";
import tab1 from "./tabs/tab1";
const screens = CreateScreens({
  urlTitle: "UsgAsnafModels",
  editUrlTitle: "UsgAsnafModel/:id/edit",
  title: "UsgAsnafModel",
  list: { endPoint: ["/UsgAsnafSetup/GetAll"] },
  post: { endPoint: ["/UsgAsnafSetup/Post"] },
  get: { endPoint: ["/UsgAsnafSetup/GetById/"] },
  put: { endPoint: ["/UsgAsnafSetup/Put"], enableFormListening: true },
  del: { endPoint: ["/UsgAsnafSetup/Delete/"] },
  enableEdit,
  enablePagination,
  // list: { params: { active: true } },
  forms: [
    {
      fields: [
        {
          id,
          hide,
          selector: "asnafSetupId",
        },
        {
          hide,
          selector: "asnafId",
        },
        {
          name: "niin",
          selector: "niin",
          hide: true,
        },
        {
          name: " fsc  ",
          selector: "fsc",
          hide: true,
        },
        {
          readOnly: true,
          name: "fsc-niin",
          value: (row) => row.fsc && row.niin && `${row.fsc}-${row.niin}`,
          cell: (row) => row.fsc && row.niin && `${row.fsc}-${row.niin}`,
          minWidth: "100px",
        },
        {
          name: "Nomenclature",
          selector: "nomenclature",
          readOnly: true,
        },
      ],
    },
  ],

  tabs: [
    {
      num: 1,
      name: "Models",
      component: (params) => {
        const Tab = tab1({ ...params });
        return <Tab />;
      },
      //
      listeners: ["asnafId"],
    },
  ],
});

export default screens;
