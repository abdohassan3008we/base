import {
  popupPages,
  enableEdit,
  enableAdd,
  enablePagination,
  hide,
  Types,
  CreateMasterPageWithPopups,
  ErrorPopup,
  StandardPages,
} from "../../../customizedComponents/generic_page";
// import addPage from "./addPage";
export default (param) => {
  const { asnafId } = param;

  return CreateMasterPageWithPopups({
    popupPages,
    title: "",
    // editPageTitle: "تعديل بيانات UsgAsnafModel",
    // addPageTitle: "اضافة بيانات  UsgAsnafModel  ",
    enableAdd,
    enableEdit,
    enablePagination,
    enableDelete: true,
    list: {
      endPoint: ["/UsgAsnafModel/GetAllByAsnafId"],
      params: { MasterID: asnafId },
    },
    put: {
      // withId: true,
      endPoint: ["UsgAsnafModel/Put/"],
      enableFormListening: true,
    },

    post: { endPoint: ["/UsgAsnafModel/Post"] },
    get: { endPoint: ["/UsgAsnafModel/GetById/"] },
    del: { withId: true, endPoint: ["/UsgAsnafModel/Delete/"] },
    // putWithId: true,

    forms: [
      {
        fields: [
          {
            hide,
            selector: "asnafModelId",
            id: true,
          },
          {
            hide,
            selector: "asnafId",
            defaultValue: asnafId,
          },
          {
            // required: true,
            name: "ModelName",
            selector: "modelId",
            labelSelector: "modelName",
            type: Types.LIST,

            listProps: {
              listEndPoint: ["UsgItemModel/GetAll"],
              enablePagination,
              valueSelector: "modelId",
              labelSelector: "modelName",
            },
          },
        ],
      },
    ],
  });
};
