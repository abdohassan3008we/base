// ** Styles
// import "@styles/react/libs/tables/react-dataTable-component.scss";
import { param } from "jquery";
import {
  unEditable,
  readOnly,
  id,
  hide,
  enableEdit,
  enablePagination,
  enableDelete,
  Types,
  hideList,
  enableAdd,
  CreateScreens,
  StandardPages,
} from "../../customizedComponents/generic_page";
import { createPostRequest } from "../../../utility/services/requests";
import { Button } from "reactstrap";
const screens = CreateScreens({
  popupPages: true,
  urlTitle: "UsgAsnafModelsUnit",
  enableAdd,
  enableEdit,
  enablePagination,
  // enableDelete,
  list: {
    endPoint: ["/UsgAsnafModel/GetAllByAsnafId"],
  },
  put: {
    withId: true,
    endPoint: ["UsgAsnafModel/Put/"],
  },

  post: { endPoint: ["/UsgAsnafModel/Post"] },
  get: { endPoint: ["/UsgAsnafModel/GetById/"] },
  del: { endPoint: ["/UsgAsnafModel/Delete/"] },

  forms: [
    {
      fields: [
        {
          hide,
          selector: "asnafModelId",
          id: true,
        },

        {
          name: "ModelName",
          selector: "modelId",
          labelSelector: "modelName",
          type: Types.LIST,

          listProps: {
            listEndPoint: ["UsgItemModel/GetAll"],
            enablePagination,
            valueSelector: "modelId",
            labelSelector: "modelName",
          },
        },
      ],
    },
  ],
  pages: [
    {
      name: "Create Cards",
      btn: (params) => (
        <Button {...params} className="btn btn-primary">
          {params.title}
        </Button>
      ),
      onClick: () => {
        // createPostRequest({endPoint:"ExcuteProc/Fill_Asnaf_Serial",})()
        createPostRequest({ endPoint: "ExcuteProc/unitFillAsnaf" })();
      },
    },
  ],
});

export default screens;
