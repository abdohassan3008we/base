
export const Ref_Filter = (e, data, key, setSearchValue, setFilteredData) => {
    const value = e.target.value
    const Code = `${key}Code`
    const NameAr = `${key}NameAr`
    const NameEn = `${key}NameEn`
    const Desc = `${key}Desc`


    let updatedData = []
    setSearchValue(value)

    if (value.length) {
      updatedData = data.filter(item => {
        const startsWith =
          item[`${Code}`].toLowerCase().startsWith(value.toLowerCase()) ||
          item[`${NameAr}`].toLowerCase().startsWith(value.toLowerCase()) ||
          item[`${NameEn}`].toLowerCase().startsWith(value.toLowerCase()) ||
          item[`${Desc}`].toLowerCase().startsWith(value.toLowerCase()) 

        const includes =
          item[`${Code}`].toLowerCase().includes(value.toLowerCase()) ||
          item[`${NameAr}`].toLowerCase().includes(value.toLowerCase()) ||
          item[`${NameEn}`].toLowerCase().includes(value.toLowerCase()) ||
          item[`${Desc}`].toLowerCase().includes(value.toLowerCase()) 
        
        if (startsWith) {
          return startsWith
        } else if (!startsWith && includes) {
          return includes
        } else return null
      })
      setFilteredData(updatedData)
      setSearchValue(value)
    }
  }

export const getTableDetail = (key, name) => {
  const Code = `${key}Code`
    const NameAr = `${key}NameAr`
    const NameEn = `${key}NameEn`
    const Desc = `${key}Desc`

  return {
    title: ` شاشة ${name}`,
    addButtonTitle: `إضافة ${name} جديدة`,
    columns:[
/*       {
        name: "كود",
        selector: Code,
        sortable: true,
        minWidth: "150px"
      }, */
      {
        name: `${name} بالعربى`,
        selector: NameAr,
        sortable: true,
        minWidth: "200px"
      },
      {
        name: `${name} بالإنجليز`,
        selector: NameEn,
        sortable: true,
        minWidth: "200px"
      },
      {
        name: `وصف ${name}`,
        selector: Desc,
        sortable: true,
        minWidth: "200px"
      },
      {
        name: "الترتيب",
        selector: "sort",
        sortable: true,
        minWidth: "50px"
      }
       
    ]
  }
}
