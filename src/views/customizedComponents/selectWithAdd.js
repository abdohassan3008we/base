import { useEffect, useState } from "react"
import { components } from "react-select"
import { Button } from "reactstrap"
import { Plus } from "react-feather"
import Select from "./Select"

/**
 * Generates a dropdown select for the given data and a create new serial button
 *
 * @param {String} title
 * @param {Array} data
 * @param {String} defaultValue
 * @param {Function} onChange
 * @param {Function} handleModal
 * @param {Boolean} isLoading
 * @param {String} id HTML ID
 * @returns {JSX.Element}
 * @author AHMED ABO BAKR
 */
export default function SelectWithAdd({
    data,
    title,
    handleModal,
    defaultValue,
    id = '',
    onChanges=(_) => {},
    isLoading = false,
}) {
    const [options, setOptions] = useState([])
    const [selected, setSelected] = useState(null)

    useEffect(() => { setOptions(data) }, [data])

    useEffect(() => {
        setSelected(defaultValue)

    }, [defaultValue])


    /**
     * Custom select option component for react-select
     *
     * @param {[Object]} data array of data to be displayed as options
     * @returns {JSX.Element}
     */
    const OptionComponent = ({ data, ...props }) => {
        if (data.type === 'button') {
            return (
                <Button
                    className='text-left rounded-0 w-100'
                    outline
                    color='success'
                    onClick={_ => handleModal()}
                >
                    <Plus size={14} /><span className='align-middle ml-50'>{data.label}</span>
                </Button>
            );
        } else {
            return <components.Option {...props}>{data.label}</components.Option>;
        }
    } 
    return (
        <>
            <Select
               id={id}
                defaultValue={selected}
                options={[...options, { type: 'button', value: 'ADD_NEW', label: title }]}
                onChange={s => {
                    setSelected(s?.value)
                    onChanges(s || { value: 'abo', label: '' })
                }}
                isClearable
                isLoading={isLoading}
                customComponents={{
                    Option: OptionComponent
                }}
            />

        </>
    )
}
