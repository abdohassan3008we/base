import { RequestedDataPath as defaultURLPath } from "./configs";

export const mixinRequestBodyQuery = true; // all parameters passed in one object as query and body to request function
export const ltr = true; // to make the field direction from left to right
export const required = true; // to make the field must have data before add/edit

export const readOnly = true; // to make the column field read only (no add no edit)
export const unEditable = true; // to make the field un editable
export const activeInList = true; // to make the switch field active in list

export const hide = true; // to hide the field from entire page (table, add, edit and report)
export const hideList = true; // to hide the field from table
export const hideAdd = true; // to hide the field from add screen
export const hideEdit = true; // to hide the field from edit screen
export const hideShow = true; // to hide the field from show screen
export const hideExtract = true; // to hide the field from table in report

export const id = true; // the id field of the table

export const popupPages = true; // switch between popup and new page modes
export const editPopupPage = true; // switch between popup and new page modes
export const addPopupPage = true; // switch between popup and new page modes
export const showPopupPage = true; // switch between popup and new page modes

export const enableShow = true; // to enable the show form
export const enableEdit = true; // to enable the edit form
export const enableAdd = true; // to enable the add form
export const enableDelete = true; // to enable the delete button
export const enableSearch = true; // to enable the Search bar
export const enableExtract = true; // to enable the extract button

export const enablePrint = true; // to enable the printing button
export const enableAttachment = true; // to enable the attachment button
export const enableLogs = true; // to enable the logs button

export const enablePagination = true; // to enable the server side requestes table
export const enableActiveFilter = true; // to enable the filter switch
export const enableAddByFile = true; // to enable the add button to add multi records in file

export const enableFormListening = true; // enable listener to the changes in form data before edit request

export const lookupValueCode = "::??";

export const FormMode = {
  SHOW: "show",
  EDIT: "edit",
  ADD: "add",
};

export const Types = {
  MESSAGE: "message",
  ERROR_MESSAGE: "error message",
  WARNING_MESSAGE: "warning message",
  SUCCESS_MESSAGE: "success message",

  UPLOAD_IMAGE: "upload image",
  RADIAN_GROUP: "radian group",
  // RADIAN_BUTTON: "radian button",
  CHECKBOX_GROUP: "checkbox group",
  
  DATE_PICKER: "date picker",
  TIME_PICKER: "Time picker",
  DATE_TIME_PICKER: "date-time picker",
  COUNTER: "counter",
  TEXT: "text",
  NUMBER: "number",
  INTEGER: "integer",
  FLOAT: "float",
  EMAIL: "email",
  PASSWORD: "password",
  STRONG_PASSWORD: "strong password",
  NID: "nid",
  PHONE: "phone",
  AR_INPUT: "ar_input",
  EN_INPUT: "en_input",
  INPUT: "input",
  TEXTAREA: "textarea",
  LIST: "list",
  SWITCH: "switch",
};

export const pageURLs = ({ title = null, id = ":id" } = {}) => {
  title = ("/" + title + "/").replaceAll("//", "/");
  title = title.slice(-1) === "/" ? title.slice(0, -1) : title;
  return {
    master: `${title}`,
    add: `${title}/new`,
    edit: id ? `${title}/${id}/edit` : "",
    show: id ? `${title}/${id}` : "",
  };
};

export function getContentPath(endPointObj, defaultPath) {
  const endPoint =
    typeof endPointObj === "object" ? endPointObj[0] : endPointObj;

  const paths = endPointObj?.slice(1);
  const selectors =
    typeof endPointObj === "object"
      ? paths && paths.length
        ? paths
        : defaultURLPath
      : defaultPath;
  return [endPoint, selectors];
}

export { Validator, Listener, isDuplicatedObject } from "./classes";
export { CreateMasterPageWithPopups, CreateScreens } from "./factory";
export {
  Counter,
  DatePicker,
  DateTimePicker,
  Email,
  Float,
  InputText,
  Integer,
  List,
  NationalID,
  NumberText,
  Password,
  PhoneNumber,
  StrongPassword,
  Switch,
  Text,
  TextArea,
  TimePicker,
  DataSelect,
  ErrorPopup,
  SuccessPopup,
  BreadCrumbs,
  ConfirmCancelPopup,
  CustomModel,
  NewLine,
  PageTitle,
  PaginationTable,
  ScapeChar2XML,
  Space,
  Tab,
  Tabs,
  UploadFileModel,
  CheckBox,
  CustomDatePicker,
  InputPopup,
  RadianButton,
  RadianGroup,
  Table,
  ImageField,
  ValidationField,
  Message,
  ErrorMessage,
  SuccessMessage,
  WarningMessage,
  DropdownButton,
} from "./components";
export {
  AddNewForm,
  AddNewModel,
  AddPage,
  DataTable,
  EditModel,
  EditPage,
  ShowPage,
  PageContent,
  ShowModel,
  Forms,
  Form,
  PrintingModel,
  StandardPages,
} from "./screens";

export {
  RequestedDataPath,
  RequestedStartPageNo,
  ResponseData,
  ResponseTotalElements,
  RequestedPaginationParams,
} from "./configs";
