export const isDuplicatedObject = (obj1, obj2, keys) => {
  return (
    // isTheSameType(obj1, obj2) &&
    isObject(obj1) &&
    isObject(obj1) &&
    isTheSameKeys(obj1, obj2, keys) &&
    isTheSameValues(obj1, obj2)
  );
};
const isTheSameType = (obj1, obj2) => {
  return (
    (obj1 === undefined && obj2 === undefined) ||
    (obj1 === null && obj2 === null) ||
    typeof obj1 === typeof obj2
  );
};
const isObject = (obj) => {
  return obj != undefined && obj != null && typeof obj === "object";
};
const isTheSameValues = (obj1, obj2) => {
  const keys = Object.keys(obj1);
  return isTheSameValuesForKeys(obj1, obj2, keys);
};
const isTheSameValuesForKeys = (obj1, obj2, keys) => {
  const keysHaveDifferentValues = keys.filter((key) => obj1[key] != obj2[key]);
  return !keysHaveDifferentValues || keysHaveDifferentValues.length == 0;
};
const isTheSameKeys = (obj1, obj2, keys) => {
  if (!(keys && keys.length > 0)) {
    const keys1 = Object.keys(obj1).sort();
    const keys2 = Object.keys(obj2).sort();
    if (keys1.length != keys2.length) return false;
    keys = keys1;
  }
  const keysNotFound = keys.filter(
    (key) =>
      !Object.hasOwnProperty.call(obj1, key) ||
      !Object.hasOwnProperty.call(obj2, key)
  );
  return !keysNotFound || keysNotFound.length == 0;
};
