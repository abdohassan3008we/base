import {
  createPhoneRegex,
  createNidRegex,
  createEmailRegex,
  createNpiRegex,
  createPasswordRegex,
  createIntegerRegex,
} from "./regex";

export class Validator {
  REQUIRED_ERROR_MESSAGE = "هذا الحقل مطلوب";
  INVALID_ERROR_MESSAGE = "بيانات غير صحيحة";
  static VERY_WEEK = 5;
  static WEEK = 4;
  static ORDINARY = 3;
  static STRONG = 2;
  static VERY_STRONG = 1;
  static IMPOSSIBLE = 0;

  constructor(errorMessage) {
    this.errorMessage = errorMessage ?? this.INVALID_ERROR_MESSAGE;
    this.validateObj = {};
    this.isValid = () => true;
  }
  required(errorMessage) {
    this.validateObj = {
      ...this.validateObj,
      required: {
        value: true,
        errorMessage:
          errorMessage ?? this.REQUIRED_ERROR_MESSAGE ?? this.errorMessage,
      },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return isValid(data) && data?.toString()?.trim() != "";
    };
    return this;
  }
  pattern(pattern, errorMessage) {
    const regex = pattern;
    this.validateObj = {
      ...this.validateObj,
      pattern: {
        value: regex,
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return (
        isValid(data) &&
        data.match(regex) != null &&
        data.match(regex) != undefined
      );
    };
    return this;
  }
  maxLength(maxLength, errorMessage) {
    this.validateObj = {
      ...this.validateObj,
      maxLength: {
        value: maxLength,
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return isValid(data) && data?.toString()?.length <= maxLength;
    };
    return this;
  }
  minLength(minLength, errorMessage) {
    this.validateObj = {
      ...this.validateObj,
      minLength: {
        value: minLength,
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return isValid(data) && data?.toString()?.length >= minLength;
    };
    return this;
  }
  date(format, errorMessage) {
    this.validateObj = {
      ...this.validateObj,
      date: {
        format,
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    // const isValid = this.isValid;
    // this.isValid = (data) => {
    //   data = data??""
    //   return isValid(data) && dateFormat(data, format) === data;
    // };
    return this;
  }
  dateRange(format, start, end, errorMessage) {
    this.validateObj = {
      ...this.validateObj,
      dateRange: {
        format,
        start: { value: start },
        end: { value: end },
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    // const isValid = this.isValid;
    // this.isValid = (data) => {
    //   data = data??""
    //   return (
    //     isValid(data) &&
    //     dateFormat(data, format) === data &&
    //     new Date(start) <= new Date(data) <= new Date(end)
    //   );
    // };
    return this;
  }
  dateTime(errorMessage) {
    this.validateObj = {
      ...this.validateObj,
      datetime: {
        value: true,
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    // isValid
    return this;
  }

  phone(country, errorMessage) {
    const phoneRE = createPhoneRegex(country);
    this.validateObj = {
      ...this.validateObj,
      pattern: {
        value: phoneRE,
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return (
        isValid(data) &&
        data?.match(phoneRE) != null &&
        data?.match(phoneRE) != undefined
      );
    };
    return this;
  }
  email(errorMessage) {
    const emailRE = createEmailRegex();
    this.validateObj = {
      ...this.validateObj,
      pattern: {
        value: emailRE,
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return (
        isValid(data) &&
        data?.match(emailRE) != null &&
        data?.match(emailRE) != undefined
      );
    };
    return this;
  }
  password(strength, errorMessage) {
    const passwordRE = createPasswordRegex(strength ?? "");
    this.validateObj = {
      ...this.validateObj,
      pattern: {
        value: passwordRE,
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return (
        isValid(data) &&
        data?.match(passwordRE) != null &&
        data?.match(passwordRE) != undefined
      );
    };
    return this;
  }
  NID(country, errorMessage) {
    let niRE = createNidRegex(country);
    this.validateObj = {
      ...this.validateObj,
      pattern: {
        value: niRE,
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return (
        isValid(data) &&
        data?.match(niRE) != null &&
        data?.match(niRE) != undefined
      );
    };
    return this;
  }
  npi(errorMessage) {
    const npiRE = createNpiRegex();
    this.validateObj = {
      ...this.validateObj,
      npi: { value: true, errorMessage: errorMessage ?? this.errorMessage },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return (
        isValid(data) &&
        data.match(npiRE) != null &&
        data.match(npiRE) != undefined
      );
    };
    return this;
  }
  integer(errorMessage) {
    const integerRE = createIntegerRegex();
    this.validateObj = {
      ...this.validateObj,
      number: { value: true, errorMessage: errorMessage ?? this.errorMessage },
      pattern: {
        value: integerRE,
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return isValid(data) && !isNaN(parseInt(data));
    };
    return this;
  }
  number(errorMessage) {
    this.validateObj = {
      ...this.validateObj,
      number: { value: true, errorMessage: errorMessage ?? this.errorMessage },
      pattern: {
        value: "^[1-9]\\d*$",
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return isValid(data) && !isNaN(parseInt(data));
    };
    return this;
  }
  step(value) {
    this.validateObj = {
      ...this.validateObj,
      step: { value },
    };
    return this;
  }
  match(fieldName, errorMessage) {
    this.validateObj = {
      ...this.validateObj,
      match: {
        value: fieldName,
        errorMessage: errorMessage ?? this.errorMessage,
      },
    };
    // isValid
    return this;
  }
  max(value, errorMessage) {
    this.validateObj = {
      ...this.validateObj,
      max: { value, errorMessage: errorMessage ?? this.errorMessage },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return isValid(data) && data <= value;
    };
    return this;
  }
  min(value, errorMessage) {
    this.validateObj = {
      ...this.validateObj,
      min: { value: value, errorMessage: errorMessage ?? this.errorMessage },
    };
    const isValid = this.isValid;
    this.isValid = (data) => {
      data = data ?? "";
      return isValid(data) && data >= value;
    };
    return this;
  }
  custom(checkFunction, errorMessage) {
    // this.validateObj = {
    //   ...this.validateObj,
    //   async: {
    //     value: checkFunction,
    //     errorMessage: errorMessage ?? this.errorMessage,
    //   },
    // };
    // const isValid = this.isValid;
    // this.isValid = (data) => {
    //   data = data ?? "";
    //   return isValid(data) && checkFunction(data);
    // };
    return this;
  }

  get() {
    return [this.validateObj, this.isValid];
  }
  getIsValid() {
    return this.isValid;
  }
  isValid(data) {
    return this.isValid(data);
  }
  getValidate() {
    return this.validateObj;
  }
}
