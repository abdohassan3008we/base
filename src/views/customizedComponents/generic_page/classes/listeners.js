export class Listener {
  constructor({ values, listeners }) {
    this.values = values ?? {};
    this.listeners = listeners ?? [];
  }
  hasChanged({ values = {} }) {
    if (!this.listeners) return true;

    // const keys = Object.keys(values);
    return this.listeners
      ?.map((listener) => {
        const sel = this.getKeyContains({
          values: this.values,
          subKey: listener,
        });
        return this.values?.[sel] !== values?.[sel] ? "changed" : "unchanged";
      })
      .includes("changed");
  }
  update({ values, listeners }) {
    this.values = values ?? this.values;
    this.listeners = listeners ?? this.listeners;
  }

  getKeyContains = ({ values = {}, subKey }) => {
    let key;
    for (const k of Object.keys(values)) {
      if (k.includes(subKey)) {
        key = k;
        break;
      }
    }
    return key;
  };
}
