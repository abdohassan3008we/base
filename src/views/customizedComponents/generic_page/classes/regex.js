import { Validator } from "./validator";
function notRequiredMatch(pattern) {
  return "^(" + pattern + ")?$";
}
export function createPhoneRegex(country) {
  let phoneRE = "";
  switch (country) {
    default:
    case "eg":
    case "egy":
    case "egypt":
      phoneRE = "((\\+2)|(002))?(01)([0125])([0-9]){8}";
      break;
  }
  return notRequiredMatch(phoneRE);
}
export function createNidRegex(country) {
  let niRE = "";
  switch (country) {
    default:
    case "eg":
    case "egy":
    case "egypt":
      niRE =
        "(?<year>(2[0-9]{2})|(3[0-9]{2}))(?<month>(0[1-9])|(1[1-2]))(?<day>(0[1-9])|([12][0-9])|(3[01]))([0-9]{2})[0-9]{3}([0-9]{1})[0-9]{1}";
      break;
  }
  return notRequiredMatch(niRE);
}
export function createEmailRegex() {
  let emailRE = "\\w+([.-]?\\w+)*@\\w+([.-]?\\w+)*(\\.\\w{1,})+";
  return notRequiredMatch(emailRE);
}
export function createNpiRegex() {
  let npiRE = "\\d{10}";
  return notRequiredMatch(npiRE);
}
export function createIntegerRegex() {
  let integerRE = "\\d+";
  return notRequiredMatch(integerRE);
}
export function createPasswordRegex(strength) {
  let passwordRE = "";
  switch (strength) {
    case Validator.IMPOSSIBLE:
    case Validator.VERY_STRONG:
    case Validator.STRONG:
      passwordRE =
        "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\\s).+";
      break;
    default:
    case Validator.ORDINARY:
      break;
    case Validator.WEEK:
      passwordRE = ".{6,}";
      break;
    case Validator.VERY_WEEK:
      passwordRE = "\\d{4,}";
      break;
  }
  return notRequiredMatch(passwordRE);
}
