export { CreateSearchModel } from "./createModel";
export { searchModels } from "./savedSearchModelsConfigs";

export const STATUSES = {
  default: "edit",
  edit: "edit",
  invalid: "invalid",
  deleted: "deleted",
  submit: "submit",
  saved: "saved",
};
export const removeKeyPrefexFromObj = (obj, pref) => {
  const newObj = {};
  Object.keys(obj).map((key) => {
    newObj[key.replace(pref, "")] = obj[key];
  });
  return newObj;
};
export const addKeyPrefexToObj = (obj, pref) => {
  const newObj = {};
  Object.keys(obj).map((key) => {
    newObj[key + pref] = obj[key];
  });
  return newObj;
};
