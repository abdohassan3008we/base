import { removeKeyPrefexFromObj, STATUSES } from ".";
import { isDuplicatedObject } from "../../classes";

export const getInvalidItemsData = (itemsData, keys) => {
  const [ids, data] = getObjectKeysValues(itemsData);
  const messages = {};

  for (let i = 0; i < data.length - 1; i++) {
    for (let j = i + 1; j < data.length; j++) {
      const item1 = removeKeyPrefexFromObj(data[i], ids[i]);
      const item2 = removeKeyPrefexFromObj(data[j], ids[j]);
      if (isDuplicatedObject(item1, item2, keys)) {
        messages[ids[i]] = "عنصر مكرر";
        messages[ids[j]] = "عنصر مكرر";
      }
    }
  }
  //////////////////
  return messages;
};

const getObjectKeysValues = (dataObj) => {
  const ids = Object.keys(dataObj);
  const data = [];
  ids.map((id, index) => {
    data[index] = dataObj[id]?.formikValues ?? {};
  });
  return [ids, data];
};

export const setInvalidItems = (items, renderFunction) => {
  const invalidItems = getInvalidItemsData(items);

  if (!invalidItems || Object.keys(invalidItems).length === 0) {
    return false;
  } else {
    const newData = { ...items };
    Object.keys(invalidItems).map((itemId) => {
      newData[itemId].errorMessage = invalidItems[itemId];
      newData[itemId].status = STATUSES.invalid;
    });
    renderFunction((_) => newData);
    return true;
  }
};

export const resetInvalidItems = (items, renderFunction) => {
  const newData = { ...items };
  Object.keys(items)
    .filter((itemId) => items[itemId].status === STATUSES.invalid)
    .map((itemId) => {
      newData[itemId].status = STATUSES.default;
    });
  renderFunction((_) => newData);
};
