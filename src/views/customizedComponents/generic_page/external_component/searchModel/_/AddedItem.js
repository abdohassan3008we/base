// import { Trash2 } from "react-feather";
// import { Label, Row, Col, FormGroup, Input } from "reactstrap";
// import { useDispatch, useSelector } from "react-redux";
// import React, { useEffect, useState } from "react";

// import { Select } from "../../fieldTypes/dropDown/Select";
// import { getAdminSiteGrantRepairGroupRequestAsync } from "../../../../../../../redux/admin/siteGrant/thunk";
// import { NUMERICALLY } from "../../../../../../../redux/admin/catalogUi/unitsCodes";
// import { getItemLotSerialByCatalogItemIdRequestAsync } from "../../../../../../../redux/admin/itemLotSerial/thunk";
// import { listCatalogUiByCatalogItemIdRequestAsync } from "../../../../../../../redux/admin/catalogUi/thunk";

// export default function ___AddedItem({
//   item,
//   handleRemoval,
//   showRepairGroups = true,
// }) {
//   const [unitMenu, setUnitMenu] = useState([]);
//   const [logSerialMenu, setLotSerialMenu] = useState([]);
//   const dispatch = useDispatch();
//   const siteGrantStore = useSelector((state) => state.AdminSiteGrantReducer);
//   const itemConditionsStore = useSelector(
//     (state) => state.RefitemConditionReducer
//   );
//   const serialTypeStore = useSelector((state) => state.RefserialTypeReducer);
//   const options = logSerialMenu.map((item) => ({
//     value: item.itemLotSerialId,
//     label: item.itemLotSerialNum,
//   }));
//   const [disadble, setDisable] = useState([false]);

//   useEffect(() => {
//     // Save catalog UI items to component state because the next AddedItem would have
//     // different set of catalog UI items. So, accessing the store directly would
//     // change all the catalog UI items for all AddedItems with the last response.
//     dispatch(
//       listCatalogUiByCatalogItemIdRequestAsync(item.catalogItemId, setUnitMenu)
//     );
//     // Get items LOT and serials for the given catalog only
//     dispatch(
//       getItemLotSerialByCatalogItemIdRequestAsync(
//         item.catalogItemId,
//         setLotSerialMenu
//       )
//     );
//     dispatch(getAdminSiteGrantRepairGroupRequestAsync());
//     serialTypeStore.eagerLoadedData[item.serialTypeCode]?.serialTypeCode ==
//     "SER"
//       ? setDisable(true)
//       : setDisable(false);
//   }, []);

//   useEffect(() => {
//     siteGrantStore.repairGroups.forEach((s) => {
//       s.value = s.grantedSiteId;
//       s.label = s.grantedSiteName;
//     });
//   }, [siteGrantStore.repairGroups]);

//   return (
//     <>
//       <Row key={item.catalogItemId} style={{ position: "relative" }}>
//         <Trash2
//           dataId={item.htmlId}
//           className="cursor-pointer text-danger"
//           size={15}
//           style={{ position: "absolute", top: 0, left: "1em", zIndex: 9999 }}
//           onClick={(e) => {
//             // User may click on the child element to this svg which has the data ID.
//             // So, we pass the dataId from the parent if clicked on child element.
//             const elem =
//               e.target.tagName === "svg" ? e.target : e.target.parentElement;
//             handleRemoval(elem.getAttribute("dataId"));
//           }}
//         />
//         <Col md={3}>
//           <FormGroup>
//             <Label for="niinCode">كود الصنف</Label>
//             <Input
//               id="niinCode"
//               defaultValue={
//                 item.fscCode ? `${item.fscCode}-${item.niinCode}` : ""
//               }
//               disabled
//             />
//           </FormGroup>
//         </Col>
//         <Col md={3}>
//           <FormGroup>
//             <Label for="partNumber">رقم العينة</Label>
//             <Input id="partNumber" defaultValue={item.partNumber} disabled />
//           </FormGroup>
//         </Col>
//         <Col md={3}>
//           <FormGroup>
//             <Label for="itemNameAr">اسم الصنف</Label>
//             <Input id="itemNameAr" defaultValue={item.itemNameAr} disabled />
//           </FormGroup>
//         </Col>
//         {/* <Col md={3}>
//           <FormGroup>
//             <Label for="serialTypeCode">نوع السيريال</Label>
//             <Input
//               id="serialTypeCode"
//               defaultValue={serialTypeStore.eagerLoadedData[item.serialTypeCode]?.serialTypeNameAr}
//               disabled
//             />
//           </FormGroup>
//         </Col> */}
//       </Row>
//       <Row>
//         <Col md={3}>
//           <FormGroup>
//             <Label for={`catalogUiId-${item.htmlId}`}>وحدة الصرف</Label>
//             <Input id={`catalogUiId-${item.htmlId}`} type="select">
//               <option selected disabled></option>
//               {unitMenu.map((catalog) => (
//                 <option
//                   value={catalog.catalogUiId}
//                   selected={catalog.itemUiCode === NUMERICALLY}
//                 >
//                   {catalog.itemUiName}
//                 </option>
//               ))}
//             </Input>
//           </FormGroup>
//         </Col>
//         <Col md={3}>
//           <FormGroup>
//             <Label for={`itemQty-${item.htmlId}`}>الكمية</Label>
//             <Input
//               id={`itemQty-${item.htmlId}`}
//               defaultValue={1}
//               disabled={disadble}
//             />
//           </FormGroup>
//         </Col>
//         {/* <Col md={3}>
//           <FormGroup>
//             <Label for={`itemLotSerialId-${item.htmlId}`}>LOT/SERIAL</Label>
//             <Input
//               type='hidden'
//               id={`itemLotSerialId-${item.htmlId}`}
//             />
//             <SerialsDropDown
//               catalogItemId={item.catalogItemId}
//               serials={options}
//               serialTypeCode={item.serialTypeCode}
//               defaultValue={document.querySelector(`#itemLotSerialId-${item.htmlId}`)?.value}
//               onChange={({ value }) => {
//                 document.querySelector(`#itemLotSerialId-${item.htmlId}`).value = value
//               }}
//             />

//           </FormGroup>
//         </Col> */}

//         <Col md={3}>
//           <FormGroup>
//             <Label for="repairGroupId">مجموعة الاصلاح</Label>
//             <Input
//               type="hidden"
//               id={`repairGroupId-${item.html}`}
//               defaultValue={null}
//             ></Input>
//             <Input
//               type="hidden"
//               id={`repairGroupName-${item.html}`}
//               defaultValue={null}
//             ></Input>
//             <Select
//               id="repairGroupId"
//               options={siteGrantStore.repairGroups}
//               onChange={(data) => {
//                 document.querySelector(`#repairGroupId-${item.html}`).value =
//                   data?.value;
//                 document.querySelector(`#repairGroupName-${item.html}`).value =
//                   data?.label;
//               }}
//             />
//           </FormGroup>
//         </Col>
//       </Row>
//       <hr />
//     </>
//   );
// }
