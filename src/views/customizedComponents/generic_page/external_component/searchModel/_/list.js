// import { PlusSquare } from "react-feather";
// import { useSelector } from "react-redux";
// import { Card, CardBody } from "reactstrap";
// import { randomString } from "../../../../../../../utility/Utils";
// import {Table} from "../tables/table/Table";

// export default function ___List({ setSelectedItems, data, hiddenCols = [] }) {
//   const store = useSelector((state) => state.AdminCatalogReducer);
//   const itemTypesStore = useSelector((state) => state.RefitemTypeReducer);

//   /**
//    * Table columns configurations
//    *
//    * @param {[Object]}
//    */
//   const columns = [
//     {
//       name: "",
//       width: "5em",
//       cell: (row) => (
//         <div>
//           <PlusSquare
//             onClick={(_) => handleSelection(row)}
//             size={22}
//             className="mr-50"
//             style={{ cursor: "pointer" }}
//           />
//         </div>
//       ),
//     },
//     /*     {
//       name: 'م',
//       selector: 'catalogItemId',
//       sortable: true,
//       width: "5em",
//     }, */
//     {
//       name: "fsc-niin",
//       cell: (row) =>
//         row.fscCode && row.niinCode && `${row.fscCode}-${row.niinCode}`,
//       minWidth: "100px",
//     },
//     {
//       name: "رقم العينة",
//       selector: "partNumber",
//       sortable: true,
//       minWidth: "100px",
//     },
//     {
//       name: "اسم الصنف",
//       selector: "itemNameAr",
//       sortable: true,
//       minWidth: "180px",
//     },
//     {
//       name: "نوع الصنف",
//       cell: (row) =>
//         itemTypesStore.eagerLoadedData[row.itemTypeCode]?.itemTypeNameAr,
//       minWidth: "100px",
//     },
//     {
//       name: "نوع السيريال",
//       selector: "serialTypeName",
//       minWidth: "120px",
//     },
//     {
//       name: "درجة الخطورة",
//       selector: "itemHmcName",
//       minWidth: "100px",
//     },
//     {
//       name: "العمر المخزني",
//       selector: "itemSlcName",
//       minWidth: "100px",
//     },
//   ];

//   /**
//    * Handle equipment selection
//    *
//    * @param {Object} row
//    */
//   const handleSelection = (row) => {
//     setSelectedItems((prevItems) => [
//       ...prevItems,
//       { ...row, htmlId: randomString() },
//     ]);
//   };

//   return (
//     <div className="app-user-list">
//       <Card>
//         <CardBody>
//           <Table
//             columns={columns.filter(
//               (col) => !hiddenCols.includes(col.selector)
//             )}
//             tableTitle={"اضافة معدة جديدة"}
//             data={data}
//             progress={store.isLoading}
//             searchFields={[]}
//             showAddBtn={false}
//             showSearch={false}
//             showExtractBtn={false}
//           />
//         </CardBody>
//       </Card>
//     </div>
//   );
// }
