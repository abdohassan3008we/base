// import {
//   Button,
//   Card,
//   CardBody,
//   Col,
//   Form,
//   FormGroup,
//   Input,
//   Label,
//   Row
// } from 'reactstrap';
// import { useFormik } from 'formik';
// import { useDispatch } from 'react-redux';
// import Modal from '../../../../../customizedComponents/Modal';
// import DatePicker from '../../../../../customizedComponents/DatePicker';
// import { addItemLotSerialdRequestAsync } from '../../../../../../redux/admin/itemLotSerial/thunk';
// import { useEffect } from 'react';

// export default function ___AddNewSerialModal({ open, handleModal, refresh, catalogItemId }) {
//   const dispatch = useDispatch()
//   const formik = useFormik({
//     initialValues: {
//       active: true,
//       catalogItemId: catalogItemId,
//       expireDate: null,
//       lotSerialNum: null,
//       productionDate: null,
//       remarks: null,
//     },
//     enableReinitialize: true,
//     onSubmit: values => {
//       dispatch(addItemLotSerialdRequestAsync({
//         values: values,
//         onSuccess: ({ itemLotSerialId, lotSerialNum }) => {
//           handleModal()
//           refresh(itemLotSerialId, lotSerialNum)
//         },
//         onFailure: _ => { handleModal() },
//       }))
//       formik.resetForm()
//     },
//   })

//   // Reset form on unmount
//   useEffect(() => formik.resetForm, [])

//   const modalContent = (
//     <Form onSubmit={formik.handleSubmit}>
//       <Card>
//         <CardBody>
//           <Row>
//             <Col md={4}>
//               <FormGroup>
//                 <Label for='productionDate'>تاريخ الانتاج</Label>
//                 <DatePicker
//                   name='productionDate'
//                   onChange={date => formik.setFieldValue('productionDate', date)}
//                 />
//               </FormGroup>
//             </Col>
//             <Col md={4}>
//               <FormGroup>
//                 <Label for='expireDate'>تاريخ انتهاء العمر المخزنى</Label>
//                 <DatePicker
//                   name='expireDate'
//                   onChange={date => formik.setFieldValue('expireDate', date)}
//                 />
//               </FormGroup>
//             </Col>
//             {/* <Col md={4}>
//               <FormGroup>
//                 <Label for='lotSerialNum'>السيريال</Label>
//                 <Input
//                   className='form-control'
//                   id='lotSerialNum'
//                   { ...formik.getFieldProps('lotSerialNum') }
//                 />
//               </FormGroup>
//             </Col> */}
//           </Row>
//         </CardBody>
//       </Card>
//       <Row>
//         <Col md={6} style={{ textAlign: 'left' }}>
//           <Button type='submit' color='primary'>حفظ</Button>
//         </Col>
//         <Col md={6}>
//           <Button type='button' color='secondary' onClick={handleModal}>إلغاء</Button>
//         </Col>
//       </Row>
//     </Form>
//   )

//   return (
//     <Modal
//       open={open}
//       title='أضف سيريال جديد'
//       handleModal={handleModal}
//       body={modalContent}
//     />
//   )
// }
