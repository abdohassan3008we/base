import { useFormik } from "formik";
import { Card, Button } from "reactstrap";
import { ignoreDropDownKeys } from "../../screens/pageContent";
import { Forms } from "../../screens/pageContent/Forms";
import { useTranslation } from "react-i18next";

export const SearchSection = ({
  setData,
  fields,
  searchRequest,
  listRequest,
}) => {
  const { t } = useTranslation();
  const formik = useFormik({
    initialValues: {
      ...initFormikValues(fields),
    },
    onSubmit: (values) => {
      values = ignoreDropDownKeys(values);
      listRequest({
        params: {
          ...searchRequest.params,
          ...values,
          pageSize: 5,
        },
        setData: (data) => {
          setData(data);
        },
      });
    },
  });
  return (
    <>
      <Card>
        <Forms parameters={{ forms: [{ fields }] }} formik={formik} />
        <div
          style={{
            paddingBottom: "20px",
            paddingLeft: "50px",
            textAlign: "left",
          }}
        >
          <Button.Ripple onClick={formik.handleSubmit} color="primary">
            {t("Search")}
          </Button.Ripple>
        </div>
      </Card>
    </>
  );
};

const initFormikValues = (fields) => {
  const initValues = {};
  fields
    .filter((f) => f.selector)
    .map((f) => (initValues[f.selector] = f.value ?? f.defaultValue ?? null));
  return initValues;
};
