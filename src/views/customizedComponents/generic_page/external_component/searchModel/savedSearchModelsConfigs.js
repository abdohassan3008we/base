import {
  SuccessPopup,
  ErrorPopup,
  Types,
  Validator,
  lookupValueCode,
} from "../..";
import apiClient from "../../../../../utility/services/apiClient";

export const searchModels = {
  F3AT_EQUIPMENT: ({ saveRequestBody }) => ({
    title: "إضافة معدة جديدة",
    enablePagination: false,
    enableAdd: false,
    // singleItem:false,

    maxWidth: "80%",
    searchRequest: { endPoint: "/adm-api/AdmCatalog/searchCatalogItem/" },
    addRequest: { endPoint: "" },
    saveRequest: {
      endPoint: "/f3at-api/form3atDetailO/",
      params: { personSiteId: apiClient.getUserData().admUserItem?.jobSiteId },
      onSuccess: () => {
        SuccessPopup("تم إضافة العناصر بنجاح");
      },
      onFailure: ({ data }) => {
        ErrorPopup(data.responseMessage ?? data.responseCode);
      },
      body: saveRequestBody,
    },

    searchFields: [
      {
        name: "تصنيف الصنف",
        selector: "fscCode",
        type: Types.LIST,
        listProps: {
          params: { active: true },
          listEndPoint: "ref-api/itemFsc/",
          valueSelector: "itemFscCode",
          labelSelector: (item) => `${item.itemFscCode}_${item.itemFscNameAr}`,
        },
      },
      {
        name: "كود الصنف",
        selector: "niinCode",
      },
      {
        name: "نوع الصنف",
        selector: "itemTypeCode",
        type: Types.LIST,
        listProps: {
          params: { active: true },
          listEndPoint: "ref-api/itemType/",
          valueSelector: "itemTypeCode",
          labelSelector: "itemTypeNameAr",
        },
      },
      {
        fieldWidth: 2 / 3,
        name: "إسم الصنف",
        selector: "itemNameAr",
      },
      {
        name: "رقم العينة",
        selector: "partNum",
        type: Types.NUMBER,
      },
    ],
    // the parameters appears in the table
    listFields: [
      {
        selector: "fscCode",
        hide: true,
      },
      {
        selector: "niinCode",
        hide: true,
      },
      {
        readOnly: true,
        name: "fsc-niin",
        value: (row) =>
          row.fscCode && row.niinCode && `${row.fscCode}-${row.niinCode}`,
        cell: (row) =>
          row.fscCode && row.niinCode && `${row.fscCode}-${row.niinCode}`,
        minWidth: "100px",
      },
      {
        name: "رقم العينة",
        selector: "partNumber",
        sortable: true,
        minWidth: "100px",
      },
      {
        name: "اسم الصنف",
        selector: "itemNameAr",
        sortable: true,
        minWidth: "180px",
        // hideForm: true, // hide the field from added item form (donot post its value)
      },
      {
        name: "نوع الصنف",
        selector: "itemTypeName",
        minWidth: "100px",
      },
      {
        name: "نوع السيريال",
        selector: "serialTypeName",
        minWidth: "120px",
      },
      {
        name: "درجة الخطورة",
        selector: "itemHmcName",
        minWidth: "100px",
      },
      {
        name: "العمر المخزني",
        selector: "itemSlcName",
        minWidth: "100px",
      },
    ],
    // to add new fields in the form of added item
    itemFields: [
      {
        fieldWidth: 0.25,
        name: "الكمية",
        selector: "itemQty",
        defaultValue: 1,
      },
      {
        fieldWidth: 0.25,
        name: "تنشيط",
        selector: "active",
        minWidth: "100px",
        type: Types.SWITCH,
      },
      {}, // {
      //   // name:"",
      //   selector: "itemConditionCode",
      // },
    ],
    // to modify exists parameter name
    modifiedSelectors: {
      itemNameAr: "itemName",
    },
  }),
  PER_REC_EQUIPMENT: ({ saveRequestBody }) => ({
    title: "إضافة معدة جديدة",
    enablePagination: false,
    enableAdd: false,
    // singleItem:false,

    maxWidth: "80%",
    searchRequest: { endPoint: "/adm-api/AdmCatalog/searchCatalogItem/" },
    addRequest: { endPoint: "" },
    saveRequest: {
      endPoint: "/permission-receive-api/PermissionReceiveSampleDetail/",
      params: { personSiteId: apiClient.getUserData().admUserItem?.jobSiteId },
      onSuccess: () => {
        SuccessPopup("تم إضافة العناصر بنجاح");
      },
      onFailure: ({ data }) => {
        ErrorPopup(data.responseMessage ?? data.responseCode);
      },
      body: saveRequestBody,
    },

    searchFields: [
      {
        name: "تصنيف الصنف",
        selector: "fscCode",
        type: Types.LIST,
        listProps: {
          params: { active: true },
          listEndPoint: "ref-api/itemFsc/",
          valueSelector: "itemFscCode",
          labelSelector: (item) => `${item.itemFscCode}_${item.itemFscNameAr}`,
        },
      },
      {
        name: "كود الصنف",
        selector: "niinCode",
      },
      {
        name: "نوع الصنف",
        selector: "itemTypeCode",
        type: Types.LIST,
        listProps: {
          params: { active: true },
          listEndPoint: "ref-api/itemType/",
          valueSelector: "itemTypeCode",
          labelSelector: "itemTypeNameAr",
        },
      },
      {
        fieldWidth: 2 / 3,
        name: "إسم الصنف",
        selector: "itemNameAr",
      },
      {
        name: "رقم العينة",
        selector: "partNum",
        type: Types.NUMBER,
      },
    ],
    // the parameters appears in the table
    listFields: [
      {
        selector: "fscCode",
        hide: true,
      },
      {
        selector: "niinCode",
        hide: true,
      },
      {
        readOnly: true,
        name: "fsc-niin",
        value: (row) =>
          row.fscCode && row.niinCode && `${row.fscCode}-${row.niinCode}`,
        cell: (row) =>
          row.fscCode && row.niinCode && `${row.fscCode}-${row.niinCode}`,
        minWidth: "100px",
      },
      {
        readOnly: true,
        name: "رقم العينة",
        selector: "partNumber",
        sortable: true,
        minWidth: "100px",
      },
      {
        readOnly: true,
        name: "اسم الصنف",
        selector: "itemNameAr",
        sortable: true,
        minWidth: "180px",
      },
      {
        readOnly: true,
        name: "نوع الصنف",
        selector: "itemTypeName",
        minWidth: "100px",
      },
      {
        readOnly: true,
        name: "نوع السيريال",
        selector: "serialTypeName",
        minWidth: "120px",
      },
      {
        readOnly: true,
        name: "درجة الخطورة",
        selector: "itemHmcName",
        minWidth: "100px",
      },
      {
        readOnly: true,
        name: "العمر المخزني",
        selector: "itemSlcName",
        minWidth: "100px",
      },
      {
        selector: "catalogItemId",
        hide: true,
      },
      {
        selector: "catalogUiId",
        hide: true,
      },
      {
        selector: "itemConditionCode",
        hide: true,
      },
      {
        selector: "itemConditionName",
        hide: true,
      },
      {
        selector: "ohdaId",
        hide: true,
      },
      {
        selector: "ohdaName",
        hide: true,
      },
      {
        selector: "storeSiteId",
        hide: true,
      },
      {
        selector: "storeSiteName",
        hide: true,
      },
    ],
    // to add new fields in the form of added item
    itemFields: [
      {
        name: "الكمية",
        selector: "itemQty",
        type: Types.INTEGER,
        defaultValue: 1,
      },

      {
        name: "تنشيط",
        selector: "active",
        minWidth: "100px",
        type: Types.SWITCH,
      },
      {
        name: "وحدة الصرف",
        selector: "catalogUiId",
        labelSelector: "itemUiName",
        type: Types.LIST,
        lookup: {
          catalogItemId: {
            listProps: {
              listEndPoint:
                "/adm-api/AdmCatalogUi/getUiByCatalogItemId?catalogItemId=" +
                lookupValueCode,
              valueSelector: "catalogUiId",
              labelSelector: "itemUiName",
            },
          },
        },
      },
      {
        name: "حالة الصنف ",
        selector: "itemConditionCode",
        labelSelector: "itemConditionName",
        type: Types.LIST,
        listProps: {
          listEndPoint: "ref-api/itemCondition/?active=true",
          valueSelector: "itemConditionCode",
          labelSelector: "itemConditionNameAr",
        },
      },
      {
        name: "المجموعة المخزنية",
        selector: "ohdaSiteId",
        labelSelector: "ohdaSiteName",
        type: Types.LIST,
        listProps: {
          listEndPoint: "/adm-api/AdmSite/listBySiteType?siteTypeCode=STRGRP",
          valueSelector: "siteId",
          labelSelector: "siteNameAr",
        },
      },
      {
        name: "مخزن الصرف",
        selector: "storeSiteId",
        labelSelector: "storeSiteName",
        type: Types.LIST,

        lookup: {
          ohdaSiteId: {
            listProps: {
              listEndPoint:
                "adm-api/AdmSite/getByParentSiteId?parentsiteId=" +
                lookupValueCode,
              valueSelector: "siteId",
              labelSelector: "siteNameAr",
            },
          },
        },
      },
      {
        name: "العهدة",
        selector: "ohdaId",
        labelSelector: "ohdaName",
        type: Types.LIST,
        lookup: {
          ohdaSiteId: {
            listProps: {
              listEndPoint:
                "/adm-api/AdmOhda/getByOhdaSiteId?OhdaSiteId=" +
                lookupValueCode,
              valueSelector: "ohdaId",
              labelSelector: "ohdaNameAr",
            },
          },
        },
      },
      {},
      // { fieldWidth: 1 / 3 },
      // {
      //   // name:"",
      //   selector: "itemConditionCode",
      // },
    ],
    // to modify exists parameter name
    modifiedSelectors: {
      itemNameAr: "itemName",
    },
  }),
  PAGES: () => ({
    title: "إضافة صفحة جديدة",
    enablePagination: false,
    enableAdd: false,
    // singleItem:false,

    maxWidth: "80%",
    searchRequest: { endPoint: "/adm-api/AdmCatalog/searchCatalogItem/" },
    addRequest: { endPoint: "" },
    saveRequest: {
      endPoint: "adm-api/AdmOhdaPage/createOhdaPage",
      // params: { personSiteId: apiClient.getUserData().admUserItem?.jobSiteId },
      onSuccess: () => {
        SuccessPopup("تم إضافة العناصر بنجاح");
      },
      onFailure: ({ data }) => {
        ErrorPopup(data.responseMessage ?? data.responseCode);
      },
    },

    searchFields: [
      {
        name: "تصنيف الصنف",
        selector: "fscCode",
        type: Types.LIST,
        listProps: {
          params: { active: true },
          listEndPoint: "ref-api/itemFsc/",
          valueSelector: "itemFscCode",
          labelSelector: (item) => `${item.itemFscCode}_${item.itemFscNameAr}`,
        },
      },
      {
        name: "كود الصنف",
        selector: "niinCode",
      },
      {
        name: "نوع الصنف",
        selector: "itemTypeCode",
        type: Types.LIST,
        listProps: {
          params: { active: true },
          listEndPoint: "ref-api/itemType/",
          valueSelector: "itemTypeCode",
          labelSelector: "itemTypeNameAr",
        },
      },
      {
        fieldWidth: 2 / 3,
        name: "إسم الصنف",
        selector: "itemNameAr",
      },
      {
        name: "رقم العينة",
        selector: "partNum",
        type: Types.NUMBER,
      },
    ],
    // the parameters appears in the table
    listFields: [
      {
        hide: true,
        selector: "fscCode",
      },
      {
        hide: true,
        selector: "niinCode",
      },
      {
        sort: -1,
        readOnly: true,
        name: "fsc-niin",
        value: (row) =>
          row.fscCode && row.niinCode && `${row.fscCode}-${row.niinCode}`,
        cell: (row) =>
          row.fscCode && row.niinCode && `${row.fscCode}-${row.niinCode}`,
        minWidth: "100px",
      },
      {
        hide: true,
        selector: "cageCode",
      },
      { hide: true, selector: "catalogItemId" },

      {
        sort: -1,
        readOnly: true,
        name: "رقم العينة",
        selector: "partNumber",
        sortable: true,
        minWidth: "100px",
      },
      {
        readOnly: true,
        name: "اسم الصنف",
        selector: "itemNameAr",
        sortable: true,
        minWidth: "180px",
        // hideForm: true, // hide the field from added item form (donot post its value)
      },
      {
        readOnly: true,
        name: "نوع الصنف",
        selector: "itemTypeName",
        minWidth: "100px",
      },
      // {
      //   name: "نوع السيريال",
      //   selector: "serialTypeName",
      //   minWidth: "120px",
      // },
      {
        hideForm: true,
        name: "درجة الخطورة",
        selector: "itemHmcName",
        minWidth: "100px",
      },
      {
        hideForm: true,
        name: "العمر المخزني",
        selector: "itemSlcName",
        minWidth: "100px",
      },
    ],

    itemFields: [
      {
        sort: -1,
        name: "نشط",
        selector: "active",
        type: Types.SWITCH,
        defaultValue: true,
      },
    ],
    // modifiedSelectors: (item) => ({
    //   itemName: item.itemNameAr,
    // }),
  }),
  INVENTORY_ITEMS: ({ ohdaUnitId, catalogItemId }) => ({
    title: "إضافة صنف جديد",
    enablePagination: true,
    enableAdd: false,
    // singleItem:false,

    maxWidth: "80%",
    searchRequest: {
      endPoint: ["/inventory-api/inventoryLocation/searchInventoryForIssue"],
      params: { ohdaUnitId },
    },
    addRequest: { endPoint: "" },
    saveRequest: {
      endPoint: "/f1at-api/form1atDetail/",
      params: { personSiteId: apiClient.getUserData().admUserItem?.jobSiteId },
      onSuccess: () => {
        SuccessPopup("تم إضافة العناصر بنجاح");
      },
      onFailure: ({ data }) => {
        ErrorPopup(data.responseMessage ?? data.responseCode);
      },
    },
    searchFields: [
      {
        name: "تصنيف الصنف",
        selector: "fscCode",
        labelSelector: "fscName",
        type: Types.LIST,
        listProps: {
          params: { active: true },
          listEndPoint: "ref-api/itemFsc/",
          valueSelector: "itemFscCode",
          labelSelector: (item) => `${item.itemFscCode}_${item.itemFscNameAr}`,
        },
      },
      {
        name: "كود الصنف",
        selector: "niinCode",
        labelSelector: "niinName",
      },
      {
        name: "نوع الصنف",
        selector: "itemTypeCode",
        labelSelector: "itemTypeName",
        type: Types.LIST,
        listProps: {
          params: { active: true },
          listEndPoint: "ref-api/itemType/",
          valueSelector: "itemTypeCode",
          labelSelector: "itemTypeNameAr",
        },
      },
      {
        name: "إسم الصنف",
        selector: "itemNameAr",
      },
      {
        name: "رقم العينة",
        selector: "partNumber",
        type: Types.NUMBER,
      },
      {
        name: "الحالة",
        selector: "itemConditionCode",
        labelSelector: "itemConditionName",
        type: Types.LIST,
        listProps: {
          params: { active: true },
          listEndPoint: "ref-api/itemCondition/",
          valueSelector: "itemConditionCode",
          labelSelector: "itemConditionNameAr",
        },
      },
    ],
    listFields: [
      {
        hide: true,
        selector: "fscCode",
      },
      {
        hide: true,
        selector: "niinCode",
      },
      {
        readOnly: true,
        name: "fsc-niin",
        value: (row) =>
          row.fscCode && row.niinCode && `${row.fscCode}-${row.niinCode}`,
        cell: (row) =>
          row.fscCode && row.niinCode && `${row.fscCode}-${row.niinCode}`,
        minWidth: "100px",
      },
      {
        name: "رقم العينة",
        selector: "partNumber",
        readOnly: true,
      },
      {
        name: "إسم الصنف",
        selector: "itemNameAr",
        readOnly: true,
      },
      {
        name: "مجموعة الصرف",
        selector: "groupSiteName",
        readOnly: true,
      },
      {
        name: "حالة الصنف",
        selector: "itemConditionCode",
        labelSelector: "itemConditionName",
        type: Types.LIST,
        listProps: { listEndPoint: "", labelSelector: "", valueSelector: "" },
      },
      {
        name: "العهدة",
        selector: "ohdaName",
        readOnly: true,
      },
      {
        name: "الكمية المتاحة",
        selector: "availableQty",
        readOnly: true,
      },
      {
        name: "وحدة الصرف المتاحة",
        selector: "itemUiName",
        // labelSelector: "itemUiName",
        readOnly: true,
      },
      {
        name: "الكمية المطلوبة",
        selector: "requestedQty",
        type: Types.NUMBER,
        lookup: {
          availableQty: {
            validation: ({ availableQty }) =>
              new Validator()
                .min(1, "يجب ان تكون الكمية المطلوبة أكبر من صفر")
                .max(
                  parseInt(availableQty),
                  "يجب ان تكون الكمية المطلوبة  اصغر من او تساوي الكمية المتاحة"
                ),
          },
        },
      },
      {
        name: "وحدة الصرف المطلوبة",
        selector: "requestedUiId",
        labelSelector: "requestedUiName",
        type: Types.LIST,
        listProps: {
          params: { catalogItemId: catalogItemId },
          listEndPoint: "/adm-api/AdmCatalogUi/getUiByCatalogItemId",
          valueSelector: "catalogUiId",
          labelSelector: "itemUiName",
          // dataAction: (data) => {
          //   const filteredData = [];
          //   data.map((item) => {
          //     if (!filteredData.find((d) => d.label === item.label))
          //       filteredData.push({ label: item.label, value: item.value });
          //   });
          //   return filteredData;
          // },
        },
      },

      {},
    ],
  }),
};
