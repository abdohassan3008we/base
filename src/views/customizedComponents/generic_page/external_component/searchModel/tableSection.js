import { PaginationTable, Table } from "../..";

export const TableSection = ({
  key,
  enableAdd,
  enablePagination,
  columns,
  data,
  totalRows,
  onPagination,
}) => {
  return (
    <>
      {enablePagination ? (
        <PaginationTable // TODO test the server side table
          columns={columns}
          enableAdd={enableAdd}
          // tableTitle,
          // columns,
          // data,
          // extractFunction,
          // onPagination = () => {},

          // isLoading = false,

          // enableAdd = true,
          // addButtonText = "إضافة",
          // handleAddButton = () => {},
          // addBtnUrl,

          // enableSearch = true,
          // enableExtraction = true,
          // extractionTableHeaders,

          refresherKey={key}
          data={data}
          totalRows={totalRows}
          onPagination={onPagination}
        />
      ) : (
        <Table
          key={key}
          showAddBtn={enableAdd}
          columns={columns}
          data={data}
          searchFields={null}
        />
      )}
    </>
  );
};
