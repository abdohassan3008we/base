import { useEffect } from "react";
import { Card, CardBody, CardHeader, Label, Row } from "reactstrap";
import { Trash2 } from "react-feather";
import { STATUSES } from ".";
import { useFormik } from "formik";
import { createPostRequest } from "../../../../../utility/services/requests";
import { resetInvalidItems } from "./itemValidation";
import { FormMode, Forms } from "../..";
import { ignoreDropDownKeys } from "../../screens/pageContent";

export const ItemsSection = ({
  selectedItems,
  // setSelectedItems,
  selectedItemsData,
  setSelectedItemsData,
  fields,
  saveRequest,
}) => {
  const handleRemoval = (id) => {
    //  ** reset inavalid for all
    resetInvalidItems(selectedItems, setSelectedItemsData);
    ////////////////////////////
    //  ** chaeck validation again
    // const activeItems = {};
    // Object.keys(selectedItemsData).map((itemId) => {
    //   if (itemId != id) activeItems[itemId] = selectedItemsData[itemId];
    // });
    // setInvalidItems(activeItems, setSelectedItemsData);
    ////////////////////////////
    setItemStatus(id, STATUSES.deleted);
  };
  const setItemStatus = (itemKey, status) => {
    setSelectedItemsData((data) => ({
      ...data,
      [itemKey]: { ...data[itemKey], status: status },
    }));
  };
  const handleFailed = (itemId) => {
    setItemStatus(itemId, STATUSES.edit);
  };
  const handleDone = (itemId) => {
    setItemStatus(itemId, STATUSES.saved);
  };

  const handleUpdateData = (itemKey, itemData) => {
    setSelectedItemsData((data) => ({
      ...data,
      [itemKey]: { ...data[itemKey], formikValues: itemData },
    }));
  };
  // items.filter((item) => item.htmlId !== id));
  const itemIds = Object.keys(selectedItemsData);
  const invalidItemIds = [];
  Object.keys(selectedItemsData).map((id) => {
    if (selectedItemsData[id].status === STATUSES.invalid)
      invalidItemIds.push(id);
  });
  return (
    <Card>
      <CardHeader>
        <h4>الأصناف المضافة ({Object.keys(itemIds).length})</h4>
        {invalidItemIds.length ? (
          <h4 style={{ color: "#ea5455" }}>
            الأصناف الخاطئة ({invalidItemIds.length})
          </h4>
        ) : (
          <></>
        )}
      </CardHeader>
      {/* <CardBody
        style={{ minHeight: "20em", maxHeight: "50em", overflowY: "scroll" }}
      > */}
      <CardBody style={{ maxHeight: "50em", overflowY: "scroll" }}>
        {itemIds.length ? (
          itemIds.map((id, index) => {
            const item = selectedItems[id];
            const status = selectedItemsData[id]?.status;
            const errorMessage = selectedItemsData[id]?.errorMessage;
            return (
              <div key={"div_" + index}>
                <hr />
                <ItemComponent
                  item={item.item}
                  status={status}
                  errorMessage={errorMessage}
                  key={"Component_" + index}
                  id={id}
                  fields={initFields(fields, item.item, id)}
                  // fields={initFields([...fields], item.item)}
                  // fields={fields}
                  handleRemoval={() => handleRemoval(id)}
                  handleUpdateData={(data) => handleUpdateData(id, data)}
                  saveRequest={saveRequest}
                  setDone={() => handleDone(id)}
                  setFailed={() => handleFailed(id)}
                />
              </div>
            );
          })
        ) : (
          <></>
          // <p style={{ textAlign: "center" }}>لا توجد عناصر مضافة</p>
        )}
        {/* </div> */}
      </CardBody>
    </Card>
  );
};

const ItemComponent = ({
  item,
  status,
  errorMessage,
  id,
  handleRemoval,
  handleUpdateData,
  saveRequest,
  fields,
  setDone,
  setFailed,
}) => {
  const formik = useFormik({
    initialValues: { ...item },
    enableReinitialize: true,
    onSubmit: (v) => {
      v = ignoreDropDownKeys(v);
      ///////////////////
      let values = {};
      Object.keys(v).map((k) => {
        values[k.replaceAll(id, "")] = v[k];
      });
      ///////////////////
      values = ignoreDropDownKeys(values);
      createPostRequest({
        api: saveRequest.api,
        endPoint: saveRequest.endPoint,
        action: saveRequest.dataAction,
        ifSuccess: saveRequest.onSuccess,
        ifFailure: saveRequest.onFailure,
        breaker: saveRequest.breaker,
      })({
        params: saveRequest.params,
        body: { ...values, ...saveRequest.body },
        onSuccess: () => {
          setDone();
        },
      });
    },
  });
  useEffect(() => {
    // setItemData(itemDetails.item, formik.values);
    handleUpdateData(formik.values);
  }, [formik.values]);
  useEffect(() => {
    if (status === STATUSES.submit) {
      formik.handleSubmit();
      setFailed();
    }
  }, [status]);

  return (
    <Row key={id} style={{ position: "relative" }}>
      <Trash2
        className="cursor-pointer text-danger hover-zoom-in"
        size={16}
        style={{
          position: "absolute",
          top: "1em",
          left: "1.5em",
          zIndex: 9999,
        }}
        onClick={(e) => {
          handleRemoval();
        }}
      />
      <div
        style={{
          boxShadow: status === STATUSES.invalid ? "-2px 0px 10px #ea5455" : "",
          width: "99%",
          borderWidth: "2px",
          borderColor: "#ea5455",
          // borderStyle: "dashed",
          borderRadius: "15px",
        }}
      >
        <Forms
          id={id}
          parameters={{ forms: [{ fields }] }}
          formik={formik}
          mode={FormMode.EDIT}
        />
        {status === STATUSES.invalid ? (
          <center>
            <h5 style={{ color: "#ea5455" }}>{errorMessage}</h5>
          </center>
        ) : (
          <></>
        )}
      </div>
    </Row>
  );
};

const initFields = (fields, item, id) => {
  const finalFields = fields.map((field) => {
    const f = { ...field };
    const key = field.selector + id;
    if (item[key]) {
      f["selector"] = key;
      f["value"] = item[key];
      f["defaultValue"] = item[key];
    }
    return f;
  });
  return finalFields;
};
