import { useEffect, useState } from "react";
import { Button, Col, Row } from "reactstrap";

import { SearchSection } from "./searchSection";
import { TableSection } from "./tableSection";
import { ItemsSection } from "./itemsSection";

import { getContentPath, Types, CustomModel } from "../..";
import { PlusSquare } from "react-feather";
import { randomString } from "../../../../../utility/Utils";
import { resetInvalidItems, setInvalidItems } from "./itemValidation";
import { STATUSES } from ".";
import { createListRequest } from "../../../../../utility/services/requests";

export const CreateSearchModel =
  (configs) =>
  ({ open, handleModel, forceUpdate, ...restParams }) => {
    const {
      title = "إضافة عنصر جديد",
      enablePagination,
      enableAdd,
      singleItem,

      searchRequest,
      saveRequest,
      addRequest,

      searchFields = [],
      listFields = [],
      itemFields = [],

      modifiedSelectors = {},

      ...restConfigs
    } = configs;
    const [totalElements, setTotalElements] = useState(0);
    const [data, setData] = useState([]);
    const [selectedItems, setSelectedItems] = useState({});
    const [selectedItemsData, setSelectedItemsData] = useState({});
    const [generalFields, setGeneralFields] = useState(
      [...listFields, ...itemFields]
        // ?.filter((f) => f.selector)
        .filter((f) => !f.hide && !f.hideForm)
        .map((field) => {
          const f = { ...field };
          f.selector = modifiedSelectors[f.selector] ?? f.selector;
          _fieldGeneralChanges(f);
          return f;
        })
    );

    setGeneralFields;
    const listRequest = createListRequest(
      requestFunctionObject({
        requestObj: searchRequest,
        setTotalElements: setTotalElements,
      })
    );

    const handleSelection = (row) => {
      // generate new random id
      const newId = randomString();
      // Construct inputs from selected array
      const itemInputs = {};
      const fields = [...itemFields, ...listFields];
      fields
        ?.filter((f) => f.selector)
        .map((field) => {
          itemInputs[field.selector + newId] =
            row[field.selector] ?? field.value ?? field.defaultValue ?? null;
        });
      ///////////////////////////////////////
      // item selectors
      const modifiedInputs = {};
      Object.keys(modifiedSelectors).map((oldSel) => {
        const newSel = modifiedSelectors[oldSel];
        modifiedInputs[newSel + newId] = row[oldSel];
      });
      ///////////////////////////////////////
      const item = { ...modifiedInputs, ...itemInputs };
      setSelectedItems((prevItems) => ({
        ...prevItems,
        [newId]: {
          item: item,
        },
      }));
      setSelectedItemsData((prevStatuses) => ({
        ...prevStatuses,
        [newId]: {
          formikValues: item,
          errorMessage: null,
          status: STATUSES.default,
        },
      }));
    };

    const columns = initTableColumns(listFields, handleSelection);
    const { activeItems, activeItemsData } = getActiveItems(
      selectedItems,
      selectedItemsData
    );
    const CardBody = (
      <>
        <Row>
          <Col sm="12">
            <SearchSection
              setData={setData}
              fields={searchFields.map((field) => _fieldGeneralChanges(field))}
              searchRequest={searchRequest}
              listRequest={listRequest}
            />
          </Col>
          <Col sm="12">
            <TableSection
              onPagination={(pageNo, pageSize, searchField) => {
                if (totalElements != 0)
                  listRequest({
                    params: {
                      ...searchRequest.params,
                      pageNo,
                      pageSize,
                      searchField,
                    },
                    setData: setData,
                  });
              }}
              totalRows={totalElements}
              enablePagination={enablePagination}
              enableAdd={enableAdd}
              columns={columns}
              data={data}
            />
          </Col>
          <Col sm="12">
            {Object.keys(activeItems).length ? (
              <ItemsSection
                saveRequest={saveRequest}
                fields={generalFields}
                selectedItems={activeItems}
                // setSelectedItems={setSelectedItems}
                selectedItemsData={activeItemsData}
                setSelectedItemsData={setSelectedItemsData}
              />
            ) : (
              <></>
            )}
          </Col>
        </Row>
        <Row>
          <Col style={{ textAlign: "left" }}>
            <Button
              color="primary"
              onClick={() => {
                resetInvalidItems(activeItemsData, setSelectedItemsData);
                const hasInvalid = setInvalidItems(
                  activeItemsData,
                  setSelectedItemsData
                );
                if (!hasInvalid) {
                  submitItems(activeItemsData, setSelectedItemsData);
                }
              }}
            >
              حفظ
            </Button>
            &nbsp;
          </Col>
          <Col style={{ textAlign: "right" }}>
            &nbsp;
            <Button onClick={handleModel}>إلغاء</Button>
          </Col>
        </Row>
      </>
    );
    useEffect(() => {
      let doneItems = 0;
      Object.keys(activeItems).map((key) => {
        if (selectedItemsData[key]?.status === STATUSES.saved) {
          doneItems += 1;
        }
      });
      if (doneItems != 0 && doneItems === Object.keys(activeItems).length) {
        handleModel();
        forceUpdate();
      }
      return () => setTotalElements((e) => 0);
    }, [selectedItemsData]);
    return (
      <CustomModel
        title={title}
        open={open}
        handleModel={handleModel}
        body={CardBody}
        {...restConfigs}
      />
    );
  };

const initTableColumns = (fields, handleSelection) => {
  let columns = [
    {
      name: "",
      width: "5em",
      cell: (row) => (
        <div>
          <PlusSquare
            onClick={(_) => handleSelection(row)}
            size={22}
            className="mr-50"
            style={{ cursor: "pointer" }}
          />
        </div>
      ),
    },
  ];
  fields
    .filter(
      (f) =>
        Object.keys(f).length > 0 &&
        !f.hide &&
        !f.hideList &&
        f.type !== Types.SWITCH
    )
    .map((f) => ({ ...f, selector: f.labelSelector ?? f.selector }))
    .map((f) => columns.push(f));

  fields
    .filter((f) => !f.hide && !f.hideList && f.type === "switch")
    .map((f) =>
      columns.push({
        name: f.name,
        cell: (row) => {
          return (
            <div className="d-flex">
              <CustomInput type="switch" defaultChecked={row[f.selector]} />
            </div>
          );
        },
      })
    );
  return columns;
};
// const seperateSelectorFromId = (id, mergedidSelector) =>
//   mergedidSelector.replace(id + "/", "");
const getActiveItems = (items, data) => {
  const itemIds = Object.keys(data).filter(
    (id) => data[id]?.status != STATUSES.deleted
  );
  const activeItems = {},
    activeItemsData = {};
  itemIds.map((key) => {
    activeItems[key] = items[key];
    activeItemsData[key] = data[key];
  });
  return { activeItems, activeItemsData };
};
const submitItems = (items, renderFunction) => {
  const newData = { ...items };
  Object.keys(items).map((key) => {
    newData[key].status = STATUSES.submit;
  });
  renderFunction((_) => newData);
};
function requestFunctionObject({ requestObj, setTotalElements }) {
  const [endPoint, selectors] = getContentPath(requestObj.endPoint);

  return {
    breaker: requestObj.breaker,
    api: requestObj.api,
    endPoint: endPoint,
    action: (data) => {
      setTotalElements((e) => data.totalElements);
      selectors
        ?.filter((sel) => sel && sel != "")
        .map((sel) => (data = data[sel] ?? data));
      return data;
    },
    ifSuccess: requestObj.onSuccess,
    ifFailure: requestObj.onFailure,
  };
}

const _fieldGeneralChanges = (field) => {
  field["fieldWidth"] = field["fieldWidth"] ?? 1 / 3;
  return field;
};
