import { useEffect, useRef, useState } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  Row,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { ConfirmCancelPopup, CustomModel } from "../..";
import ImageGallery from "react-image-gallery";
import { Trash2 } from "react-feather";
import {
  createDeleteRequest,
  createGetRequest,
  createPostRequest,
} from "../../../../../utility/services/requests";

export function Albom({ post, del, get, id, forceUpdate, disable = false }) {
  const [images, setImages] = useState([]);
  // const [open, setOpen] = useState(false);
  const [selectedImages, setSelectedImages] = useState([]);
  const [base64, setBase64] = useState([]);
  const [key, setKey] = useState(1);
  const imageRef = useRef(null);

  const handleUploaderModel = () => {
    setSelectedImages((s) => undefined);
  };

  const albomforceupdate = () => {
    setKey((k) => k + 1);
  };

  const getCatalogImageId = (currentImageIndex) =>
    images[currentImageIndex]?.id;

  const handleImageDeletion = () => {
    const catalogImageId = getCatalogImageId(
      imageRef.current.getCurrentIndex()
    );
    ConfirmCancelPopup("هل انت متأكد من حذف هذه الصورة؟", "تأكيد").then(
      (out) => {
        if (out.isConfirmed) {
          createDeleteRequest({ endPoint: del.endPoint, api: del.api })({
            dataAction: del.dataAction,
            id: catalogImageId,
            params: del.params,
            onSuccess: (_) => {
              del.onSuccess && del.onSuccess();
              forceUpdate ? forceUpdate() : albomforceupdate();
            },
            onFailure: del.onFailure,
          });
        }
      }
    );
  };

  const imageDeleterIcon = () => {
    return (
      <div
        style={{
          backgroundColor: "#15246322",
          borderRadius: "0 0 40px 0",
        }}
      >
        <a
          href="#"
          className="image-gallery-icon image-gallery-svg "
          style={{
            left: 0,
            padding: "20px",
            width: "60%",
            backgroundColor: "#152463cc",
            borderRadius: "0 0 40px 0",
          }}
          onClick={(_) => {
            handleImageDeletion();
          }}
        >
          <Trash2 size={20} className="hover-zoom-in" />
        </a>
      </div>
    );
  };

  useEffect(() => {
    createGetRequest({
      endPoint: get.endPoint,
      api: get.api,
      breaker: get.breaker,
    })({
      id: get.id,
      params: get.params,
      dataAction: get.dataAction,
      onFailure: get.onFailure,
      onSuccess: (data) => {
        get.onSuccess && get.onSuccess(data);
        setImages(data);
      },
    });

    return () => {
      setImages([]);
    };
  }, [key]);

  const previewImage = (e) => {
    const files = e.target.files;
    for (let i = 0; i < files.length; i++) {
      const reader = new FileReader();
      reader.readAsDataURL(files[i]);
      reader.onloadend = () => {
        setBase64((bases) => [
          ...(bases ?? []),
          reader.result.replace("data:", "").replace(/^.+,/, ""),
        ]);
        setSelectedImages((oldFiles) => [...oldFiles, files[i]]);
      };
    }
  };
  const removeImage = () => {
    setSelectedImages(undefined);
  };

  const saveImages = () => {
    const postRequest = createPostRequest({
      endPoint: post.endPoint,
      api: post.api,
    });

    base64.map((base) => {
      const body =
        typeof post.body === "function"
          ? post.body({ base64: base })
          : post.body;
      postRequest({
        body,
        dataAction: post.dataAction,
        params: post.params,
        onSuccess: (_) => {
          post.onSuccess && post.onSuccess(_);
          handleUploaderModel();
          forceUpdate ? forceUpdate() : albomforceupdate();
        },
        onFailure: (_) => {
          post.onFailure && post.onFailure(_);
        },
      });
    });
  };
  return (
    <Card>
      <CardBody>
        {images.length > 0 ? (
          <ImageGallery
            items={images.map(({ base64 }) => ({
              original: `data:image/jpeg;charset=utf-8;base64,${base64}`,
              thumbnail: `data:image/jpeg;charset=utf-8;base64,${base64}`,
            }))}
            lazyLoad
            autoPlay
            // showIndex
            useTranslate3D
            showThumbnails
            showBullets
            showNav={false}
            isRTL={false}
            ref={imageRef}
            renderCustomControls={(_) => {
              return !disable && imageDeleterIcon();
            }}
          />
        ) : (
          <p className="text-center">لا تتوفر صور لهذا العنصر</p>
        )}
        {!disable && (
          <Row>
            <Col md="12">
              <FormGroup>
                <Label for="image-uploader" className="btn btn-primary">
                  إضافة
                </Label>
                {/* TODO make the button read the same image two times arow*/}
                <Input
                  multiple
                  type="file"
                  className="hidden"
                  id="image-uploader"
                  onChange={previewImage}
                />
              </FormGroup>
            </Col>
          </Row>
        )}
      </CardBody>
      {selectedImages && selectedImages.length ? (
        <>
          <CustomModel
            open={selectedImages}
            handleModel={handleUploaderModel}
            title="إضافة صورة"
            maxWidth="40%"
            body={
              <>
                <FormGroup>
                  {/* TODO display multible images */}
                  {selectedImages.map((img) => (
                    <img
                      src={URL.createObjectURL(img)}
                      style={{ maxWidth: "200px" }}
                    />
                  ))}
                </FormGroup>
                <FormGroup>
                  <Button.Ripple
                    className="btn-sm"
                    color="primary"
                    onClick={saveImages}
                  >
                    حفظ
                  </Button.Ripple>
                  &nbsp; &nbsp;
                  <Button.Ripple className="btn-sm" onClick={removeImage}>
                    إزالة
                  </Button.Ripple>
                </FormGroup>
              </>
            }
          />
        </>
      ) : (
        <></>
      )}
    </Card>
  );
}
