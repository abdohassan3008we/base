export const RequestedDataPath = ["data"];
export const ResponseData = (data) => data.data;
export const ResponseTotalElements = (data) => data.totalRecords;
export const RequestedPaginationParams = (pageNo, pageSize, searchField) => ({
  PageNumber: pageNo,
  PageSize: pageSize,
  PageSearch: searchField,
});
export const RequestedStartPageNo = 1;
// export const RequestedStartPaged = 1;
