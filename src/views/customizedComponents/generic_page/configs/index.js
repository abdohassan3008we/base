export {
  RequestedDataPath,
  RequestedStartPageNo,
  ResponseData,
  ResponseTotalElements,
  RequestedPaginationParams,
} from "./backend_configs";
