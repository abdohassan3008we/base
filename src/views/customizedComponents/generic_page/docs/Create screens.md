> # <h1 style = 'font-size: 300%'>Generic page creator <sub>by:[Mohamed Ashraf Hamza][author]</sub></h2>

The **[CreateScreens]** functions returns the screens as object of screens <br>

# **[CreateScreens]** function

```jsx
const screens = CreateScreens({
  /* titles */
  urlTitle: "",
  addUrlTitle: "",
  editUrlTitle: "",
  showUrlTitle: "",

  title: "شاشة عرض البيانات",
  editPageTitle: "تعديل البيانات",
  showPageTitle: "عرض البيانات",
  addPageTitle: "إضافة بيانات جديدة",
  tableTitle: "جدول البيانات",

  /* requests */
  // general requests attributes
  api: axiosObject,
  endPoint: "https://dummyjson.com/products",
  params: {},
  dataAction: (_) => _,
  onSuccess: (_) => _,
  onFailure: (_) => _,
  breaker:"BreAkeR",

  mixinRequestBodyQuery: false,
  // different methods requests attributes
  list: {
    listeners: {
      field1: value1,
      field2: value2,
    },
  },
  get: {},
  post: {},
  put: {
    enableFormListening: false,
  },
  del: {},

  /* enable actions in table */
  enablePagination: false,
  enableActiveFilter: true,
  enableAdd: false,
  enableSearch: true,
  enableExtract: true,
  enableDelete: false,
  searchFields: undefined,
  enableAddByFile : true,

  /* enable pages */
  enableEdit: false,
  enableShow: false,
  /* enable actions in navbar */
  enablePrint: false,
  enableAttachment: false,
  enableLogs: false,

  /* pages mode */
  // general screen view attributes
  popupPages: false,
  popupWidth: "50%",
  // add screen view attributes
  addPopupPage: input.popupPages ?? false,
  addPopupWidth: input.popupWidth ?? "50%",
  // edit screen view attributes
  editPopupPage: input.popupPages ?? false,
  editPopupWidth: input.popupWidth ?? "50%",
  // show screen view attributes
  showPopupPage: input.popupPages ?? false,
  showPopupWidth: input.popupWidth ?? "50%",

  /* content */
  // functions
  forceUpdate,
  // list of forms fields and properties
  forms: [],
  // list of tabs properties
  tabs: [],
  // list of reports to print
  reports: [],
  // each row detail component
  rowDetailsComponent: () => <></>
  // list of defined pages
  pages: [],
});
```

| **argument**                                    | **type** | **default**                                                                                                | **desc.**                                                                                                   |
| ----------------------------------------------- | -------- | ---------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| `urlTitle`                                      | string   | ""                                                                                                         | the url title (link) of the master screen                                                                   |
| `addUrlTitle`                                   | string   | ""                                                                                                         | the url title (link) of the add screen                                                                      |
| `editUrlTitle`                                  | string   | ""                                                                                                         | the url title (link) of the edit screen                                                                     |
| `showUrlTitle`                                  | string   | ""                                                                                                         | the url title (link) of the show screen                                                                     |
| `title`                                         | string   | "شاشة عرض البيانات"                                                                                        | the title of the main screen                                                                                |
| `editPageTitle`                                 | string   | "تعديل البيانات"                                                                                           | the title of the edit page screen                                                                           |
| `showPageTitle`                                 | string   | "عرض البيانات"                                                                                             | the title of the show page screen                                                                           |
| `addPageTitle`                                  | string   | "إضافة بيانات جديدة"                                                                                       | the title of the add page                                                                                   |
| `tableTitle`                                    | string   | "جدول البيانات"                                                                                            | the title of the table to save the file extracted with it                                                   |
| `api`                                           | axios    | [apiCliend]                                                                                                | the api library that used to create requests to back-end                                                    |
| `endPoint`                                      | string   | undefined                                                                                                  | the back-end end-point to fetch the data                                                                    |
| `params`                                        | objeect  | {}                                                                                                         | object of the query that sent with requests                                                                 |
| `dataAction`                                    | function | _ => _                                                                                                     | take the request response and returns it after modifing it                                                  |
| `onSuccess`                                     | function | _ => _                                                                                                     | actions that done if the request successed                                                                  |
| `onFailure`                                     | function | _ => _                                                                                                     | actions that done if the request failed                                                                     |
| `breaker`                                       | object   | "BreAkeR"                                                                                                  | no request sent if any value of params matches the breaker                                                  |
| `mixinRequestBodyQuery`                         | boolean  | false                                                                                                      | compine the request params and data object then send the final object in request body and query             |
| `list`<br>`get`<br>`post`<br>`put`<br>`del`<br> | object   | {<br>`api`,<br>`endPoint`,<br>`params`,<br>`dataAction`,<br>`onSuccess`,<br>`onFailure`,<br>`breaker`<br>} | object of arguments for (list / get / post / put / delete) request                                          |
| `list.listeners`                                | object   | {}                                                                                                         | list parameter argument (listeners) is an object of data for listenning to list data when its value changes |
| `put.enableFormListening`                       | boolean  | true                                                                                                       | listenning to data changing to not put-request if edit-form-data not changed                                |
| `put.withId`                                    | boolean  | false                                                                                                      | request the action by the endPoint with the id added in the end                                             |
| `del.withId`                                    | boolean  | false                                                                                                      | request the action by the endPoint with the id added in the end                                             |
| `enablePagination`                              | boolean  | false                                                                                                      | using pagenated table or normal one                                                                         |
| `enableActiveFilter`                            | boolean  | false                                                                                                      | enable or disable switch input to requests active or non-active data                                        |
| `enableAdd`                                     | boolean  | false                                                                                                      | enable or disable add button                                                                                |
| `enableDelete`                                  | boolean  | false                                                                                                      | enable or disable delete button                                                                             |
| `enableSearch`                                  | boolean  | true                                                                                                       | enable or disable search field                                                                              |
| `enableExtract`                                 | boolean  | true                                                                                                       | enable or disable extract button                                                                            |
| `searchFields`                                  | object   | undefined                                                                                                  | list of field selectors to search by (by default all selectors)                                             |
| `enableAddByFile`                               | boolean  | undefined                                                                                                  | enable the add button to add multi records written in file                                                  |
| `enableEdit`                                    | boolean  | false                                                                                                      | enable or disable edit button                                                                               |
| `enableShow`                                    | boolean  | false                                                                                                      | enable or disable show button                                                                               |
| `enablePrint`                                   | boolean  | false                                                                                                      | enable or disable print button                                                                              |
| `enableAttachment`                              | boolean  | false                                                                                                      | enable or disable attachment button                                                                         |
| `enableLogs`                                    | boolean  | false                                                                                                      | enable or disable logs button                                                                               |
| `popupPages`                                    | boolean  | true                                                                                                       | add, edit and show screens in popup mode                                                                    |
| `popupWidth`                                    | string   | "50%"                                                                                                      | the width of the popup pages related to screen                                                              |
| `addPopupPage`                                  | boolean  | `popupPages`                                                                                               | add screen in popup mode                                                                                    |
| `addPopupWidth`                                 | string   | `popupWidth`                                                                                               | the width of the add popup page related to screen                                                           |
| `editPopupPage`                                 | boolean  | `popupPages`                                                                                               | edit screen in popup mode                                                                                   |
| `editPopupWidth`                                | string   | `popupWidth`                                                                                               | the width of the edit popup page related to screen                                                          |
| `showPopupPage`                                 | boolean  | `popupPages`                                                                                               | show screen in popup mode                                                                                   |
| `showPopupWidth`                                | string   | `popupWidth`                                                                                               | the width of the show popup page related to screen                                                          |
| `forceUpdate`                                   | function | undefined                                                                                                  | page force-render function (could passed to use in tabs from the master page)                               |
| `rowDetailsComponent`                           | object   | undefined                                                                                                  | react component that desplayed when row expand                                                              |
| `forms`                                         | object   | []                                                                                                         | list of forms objects                                                                                       |
| `tabs`                                          | object   | []                                                                                                         | list of tabs objects                                                                                        |
| `reports`                                       | object   | []                                                                                                         | list of reports objects                                                                                     |
| `pages`                                         | object   | [defaultPages]                                                                                             | list of pages and its props                                                                                 |

## 1. **`pages`**

```jsx
pages: [
  {
    id: 0,
    name: "add",
    disable: false,
    // disable: (params) => !params.active,

    group: "group id" /*done for header buttons*/,

    onClick: (params) => {}, // on click icon/btn the function called

    url: "link/add", // redirect to the page url on click
    component: (params) => <Component {...params} />, // open component if url == "undefined"

    icon: (params) => <Icon {...params} />, // apply the page by icon in each row

    btn: (params) => <Btn {...params} />, // apply the page by btn table header
  },
];
```

| **argument** | **type**                      | **default** | **desc.**                                                                             |
| ------------ | ----------------------------- | ----------- | ------------------------------------------------------------------------------------- |
| `id`         | number                        | undefined   | page route id                                                                         |
| `name`       | string                        | undefined   | page parameter name (the main key for referencing the page)                           |
| `disable`    | boolean / function of boolean | false       | hide the page component icon/btn                                                      |
| `group`      | string                        | undefined   | group the icons/btns by id                                                            |
| `onClick`    | function                      | undefined   | function that applies beside the component/url                                        |
| `url`        | string                        | `name`      | page url to direct                                                                    |
| `component`  | function                      | undefined   | function of react component shows on click the icon/btn                               |
| `icon`       | function                      | undefined   | function of react component that apply the page action for each row in the table      |
| `btn`        | function                      | undefined   | function of react component that apply the page action for all table the table header |

## 2. **`reports`**

```jsx
reports: [
  {
    title: "إسم التقرير الظاهر للمستخدم",
    fileName: "إسم الملف بعد الحفظ",
    component: myReport({
      header: {},
      body: {},
      footer: {},
    }),
  },
];
```

| **argument** | **type** | **default**             | **desc.**                                                           |
| ------------ | -------- | ----------------------- | ------------------------------------------------------------------- |
| `title`      | string   | undefined               | the title of the report displayed in the printing button            |
| `fileName`   | string   | "report\_" + Date.now() | the name of saved file                                              |
| `component`  | function | undefined               | returned function from [createReport] that creates the rerport page |

## 3. **`tabs`**

```jsx
tabs: [
  {
    num: 1,
    name: "tab name",
    component: (selectors) => <TabComponent {...selectors} />,
    listeners: ["selector1", "selector2"],
  },
];
```

| **argument** | **type** | **default** | **desc.**                                                                                                                                                        |
| ------------ | -------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `num`        | number   | undefined   | the number of the tab as id                                                                                                                                      |
| `name`       | string   | undefined   | the name of the tab                                                                                                                                              |
| `component`  | function | undefined   | function takes all selectors and returns the tab component                                                                                                       |
| `listeners`  | object   | undefined   | an array of formik keys to listen to its value changes to re-render the tabs<br/> :: (**undefined** => always re-render) , (**empty array []** => no re-renders) |

## 4. **`forms`**

```jsx
forms: [
  {
    title: "form title",

    hide: false,
    hideAdd: false,
    hideEdit: false,
    hideShow: false,
    unEditable: false,
    readOnly: false,

    size: 0.5,
    style: { textAlign: "start", direction: "ltr" },

    component: ({ id, formik, parameters, forceUpdate, disable }) => (
      <ReactComponent />
    ),

    fields: [],
  },
];
```

| **argument** | **type** | **default** | **desc.**                                                                            |
| ------------ | -------- | ----------- | ------------------------------------------------------------------------------------ |
| `title`      | string   | undefined   | the title of the form                                                                |
| `hide`       | boolean  | undefined   | hide the form from the screen                                                        |
| `hideAdd`    | boolean  | undefined   | hide the form from the add page                                                      |
| `hideEdit`   | boolean  | undefined   | hide the form from the edit page                                                     |
| `hideshow`   | boolean  | undefined   | hide the form from the show page                                                     |
| `unEditable` | boolean  | undefined   | disable editing in the form in edit mode                                             |
| `readOnly`   | boolean  | undefined   | disable editing in the form                                                          |
| `size`       | number   | 1           | the width of the form card related to the screen                                     |
| `style`      | object   | undefined   | the style of the form container as all                                               |
| `component`  | function | undefined   | function that returns the form as a component instead of forms                       |
| `fields`     | object   | undefined   | the fields that form contains such as (text, textarea, list, switch, number,... etc) |

### 4.1. **`fields`**

```jsx
fields: [
    {
        id: true,
        name: "المسلسل",
        selector: "value",
        labelSelector: "label",
        sort:1,

        sortable: true,
        minWidth: "150px",
        maxWidth: "150px",
        hideList: true,

        hide: false,
        hideAdd: true,
        hideEdit: false,
        hideShow: false,
        hideExtract: false,
        readOnly: true,
        unEditable:true,
        activeInList:true,

        xs,sm,md,lg,xl,xxl,
        fieldWidth: 0.25,
        ltr: false,
        rtl: false,
        style:{},

        placeholder: "الرقم التسلسلي"
        value: "value"
        defaultValue: "defaultValue"
        // value: (formikValues) => formikValues.selector1_value
        // defaultValue: (formikValues) => formikValues.selector2_value

        required: true,
        validation: new Validator("the main error message").required("the (required) error message"),
        // validation: (formValues) =>  {
        //     new Validator("the main error message").min(formValues.num,`the minimam number is${formValues.num}`),
        //   }
        // lookup: {
        //   "selector::lookupValue": {}
        // }

        // type: 'counter',
        // counterProps:{}

        // type: 'radian group',
        // groupProps:{}

        type: 'list',
        // listProps:{}
        // data: [1,2,3]
        data: [
            {
                label: 1,
                value: 1
            },{
                label: 2,
                value: 2
            },{
                label: 3,
                value: 3
            },
        ]

    },
]
```

| **argument**                               | **type**    | **default** | **desc.**                                                                                                |
| ------------------------------------------ | ----------- | ----------- | -------------------------------------------------------------------------------------------------------- |
| `id`                                       | boolean     | false       | difine this field as the ID (primary key)                                                                |
| `name`                                     | string      | undefined   | the name of the column that show the data of the field                                                   |
| `selector`                                 | string      | undefined   | the argument name from database                                                                          |
| `labelSelector`                            | string      | undefined   | the argument name from database for list                                                                 |
| `sort`                                     | number      | 0           | the sort of the field in the form                                                                        |
| `sortable`                                 | boolean     | false       | to sort the column alphapitically                                                                        |
| `minWidth`                                 | number      | undefined   | the minimum width of the column                                                                          |
| `maxWidth`                                 | number      | undefined   | the maximum width of the column                                                                          |
| `hideList`                                 | boolean     | false       | hide field from table page                                                                               |
| `hide`                                     | boolean     | false       | hide field from all pages                                                                                |
| `hideAdd`                                  | boolean     | false       | hide field from add form                                                                                 |
| `hideEdit`                                 | boolean     | false       | hide field from edit form and show page                                                                  |
| `hideShow`                                 | boolean     | false       | hide field from show form and show page                                                                  |
| `hideExtract`                              | boolean     | false       | hide field from report table when download                                                               |
| `readOnly`                                 | boolean     | false       | disable / enable the field in all forms                                                                  |
| `unEditable`                               | boolean     | false       | disable / enable the field in edit form                                                                  |
| `activeInList`                             | boolean     | undefined   | active the field action in table                                                                         |
| `ltr`                                      | boolean     | false       | direction of the text (false = arabic , true = english)                                                  |
| `rtl`                                      | boolean     | false       | direction of the text (true = arabic , false = english)                                                  |
| `style`                                    | object      | undefined   | the style of the field                                                                                   |
| `xs` , `sm`<br>`md` , `lg`<br>`xl` , `xxl` | number      | undefined   | the size of the field in different screen size                                                           |
| `fieldWidth`                               | number      | 0.5         | the field width based on the form width                                                                  |
| `placeholder`                              | string      | undefined   | the field placeholder                                                                                    |
| `value`                                    | string      | undefined   | the value of the field                                                                                   |
| `defaultValue`                             | string      | undefined   | default value of the field                                                                               |
| `required`                                 | boloean     | false       | must enter value to the field when add and edit                                                          |
| `validation`                               | [validator] | undefined   | define validation methods as object or a function that takes the form values and return validator object |

| `type` | string | 'text' | field type (text, email, password, switch, list ,... etc) |
| `data` | object | [] | difined data to add in dropdown list if the type is 'list' |
| `listProps` | object | undefined | object of params that describe the data in the dropdown list |
| `counterProps` | object | undefined | object of params that describe the counter |
| `lookup` | object | undefined | list of objects make the data in the field depends on lookup value in aother field |

#### 4.1.1 **`counterProps`**

```jsx
counterProps: {
  min: -5,
    max: 10,
    step: 2,
    wrap: true,
},
```

| **argument** | **type** | **default** | **desc.**                                              |
| ------------ | -------- | ----------- | ------------------------------------------------------ |
| `min`        | number   | 1           | the minimum number to reach in the counter             |
| `max`        | number   | Infinity    | the maximum number to reach in the counter             |
| `step`       | number   | 1           | the step number for changeing the count                |
| `wrap`       | boolean  | false       | if reach the end restart the counter from start or end |

#### 4.1.2 **`groupProps`**

```jsx
groupProps: {
  children: [
    { value: "1", label: "1", check: true , disable: false},
    { value: "2", label: "2", check: false, disable: false},
    { value: "3", label: "3", check: false, disable: false},
  ];
}
```

| **argument** | **type** | **default** | **desc.**                                  |
| ------------ | -------- | ----------- | ------------------------------------------ |
| `children`   | object   | []          | array of radian or checkbox buttons object |

#### 4.1.3 **`listProps`**

```jsx
listProps: {
    listEndPoint: ["https://dummyjson.com/products","content"],
    // listEndPoint: "https://dummyjson.com/products"
    params: {active: true}
    valueSelector: "id"
    labelSelector: "name"
    // valueSelector: (item) => item.id
    // labelSelector: (item) => item.name

    data: []

    enablePagination: true,
    pageSize: 5,

    enableSearch: true,

    enableAdd: true,
    addTitle: "إضافة عنصر جديد"
    addComponent: (params) => <Form {...params}>
}
```

| **argument**       | **type** | **default** | **desc.**                                                      |
| ------------------ | -------- | ----------- | -------------------------------------------------------------- |
| `listEndPoint`     | string   | undefined   | the endPoint of backend to fetch data to list in dropdown      |
| `listEndPoint`     | object   | []          | the endPoint in index 0 then the selectors path to the content |
| `enableSearch`     | boolean  | true        | enable search from the data                                    |
| `enableAdd`        | boolean  | false       | enable add new item to list by enable add-button               |
| `addTitle`         | string   | "إضافة"     | the add-button title                                           |
| `addComponent`     | function | () => <></> | function returns a component that shows when click add-button  |
| `valueSelector`    | string   | undefined   | the backend argument name to make as value of dropdown item    |
| `labelSelector`    | string   | undefined   | the backend argument name to make as label of dropdown item    |
| `data`             | object   | undefined   | the default data to display if wrong request                   |
| `enablePagination` | boolean  | undefined   | enable slicing the data in the list menu                       |
| `pageSize`         | number   | 6           | the number of data shown in the dropdown menu                  |
| `params`           | object   | {}          | the number of data shown in the dropdown menu                  |

#### 4.1.4 **`lookup`**

```jsx
lookup: {
    "selector": {
      type:"list",
      listProps:{
        listEndPoint: "https://dummyjson.com/products/::??", // auto replace the ::?? with the form value of selector
        // ::?? declared in generic as variable 'lookupValueCode'
        valueSelector: "value",
        labelSelector: "label",
      }
    },
    "select::lookupValue": {
      defaultValue: "::??" // auto replace the ::?? with the lookupValue
      validation: new Validator().required("error message"),
    },
},
```

| **argument**  | **type** | **default** | **desc.**                                      |
| ------------- | -------- | ----------- | ---------------------------------------------- |
| `selector`    | string   | undefined   | the selector of the lookup field or part of it |
| `lookupValue` | string   | undefined   | the lookup value to change the data on         |

[author]: https://github.com/emam1999
[createscreens]: ../factory/CreateScreens.js
[pagescreens]: ../classes/page.js
[apicliend]: ../../../../utility/services/apiClient.js
[mainscreen]: ../components/screens/masterPage/DataTable.js
[popupaddscreen]: ../components/screens/popUpMode/AddNewModal.js
[popupeditscreen]: ../components/screens/popUpMode/EditModel.js
[addscreen]: ../components/screens/newPageMode/Add.js
[editscreen]: ../components/screens/newPageMode/Edit.js
[addform]: ../components/screens/popUpMode/AddNewForm.js
[validator]: ../classes/validator.js
[createreport]: ../../../reports/reportsFunctions/create_print/create.js
[defaultpages]: ../factory/defaults.js
