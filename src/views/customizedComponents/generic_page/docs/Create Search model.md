> # <h1 style = 'font-size: 300%'>Generic search model creator <sub>by:[Mohamed Ashraf Hamza][author]</sub></h2>

The **[CreateSearchModel]** functions returns the search model as a component <br>

# **[CreateSearchModel]** function

```jsx
const searchModel = CreateSearchModel({
    // ** configurations
    title: "إضافة عنصر جديد",
    enablePagination: false,
    maxWidth: "80%",

    // ** requests
    searchRequest: { endPoint, params, dataAction, onSuccess, onFailure},
    addRequest: { endPoint, params, dataAction, onSuccess, onFailure},
    saveRequest: { endPoint, params, dataAction, onSuccess, onFailure, body},

    // ** fields
    // search section
    searchFields: [
        {
            fieldWidth: 0.5,
            name: "الكود",
            selector: "code",
        },
    ],
    // table section
    listFields: [
        {
            name: "الكود",
            selector: "code",
            id: true,
            minWidth: "100px",
            value: (row) => row.code && row.name && `${row.code}-${row.name}`,
            cell: (row) => row.code && row.name && `${row.code}-${row.name}`,
        },
    ],
    // item section
    itemFields: [
        {
            name:"الكمية",
            selector:"itemQty",
            defaultValue: 1,
        },
    ],
    /// to modify exists parameter name
    modifiedSelectors: {
        oldSelector: "newSelector",
    },


}),
```

| **argument**        | **type** | **default**       | **desc.**                                   |
| ------------------- | -------- | ----------------- | ------------------------------------------- |
| `title`             | string   | "إضافة عنصر جديد" | the title of the model                      |
| `enablePagination`  | boolean  | false             | using pagenated table or normal one         |
| `maxWidth`          | number   | undefined         | the maximum width of the model              |
| `searchRequest`     | object   | undefined         | the search action (get request) requirments |
| `addRequest`        | object   | undefined         | the add action (post request) requirments   |
| `saveRequest`       | object   | undefined         | the save action (post request) requirments  |
| `searchFields`      | object   | []                | array of the search form fields             |
| `listFields`        | object   | []                | array of the table columns and item fields  |
| `itemFields`        | object   | []                | array of the item form additonal fields     |
| `modifiedSelectors` | object   | {}                | object of modified fields selectors         |

[author]: https://github.com/emam1999
[createsearchmodel]: ../components/customComponent/general/searchModel/createModel.js

<!-- [pagescreens]: ../classes/page.js
[apicliend]: ../../../../utility/services/apiClient.js
[mainscreen]: ../components/screens/masterPage/DataTable.js
[popupaddscreen]: ../components/screens/popUpMode/AddNewModal.js
[popupeditscreen]: ../components/screens/popUpMode/EditModel.js
[addscreen]: ../components/screens/newPageMode/Add.js
[editscreen]: ../components/screens/newPageMode/Edit.js
[addform]: ../components/screens/popUpMode/AddNewForm.js
[Validator]: ../classes/validator.js
[createreport]: ../../../reports/reportsFunctions/create_print/create.js
[defaultpages]: ../factory/defaults.js -->
