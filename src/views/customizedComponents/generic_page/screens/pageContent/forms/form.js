import { Label, Row, Col, Container, Card, CardBody } from "reactstrap";
import { AvForm } from "availity-reactstrap-validation-safe";

import {
  InputText,
  Float,
  Integer,
  PhoneNumber,
  Text,
  Counter,
  List,
  NumberText,
  Switch,
  TextArea,
  Email,
  Password as Password1,
  StrongPassword as Password2,
  DateTimePicker as DTPicker,
  TimePicker,
  DatePicker,
  NationalID,
  RadianGroup,
  RadianButton,
  FormMode,
  Types,
  ImageField,
  lookupValueCode,
  Message,
  ErrorMessage,
  WarningMessage,
  SuccessMessage,
} from "../../..";
import { lookup } from "../../../components/customComponent";
import { useTranslation } from "react-i18next";

/**
 *@author Mohamed Ashraf Hamza
 */
export const Form = ({
  id,
  title,
  fields,
  formik,
  mode = FormMode.ADD,
  props = {},
  disable,
  ...restProps
}) => {
  const xsmall = 12;
  const small = 12;
  const medium = 6;
  const large = 6;
  const xlarge = 6;
  const xxlarge = 3;
  const { t } = useTranslation();
  return (
    <>
      <h2>{title}</h2>
      <Card>
        <CardBody>
          <AvForm>
            <Container>
              <Row>
                {sortFields(fields)
                  ?.map((field) =>
                    refactField({ restProps, field, formik, id, translat: t })
                  )
                  ?.map((field) => {
                    return field;
                  })
                  ?.filter((field) => {
                    const hide =
                      field.hide ||
                      (mode === FormMode.EDIT && field.hideEdit) ||
                      (mode === FormMode.SHOW && field.hideShow) ||
                      (mode === FormMode.ADD && field.hideAdd);
                    return !hide;
                  })
                  ?.map((field, index) => {
                    return (
                      <Col
                        key={field.selector + "_Col_" + index}
                        xs={
                          (field.xs ?? xsmall * (field.fieldWidth ?? 0.5)) * 2
                        }
                        sm={(field.sm ?? small * (field.fieldWidth ?? 0.5)) * 2}
                        md={
                          (field.md ?? medium * (field.fieldWidth ?? 0.5)) * 2
                        }
                        lg={(field.lg ?? large * (field.fieldWidth ?? 0.5)) * 2}
                        xl={
                          (field.xl ?? xlarge * (field.fieldWidth ?? 0.5)) * 2
                        }
                        xxl={field.xxl ?? xxlarge * (field.fieldWidth ?? 0.5)}
                        style={{ margin: "auto", textAlign: "start" }}
                      >
                        <FieldComponent
                          key={id}
                          id={id}
                          disable={
                            disable ||
                            field.readOnly ||
                            mode === FormMode.SHOW ||
                            (mode === FormMode.EDIT &&
                              (field.unEditable || field.id))
                          }
                          field={field}
                          formik={formik}
                          mode={mode}
                          props={props}
                        />
                      </Col>
                    );
                  })}
              </Row>
            </Container>
          </AvForm>
        </CardBody>
      </Card>
    </>
  );
};
const FieldComponent = ({ id, disable, field, formik, mode, props }) => {
  if (field["selector"] || field["value"] || field["defaultValue"]) {
    switch (field.type) {
      case Types.MESSAGE:
        return (
          <Message id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.ERROR_MESSAGE:
        return (
          <ErrorMessage
            id={id}
            disable={disable}
            field={field}
            formik={formik}
          />
        );
      case Types.WARNING_MESSAGE:
        return (
          <WarningMessage
            id={id}
            disable={disable}
            field={field}
            formik={formik}
          />
        );
      case Types.SUCCESS_MESSAGE:
        return (
          <SuccessMessage
            id={id}
            disable={disable}
            field={field}
            formik={formik}
          />
        );
      case Types.UPLOAD_IMAGE:
        return (
          <ImageField id={id} disable={disable} field={field} formik={formik} />
        );
      // case Types.RADIAN_BUTTON:
      //   return (
      //     <RadianButton
      //       id={id}
      //       // onChange={props?.setRadianChoice}
      //       disable={disable}
      //       field={field}
      //       formik={formik}
      //     />
      //   );
      case Types.RADIAN_GROUP:
        return (
          <RadianGroup
            id={id}
            // onChange={props?.setRadianChoice}
            disable={disable}
            field={field}
            formik={formik}
          />
        );
      case Types.DATE_PICKER:
        return (
          <DatePicker id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.TIME_PICKER:
        return (
          <TimePicker id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.DATE_TIME_PICKER:
        return (
          <DTPicker id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.LIST:
        return (
          <>
            <List id={id} disable={disable} field={field} formik={formik} />
            <label></label>
          </>
        );
      case Types.SWITCH:
        return (
          <>
            <label></label>
            <Switch
              // key={id}
              id={id}
              disable={disable}
              field={field}
              formik={formik}
            />
          </>
        );
      case Types.COUNTER:
        return (
          <Counter id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.INTEGER:
        return (
          <Integer id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.FLOAT:
        return (
          <Float id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.EMAIL:
        return (
          <Email id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.PASSWORD:
        return (
          <Password1 id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.STRONG_PASSWORD:
        return (
          <Password2 id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.PHONE:
        return (
          <PhoneNumber
            id={id}
            disable={disable}
            field={field}
            formik={formik}
          />
        );
      case Types.NID:
        return (
          <NationalID id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.NUMBER:
        return (
          <NumberText id={id} disable={disable} field={field} formik={formik} />
        );
      case Types.TEXTAREA:
        return (
          <TextArea id={id} disable={disable} field={field} formik={formik} />
        );
      default:
      case Types.TEXT:
        return <Text id={id} disable={disable} field={field} formik={formik} />;
      case Types.INPUT:
      case Types.AR_INPUT:
        return (
          <InputText
            id={id}
            lang={"ar"}
            disable={disable}
            field={field}
            formik={formik}
          />
        );
      case Types.EN_INPUT:
        return (
          <InputText
            id={id}
            lang={"en"}
            disable={disable}
            field={field}
            formik={formik}
          />
        );
    }
  }
  return <></>;
};
const sortFields = (fields) => {
  return fields.sort((f1, f2) => {
    return (f1.sort ?? 0) - (f2.sort ?? 0);
  });
};
const refactField = ({ restProps, field, formik, id, translat }) => {
  const values = removeKeyPrefexFromObj(formik?.values, id);
  //////////////////// add rest props ////////////////////
  field = { ...restProps, ...field };
  //////////////////// lookup modifing ////////////////////
  const modefiedField = lookup({
    field,
    values,
    lookupFun: ({ lookupValue, formikValue, obj }) => {
      let modefiedField = undefined;
      if (!lookupValue || lookupValue == formikValue) {
        if (typeof obj === "function") {
          obj = obj(values);
        }
        const validation =
          typeof obj.validation === "function"
            ? obj.validation(values)
            : obj.validation;
        // modefiedField = Object.assign({}, obj);

        modefiedField = {
          ...obj,
          ...JSON.parse(
            JSON.stringify(obj).replaceAll(lookupValueCode, formikValue)
          ),
          validation: validation ?? field.validation,
        };
      }
      return modefiedField;
    },
  });

  field = { ...field, ...modefiedField };
  //////////////////// selector id prefex ////////////////////
  if (typeof field.value === "function") {
    field["_value_"] = field.value(values);
  }
  if (typeof field._defaultValue === "function") {
    field["_defaultValue_"] = field.defaultValue(values);
  }
  //////////////////// init. name ////////////////////
  field["name"] = field.name ? translat(field.name) : field.selector;
  //////////////////// -0_0- ////////////////////
  return field;
};

export const removeKeyPrefexFromObj = (obj, pref) => {
  const newObj = {};
  Object.keys(obj ?? {}).map((key) => {
    newObj[key.replace(pref, "")] = obj[key];
  });
  return newObj;
};
