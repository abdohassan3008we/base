import { useEffect, useState } from "react";
import { Col, Row } from "reactstrap";
import { FormMode } from "../../..";
import { Form } from "./form";
import { useTranslation } from "react-i18next";

/**
 *@author Mohamed Ashraf Hamza
 */
export function Forms({
  id = "",
  parameters,
  formik,
  mode = FormMode.ADD,
  setActive = () => {},
  errorMessage,
  setErrorMessage = () => {},
  ...restProps
}) {
  // const [key, setKey] = useState(0);
  // const forceUpdate = () => {
  //   setKey((k) => k + 1);
  // };

  // check if all fields data meet the validation requirments
  // then return the invalid fields
  const getInvalidFields = () => {
    let validationStatus = [];
    parameters.forms?.map((form) => {
      validationStatus = [
        ...validationStatus,
        ...(form?.fields
          ?.filter((field) => field.validation)
          ?.filter(
            (field) =>
              !field.validation?.isValid(formik?.values[field.selector])
          )
          ?.map((field) => field.name ?? field.selector) ?? []),
      ];
      // .includes(false)
      // ? "invalid"
      // : "valid";
    });
    // printValidationLogs({ params: p, values: formik?.values });
    // return !validationStatus.includes("invalid");
    return validationStatus;
  };

  // check if the data changed
  const isDataNotChanged = () => {
    return (
      ((mode === FormMode.EDIT && parameters.put_EnableFormListening) ||
        (mode === FormMode.ADD && parameters.post_EnableFormListening)) &&
      !parameters._formListener_?.hasChanged({ values: formik?.values })
    );
  };
  const { t } = useTranslation();

  useEffect(() => {
    if (mode != FormMode.SHOW)
      if (!isDataNotChanged()) {
        const invalidFields = getInvalidFields();
        setActive((e) => invalidFields.length === 0);
        setErrorMessage(errorMessage + "\n" + invalidFields?.join("\n"));
      } else {
        setActive(false);
        setErrorMessage(t("Change the data before saving it"));
      }
  }, [formik?.values]);

  return (
    <Row>
      {parameters.forms
        ?.filter(
          (form) =>
            !form?.hide &&
            ((mode === FormMode.EDIT && !form?.hideEdit) ||
              (mode === FormMode.SHOW && !form?.hideShow) ||
              (mode === FormMode.ADD && !form?.hideAdd))
        )
        .map((form, index) => {
          return (
            <Col key={`form_${index}`} md={12 * form?.size ?? 1} sm={12}>
              <div style={{ ...form?.style }}>
                <h2>{form?.title}</h2>
                {form?.fields ? (
                  <Form
                    title={null}
                    key={id}
                    id={id}
                    formik={formik}
                    fields={form?.fields}
                    disable={
                      form?.readOnly ||
                      (mode === FormMode.EDIT && form?.unEditable)
                    }
                    mode={mode}
                    {...restProps}
                    // props={{
                    //   setRadianChoice: ({ group, buttonValue }) => {
                    //     params.forms?.map((form) => {
                    //       form?.fields?.map((field) => {
                    //         if (field.selector === group) {
                    //           field.buttons?.map((btn) => {
                    //             if (btn.value === buttonValue) {
                    //               btn["checked"] = true;
                    //             } else {
                    //               btn["checked"] = false;
                    //             }
                    //           });
                    //         } else if (field.group === group) {
                    //           if (field.value === buttonValue) {
                    //             field["checked"] = true;
                    //           } else {
                    //             field["checked"] = false;
                    //           }
                    //         }
                    //       });
                    //     });
                    //     // forceUpdate();
                    //   },
                    // }}
                  />
                ) : form?.component ? (
                  form?.component({
                    parameters,
                    formik,
                    disable:
                      form?.readOnly ||
                      mode === FormMode.SHOW ||
                      (mode === FormMode.EDIT && form?.unEditable),
                  })
                ) : (
                  <></>
                )}
              </div>
            </Col>
          );
        })}
    </Row>
  );
}

const printValidationLogs = ({ params, values }) => {
  const isAllValid = !params.forms
    ?.map((form) => {
      return form?.fields
        ?.filter((field) => field.validation)
        .map((field) => {
          console.log(
            "\t",
            formatStringWithSpaces(field.selector, 25),
            "\t",
            formatStringWithSpaces(values[field.selector], 10),
            "\t",
            field.validation?.isValid(values[field.selector])
          );
          return field.validation?.isValid(values[field.selector]);
        })
        .includes(false)
        ? "invalid"
        : "valid";
    })
    .includes("invalid");
};
const formatStringWithSpaces = (text, num) => {
  text = text ?? "";
  num = num ?? 0;
  const len = text.length;
  const spacesNum = num - len;
  let spaces = "";
  for (let i = 0; i < spacesNum; i++) {
    spaces += " ";
  }
  return text + spaces;
};
