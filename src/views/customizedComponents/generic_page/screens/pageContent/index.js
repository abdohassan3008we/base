export { PageContent } from "./PageContent";
export { Forms ,Form} from "./forms";

export const addDropDownKeys = (formik, selector, data) => {
  return addKeysContains(formik, selector, data, "_$_");
};
export const addKeysContains = (formik, selector, data, key) => {
  formik?.setFieldValue(key + selector, data);
};
export const ignoreDropDownKeys = (values) => {
  return ignoreKeysContains(values, "_$_");
};
export const ignoreKeysContains = (values, key) => {
  const obj = {};
  Object.keys(values)
    .filter((k) => !k.includes(key))
    .map((k) => (obj[k] = values[k]));
  return obj;
};

// export const isAllFieldsValuesValid = (fields) => {
//   return !fields
//     ?.filter((field) => field.validation)
//     ?.filter((field) => field.validation?.isValid(field[value]))
//     .includes(false);
// };
