import { useTranslation } from "react-i18next";
import { Forms } from ".";
import { FormMode, Tabs } from "../..";

/**
 *@author Mohamed Ashraf Hamza
 */
export function PageContent({
  parameters,
  formik,
  setActive,
  errorMessage,
  setErrorMessage,
  mode = FormMode.ADD,
  tabParams,
  ...restProps
}) {
  const { t } = useTranslation();
  parameters["_tabs_"] = initTabs({
    formik,
    params: parameters,
    tabParams,
    translate: t,
  });
  // params["_tabs_"] =
  //   params.tabs.map((tab) => ({
  //     ...tab,
  //     component: tab.component(formik?.values),
  //   }));

  return (
    <>
      <Forms
        parameters={parameters}
        formik={formik}
        setActive={setActive}
        errorMessage={errorMessage}
        setErrorMessage={setErrorMessage}
        mode={mode}
        {...restProps}
      />
      {mode != FormMode.ADD &&
      parameters._tabs_ &&
      parameters._tabs_.length &&
      parameters._tabs_.length > 0 ? (
        <Tabs items={parameters._tabs_} />
      ) : (
        <></>
      )}
    </>
  );
}

let oldValues = undefined;

const initTabs = ({ formik, params, tabParams, translate }) => {
  // get the tabs that changed by tracking the changes of them listeners in the values of formik
  const newTabs = params.tabs
    ?.filter(
      (tab) =>
        !tab.listeners ||
        tab.listeners
          ?.map((sel) => {
            return formik?.values?.[sel] !== oldValues?.[sel]
              ? "changed"
              : "unchanged";
          })
          .includes("changed")
    )
    .map((tab) => ({
      ...tab,
      component: tab.component({ ...tabParams }),
    }));

  // get the tabs that not changed by deleting the changed tabs from them all
  const oldTabs =
    !newTabs || newTabs.length === 0
      ? params._tabs_
      : params._tabs_?.filter(
          (tab) => !newTabs?.find((t) => t.num === tab.num)
        );
  oldValues = formik?.values;

  const noListenersTabs = params.tabs
    .filter((tab) => tab.listeners && tab.listeners.length === 0)
    .filter((tab) => !oldTabs?.find((oldTab) => oldTab.num === tab.num))
    .map((tab) => ({
      ...tab,
      component: tab.component({ ...tabParams }),
    }));
  const all_tabs = [
    ...(noListenersTabs ?? []),
    ...(oldTabs ?? []),
    ...(newTabs ?? []),
  ];
  return all_tabs.map((tab) => ({ ...tab, name: translate(tab.name) }));
};
