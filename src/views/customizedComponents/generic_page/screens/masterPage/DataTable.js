// ** React Imports
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
// **
import {
  ErrorPopup,
  SuccessPopup,
  ConfirmCancelPopup,
  BreadCrumbs,
  PaginationTable,
  Table,
  Types,
  StandardPages,
  ResponseTotalElements,
  RequestedStartPageNo,
} from "../..";
// ** Third Party Components
import { RiFileExcel2Line } from "react-icons/ri";
import { BiShowAlt as Eye } from "react-icons/bi";
import { MdDeleteForever as Trash2 } from "react-icons/md";
import { TbEditCircle as Edit } from "react-icons/tb";

import { Button, CustomInput, Input, Label } from "reactstrap";
// ** import models
import { isDuplicatedObject, Listener } from "../../classes";
import { createPagesObject } from "../../factory/params";
import { useTranslation } from "react-i18next";
import { RequestedPaginationParams } from "../..";
import { excel_to_JSON } from "../../components/IO";
import { downloadExampleExcel } from "../../components/customComponent/general/_extraction";

let listListener;
let requestParamsListener;
const iconsClassNames = "active_icon";
const defaultIconColor = "#888888";
const showIconColor = "#4587cd";
const editIconColor = "#006d43";
const deleteIconColor = "#DC4545";
/**
 * @author Mohamed Ashraf Hamza
 */
export const DataTable = ({ parameters }) => {
  const pages = createPagesObject(parameters);

  // ** Store Vars
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState([]);
  const [totalElements, setTotalElements] = useState(0);
  const [item, setItem] = useState({});
  const { t } = useTranslation();

  // parameters["item"] = item;
  // parameters["setItem"] = (item) => {
  //   setItem(item);
  //   parameters["item"] = item;
  // };
  // **
  const breadcrumbs = initBreadcrumbs(t(parameters.title));
  const searchFields = initSearchFields(parameters);

  // ** States
  const [addModel, setAddModel] = useState(false);
  const [updateModel, setUpdateModel] = useState(false);
  const [showModel, setShowModel] = useState(false);
  const [restModels, setRestModels] = useState({});
  const [openFileUploader, setOpenFileUploader] = useState(false);
  const [requestActiveData, setRequestActiveData] = useState(
    parameters.listRequestParams?.active ?? parameters.requestParams?.active
  );

  const handleFileUploader = () => {
    setOpenFileUploader((s) => !s);
    forceUpdate();
  };
  // ** Update page
  const [key, setKey] = useState(0);
  const forceUpdate =
    parameters.forceUpdate ??
    (() => {
      setKey((key) => key + 1);
    });

  useEffect(() => {
    if (listListener)
      listListener.update({ listeners: parameters.toList_Listeners });
    return () => {
      listListener = undefined;
    };
  }, [parameters.toList_Listeners]);

  // list request to get all data to fill the table (listenning to data changing)
  useEffect(() => {
    if (!listListener)
      listListener = new Listener({
        listeners: parameters.toList_Listeners,
      });

    if (
      !parameters.enablePagination
      // && listListener.hasChanged({ values: parameters.toList_ListenersValues })
    ) {
      const params = parameters.listRequestParams ?? parameters.requestParams;
      // if (!isDuplicatedObject(requestParamsListener, params)) {
      //   requestParamsListener = params;
      //   for (let i = 0; i < 5; i++)
      parameters.listRequest({
        params: { ...params, active: requestActiveData },
        setIsLoading: setIsLoading,
        dataAction: (data) => [
          ...data.filter((d) => d.active),
          ...data.filter((d) => !d.active),
        ],
        setData: setData,
        onSuccess: () => {
          listListener.update({
            values: parameters.toList_ListenersValues,
          });
        },
        onFailure: ({ data }) => {
          ErrorPopup(
            data.responseMessage ??
              data.responseCode ??
              t("Error Displaying Data")
          );
        },
      });
      // }
    }
    // listListener.update({ values: params.toList_ListenersValues });
    return () => {
      setAddModel(false);
      setUpdateModel(false);
      setShowModel(false);
      setKey(0);
    };
  }, [key, parameters.listRequestParams, requestActiveData]);

  // ** Function to handle Model toggle
  const handleAddModel = () => setAddModel((addModel) => !addModel);
  const handleUpdateModel = () => setUpdateModel((updateModel) => !updateModel);
  const handleShowModel = () => setShowModel((showModel) => !showModel);
  const handleRestModels = (modelName) => {
    setRestModels((models) => ({ ...models, [modelName]: !models[modelName] }));
  };

  // ** handle delete
  const handleActivation = (e, row, selector) => {
    e.preventDefault();
    row[selector] = !row[selector];

    const requestParams =
      parameters.postRequestParams ?? parameters.requestParams;
    const allParams = { ...requestParams, ...row };
    parameters.updateRequest({
      id: parameters.putWithId ? row[parameters.idField?.selector] : "",
      body: parameters.mixinRequestBodyQuery ? allParams : row,
      params: parameters.mixinRequestBodyQuery ? allParams : requestParams,
      setIsLoading: setIsLoading,
      onSuccess: () => {
        if (row[selector]) SuccessPopup(t("Success active"));
        else SuccessPopup(t("Success unActive"));
        forceUpdate();
      },
    });
  };
  // ** handle update
  const handleUpdate = (e, row) => {
    e.preventDefault();
    // parameters.getRequest({
    //   id: row[parameters.idField?.selector],
    //   params: parameters.getRequestParams ?? parameters.requestParams,
    //   setIsLoading: setIsLoading,
    //   setData: setItem,
    //   onSuccess: () => {
    //     handleUpdateModel();
    //   },
    //   onFailure: (_) => {
    //     ErrorPopup("حدث خطأ أثناء عرض البيانات");
    //   },
    // });
    setItem((i) => row);
    handleUpdateModel();
  };
  // ** handle show
  const handleShow = (e, row) => {
    e.preventDefault();
    // parameters.getRequest({
    //   id: row[parameters.idField?.selector],
    //   params: parameters.getRequestParams ?? parameters.requestParams,
    //   setIsLoading: setIsLoading,
    //   setData: setItem,
    //   onSuccess: () => {
    //     handleShowModel();
    //   },
    //   onFailure: (_) => {
    //     ErrorPopup("حدث خطأ أثناء عرض البيانات");
    //   },
    // });
    setItem((i) => row);
    handleShowModel();
  };
  // ** handleDelete
  const handleDelete = (id) => {
    ConfirmCancelPopup(
      t("Are you sure deleting this element ?"),
      t("Confirm")
    ).then((out) => {
      if (out.isConfirmed) {
        parameters.deleteRequest({
          id,
          params: parameters.deleteRequestParams ?? parameters.requestParams,
          setIsLoading: setIsLoading,
          onSuccess: (_) => {
            SuccessPopup(t("Success Delete"));
            forceUpdate();
          },
          onFailure: (_) => {
            ErrorPopup(
              data.responseMessage ?? data.responseCode ?? t("Error Delete")
            );
          },
        });
      }
    });
  };
  // ** handle rest models (each page.component)
  const handleRest = (e, row, name) => {
    e.preventDefault();
    setItem((i) => row);
    handleRestModels(name);
  };
  const restPagesComponentParameters = {
    parameters: parameters,
    forceUpdate: forceUpdate,

    pages: parameters.pages,
    idSelector: parameters.idField?.selector,
    handleModelWithName: (name) => handleRestModels(name),
    handleModels: handleRest,
  };
  const restIconsComponentParameters = {
    ...restPagesComponentParameters,
    color: defaultIconColor,
    // size: 20,
    className: iconsClassNames,
    id: item[parameters.idField?.selector],
    item: item,
  };

  const handlers = {};
  parameters.pages
    .filter((p) => p.component)
    .map(
      (p) =>
        (handlers[p.name] = {
          open: restModels[p.name] ?? false,
          handleModel: () => handleRestModels(p.name),
        })
    );

  let extractionTableHeaders = InitExtractionTableHeaders(parameters);

  let columns = initTableColumns(parameters, handleActivation, t);
  // if (data[0]?.active !== undefined) columns.push({});
  columns.push({
    maxWidth: "100px",
    allowOverflow: true,
    cell: (row) => {
      return (
        <div className="d-flex">
          {parameters.enableShow ? (
            parameters.showPopupPage ? (
              <div>
                <Eye
                  color={showIconColor}
                  // size={25}
                  className={iconsClassNames}
                  onClick={(e) => handleShow(e, row)}
                />
              </div>
            ) : (
              <>
                <Link
                  to={parameters.showUrlTitle.replace(
                    ":id",
                    row[parameters.idField?.selector]
                  )}
                >
                  <Eye
                    color={showIconColor}
                    // size={25}
                    className={iconsClassNames}
                    // onClick={(e) => {
                    //   params.setItem(row);
                    // }}
                  />
                </Link>
              </>
            )
          ) : (
            <></>
          )}
          {parameters.enableEdit ? (
            parameters.editPopupPage ? (
              <div>
                <Edit
                  color={editIconColor}
                  // size={25}
                  className={iconsClassNames}
                  onClick={(e) => handleUpdate(e, row)}
                />
              </div>
            ) : (
              <>
                <Link
                  // to={parameters.pages
                  //   .find((page) => page.name === StandardPages.EDIT)
                  //   .url.replace(":id", row[parameters.idField?.selector])}
                  to={parameters.editUrlTitle.replace(
                    ":id",
                    row[parameters.idField?.selector]
                  )}
                >
                  <Edit
                    color={editIconColor}
                    // size={25}
                    className={iconsClassNames}
                    // onClick={(e) => {
                    //   params.setItem(row);
                    // }}
                  />
                </Link>
              </>
            )
          ) : (
            <></>
          )}
          {parameters.enableDelete ? (
            <div>
              <Trash2
                // color="#DC3545"
                // color="#CC5555"
                color={deleteIconColor}
                // size={25}
                className={iconsClassNames}
                onClick={(_) => {
                  handleDelete(row[parameters.idField?.selector]);
                }}
              />
            </div>
          ) : (
            <></>
          )}
        </div>
      );
    },
  });
  // columns .push( initStandardPagesIcons(parameters))
  columns = [
    ...columns,
    ...initRestPagesIcons({
      translate: t,
      ...restIconsComponentParameters,
    }),
  ];
  const AddPage = pages[StandardPages.ADD_MODEL];
  const EditPage = pages[StandardPages.EDIT_MODEL];
  const ShowPage = pages[StandardPages.SHOW_MODEL];

  const extractExambleExcelBtn = {
    name: "Example EXCEL",
    group: "Add File",
    disable: !parameters.enableAddByFile,

    onClick: () => {
      downloadExampleExcel([], {
        title: parameters.tableTitle + "_examble",
        headers: extractionTableHeaders,
      });
    },
    btn: (params) => <RiFileExcel2Line onClick={params.onClick} size={15} />,
  };
  const addByFileBtn = {
    name: "Add File",
    group: "Add File",
    disable: !parameters.enableAddByFile,

    onClick: (e) =>
      excel_to_JSON({
        file: e.target.files[0],
      }).then((data) => {
        // TODO add multi-data post request
      }),

    btn: (params) => (
      <>
        <Label for="excel-uploader" className="btn btn-primary">
          {params.title}
        </Label>
        <Input
          multiple
          type="file"
          className="hidden"
          id="excel-uploader"
          onChange={params.onClick}
        />
      </>
    ),
  };
  return (
    <>
      <div className="app-user-list">
        {parameters.title && parameters.title != "" ? (
          // typeof parameters.title === "string" ? (
          // <BreadCrumbs list={breadcrumbs} />
          // ) : (
          //   parameters.title
          // )
          <BreadCrumbs list={breadcrumbs} />
        ) : (
          <></>
        )}
        {parameters.enablePagination ? (
          <PaginationTable
            rowDetailsComponent={parameters.rowDetailsComponent}
            showAllSwitch={parameters.enableActiveFilter}
            showAllSwitchTitle={""}
            customSwitchHandle={(e) => {
              setRequestActiveData((_) => e.target.checked);
            }}
            buttons={[
              extractExambleExcelBtn,
              addByFileBtn,
              ...parameters.pages.filter((page) => page.btn),
            ]}
            buttonsParams={restPagesComponentParameters}
            buttonModelHandlers={handleRestModels}
            enableSearch={parameters.enableSearch}
            enableExtraction={parameters.enableExtract}
            tableTitle={t(parameters.tableTitle)}
            refresherKey={key}
            extractFunction={({ requestDataSize, filter, appendData }) => {
              const lastPage = Math.ceil(totalElements / requestDataSize);
              for (let i = 0; i < lastPage; i++) {
                parameters.listRequest({
                  params: {
                    ...(parameters.listRequestParams ??
                      parameters.requestParams),
                    active: requestActiveData,
                    ...RequestedPaginationParams(
                      i + RequestedStartPageNo,
                      requestDataSize,
                      filter
                    ),
                  },
                  setData: appendData,
                });
              }
            }}
            extractionTableHeaders={extractionTableHeaders}
            columns={columns}
            data={data}
            totalRecords={totalElements ?? data.length}
            onPagination={(pageNo, pageSize, searchField) => {
              const params = {
                ...(parameters.listRequestParams ?? parameters.requestParams),
                ...RequestedPaginationParams(
                  pageNo + RequestedStartPageNo,
                  pageSize,
                  searchField
                ),
              };
              // if (!isDuplicatedObject(requestParamsListener, params)) {
              //   requestParamsListener = params;
              // for (let i = 0; i < 2; i++)
              parameters.listRequest({
                params: params,
                setIsLoading: setIsLoading,
                dataAction: (data) => {
                  setTotalElements(ResponseTotalElements(data));

                  // return [
                  //   ...data.filter((d) => d.active),
                  //   ...data.filter((d) => !d.active),
                  // ];
                  return data;
                },
                setData: setData,
                onFailure: (_) => {
                  ErrorPopup(
                    data.responseMessage ??
                      data.responseCode ??
                      t("Error Displaying Data")
                  );
                },
              });
              // }
            }}
            isLoading={isLoading}
            addButtonText={t("Add")}
            handleAddButton={handleAddModel}
            addBtnUrl={
              parameters.addPopupPage ? undefined : parameters.addUrlTitle
            }
            enableAdd={parameters.enableAdd}
          />
        ) : (
          <Table
            showAllSwitch={parameters.enableActiveFilter}
            showAllSwitchTitle={""}
            customSwitchHandle={(e) => {
              setRequestActiveData((_) => e.target.checked);
            }}
            tableTitle={parameters.tableTitle}
            columns={columns}
            data={data}
            searchFields={searchFields}
            progress={isLoading}
            handleAddModal={handleAddModel}
            showExtractBtn={parameters.enableExtract}
            extractionTableHeaders={extractionTableHeaders}
            showSearch={parameters.enableSearch}
            showAddBtn={parameters.enableAdd}
            addingButton={
              parameters.addPopupPage ? undefined : parameters.addUrlTitle
            }
          />
        )}
      </div>
      {parameters.addPopupPage && addModel ? (
        <AddPage
          parameters={parameters}
          open={addModel}
          handleModel={handleAddModel}
          forceUpdate={forceUpdate}
          setItem={setItem}
        />
      ) : (
        <></>
      )}

      {parameters.editPopupPage && updateModel ? (
        <EditPage
          parameters={parameters}
          open={updateModel}
          handleModel={handleUpdateModel}
          item={item}
          forceUpdate={forceUpdate}
        />
      ) : (
        <></>
      )}

      {parameters.showPopupPage && showModel ? (
        <ShowPage
          parameters={parameters}
          open={showModel}
          handleModel={handleShowModel}
          item={item}
        />
      ) : (
        <></>
      )}
      {restPageComponents({
        pages: parameters.pages,
        handlers: handlers,

        parameters: parameters,
        forceUpdate: forceUpdate,
        id: item[parameters.idField?.selector],
        item: item,
        setItem: setItem,
      })}
      {/* <UploadFileModel
        isOpen={openFileUploader}
        handleModel={handleFileUploader}
        postRequest={parameters.postRequest}
        params={parameters.postRequestParams}
      /> */}
    </>
  );
};

const initBreadcrumbs = (title) => [
  {
    link: null,
    name: title,
    bold: true,
  },
];
const initSearchFields = (params) => {
  let selectors = params.searchFields;
  if (typeof selectors === "undefined") {
    selectors = [];
    params.forms.map(
      (form) =>
        (selectors = [
          ...selectors,
          ...(form.fields
            ?.filter((col) => col.selector)
            .map((col) => col.selector) ?? []),
        ])
    );
  }
  return selectors;
};
const InitExtractionTableHeaders = (params) => {
  const { t } = useTranslation();

  let headers = [];
  params.forms?.map((form) => {
    form.fields
      ?.filter(
        (f) =>
          Object.keys(f).length > 0 &&
          !f.hide &&
          !f.hideExtract &&
          f.type !== Types.SWITCH
      )
      .map((f) =>
        headers.push({
          name: t(f.name),
          selector: f.labelSelector ?? f.selector,
          value: f.value ?? f.defaultValue,
        })
      );
    form.fields
      ?.filter(
        (f) =>
          Object.keys(f).length > 0 &&
          !f.hide &&
          !f.hideExtract &&
          f.type === Types.SWITCH
      )
      .map((f) =>
        headers.push({
          name: t(f.name),
          selector: f.selector,
          value: f.value ?? f.defaultValue,
        })
      );
  });
  return headers;
};
const initTableColumns = (params, handleActivation, t) => {
  let columns = [];
  params.forms?.map((form) => {
    form.fields
      ?.filter(
        (f) =>
          Object.keys(f).length > 0 &&
          !f.hide &&
          !f.hideList &&
          f.type !== Types.SWITCH
      )
      .map((f) => ({
        ...f,
        name: t(f.name),
        selector: f.labelSelector ?? f.selector,
      }))
      .map((f) => columns.push(f));

    form.fields
      ?.filter((f) => !f.hide && !f.hideList && f.type === Types.SWITCH)
      .map((f) =>
        columns.push({
          name: t(f.name),
          cell: (row) => {
            return (
              <div className="d-flex">
                <CustomInput
                  type="switch"
                  key={
                    f.activeInList && !f.unEditable
                      ? row[params.idField?.selector]
                      : ""
                  }
                  id={
                    f.activeInList && !f.unEditable
                      ? row[params.idField?.selector]
                      : ""
                  }
                  name={
                    f.activeInList && !f.unEditable
                      ? row[params.idField?.selector]
                      : ""
                  }
                  defaultChecked={row[f.selector]}
                  onClick={(e) => {
                    if (f.activeInList && !f.unEditable)
                      handleActivation(e, row, f.selector);
                  }}
                />
              </div>
            );
          },
        })
      );
  });
  return columns;
};
const initRestPagesIcons = ({
  pages,
  translate,
  idSelector = "",
  handleModels,
  ...restParams
}) => {
  const columns = [];
  columns.push({
    allowOverflow: true,
    cell: (row) => {
      return pages
        .filter((p) => p.icon)
        .map((page) => {
          let disable = page.disable;
          if (typeof page.disable === "function") {
            disable = page.disable(row);
          }
          return disable ? (
            <></>
          ) : page.url ? (
            <>
              <Link
                key={`Link_${page.name}`}
                to={page.url.replace(":id", row[idSelector])}
              >
                {page.icon({
                  title: translate(page.name),
                  ...restParams,
                  ...row,
                  // onClick: page.onClick,
                })}
              </Link>
            </>
          ) : (
            <div key={`Div_${page.name}`}>
              {page.icon({
                title: translate(page.name),
                ...restParams,
                ...row,
                onClick: (e) => {
                  page.onClick && page.onClick(e);
                  handleModels(e, row, page.name);
                },
              })}
            </div>
          );
        });
    },
  });
  return columns;
};
const restPageComponents = ({ pages, handlers, ...restParams }) => {
  return (
    <>
      {pages.map((page) => {
        const Component = page.component;
        return handlers[page.name]?.open ? (
          <Component {...{ ...restParams, ...handlers[page.name] }} />
        ) : (
          <></>
        );
      })}
    </>
  );
};
