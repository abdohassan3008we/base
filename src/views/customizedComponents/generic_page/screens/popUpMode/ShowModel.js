// ** React Imports
import React from "react";
import { Card } from "reactstrap";

// ** Styles
// import '@styles/react/libs/flatpickr/flatpickr.scss'

// ** State & Actions
import { useFormik } from "formik";
import { CustomModel, FormMode } from "../..";
import { PageContent } from "../pageContent";
import { useTranslation } from "react-i18next";

/**
 *@author Mohamed Ashraf Hamza
 */
export const ShowModel = ({ parameters, open, handleModel, item }) => {
  const { t } = useTranslation();
  const formik = useFormik({
    initialValues: { ...item },
    enableReinitialize: true,
  });

  return (
    <CustomModel
      body={
        <Card style={{ textAlign: "center" }}>
          <PageContent
            // key={key}
            mode={FormMode.SHOW}
            parameters={parameters}
            formik={formik}
          />
        </Card>
      }
      handleModel={handleModel}
      open={open}
      title={t(parameters.showPageTitle)}
      maxWidth={parameters.showPopupWidth}
    />
  );
};
