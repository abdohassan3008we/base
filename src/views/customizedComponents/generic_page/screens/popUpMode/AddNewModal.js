// ** React Imports
import React from "react";

// ** Store & Actions
import { CustomModel } from "../..";
import { AddNewForm } from "./AddNewForm";
import { useTranslation } from "react-i18next";

/**
 *@author Mohamed Ashraf Hamza
 */
export const AddNewModel = ({
  parameters,
  open,
  handleModel,
  setItem,
  forceUpdate,
}) => {
  const { t } = useTranslation();
  return (
    <CustomModel
      body={
        <AddNewForm
          parameters={parameters}
          handleAddModel={handleModel}
          setItem={setItem}
          forceUpdate={forceUpdate}
        />
      }
      handleModel={handleModel}
      open={open}
      title={t(parameters.addPageTitle)}
      maxWidth={parameters.addPopupWidth}
    />
  );
};
