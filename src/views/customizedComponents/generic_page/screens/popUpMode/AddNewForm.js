import { useFormik } from "formik";
import { useState } from "react";
import { Button, Card } from "reactstrap";
import { ErrorPopup, SuccessPopup, Types } from "../..";
import { ignoreDropDownKeys, PageContent } from "../pageContent";
import { useTranslation } from "react-i18next";

/**
 *@author Mohamed Ashraf Hamza
 */
export const AddNewForm = ({
  parameters,
  handleAddModel,
  setItem,
  forceUpdate,
}) => {
  const { t } = useTranslation();
  const ERROR_MESSAGE = t("Incorrect Field Data");

  const [savable, setSavable] = useState(false);
  const [unSavableErrorMessage, setUnSavableErrorMessage] =
    useState(ERROR_MESSAGE);
  const handleAdd = () => {
    formik.submitForm();
    handleAddModel();
    setSavable((save) => false);
  };
  const initValues = initFormikValues(parameters);

  const formik = useFormik({
    initialValues: initValues,

    enableReinitialize: true,
    onSubmit: (values) => {
      values = ignoreDropDownKeys(values);

      if (savable) {
        const requestParams =
          parameters.postRequestParams ?? parameters.requestParams;
        const allParams = { ...requestParams, ...values };
        parameters.postRequest({
          body: parameters.mixinRequestBodyQuery ? allParams : values,
          params: parameters.mixinRequestBodyQuery ? allParams : requestParams,
          onSuccess: (item) => {
            SuccessPopup(t("Success Add"));
            setItem(item);
            forceUpdate();
          },
          onFailure: (data) => {
            ErrorPopup(
              data.responseMessage ?? data.responseCode ?? t("Error Add")
            );
          },
        });
      } else ErrorPopup(unSavableErrorMessage);
    },
  });
  return (
    <>
      <Card style={{ textAlign: "center" }}>
        <PageContent
          parameters={parameters}
          setActive={setSavable}
          errorMessage={ERROR_MESSAGE}
          setErrorMessage={setUnSavableErrorMessage}
          formik={formik}
        />
        <Button
          style={{ margin: "auto" }}
          disabled={!savable}
          color={savable ? "primary" : "secondary"}
          type="submit"
          onClick={handleAdd}
        >
          {t("Save")}
        </Button>
        <br />
      </Card>
    </>
  );
};

const initFormikValues = (params) => {
  const initValues = {};
  // params.forms?.map((form) =>
  //   form.fields?.map(
  //     (f) => (initValues[f.selector] = f.value ?? f.defaultValue ?? null)
  //   )
  // );
  params.forms?.map((form) =>
    form.fields
      ?.filter((f) => f.selector && f.type === Types.LIST)
      .map((f) => {
        if (f.defaultValue?.value) {
          initValues[f.selector] = f.defaultValue?.value;
          initValues[f.labelSelector] = f.defaultValue?.label;
        } else {
          initValues[f.selector] = f.defaultValue;
          initValues[f.labelSelector] = null;
        }
      })
  );
  params.forms?.map((form) =>
    form.fields
      ?.filter((f) => f.selector && f.type != Types.LIST)
      .map((f) => (initValues[f.selector] = f.defaultValue ?? null))
  );
  return initValues;
};
