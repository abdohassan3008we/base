// ** React Imports
import React, { useEffect, useState } from "react";
import { Button, Card } from "reactstrap";

// ** Styles
// import '@styles/react/libs/flatpickr/flatpickr.scss'

// ** State & Actions
import { useFormik } from "formik";
import { CustomModel, ErrorPopup, SuccessPopup, FormMode } from "../..";
import { ignoreDropDownKeys, PageContent } from "../pageContent";
import { Listener } from "../../classes";
import { useTranslation } from "react-i18next";

/**
 *@author Mohamed Ashraf Hamza
 */
export const EditModel = ({
  parameters,
  open,
  handleModel,
  forceUpdate,
  item,
}) => {
  const { t } = useTranslation();
  const ERROR_MESSAGE = t("Incorrect Field data");

  const [editable, setEditable] = useState(false);
  const [unEditable_ErrorMessage, setUnEditable_ErrorMessage] =
    useState(ERROR_MESSAGE);

  // formik
  const formik = useFormik({
    initialValues: item,

    enableReinitialize: true,
    onSubmit: (values) => {
      values = ignoreDropDownKeys(values);

      const requestParams =
        parameters.postRequestParams ?? parameters.requestParams;
      const allParams = { ...requestParams, ...values };
      parameters.updateRequest({
        id: parameters.putWithId ? values[parameters.idField?.selector] : "",
        body: parameters.mixinRequestBodyQuery ? allParams : values,
        params: parameters.mixinRequestBodyQuery ? allParams : requestParams,
        onSuccess: () => {
          SuccessPopup(t("Success Update"));
          forceUpdate();
        },
        onFailure: (data) => {
          ErrorPopup(
            data.responseMessage ?? data.responseCode ?? t("Error Update")
          );
        },
      });
    },
  });
  // the form listener to track any change
  useEffect(() => {
    parameters["_formListener_"] =
      parameters["_formListener_"] ??
      new Listener({
        listeners: Object.keys(item),
        values: item,
      });
    return () => {
      parameters["_formListener_"] = undefined;
    };
  }, [item]);
  const handleEdit = () => {
    if (editable) {
      formik.submitForm();
      setEditable((edit) => false);
      handleModel();
    } else {
      ErrorPopup(unEditable_ErrorMessage);
    }
  };
  return (
    <CustomModel
      body={
        <Card style={{ textAlign: "center" }}>
          <PageContent
            parameters={parameters}
            mode={FormMode.EDIT}
            setActive={setEditable}
            errorMessage={ERROR_MESSAGE}
            setErrorMessage={setUnEditable_ErrorMessage}
            formik={formik}
          />
          <Button
            style={{ margin: "auto" }}
            disabled={!editable}
            color={editable ? "primary" : "secondary"}
            type="submit"
            onClick={handleEdit}
          >
            {t("update")}
          </Button>
          <br />
        </Card>
      }
      handleModel={handleModel}
      open={open}
      title={t(parameters.editPageTitle)}
      maxWidth={parameters.editPopupWidth}
    />
  );
};
