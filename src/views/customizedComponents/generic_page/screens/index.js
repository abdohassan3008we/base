export { DataTable } from "./masterPage";
export { AddPage, EditPage, ShowPage } from "./newPageMode";
export { AddNewForm, AddNewModel, EditModel, ShowModel } from "./popUpMode";
export { PageContent, Forms ,Form} from "./pageContent";
export { PrintingModel } from "./navbarContent";

export const StandardPages = {
  MASTER: "master",
  MASTER_MODEL: "master_model",

  ADD: "add",
  ADD_MODEL: "add_model",
  ADD_FORM: "add_form",

  EDIT: "edit",
  EDIT_MODEL: "edit_model",

  SHOW: "show",
  SHOW_MODEL: "show_model",
};
