import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
// import Forms from "./Forms";
import {
  disableDeleteButton,
  disablePrintButton,
  disableSaveButton,
  disableUploadButton,
  enableDeleteButton,
  enablePrintButton,
  enableSaveButton,
  enableUploadButton,
  updateNumberUploadButton,
} from "../../../../../redux/reducers/navbar/actionCreators";
import { useDispatch } from "react-redux";
import { ignoreDropDownKeys, PageContent } from "../pageContent";
import { useParams } from "react-router-dom";
// import { searchAttachmentRequestAsync } from "../../../../../redux/admin/attachement/thunk";
import {
  ErrorPopup,
  SuccessPopup,
  PageTitle,
  BreadCrumbs,
  ConfirmCancelPopup,
  FormMode,
  pageURLs,
  Types,
} from "../..";
import { NavbarContent } from "../navbarContent";
import { Listener } from "../../classes";
import { useTranslation } from "react-i18next";

import { prevPageLink } from ".";

/**
 * @author Mohamed Ashraf Hamza
 */
export function EditPage({ parameters, history }) {
  const { t } = useTranslation();
  const ERROR_MESSAGE = t("Incorrect Field Data");
  const { id } = useParams();

  const dispatch = useDispatch();
  const breadcrumbs = initBreadcrumbs(
    t(parameters.title),
    prevPageLink({
      pageURL: parameters.editUrlTitle,
      prevURL: parameters.urlTitle,
    }),
    t(parameters.editPageTitle)
  );
  const [updatable, setUpdatable] = useState(false);
  const [unUpdatable_ErrorMessage, setUnUpdatable_ErrorMessage] =
    useState(ERROR_MESSAGE);

  const [key, setKey] = useState(0);
  const [tabsKey, setTabsKey] = useState(0);
  const forceUpdate = () => setKey((k) => k + 1);
  const tabsforceupdate = () => setTabsKey((k) => k + 1);

  // GET item data request
  useEffect(() => {
    parameters.getRequest({
      id: id,
      params: parameters.getRequestParams ?? parameters.requestParams,
      setData: parameters.setItem,
      onSuccess: (data) => {
        if (data.length) data = data[0]; // if the object returned as list
        parameters?.forms?.map((form) => {
          form.fields
            ?.filter((field) => Object.keys(data).includes(field.selector))
            ?.map(
              (field) => {
                formik?.setFieldValue(field.selector, data[field.selector]);
              }
              // field["value"] = data[field.selector]
            );
          form.fields
            ?.filter((f) => f.type === Types.LIST)
            .map((field) => {
              formik?.setFieldValue(field.selector, data[field.selector]);
              formik?.setFieldValue(
                field.labelSelector,
                data[field.labelSelector]
              );
              //   field["value"] = {
              //   value: data[field.selector],
              //   label: data[field.labelSelector],
              // }
            });
        });
        updateListenerValues(data);
        forceUpdate();
      },
      onFailure: (data) => {
        ErrorPopup(
          data?.responseMessage ??
            data?.responseCode ??
            "حدث خطأ أثناء عرض البيانات"
        );
      },
    });
    return () => formik?.resetForm();
  }, [tabsKey]);

  // formik
  const initValues = initFormikValues(parameters);

  const formik = useFormik({
    initialValues: {},
    enableReinitialize: true,
    isValidating: false,
    validateOnChange: false,
    validateOnBlur: false,
    onSubmit: (values) => {
      values = ignoreDropDownKeys(values);

      if (updatable) {
        const requestParams =
          parameters.updateRequestParams ?? parameters.requestParams;
        const allParams = { ...requestParams, ...values };

        parameters.updateRequest({
          id: parameters.putWithId ? id : "",
          body: parameters.mixinRequestBodyQuery ? allParams : values,
          params: parameters.mixinRequestBodyQuery ? allParams : requestParams,
          onSuccess: () => {
            updateListenerValues(values);
            SuccessPopup("تم التعديل بنجاح");
            forceUpdate();
          },
          onFailure: (data) => {
            ErrorPopup(
              data.responseMessage ??
                data.responseCode ??
                "حدث خطأ أثناء التعديل"
            );
          },
        });
      } else ErrorPopup(unUpdatable_ErrorMessage);
    },
  });
  // the form listener to track any change
  useEffect(() => {
    parameters["_formListener_"] =
      // parameters["_formListener_"] ??
      new Listener({
        listeners: Object.keys(initValues),
        values: initValues,
      });
    // return () => {
    //   parameters["_formListener_"] = undefined;
    // };
  }, []);
  const updateListenerValues = (values) => {
    parameters._formListener_?.update({ values: values });
  };
  // models actions states
  const [openPrintModel, setOpenPrintModel] = useState(false);
  const [openAttachment, setOpenAttachment] = useState(false);
  const [openLogsModel, setOpenLogsModel] = useState(false);
  // handle actions
  const handlePrintModel = () => {
    setOpenPrintModel((e) => !e);
  };
  const handleAttachmentModel = () => {
    setOpenAttachment((e) => !e);
  };
  const handleLogsModel = () => {
    setOpenLogsModel((e) => !e);
  };
  // const handleSave = formik?.submitForm;
  const handleUpdate = formik?.submitForm;

  const handleDelete = () => {
    ConfirmCancelPopup("هل انت متأكد من حذف هذا النموذج؟", "تأكيد").then(
      (out) => {
        if (out.isConfirmed) {
          parameters.deleteRequest({
            id,
            params: parameters.deleteRequestParams ?? parameters.requestParams,
            // setIsLoading: setIsLoading,
            onSuccess: (_) => {
              SuccessPopup("تم الحذف بنجاح");

              const masterUrl = pageURLs({
                title: parameters.urlTitle,
                id,
              })["master"];
              history.push(masterUrl);
            },
            onFailure: (data) => {
              ErrorPopup(
                data.responseMessage ?? data.responseCode ?? "تعذر الحذف!"
              );
            },
          });
        }
      }
    );
  };
  // get the attachments data
  useEffect(() => {
    // parameters.enableAttachment &&
    // dispatch(
    //   searchAttachmentRequestAsync({
    //     formId: id,
    //     formTypeCode: parameters.requestParams?.formTypeCode,
    //   })
    // );
  }, []);

  // enable actions buttons
  useEffect(() => {
    parameters.enableEdit && dispatch(enableSaveButton(handleUpdate, "تعديل"));
    parameters.enableDelete && dispatch(enableDeleteButton(handleDelete));
    parameters.enablePrint && dispatch(enablePrintButton(handlePrintModel));
    parameters.enableAttachment &&
      dispatch(enableUploadButton(handleAttachmentModel));
    // params.enableLogs && dispatch(enablePrintButton(handlePrintModel));

    return (_) => {
      dispatch(disableSaveButton());
      dispatch(disableDeleteButton());
      dispatch(disablePrintButton());
      dispatch(disableUploadButton());

      formik?.resetForm({ values: {} });
    };
  }, []);

  return (
    <>
      <PageTitle title={parameters.editPageTitle} />
      <BreadCrumbs list={breadcrumbs} />
      <NavbarContent
        formTypeCode={parameters.requestParams?.formTypeCode}
        parameters={parameters}
        id={id}
        //
        openPrinting={openPrintModel}
        handlePrintingModal={handlePrintModel}
        //
        openAttachment={openAttachment}
        handleAttachmentModal={handleAttachmentModel}
        //
        openLogs={openLogsModel}
        handleLogsModal={handleLogsModel}
      />
      <PageContent
        key={key} // REQUIRED
        parameters={parameters}
        mode={FormMode.EDIT}
        setActive={setUpdatable}
        errorMessage={ERROR_MESSAGE}
        setErrorMessage={setUnUpdatable_ErrorMessage}
        formik={formik}
        tabParams={{ forceUpdate: tabsforceupdate, ...formik?.values }}
      />
    </>
  );
}

const initBreadcrumbs = (prevPageTitle, prevPageLink, pageTitle) => [
  {
    name: prevPageTitle,
    link: prevPageLink,
    bold: true,
  },
  {
    name: pageTitle,
    link: null,
    bold: true,
  },
];

const initFormikValues = (params) => {
  const initValues = {};
  params.forms?.map((form) =>
    form.fields
      ?.filter((f) => f.selector && f.type === Types.LIST)
      .map((f) => {
        if (f.value?.value) {
          initValues[f.selector] = f.value?.value;
          initValues[f.labelSelector] = f.value?.label;
        } else {
          initValues[f.selector] = f.value;
          initValues[f.labelSelector] = null;
        }
      })
  );
  params.forms?.map((form) =>
    form.fields
      ?.filter((f) => f.selector && f.type != Types.LIST)
      .map((f) => (initValues[f.selector] = f.value ?? null))
  );
  return initValues;
};
