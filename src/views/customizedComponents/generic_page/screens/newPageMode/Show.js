import React, { useEffect, useState } from "react";
import { PageContent } from "../pageContent";
import { useParams } from "react-router-dom";
import { PageTitle, BreadCrumbs, FormMode, ErrorPopup } from "../..";
import { useTranslation } from "react-i18next";

import { prevPageLink } from ".";
const initBreadcrumbs = (prevPageTitle, prevPageLink, pageTitle) => [
  {
    name: prevPageTitle,
    link: prevPageLink,
    bold: true,
  },
  {
    name: pageTitle,
    link: null,
    bold: true,
  },
];

/**
 *@author Mohamed Ashraf Hamza
 */
export function ShowPage({ parameters }) {
  const { id } = useParams();
  const { t } = useTranslation();

  const breadcrumbs = initBreadcrumbs(
    t(parameters.title),
    prevPageLink({
      pageURL: parameters.showUrlTitle,
      prevURL: parameters.urlTitle,
    }),
    t(parameters.showPageTitle)
  );
  const [key, setKey] = useState(0);

  const forceUpdate = () => setKey((k) => k + 1);

  useEffect(() => {
    parameters.getRequest({
      id: id,
      params: parameters.getRequestParams ?? parameters.requestParams,
      setData: parameters.setItem,
      onSuccess: (data) => {
        parameters.forms.map((form) =>
          form.fields
            .filter((field) => Object.keys(data).includes(field.selector))
            .map((field) => (field["value"] = data[field.selector]))
        );
        forceUpdate();
      },
      onFailure: (data) => {
        ErrorPopup(
          data.responseMessage ??
            data.responseCode ??
            t("Error Displaying Data")
        );
      },
    });
  }, []);

  return (
    <>
      <PageTitle title={parameters.showPageTitle} />
      <BreadCrumbs list={breadcrumbs} />
      <PageContent
        key={key}
        parameters={parameters}
        mode={FormMode.SHOW}
        // disable
        // setActive={setSavable}
      />
    </>
  );
}
