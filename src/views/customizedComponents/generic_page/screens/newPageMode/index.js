export { AddPage } from "./Add";
export { EditPage } from "./Edit";
export { ShowPage } from "./Show";

const duplicate = (s, times) => {
  let str = "";
  for (let i = 0; i < times; i++) {
    str += s;
  }
  return str;
};
export const prevPageLink = ({ prevURL, pageURL }) => {
  return (
    "./" +
    duplicate("../", pageURL.replaceAll("/", " ").trim().split(" ").length) +
    prevURL
  );
};
