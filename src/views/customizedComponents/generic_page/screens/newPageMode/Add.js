import React, { useEffect, useState } from "react";

import { useFormik } from "formik";
import {
  PageTitle,
  SuccessPopup,
  ErrorPopup,
  BreadCrumbs,
  pageURLs,
  Types,
} from "../..";
// import Forms from "./Forms";
import {
  disableSaveButton,
  enableSaveButton,
} from "../../../../../redux/reducers/navbar/actionCreators";
import { useDispatch } from "react-redux";
import { ignoreDropDownKeys, PageContent } from "../pageContent";
import { useTranslation } from "react-i18next";

import { prevPageLink } from ".";

/**
 * @author Mohamed Ashraf Hamza
 */
export function AddPage({ parameters, history }) {
  const { t } = useTranslation();

  const ERROR_MESSAGE = t("Incorrect Field Data");
  const dispatch = useDispatch();
  const breadCrumbs = initBreadCrumbs(
    t(parameters.title),
    prevPageLink({
      pageURL: parameters.addUrlTitle,
      prevURL: parameters.urlTitle,
    }),
    t(parameters.addPageTitle)
  );

  const [savable, setSavable] = useState(false);
  const [unSavableErrorMessage, setUnSavableErrorMessage] =
    useState(ERROR_MESSAGE);

  // const [item, setItem] = useState({});
  // parameters["item"] = item;
  // parameters["setItem"] = (item) => {
  //   setItem(item);
  //   parameters["item"] = item;
  // };

  const initValues = initFormikValues(parameters);
  const formik = useFormik({
    initialValues: initValues,

    enableReinitialize: true,
    onSubmit: (values) => {
      values = ignoreDropDownKeys(values);

      if (savable) {
        const requestParams =
          parameters.postRequestParams ?? parameters.requestParams;
        const allParams = { ...requestParams, ...values };
        parameters.postRequest({
          body: parameters.mixinRequestBodyQuery ? allParams : values,
          params: parameters.mixinRequestBodyQuery ? allParams : requestParams,
          onSuccess: (item) => {
            SuccessPopup(t("Success Add"));
            parameters.setItem(item);
            const editUrl = pageURLs({
              title: parameters.urlTitle,
              id: item[parameters.idField?.selector],
            })["edit"];
            history.push(editUrl);
          },
          onFailure: (data) => {
            ErrorPopup(
              data.responseMessage ?? data.responseCode ?? t("Error Add")
            );
          },
        });
      } else ErrorPopup(unSavableErrorMessage);
    },
  });
  const handleSave = formik.submitForm;

  useEffect(() => {
    dispatch(enableSaveButton(handleSave));
    return (_) => {
      dispatch(disableSaveButton());
      formik.resetForm({ values: {} });
    };
  }, []);

  return (
    <>
      <PageTitle title={parameters.addPageTitle} />
      <BreadCrumbs list={breadCrumbs} />
      <PageContent
        parameters={parameters}
        setActive={setSavable}
        errorMessage={ERROR_MESSAGE}
        setErrorMessage={setUnSavableErrorMessage}
        formik={formik}
      />
    </>
  );
}

const initBreadCrumbs = (prevPageTitle, prevPageLink, pageTitle) => [
  {
    name: prevPageTitle,
    link: prevPageLink,
    bold: true,
  },
  {
    name: pageTitle,
    link: null,
    bold: true,
  },
];

const initFormikValues = (params) => {
  const initValues = {};
  params.forms?.map((form) =>
    form.fields
      ?.filter((f) => f.selector && f.type === Types.LIST)
      .map((f) => {
        if (f.defaultValue?.value) {
          initValues[f.selector] = f.defaultValue?.value;
          initValues[f.labelSelector] = f.defaultValue?.label;
        } else {
          initValues[f.selector] = f.defaultValue;
          initValues[f.labelSelector] = null;
        }
      })
  );
  params.forms?.map((form) =>
    form.fields
      ?.filter((f) => f.selector && f.type != Types.LIST)
      .map((f) => (initValues[f.selector] = f.defaultValue ?? null))
  );
  return initValues;
};
