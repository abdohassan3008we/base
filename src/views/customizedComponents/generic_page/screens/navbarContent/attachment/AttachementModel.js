// ** React Imports
import React, { useEffect, useState } from "react";
// import Table from "@src/views/customizedComponents/Table";
import { Eye, PlusCircle, Download } from "react-feather";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  Row,
  Col,
  Container,
  Card,
  CardHeader,
  Form,
  CardTitle,
  CardBody,
  FormGroup,
  Input,
} from "reactstrap";
import { useFormik } from "formik";

// ** Store & Actions
import { useDispatch, useSelector } from "react-redux";
// import { addAttachmentRequestAsync } from "@src/redux/admin/attachement/thunk";
import proImage from "./attachedFile.png";
import { updateNumberUploadButton } from "@src/redux/reducers/navbar/actionCreators";
import { CustomModel, ErrorPopup, Table } from "../../..";

export const AttachmentModel = ({ isOpen, handleModel, id, formTypeCode }) => {
  const dispatch = useDispatch();

  const attachmentStore = useSelector((state) => state.AdminAttachmentReducer);

  const [attachmentState, setAttachmentState] = useState(null);
  // On file select (from the pop up)
  const onFileChange = (event) => {
    // Update the state
    const file = event.target.files[0];
    if (file) {
      const extension = file.name.split(".").pop().toLowerCase();
      file["typeAr"] =
        extension === "jpg" || extension === "png" || extension === "jpeg"
          ? "صورة"
          : extension === "pdf"
          ? "PDF"
          : extension === "xls" || extension === "csv" || extension === "xlsx"
          ? "جدول بيانات"
          : extension === "pptx"
          ? "عرض تقديمي"
          : extension === "doc" || extension === "docx"
          ? "ملف وورد"
          : extension === "txt"
          ? "ملف نص"
          : "";
    }
    setAttachmentState(file);
  };

  // useEffect(() => {
  // attachmentStore.searchData = [];
  // setAttachmentState(null);
  // dispatch(
  //   searchAttachmentRequestAsync({
  //     formId: id,
  //     formTypeCode,
  //   })
  // );
  // dispatch(listattachTypeRequestAsync());
  // return () => {
  //   setAttachmentState(null);
  // };
  // }, []);

  useEffect(() => {
    dispatch(updateNumberUploadButton(attachmentStore.searchData.length));
  }, [attachmentStore.searchData]);

  const formik = useFormik({
    initialValues: {
      active: true,
      versionNo: 0,
      uploadSiteId: parseInt(
        JSON.parse(localStorage.getItem("userData")).siteId
      ),
      uploadUserId: parseInt(
        JSON.parse(localStorage.getItem("userData")).userId
      ),
      formId: id,
      formTypeCode,
      uploadDate: new Date().toISOString().slice(0, 10),
      attachTypeCode: "",
      attachmentDesc: "",
      remarks: "",
      fileEncoding: "",
      fileContent: "",
      fileExtension: "",
      fileName: "",
    },
    onSubmit: (values) => {
      if (attachmentState) {
        values["fileExtension"] = attachmentState.name.split(".").pop();
        const extension = values["fileExtension"].toLowerCase();
        if (
          extension !== "png" &&
          extension !== "jpeg" &&
          extension !== "jpg" &&
          extension !== "xls" &&
          extension !== "xlsx" &&
          extension !== "csv" &&
          extension !== "pdf" &&
          extension !== "pptx" &&
          extension !== "doc" &&
          extension !== "docx" &&
          extension !== "txt"
        ) {
          ErrorPopup("نوع الملف غير مدعوم");
          setAttachmentState(null);
          return;
        }
        values["fileName"] = attachmentState.name;
        const reader = new FileReader();
        reader.readAsDataURL(attachmentState);
        reader.onload = function () {
          const [first, ...rest] = reader.result.split(",");
          values["fileContent"] = rest.join(",");
          values["fileEncoding"] = first;
          // dispatch(addAttachmentRequestAsync(values));
          setAttachmentState(null);
        };
      } else {
        ErrorPopup("من فضلك اختر ملف قبل الرفع");
      }
    },
  });

  const fileData = () => {
    if (attachmentState) {
      return (
        <div style={{ marginBottom: "20px" }}>
          <h2>بيانات الملف</h2>
          <Row>
            <Col sm={6}>
              <p>اسم الملف : </p>
              <p style={{ color: "#B8922B" }}>{attachmentState.name}</p>
            </Col>
            <Col sm={6}>
              <p>نوع الملف : </p>
              <p style={{ color: "#B8922B" }}>{attachmentState.typeAr}</p>
            </Col>
          </Row>
        </div>
      );
    } else {
      return <div></div>;
    }
  };

  // to show attachements as tiles
  const handleAttachments = () => {
    return (
      <>
        {" "}
        {attachmentStore.searchData && attachmentStore.searchData.length > 0 ? (
          <div>
            <Row
              style={{
                border: "2px solid #B8922B",
                borderRadius: "20px",
                padding: "10px",
              }}
            >
              {attachmentStore.searchData.map(function (object, i) {
                return (
                  <Col style={{ textAlign: "center" }} sm={2}>
                    <a
                      href={`${object.fileEncoding},${object.fileContent}`}
                      download={object.fileName}
                    >
                      <div
                        style={{
                          display: "table-caption",
                          textAlign: "center",
                        }}
                      >
                        <img width={"65px"} src={proImage} />
                        <Label>{object.fileName}</Label>
                      </div>
                    </a>
                  </Col>
                );
              })}
            </Row>
          </div>
        ) : (
          <label>Loading...</label>
        )}{" "}
      </>
    );
  };
  const download = (row) => {
    const a = document.createElement("a");
    a.href = `${row.fileEncoding},${row.fileContent}`;
    a.download = row.fileName;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  };
  const AttachmentsList = () => {
    const columns = [
      {
        name: "اسم الملف",
        selector: "fileName",
        sortable: true,
        minWidth: "300px",
      },
      {
        name: "نوع الملف",
        selector: "fileExtension",
        sortable: true,
        cell: (row) => {
          const extension = row.fileExtension.toLowerCase();
          return extension === "jpg" ||
            extension === "png" ||
            extension === "jpeg" ? (
            <label>صورة</label>
          ) : extension === "pdf" ? (
            <label>PDF</label>
          ) : extension === "xls" ||
            extension === "csv" ||
            extension === "xlsx" ? (
            <label>جدول بيانات</label>
          ) : extension === "pptx" ? (
            <label>عرض تقديمي</label>
          ) : extension === "doc" || extension === "docx" ? (
            <label>ملف وورد</label>
          ) : extension === "txt" ? (
            <label>ملف نص</label>
          ) : (
            <></>
          );
        },
      },
      {
        name: "تاريخ الإضافة",
        selector: "uploadDate",
        sortable: true,
        minWidth: "120px",
      },
      {
        name: "وصف الملف",
        selector: "attachmentDesc",
        sortable: true,
      },
      {
        name: "ملاحظات",
        selector: "remarks",
        sortable: true,
      },
      {
        name: "عرض",
        cell: (row) => {
          const extension = row.fileExtension.toLowerCase();
          return extension === "jpg" ||
            extension === "png" ||
            extension === "jpeg" ||
            extension === "pdf" ? (
            <div>
              <Eye
                size={14}
                className="mr-50 cursor-pointer"
                onClick={(_) => showFrame(row)}
              />
            </div>
          ) : (
            <></>
          );
        },
      },
      {
        name: "تحميل",
        cell: (row) => (
          <div>
            <Download
              size={14}
              className="mr-50 cursor-pointer"
              onClick={(_) => download(row)}
            />
          </div>
        ),
      },
    ];
    return (
      <Table
        columns={columns}
        data={attachmentStore.searchData}
        showAddBtn={false}
        showExtractBtn={false}
        showSearch={false}
        progress={attachmentStore.isLoading}
      />
    );
  };

  const showFrame = (row) => {
    if (row.fileExtension === "pdf") {
      const pdfWindow = window.open("");
      pdfWindow.document.write(
        `<iframe width='100%' height='100%' src='data:application/pdf;base64,${encodeURI(
          row.fileContent
        )}'></iframe>`
      );
    } else {
      const base64URL = `data:image/${row.fileExtension};base64,${row.fileContent}`;
      const imgWin = window.open("");
      imgWin.document.write(
        `<iframe src="${base64URL}" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>`
      );
    }
  };

  return (
    <CustomModel
      body={
        <>
          <Card>
            <CardBody>
              <hr />
              <Form onSubmit={formik.handleSubmit}>
                <Container>
                  <Row
                    style={{
                      border: "2px solid #B8922B",
                      borderRadius: "20px",
                      padding: "10px 0px",
                      marginBottom: "20px",
                    }}
                  >
                    <Col sm={12}>
                      <div style={{ textAlign: "center" }}>
                        <label
                          style={{
                            cursor: "pointer",
                            width: "100%",
                          }}
                        >
                          <PlusCircle
                            size={50}
                            className="mr-50"
                            style={{ color: "#B8922B" }}
                          />
                          <input
                            style={{ display: "none" }}
                            type="file"
                            onChange={onFileChange}
                            accept=".jpeg, .jpg, .png, .xls, .xlsx, .csv, .pdf, .pptx, .doc, .docx, .txt,"
                          />
                        </label>
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={12}>{fileData()}</Col>
                    {/*  <Col sm={4}>
                <CustomDropDown
                arabicIdentifier='نوع الملف'
                keyName='attachType'
                isAdd={true}
                data={attachTypeStore.data}
                formik={formik}
                >
                </CustomDropDown>
              </Col>*/}
                  </Row>
                  <Row>
                    <Col sm={6}>
                      <FormGroup>
                        <Label for="attachmentDesc">وصف الملف</Label>
                        <Input
                          type="textarea"
                          name="attachmentDesc"
                          id="attachmentDesc"
                          placeholder="أدخل وصف الملف"
                          onChange={formik.handleChange}
                          style={{ minHeight: "70px", maxHeight: "100px" }}
                        />
                      </FormGroup>
                    </Col>
                    <Col sm={6}>
                      <FormGroup>
                        <Label for="remarks">ملاحظات</Label>
                        <Input
                          type="textarea"
                          name="remarks"
                          id="remarks"
                          placeholder="أدخل بعض الملاحظات"
                          onChange={formik.handleChange}
                          style={{ minHeight: "70px", maxHeight: "100px" }}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <div style={{ textAlign: "end" }}>
                    <Row>
                      <Col sm="12">
                        <FormGroup>
                          <Button.Ripple color="primary" type="submit">
                            رفع
                          </Button.Ripple>
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                </Container>
              </Form>
              <hr />
            </CardBody>
          </Card>
          <Card>
            <CardBody>
              <AttachmentsList />
            </CardBody>
          </Card>
        </>
      }
      handleModel={handleModel}
      open={isOpen}
      title={"المرفقات"}
      key={"ATTACHEMENT_MODEL"}
      maxWidth="70%"
    />
  );
};
