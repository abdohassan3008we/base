// import { PrintingModel, AttachmentModel, LogsModel } from ".";

import { LogsModel } from "./logs";
import { AttachmentModel } from ".";
import { PrintingModel } from "./printing";

/**
 *@author Mohamed Ashraf Hamza
 */
export function NavbarContent({
  parameters,
  id,
  formTypeCode,

  openPrinting,
  handlePrintingModal,
  openAttachment,
  handleAttachmentModal,
  openLogs,
  handleLogsModal,
}) {
  return (
    <>
      {openPrinting && (
        <PrintingModel
          isOpen={openPrinting}
          handleModel={handlePrintingModal}
          files={parameters.reports ?? []}
        />
      )}
      {openAttachment && (
        <AttachmentModel
          isOpen={openAttachment}
          handleModel={handleAttachmentModal}
          id={id}
          formTypeCode={formTypeCode}
        />
      )}
      {openLogs && (
        <LogsModel isOpen={openLogs} handleModel={handleLogsModal} />
      )}
    </>
  );
}
