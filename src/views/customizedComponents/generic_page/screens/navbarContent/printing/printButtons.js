import React from "react";
import { Row } from "reactstrap";
import { PrintReport } from "../../../../../reports";

export const PrintingButtons = ({
  showCard = false,
  reports = [],
  buttonScale = 1,
  height,
  width,
} = {}) => {
  return (
    <div>
      <Row style={{ margin: "auto", justifyContent: "center" }}>
        {reports.map((report) => (
          <div style={{ padding: "10px" }}>
            <PrintReport
              height={height}
              width={width}
              Document={report.component}
              title={report.title}
              documentDownloadName={report.fileName}
              pageLayout={report.pageLayout?.toLowerCase()}
              showCard={showCard}
              scale={buttonScale}
            />
          </div>
        ))}
      </Row>
    </div>
  );
};
