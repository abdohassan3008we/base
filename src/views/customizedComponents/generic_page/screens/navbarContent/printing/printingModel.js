import { useTranslation } from "react-i18next";
import { PrintingButtons } from ".";
import { CustomModel } from "../../..";

export const PrintingModel = ({
  title = "Printing",
  isOpen,
  handleModel,
  showCard = false,
  cardSizeScale = 1,
  files = [],
  // width,
  // height,
}) => {
  const {t}  =useTranslation()
  const num = showCard ? files.length : files.length / 3;
  const cardsCountInRow = Math.ceil(Math.sqrt(num));
  return (
    <CustomModel
      title={t(title)}
      open={isOpen}
      handleModel={handleModel}
      body={
        <>
          <PrintingButtons
            reports={files}
            showCard={showCard}
            buttonScale={cardSizeScale}
            // height={height}
            // width={width}
          />
        </>
      }
      minWidth={`${cardSizeScale * 250}px`}
      maxWidth={Math.min(cardsCountInRow * 20 * cardSizeScale, 80) + "%"}
    />
  );
};
