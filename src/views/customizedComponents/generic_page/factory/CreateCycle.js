// import { lazy } from "react";
import { lazy } from "react";
import { CreateScreens } from ".";
import { pageURLs } from "..";
import { StandardPages } from "../screens";
import { defaultPages } from "./defaults";

export const CreateCycle = ({ configObj = {}, pages = defaultPages } = {}) => {
  pages = initPages(pages, configObj.urlTitle);
  const configs = { ...configObj, pages };
  const screens = CreateScreens(configs);
  const Master = screens[StandardPages.MASTER];
  const routes = [
    {
      id: configObj.cycleId ?? 0,
      path: configObj.urlTitle,
      component: () => <Master />,
      exact: false,
    },
    ...pages?.map((page) => {
      const Page = screens[page.name];
      return {
        id: page.id ?? 0,
        path: page.url,
        component: () => <Page />,
        exact: false,
      };
    }),
  ];
  return screens;
  // return routes;
};

const initPages = (pages, urlTitle) => {
  return pages?.map((page) => {
    page["url"] =
      pageURLs({ title: urlTitle }).master + (page.url ?? `/${page.name}`);
    return page;
  });
};
