// ** React Imports
// ** Third Party Components
// ** Tables
import {
  AddNewModel,
  DataTable,
  EditModel,
  ShowModel,
  StandardPages,
} from "..";
import { createPagesObject, createParams } from "./params";
// ** Styles
// import "@styles/react/libs/tables/react-dataTable-component.scss";

/**
 *@author Mohamed Ashraf Hamza
 */

export const CreateMasterPageWithPopups = (input = {}) => {
  const p = createParams(input);
  const idField = p.forms?.map(
    (form) => form.fields?.filter((f) => f.id)[0]
  )[0];
  const parameters = { ...p, idField };
  const Master = parameters.pages.find(
    (p) => p.name === StandardPages.MASTER
  ).component;
  // const addPage = parameters.pages.find(page=>page.name===StandardPages.ADD_MODEL)[0].component
  return () => <Master parameters={parameters} />;
};
