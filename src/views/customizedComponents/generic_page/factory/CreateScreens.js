import {
  CreateMasterPageWithPopups,
  StandardPages,
  EditPage,
  ShowPage,
  AddPage,
  AddNewModel,
  ShowModel,
  AddNewForm,
  EditModel,
} from "..";
import { createPagesObject, createParams } from "./params";

// ** Styles
// import "@styles/react/libs/tables/react-dataTable-component.scss";

/**
 *@author Mohamed Ashraf Hamza
 */
export const CreateScreens = (input = {}) => {
  const p = createParams(input);
  const idField = p.forms?.map(
    (form) => form.fields?.filter((f) => f.id)[0]
  )[0];
  const parameters = { ...p, idField };

  const pages = createPagesObject(parameters);
  return {
    [StandardPages.MASTER]: CreateMasterPageWithPopups(input),
    [StandardPages.ADD]: ({ history }) => (
      <AddPage parameters={parameters} history={history} />
    ),
    [StandardPages.EDIT]: ({ history }) => (
      <EditPage parameters={parameters} history={history} />
    ),
    [StandardPages.SHOW]: ({ history }) => (
      <ShowPage parameters={parameters} history={history} />
    ),
    [StandardPages.ADD_MODEL]: ({
      open,
      handleModel,
      setItem,
      forceUpdate,
    }) => (
      <AddNewModel
        parameters={parameters}
        open={open}
        handleModel={handleModel}
        forceUpdate={forceUpdate}
        setItem={setItem}
      />
    ),
    [StandardPages.EDIT_MODEL]: ({ open, handleModel, item, forceUpdate }) => (
      <EditModel
        parameters={parameters}
        open={open}
        handleModel={handleModel}
        forceUpdate={forceUpdate}
        item={item}
      />
    ),
    [StandardPages.SHOW_MODEL]: ({ open, handleModel, item }) => (
      <ShowModel
        parameters={parameters}
        open={open}
        handleModel={handleModel}
        item={item}
      />
    ),
    [StandardPages.ADD_FORM]: ({
      handleAddModel = () => {},
      setItem = () => {},
      forceUpdate = () => {},
      // editParams = {},
      // addFields = {},
      // editFields = {},
    }) => {
      // let addFormParams = new parameters(parametersObj.getParams());
      // addFormParams.addParams(editParams);
      // let p = addFormParams.getParams();
      // for (let i = 0; i < p.forms?.length; i++) {
      //   for (let j = 0; j < p.forms[i].fields?.length; j++) {
      //     p.forms[i].fields[j] = {
      //       ...p.forms[i].fields[j],
      //       ...editFields[p.forms[i].fields[j].selector],
      //     };
      //   }
      // }
      return (
        <>
          <AddNewForm
            parameters={parameters}
            forceUpdate={forceUpdate}
            handleAddModel={handleAddModel}
            setItem={setItem}
          />
        </>
      );
    },
    ...pages,
  };
};
