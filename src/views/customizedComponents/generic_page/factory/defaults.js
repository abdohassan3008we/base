import { Edit, Eye } from "react-feather";
import {
  AddNewModel,
  EditModel,
  ShowModel,
  ShowPage,
  EditPage,
  AddPage,
  StandardPages,
  DataTable,
} from "../screens";
const defaultIconColor = "#888888";
const showIconColor = "#4587cd";
const editIconColor = "#B8922B";
const deleteIconColor = "#DC4545";

export const defaultPages = [
  { id: 0, url: "", name: StandardPages.MASTER_MODEL, component: DataTable },
  {
    id: 1,
    url: "",
    name: StandardPages.MASTER,
    component: DataTable,
  },
  {
    id: 10,
    url: "",
    name: StandardPages.ADD_MODEL,
    component: AddNewModel,
  },
  {
    id: 11,
    url: "/new",
    name: StandardPages.ADD,
    component: AddPage,
  },
  {
    id: 20,
    url: "",
    name: StandardPages.EDIT_MODEL,
    component: EditModel,
  },
  {
    id: 21,
    url: "/:id/edit",
    name: StandardPages.EDIT,
    component: EditPage,
  },

  {
    id: 30,
    url: "",
    name: StandardPages.SHOW_MODEL,
    component: ShowModel,
  },
  {
    id: 31,
    url: "/:id",
    name: StandardPages.SHOW,
    component: ShowPage,
  },
];
// export const defaultPages = {
//   [StandardPages.ADD_MODEL]: AddNewModel,
//   [StandardPages.ADD]: AddPage,
//   [StandardPages.EDIT_MODEL]: EditModel,
//   [StandardPages.EDIT]: EditPage,
//   [StandardPages.SHOW_MODEL]: ShowModel,
//   [StandardPages.SHOW]: ShowPage,
// };
