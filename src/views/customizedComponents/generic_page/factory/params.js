import { getContentPath, pageURLs, StandardPages } from "..";
import {
  createGetRequest,
  createListRequest,
  createDeleteRequest,
  createPostRequest,
  createUpdateRequest,
} from "../../../../utility/services/requests";
import { defaultPages } from "./defaults";

/**
 *@author Mohamed Ashraf Hamza
 */
export const createParams = (input = {}) => {
  const [master, add, edit, show, restPages] = initPages({
    inputURLTitle: input.urlTitle,
    inputPages: input?.pages,
  });

  return {
    //////////////////////////////////////////////////
    //** deafult values **//
    urlTitle: master?.url,
    addUrlTitle: add?.url,
    editUrlTitle: edit?.url,
    showUrlTitle: show?.url,
    // titles
    title: "Data Display",
    editPageTitle: "Data Editing",
    showPageTitle: "Data Show",
    addPageTitle: "Data Adding",
    tableTitle: input.title ?? "Data table",
    // enable actions in table
    enablePagination: false,
    enableAdd: false,
    enableExtract: true,
    enableSearch: true,
    searchFields: undefined,
    // enable pages
    enableShow: false,
    enableEdit: false,
    enableDelete: false,
    // enable actions in navbar
    enablePrint: false,
    enableAttachment: false,
    enableLogs: false,
    // pages mode
    popupPages: true,
    popupWidth: "50%",
    addPopupPage: input.popupPages ?? false,
    addPopupWidth: input.popupWidth ?? "50%",
    editPopupPage: input.popupPages ?? false,
    editPopupWidth: input.popupWidth ?? "50%",
    showPopupPage: input.popupPages ?? false,
    showPopupWidth: input.popupWidth ?? "50%",
    // requests
    mixinRequestBodyQuery: false,

    // content
    forms: [],
    tabs: [],
    reports: [],

    //////////////////////////////////////////////////
    //** input **//
    ...input,
    //////////////////////////////////////////////////
    //** override or add parameters **//

    // requests
    // request function
    listRequest: createListRequest(
      requestFunctionObject({ input, method: "list" })
    ),
    getRequest: createGetRequest(
      requestFunctionObject({ input, method: "get" })
    ),
    postRequest: createPostRequest(
      requestFunctionObject({ input, method: "post" })
    ),
    updateRequest: createUpdateRequest(
      requestFunctionObject({ input, method: "put" })
    ),
    deleteRequest: createDeleteRequest(
      requestFunctionObject({ input, method: "del" })
    ),
    // // request
    putWithId: input.put?.withId ?? false,
    delWithId: input.del?.withId ?? false,
    // request params
    requestParams: input.params ?? {},
    listRequestParams: input.list?.params,
    getRequestParams: input.get?.params,
    updateRequestParams: input.put?.params,
    postRequestParams: input.post?.params,
    deleteRequestParams: input.del?.params,

    // request listeners
    toList_Listeners: input.list?.listeners
      ? Object.keys(input.list?.listeners) ?? []
      : undefined,
    toList_ListenersValues: input.list?.listeners,

    put_EnableFormListening: input.put?.enableFormListening ?? true,
    // post_EnableFormListening: input.post?.enableFormListening ?? true,

    // contents
    pages: restPages,
  };
};

function requestFunctionObject({ input, method = "get" }) {
  const [endPoint, selectors] = getContentPath(input.endPoint);
  const [methodEndPoint, methodSelectors] = getContentPath(
    input[method]?.endPoint
  );
  return {
    breaker: input[method]?.breaker ?? input.breaker,
    api: input[method]?.api ?? input.api,
    endPoint: methodEndPoint ?? endPoint,
    action: (data) => {
      if (methodEndPoint) {
        methodSelectors
          ?.filter((sel) => sel && sel != "")
          .map((sel) => (data = data[sel] ?? data));
      } else {
        selectors
          ?.filter((sel) => sel && sel != "")
          .map((sel) => (data = data[sel] ?? data));
      }
      return input[method]?.dataAction
        ? input[method]?.dataAction(data)
        : input.dataAction
        ? input.dataAction(data)
        : data;
    },
    ifSuccess: input[method]?.onSuccess ?? input.onSuccess,
    ifFailure: input[method]?.onFailure ?? input.onFailure,
  };
}

export const createPagesObject = (parameters) => {
  let pages = {};
  parameters.pages?.map((page) => {
    const Page = page.component;
    pages = {
      ...pages,
      [page.name]: (params) => {
        return page.disable ? (
          <></>
        ) : (
          <Page {...params} parameters={parameters} />
        );
      },
    };
  });
  return pages;
};
const addInputPagesToDefaultPages = ({ defaultPages, inputPages }) => {
  // reverse pages to  read the last added pages
  inputPages = inputPages.reverse();
  // add default pages with replacing the old page with new-defined-same-name one
  const allPages = defaultPages.map((page) => {
    const p = inputPages.find((p) => p.name === page.name);
    return p ? { ...page, ...p } : page;
  });
  // add new defined pages
  inputPages
    .filter((p) => !allPages.find((page) => page.name === p.name))
    .map((p) => allPages.push(p));

  return allPages;
  // return [
  //   ...defaultPages.map((page) => {
  //     return { ...page, url: urlTitle + (page.url ?? `/${page.name}`) };
  //   }),
  //   ...inputPages,
  // ];
};

const createDefaultPagesWithNewURL = ({ inputURLTitle = "" }) => {
  const urlTitle = pageURLs({
    title: inputURLTitle,
  })["master"];
  const addUrlTitle = pageURLs({
    title: inputURLTitle,
  })["add"];
  const editUrlTitle = pageURLs({
    title: inputURLTitle,
  })["edit"];
  const showUrlTitle = pageURLs({
    title: inputURLTitle,
  })["show"];

  const defaultPagesClone = defaultPages.slice().map((p) => {
    switch (p.name) {
      case StandardPages.MASTER:
        return { ...p, url: urlTitle };
      case StandardPages.ADD:
        return { ...p, url: addUrlTitle };
      case StandardPages.EDIT:
        return { ...p, url: editUrlTitle };
      case StandardPages.SHOW:
        return { ...p, url: showUrlTitle };
      default:
        return p;
    }
  });
  return defaultPagesClone;
};

const initPages = ({ inputURLTitle = "", inputPages = [] }) => {
  const pages = addInputPagesToDefaultPages({
    inputPages: inputPages ?? [],
    defaultPages: createDefaultPagesWithNewURL({
      inputURLTitle: inputURLTitle,
    }),
  });
  const master = pages.find((p) => p.name === StandardPages.MASTER);
  const add = pages.find((p) => p.name === StandardPages.ADD);
  const edit = pages.find((p) => p.name === StandardPages.EDIT);
  const show = pages.find((p) => p.name === StandardPages.SHOW);
  return [master, add, edit, show, pages];
};
