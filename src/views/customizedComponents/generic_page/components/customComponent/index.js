export {
  Counter,
  DatePicker,
  DateTimePicker,
  TimePicker,
  Email,
  Float,
  InputText,
  Integer,
  NID as NationalID,
  NumberText,
  Password,
  PhoneNumber,
  StrongPassword,
  Switch,
  Text,
  TextArea,
  RadianGroup,
  RadianButton,
  CheckBox,
  List,
  ImageField,
  ValidationField,
  Message,
  ErrorMessage,
  SuccessMessage,
  WarningMessage,
} from "./fieldTypes";
export {
  PageTitle,
  BreadCrumbs,
  Tabs,
  CustomDatePicker,
  Table,
  NewLine,
  PaginationTable,
  Space,
  ScapeChar2XML,
  Tab,
  DropdownButton,
} from "./general";
export { UploadFileModel, CustomModel } from "./models";

export { DataSelect } from "./fieldTypes/dropDown/DataSelect";

export const lookup = ({ field, values, lookupFun }) => {
  let output = {};
  if (field.lookup && Object.keys(field.lookup).length > 0) {
    const keys = Object.keys(field.lookup);
    keys.map((k) => {
      const sepIndex = k.indexOf("::");
      const selector = sepIndex === -1 ? k : k.slice(0, sepIndex);
      const lookupValue = sepIndex === -1 ? null : k.slice(sepIndex + 2);

      const obj = field.lookup[k];
      // const formikValue = formik.values[selector];
      // const formikValue = getKeyContains({
      //   values: values,
      //   subKey: selector,
      // });
      const formikValue = values[selector];

      output = lookupFun({ lookupValue, formikValue, obj }) ?? output;
    });
  }
  return output;
};

export const getKeyContains = ({ values, subKey }) => {
  let keyValue;
  for (const key of Object.keys(values)) {
    if (key.includes(subKey)) {
      keyValue = values[key];
      break;
    }
  }
  return keyValue;
};
