import { ValidationField } from ".";
import { Validator } from "../../../classes/validator";

export const InputText = ({
  id,
  disable,
  field,
  formik,
  lang = "ar", // "ar" | "en"
}) => {
  switch (lang) {
    default:
    case "ar":
      let ar_validation = new Validator("أدخل حروف عربية وأرقام فقط").pattern(
        "^[\u0621-\u064A\u0660-\u06690-9 ]*$"
      );
      if (field.required) ar_validation = ar_validation.required();
      return (
        <ValidationField
          id={id}
          errorMessage="خطأ في الإدخال"
          disable={disable}
          field={field}
          formik={formik}
          validation={ar_validation}
        />
      );

    case "en":
      let en_validation = new Validator(
        "أدخل حروف إنجليزية وأرقام فقط"
      ).pattern("^[a-zA-Z0-9 ]*$");
      if (field.required) en_validation = en_validation.required();
      return (
        <ValidationField
          id={id}
          errorMessage="خطأ في الإدخال"
          disable={disable}
          field={field}
          formik={formik}
          validation={en_validation}
        />
      );
  }
};
