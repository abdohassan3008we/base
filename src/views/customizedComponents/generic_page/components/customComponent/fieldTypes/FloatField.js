import { Validator } from "../../../classes/validator";
import { ValidationField } from ".";

export const Float = ({ id, disable, field, formik }) => {
  let validation = new Validator("أدخل رقم").number().min(0);
  if (field.required) validation = validation.required();

  return (
    <ValidationField
      id={id}
      type="number"
      disable={disable}
      field={field}
      formik={formik}
      validation={validation}
    />
  );
};
