import { Text } from ".";

export const TextArea = ({ id, disable, field, formik }) => {
  return (
    <Text
      id={id}
      disable={disable}
      field={field}
      formik={formik}
      type="textarea"
      style={{
        minHeight: "50px",
        maxHeight: "250px",
      }}
    />
  );
};
