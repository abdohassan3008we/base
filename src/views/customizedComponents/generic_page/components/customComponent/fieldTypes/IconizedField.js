export const IconizedField = ({
  component,
  icons,
  paddingTop = "50%",
  paddingStart = 5,
}) => {
  return (
    <>
      <div style={{ width:"100%",display: "stack", position: "relative" }}>
        {component}
        {icons?.map((icon, index) => {
          return (
            <div
              key={"icon_div_" + index}
              style={{
                position: "absolute",
                zIndex: "2",
                top: paddingTop,
                left: `${index * 7 + paddingStart}%`,
              }}
            >
              {icon}
            </div>
          );
        })}
      </div>
    </>
  );
};
