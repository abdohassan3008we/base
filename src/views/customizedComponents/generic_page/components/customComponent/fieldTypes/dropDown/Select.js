import { useEffect, useState } from "react";
import { Button, Label } from "reactstrap";
import { Plus, PlusCircle, PlusSquare } from "react-feather";
import ReactSelect, { components } from "react-select";
import { selectThemeColors } from "@utils";
import { CustomModel } from "../../..";
import { IconizedField } from "../IconizedField";
import { useTranslation } from "react-i18next";

export function Select({
  name = "",
  id = "",
  label = "",

  isLoading = false,
  isDisabled = false,
  isClearable = true,
  isRtl = true,
  isMulti = false,

  optionComponent,
  customComponents = {},

  theme = selectThemeColors,
  loadingMessage,
  noOptionsMessage = "No Data Found",
  menuShouldScrollIntoView = true,

  placeholder = "",
  defaultValue = undefined,
  data = [],

  onChange,
  onInputChange,
  isCreatable = false,
  addTitle = "إضافة",
  addComponent = () => <></>,
  isSearchable = true,
}) {
  const { t } = useTranslation();
  const addBtnData = {
    type: "add_button",
    value: t(addTitle),
    label: t(addTitle),
  };
  if (data && data.length) {
    data = data.map((d) => (typeof d != "object" ? { value: d, label: d } : d));
  }

  const [isOpen, setIsOpen] = useState(false);
  const [selected, setSelected] = useState(undefined);
  const [key, setKey] = useState(1);
  const forceUpdate = () => {
    setKey((key) => key + 1);
  };

  const toggle = () => {
    setIsOpen((prevToggle) => !prevToggle);
  };
  const OptionComponent =
    optionComponent ??
    function ({ data, ...props }) {
      return (
        <components.Option {...props}>
          {data.label ?? data.value}
        </components.Option>
      );
    };

  const SingleValue = ({ children, ...props }) => (
    <components.SingleValue {...props}>
      <span title={children}>{children}</span>
    </components.SingleValue>
  );
  const AddModel = () =>
    addComponent({
      handleAddModel: toggle,
      setItem: (item) => {
        setSelected(item?.value);
        onChange(item || { value: "", label: "" });
      },
      forceUpdate,
    });

  return (
    <>
      <IconizedField
        key={key}
        component={
          <>
            {label && <Label for={id}>{label}</Label>}
            <ReactSelect
              key={key}
              name={name}
              id={id}
              isMulti={isMulti}
              isSearchable={isSearchable}
              isDisabled={isDisabled}
              isClearable={isClearable}
              backspaceRemovesValue={false}
              tabSelectedOptions={false}
              isOptionDisabled={(option) =>
                typeof option.active != "undefined" && option.active
              }
              menuShouldScrollIntoView={menuShouldScrollIntoView}
              placeholder={placeholder}
              isRtl={isRtl}
              menuPlacement="auto"
              menuPortalTarget={document.body}
              minWidth="150px"
              styles={{
                menuPortal: (base) => ({
                  ...base,
                  zIndex: 9999,
                }),
              }}
              value={
                defaultValue?.value
                  ? defaultValue
                  : data?.find((o) => o?.value === defaultValue)
              }
              isLoading={isLoading}
              loadingMessage={loadingMessage}
              noOptionsMessage={
                isCreatable
                  ? () => AddBtn({ text: addBtnData.label, onClick: toggle })
                  : (_) => t(noOptionsMessage)
              }
              theme={theme}
              className="react-select"
              classNamePrefix="select"
              options={data}
              onInputChange={onInputChange}
              onChange={(s) => {
                setSelected(s?.value);
                onChange(s || { value: "", label: "" });
              }}
              components={{
                SingleValue,
                Option: OptionComponent,
                ...customComponents,
              }}
            />

            <CustomModel
              title={t(addTitle)}
              open={isOpen}
              handleModel={toggle}
              body={<AddModel />}
              maxWidth="60%"
            />
          </>
        }
        icons={[
          isCreatable && !selected ? (
            <PlusSquare
              color="#999999"
              style={{ cursor: "pointer" }}
              onClick={toggle}
            />
          ) : null,
        ]}
        paddingStart={15}
      />
    </>
  );
}
const AddBtn = ({ text, onClick, disabled }) => (
  <>
    <Button
      className="text-left rounded-0 w-100"
      outline
      color="success"
      onClick={onClick}
      disabled={disabled}
    >
      <Plus size={15} />
      <span className="align-middle ml-50">{text}</span>
    </Button>
  </>
);
