import { selectThemeColors } from "@utils";
import { Select } from "./Select";
import { useTranslation } from "react-i18next";

export function DataSelect({
  name = "",
  id = "",
  label = "",

  isLoading = false,
  isDisabled = false,
  isClearable = true,
  isRtl = true,
  isMulti = false,

  customComponents = {},

  theme = selectThemeColors,
  loadingMessage = "Loading",
  noOptionsMessage = "No Data Found",
  menuShouldScrollIntoView = true,

  placeholder = "",
  defaultValue = undefined,
  data = [],

  onChange,

  enableAdd = false,
  addTitle = "Add",
  addComponent = () => <></>,
  enableSearch = true,
}) {
  const { t } = useTranslation();
  
  return (
    <Select
      name={name}
      id={id}
      label={label}
      //
      isMulti={isMulti}
      isSearchable={enableSearch}
      isCreatable={enableAdd}
      isDisabled={isDisabled}
      isClearable={isClearable}
      isRtl={isRtl}
      //
      isLoading={isLoading}
      loadingMessage={t(loadingMessage) + "....."}
      noOptionsMessage={t(noOptionsMessage)}
      //
      defaultValue={defaultValue}
      placeholder={placeholder}
      data={data}
      //
      theme={theme}
      menuShouldScrollIntoView={menuShouldScrollIntoView}
      //
      addTitle={t(addTitle)}
      addComponent={addComponent}
      //
      customComponents={customComponents}
      //
      onChange={onChange}
    />
  );
}
