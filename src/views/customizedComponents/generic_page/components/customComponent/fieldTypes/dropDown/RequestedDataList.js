import { useEffect, useState } from "react";
import {
  ArrowRightCircle,
  ArrowLeftCircle,
  ArrowDownCircle,
  ArrowUpCircle,
} from "react-feather";
import { components } from "react-select";
import { selectThemeColors } from "@utils";
import { Select } from "./Select";
import { useTranslation } from "react-i18next";
import { RequestedStartPageNo } from "../../../..";

const DEFAULT_PAGE_SIZE = 5;

export function RequestedDataSelect({
  name = "",
  id = "",
  label = "",

  isLoading = false,
  isDisabled = false,
  isClearable = true,
  isRtl = true,
  isMulti = false,

  customComponents = {},

  theme = selectThemeColors,
  loadingMessage =  "Loading",
  noOptionsMessage =  "No Data Found",
  menuShouldScrollIntoView = true,

  placeholder = "",
  defaultValue = undefined,

  onChange,

  enableAdd = false,
  addTitle = "Add",
  addComponent = () => <></>,
  enableSearch = true,

  nextTitle = "",
  prevTitle = "",

  data = [],
  getDataRequest = () => {},
  totalItems = 0,
  pageSize = DEFAULT_PAGE_SIZE,

  refresherKey = 0,
}) {
  data = data ?? [];
  const allDataValue = data.map((item) => item.label).join(",");
  const { pageButtonsData } = initSelectButtonsData({
    nextTitle,
    prevTitle,
    allDataValue,
  });
  const [searchField, setSearchField] = useState("");
  const [pageNo, setPageNo] = useState(RequestedStartPageNo);
  const [options, setOptions] = useState([...data, pageButtonsData]);
  const [key, setKey] = useState(0);

  useEffect(() => {
    setPageNo((p) => RequestedStartPageNo);
    getDataRequest(RequestedStartPageNo, pageSize, searchField); // first request
  }, [searchField]);

  useEffect(() => {
    // to reduce the number of requests
    // no addition request with the first request
    if (key === 0) {
      setKey((k) => k + 1);
    } else {
      getDataRequest(pageNo, pageSize, searchField);
    }
  }, [pageNo, pageSize, refresherKey]);

  useEffect(() => {
    setOptions((o) => [...data, pageButtonsData]);
  }, [data]);

  const getNextPage = () => {
    setPageNo((page) => page + 1);
  };
  const getPrevPage = () => {
    setPageNo((page) => page - 1);
  };
  const OptionComponent = ({ data, ...props }) => {
    if (data.type === "page_buttons")
      return (
        <div
          style={{
            height: "40px",
            // justifyItems: "stretch",
            backgroundColor: "#B8922B",
            // justifyContent: "center",
          }}
        >
          <PaginationBtns
            prevTitle={data.prevTitle}
            nextTitle={data.nextTitle}
            pageNo={pageNo - RequestedStartPageNo + 1}
            pagesCount={Math.ceil(totalItems / pageSize)}
            handleNextPage={getNextPage}
            handlePrevPage={getPrevPage}
          />
        </div>
      );

    return <components.Option {...props}>{data.label}</components.Option>;
  };
  const { t } = useTranslation();

  return (
    <Select
      key={key}
      name={name}
      id={id}
      label={label}
      //
      isMulti={isMulti}
      isSearchable={enableSearch}
      isCreatable={enableAdd}
      isDisabled={isDisabled}
      isClearable={isClearable}
      isRtl={isRtl}
      //
      isLoading={isLoading}
      loadingMessage={t(loadingMessage) + "....."}
      noOptionsMessage={t(noOptionsMessage)}
      //
      defaultValue={defaultValue}
      placeholder={placeholder}
      data={options}
      //
      theme={theme}
      menuShouldScrollIntoView={menuShouldScrollIntoView}
      //
      addTitle={t(addTitle)}
      addComponent={addComponent}
      //
      customComponents={customComponents}
      //
      onInputChange={(input) => {
        setSearchField(input);
      }}
      onChange={onChange}
      //
      optionComponent={OptionComponent}
      //
    />
  );
}

const PaginationBtns = ({
  pageNo,
  pagesCount,
  prevTitle,
  nextTitle,
  handleNextPage,
  handlePrevPage,
}) => (
  <div style={{ margin: "0 auto" }}>
    <PrevBtn
      onClick={() => pageNo > 1 && handlePrevPage()}
      disabled={pageNo === 1}
      text={prevTitle}
    />
    <PageNumber pageNo={pageNo} pagesCount={pagesCount} />
    <NextBtn
      onClick={() => pageNo < pagesCount && handleNextPage()}
      disabled={pageNo >= pagesCount}
      text={nextTitle}
    />
  </div>
);
const PageNumber = ({ pageNo, pagesCount }) => (
  <span
    style={{
      // margin: "auto",
      // buttom: "0%",
      // backgroundColor: "#985476",
      paddingTop: "5px",
      position: "absolute",
      right: "42%",
      color: "#000",
    }}
  >
    <span style={{ fontSize: "150%" }}>{pageNo}</span>
    <span style={{ fontSize: "100%" }}>
      {"/"}
      {pagesCount}
    </span>
  </span>
);
const NextBtn = ({ text, onClick, disabled }) => (
  <span
    style={{
      padding: "8px",
      paddingInlineEnd: "20%",
      float: "left",
      cursor: disabled ? undefined : "pointer",
    }}
    onClick={onClick}
  >
    <span color={disabled ? "transparent" : undefined}>{text}</span>
    &nbsp;&nbsp;
    <ArrowDownCircle color={disabled ? "transparent" : "#000"} />
  </span>
);
const PrevBtn = ({ text, onClick, disabled }) => (
  <span
    style={{
      padding: "8px",
      paddingInlineStart: "20%",
      float: "right",
      cursor: disabled ? undefined : "pointer",
    }}
    onClick={onClick}
  >
    <ArrowUpCircle color={disabled ? "transparent" : "#000"} />
    &nbsp;&nbsp;
    <span color={disabled ? "transparent" : undefined}>{text}</span>
  </span>
);

const initSelectButtonsData = ({
  prevTitle = "prev",
  nextTitle = "next",
  allDataValue = "",
} = {}) => {
  const pageButtonsData = {
    type: "page_buttons",
    value: allDataValue,
    prevTitle: prevTitle,
    nextTitle: nextTitle,
    label: allDataValue,
  };
  return {
    pageButtonsData,
  };
};
