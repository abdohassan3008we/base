import { Calendar } from "react-feather";
import { Label } from "reactstrap";
import { CustomDatePicker } from "../general";
import { IconizedField } from "./IconizedField";

export const DatePicker = ({ id, disable, field, formik }) => {
  return (
    <>
      <IconizedField
        component={
          <>
            <Label for={field.selector}>{field.name}</Label>
            <CustomDatePicker
              enableTime={false}
              name={id ?? field.selector}
              defaultValue={
                field["_value_"] ??
                field.value ??
                formik?.values[field.selector] ??
                field["_defaultValue_"] ??
                field.defaultValue
              }
              disabled={disable}
              placeholder={field.placeholder}
              onChange={(date) => {
                formik.setFieldValue(field.selector, date);
              }}
            />
          </>
        }
        icons={[<Calendar color="#999999" />]}
      />
      <br />
    </>
  );
};

export const TimePicker = ({ id, disable, field, formik }) => <>comming soon</>;

export const DateTimePicker = ({ id, disable, field, formik }) => {
  return (
    <>
      <IconizedField
        component={
          <>
            <Label for={field.selector}>{field.name}</Label>
            <CustomDatePicker
              enableTime
              name={id ?? field.selector}
              defaultValue={
                field["_value_"] ??
                field.value ??
                formik?.values[field.selector] ??
                field["_defaultValue_"] ??
                field.defaultValue
              }
              disabled={disable}
              placeholder={field.placeholder}
              onChange={(date) => {
                formik.setFieldValue(field.selector, date);
              }}
            />
          </>
        }
        icons={[<Calendar color="#999999" />]}
      />
      <br />
    </>
  );
};
