import React, { useState, useEffect } from "react";
import { X } from "react-feather";
import { Button } from "reactstrap";
import { ConfirmCancelPopup } from "../../..";

import proImage from "./add_user_profile.png";
import { useTranslation } from "react-i18next";

export const ImageField = ({ id, disable, field, formik }) => {
  const [selectedImage, setSelectedImage] = useState(
    field.value ?? formik?.values[field.selector] ?? field.defaultValue
  );
  // const [selectedImage, setSelectedImage] = useState(null);
  useEffect(() => {
    if (selectedImage) {
      sessionStorage.setItem(
        `${field.selector}-UploadedImageBase64`,
        selectedImage
      );
    }
  }, []);

  const handleImageDeletion = () => {
    setSelectedImage((i) => null);
    sessionStorage.removeItem(`${field.selector}-UploadedImageBase64`);
  };
  const handleFileInputChange = (e) => {
    let file = e.target.files[0];
    getBase64(file)
      .then((content) => {
        file["base64"] = content;
        setSelectedImage(content);
        const type = file.type.split("/");
        formik?.setFieldValue(
          field.selector,
          // {content,name: file.name,extension: type[type.length - 1]}
          content
        );
      })
      .catch((err) => {
        setSelectedImage(file);
      });
  };
  const getBase64 = (file) => {
    return new Promise((resolve) => {
      let baseURL = "";
      // Make new FileReader
      let reader = new FileReader();
      // Convert the file to base64 text
      reader.readAsDataURL(file);
      // on reader load somthing...
      reader.onload = () => {
        baseURL = reader.result;
        resolve(baseURL);
        sessionStorage.setItem(
          `${field.selector}-UploadedImageBase64`,
          baseURL
        );
      };
    });
  };
  return (
    <>
      <h4>{field.name}</h4>
      <br />
      <div style={{ position: "relative" }}>
        <div style={{ position: "absolute", top: "0.5em", left: "1em" }}>
          <Controllers
            disable={!selectedImage || disable}
            handleDelete={handleImageDeletion}
            handleAdd={handleFileInputChange}
          />
        </div>
        <DefaultImage
          disable={selectedImage}
          handleAdd={handleFileInputChange}
          style={{
            direction: field.rtl ? "rtl" : field.ltr ? "ltr" : "rtl",
          }}
        />
        <Image
          disable={!selectedImage}
          imageSrc={selectedImage}
          style={{
            direction: field.rtl ? "rtl" : field.ltr ? "ltr" : "rtl",
          }}
        />
      </div>
      <br />
    </>
  );
};

const Controllers = ({ disable, handleAdd, handleDelete, ...rest }) => {
  const { t } = useTranslation();
  return disable ? (
    <></>
  ) : (
    <>
      <X
        className="active_icon"
        color={"red"}
        onClick={() => {
          ConfirmCancelPopup(
            t("Are you sure deleting this element ?"),
            t("Delete"),
            t("Cancel")
          ).then((out) => {
            if (out.isConfirmed) {
              handleDelete();
            }
          });
        }}
      />
      {/* <label className="cursor-pointer">
        <div
          color="primary"
          type="submit"
          className="cursor-pointer"
          //  onClick={() => {
          //    ConfirmCancelPopup("هل تريد الحذف ", "حذف").then((out) => {
          //      if (out.isConfirmed) {
          //        setSelectedImage(null);
          //        sessionStorage.removeItem(
          //          `${field.selector}-UploadedImageBase64`
          //        );
          //      }
          //    });
          //  }}
        >
          تغيير
        </div>
        <input
          id="file-input"
          style={{ display: "none" }}
          name="myImage"
          type={disable ? "" : "file"}
          accept="image/jpeg, image/jpg, image/png"
          onClick={() => {}}
          onChange={handleAdd}
        />
      </label> */}
    </>
  );
};

const DefaultImage = ({ disable, handleAdd, ...rest }) => {
  return disable ? (
    <></>
  ) : (
    <>
      <label className="cursor-pointer">
        <img id="my-image" width={"100%"} src={proImage} {...rest} />
        <input
          id="file-input"
          style={{ display: "none" }}
          name="myImage"
          type={disable ? "" : "file"}
          accept="image/jpeg, image/jpg, image/png"
          onClick={() => {}}
          onChange={handleAdd}
        />
      </label>
    </>
  );
};

const Image = ({ disable, imageSrc, ...rest }) => {
  return disable ? (
    <></>
  ) : (
    <div>
      <img id="my-image" width={"100%"} src={imageSrc} {...rest} />
    </div>
  );
};
