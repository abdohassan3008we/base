import { useState } from "react";
import { Eye, EyeOff } from "react-feather";
import { Validator } from "../../../classes/validator";
import { ValidationField } from "./ValidationField";
import { IconizedField } from "./IconizedField";

export const Password = ({ id, disable, field, formik }) => {
  const [inputVisability, setInputVisability] = useState(false);

  const toggleVisability = () => {
    setInputVisability((v) => !v);
  };

  let validation = new Validator("أدخل كلمة سر من 6 حروف على الأقل")
    .password(Validator.WEEK, "كلمة سر ضعيفة")
    .minLength(6);

  if (field.required) validation = validation.required();

  const eyeIcon = inputVisability ? (
    <Eye
      color="#999999"
      onClick={toggleVisability}
      style={{ cursor: "pointer" }}
    />
  ) : (
    <EyeOff
      color="#999999"
      onClick={toggleVisability}
      style={{ cursor: "pointer" }}
    />
  );

  return (
    <IconizedField
      component={
        <ValidationField
          id={id}
          type={inputVisability ? "text" : "password"}
          disable={disable}
          field={field}
          formik={formik}
          validation={validation}
          icons={[eyeIcon]}
          style={{ height: "150px" }}
        />
      }
      icons={[eyeIcon]}
      paddingTop="30px"
      paddingStart={7}
    />
  );
};
export const StrongPassword = ({ id, disable, field, formik }) => {
  const [inputVisability, setInputVisability] = useState(false);

  const toggleVisability = () => {
    setInputVisability((v) => !v);
  };

  let validation = new Validator("أدخل كلمة السر")
    .password(
      Validator.STRONG,
      "أدخل كلمة سر تحتوي على رقم ورمز وحرف كبير وآخر صغير"
    )
    .minLength(8, "كلمة سر ضعيفة")
    .maxLength(20, "كلمة سر طويلة");
  if (field.required) validation = validation.required();

  const eyeIcon = inputVisability ? (
    <Eye
      color="#999999"
      onClick={toggleVisability}
      style={{ cursor: "pointer" }}
    />
  ) : (
    <EyeOff
      color="#999999"
      onClick={toggleVisability}
      style={{ cursor: "pointer" }}
    />
  );

  return (
    <IconizedField
      component={
        <ValidationField
          id={id}
          type={inputVisability ? "text" : "password"}
          disable={disable}
          field={field}
          formik={formik}
          validation={validation}
          icons={[eyeIcon]}
        />
      }
      icons={[eyeIcon]}
      paddingTop="30px"
      paddingStart={7}
    />
  );
};
