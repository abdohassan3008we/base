import { Validator } from "../../../classes/validator";
import { ValidationField } from "./ValidationField";

export const NumberText = ({ id, disable, field, formik, onChange }) => {
  let validation = new Validator().number().min(0);
  if (field.required) validation = validation.required();

  return (
    <ValidationField
      id={id}
      type="number"
      errorMessage="أدخل رقم موجب"
      disable={disable}
      field={field}
      formik={formik}
      validation={validation}
      onChange={onChange}
    />
  );
};
