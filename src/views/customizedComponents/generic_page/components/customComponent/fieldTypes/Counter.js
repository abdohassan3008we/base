import { AvForm, AvGroup } from "availity-reactstrap-validation-safe";
// import { NumberInput } from "../common";
import NumberInput from "@components/number-input";
import { Label } from "reactstrap";

export const Counter = ({ id, name, disable, field, formik, onChange }) => {
  return (
    <>
      <Label for={field.selector}>{field.name}</Label>
      <AvGroup>
        <NumberInput
          disabled={disable}
          name={name ?? field.selector}
          id={id ?? field.selector}
          type="number"
          min={field.counterProps?.min}
          max={field.counterProps?.max}
          step={field.counterProps?.step}
          wrap={field.counterProps?.wrap}
          readonly={false}
          value={parseInt(
            field["_value_"] ??
              field.value ??
              formik?.values[field.selector] ??
              field["_defaultValue_"] ??
              field.defaultValue ??
              field.counterProps?.min ??
              "1"
          )}
          onChange={(e) => {
            formik?.setFieldValue(field.selector, e);
            onChange && onChange(e);
          }}
        />
        </AvGroup>
    </>
  );
};
