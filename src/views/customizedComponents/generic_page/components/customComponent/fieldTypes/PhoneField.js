import { Validator } from "../../../classes/validator";
import { ValidationField } from "./ValidationField";

export const PhoneNumber = ({ id, disable, field, formik }) => {
  let validation = new Validator("أدخل رقم الهاتف").phone(
    "egypt",
    "رقم هاتف خاطئ"
  );
  if (field.required) validation = validation.required();

  return (
    <ValidationField
      id={id}
      disable={disable}
      field={field}
      formik={formik}
      validation={validation}
    />
  );
};
