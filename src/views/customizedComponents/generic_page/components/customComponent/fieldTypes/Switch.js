import { CustomInput } from "reactstrap";
import { AvGroup } from "availity-reactstrap-validation-safe";

export const Switch = ({ disable, key, id, name, field, formik }) => {
  return (
    <>
      <AvGroup>
        <CustomInput
          type="switch"
          key={disable ? "" : key != "" ? key : field.selector}
          id={disable ? "" : id != "" ? id : field.selector}
          name={disable ? "" : name != "" ? name : field.selector}
          defaultChecked={
            field["_value_"] ??
            field.value ??
            formik?.values[field.selector] ??
            field["_defaultValue_"] ??
            field.defaultValue
          }
          label={field.name}
          onChange={(e) => {
            formik?.setFieldValue(field.selector, e.target.checked);
          }}
        />
      </AvGroup>
    </>
  );
};
