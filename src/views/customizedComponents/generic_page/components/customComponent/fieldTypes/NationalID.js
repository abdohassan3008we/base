import   { Validator } from "../../../classes/validator";
import { ValidationField } from "./ValidationField";

export const NID = ({ id, disable, field, formik }) => {
  let validation = new Validator("أدخل الرقم القومي").NID(
    "egypt",
    "الرقم القومي غير صحيح"
  );
  if (field.required) validation = validation.required();

  return (
    <ValidationField
      id={id}
      disable={disable}
      field={field}
      formik={formik}
      validation={validation}
    />
  );
};
