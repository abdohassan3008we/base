import { Validator } from "../../../classes/validator";
import { ValidationField } from ".";

export const Text = ({
  id,
  disable,
  errorMessage,
  field,
  formik,
  secure = false,
  type = "text",
  style,
  onChange
}) => {
  //   const [inputVisibility, setInputVisibility] = useState(!secure);
  // <Eye size={15} />;
  //   <EyeOff size={15} />;
  return (
    <>
      <ValidationField
        id={id}
        validation={new Validator()}
        errorMessage={errorMessage}
        type={type}
        disable={disable}
        field={field}
        formik={formik}
        secure={secure}
        style={style}
        onChange={onChange}
      />
    </>
  );
};
