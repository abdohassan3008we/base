import { useEffect, useState } from "react";
import { lookup } from "..";
import {
  RequestedPaginationParams,
  ResponseTotalElements,
  getContentPath,
} from "../../..";
import {
  createGetRequest,
  createListRequest,
} from "../../../../../../utility/services/requests";
import { Validator, Listener } from "../../../classes";
import { addDropDownKeys } from "../../../screens/pageContent";
import { RequestedDataSelect, DataSelect } from "./dropDown";

// let listener;
export const List = ({ id, name, disable, field, formik, onChange }) => {
  const [data, setData] = useState(field.data ?? []);
  const [totalElements, setTotalElements] = useState(0);
  const [key, setKey] = useState(0);
  function forceUpdate() {
    setKey((k) => k + 1);
  }

  // get dropdown data if lookup field changes
  // useEffect(() => {
  //   if (field.lookup && Object.keys(field.lookup).length > 0) {
  //     if (!listener) {
  //       listener = new Listener({
  //         values: formik?.values,
  //         listeners: Object.keys(field.lookup),
  //       });
  //     } else {
  //       if (listener.hasChanged({ values: formik?.values })) {
  //         initLookupListByIdFieldData({ field, formik, setData, forceUpdate });
  //         listener.update({ values: formik?.values });
  //       }
  //     }
  //   }
  // }, [formik?.values]);

  // list request to get all data to fill the select
  useEffect(() => {
    if (field.listProps?.listEndPoint) {
      if (!field.listProps?.enablePagination) {
        createListRequest({
          endPoint: field.listProps?.listEndPoint,
          action: (data) => {
            //////////////////
            addDropDownKeys(formik, field.selector, data);
            //////////////////
            return data.map((item) =>
              createDropdownLabelValue({
                item,
                labelSelector: field.listProps?.labelSelector,
                valueSelector: field.listProps?.valueSelector,
              })
            );
          },
        })({
          params: field.listProps?.params ?? {},
          // dataAction: field.listProps?.dataAction,

          setData: setData,
          onSuccess: (data) => {
            // initLookupListByIdFieldData({
            //   field,
            //   formik,
            //   setData,
            //   forceUpdate,
            // });
          },
          onFailure: (data) => {
            // initLookupListByIdFieldData({
            //   field,
            //   formik,
            //   setData,
            //   forceUpdate,
            // });
          },
        });
      }
    }
    //  else if (field.lookup) {
    //   initLookupListByIdFieldData({
    //     field,
    //     formik,
    //     setData,
    //     forceUpdate,
    //   });
    // }
    // return () => {
    // setTotalElements(0);
    // setKey(0);
    // setData(null);
    // };
  }, [field.listProps?.listEndPoint]);

  if (field.required) field["validation"] = new Validator().required();

  return (
    <>
      {/* <AvGroup> */}
      {field.listProps?.enablePagination ? (
        <RequestedDataSelect
          refresherKey={key}
          name={name ?? field.selector}
          id={id ?? field.selector}
          label={field.name}
          placeholder={field.placeholder}
          isClearable={field.validation?.isValid("")}
          // isLoading={isLoading}
          // isMulti={field.listProps?.isMulti}
          defaultValue={
            field["_value_"] ??
            field.value ??
            formik?.values[field.labelSelector]
              ? {
                  value: formik?.values[field.selector],
                  label: formik?.values[field.labelSelector],
                }
              : field["_defaultValue_"] ??
                field.defaultValue ?? {
                  value: null,
                  label: null,
                }
          }
          isDisabled={disable}
          enableAdd={field.listProps?.enableAdd}
          addTitle={field.listProps?.addTitle}
          addComponent={field.listProps?.addComponent}
          enableSearch={field.listProps?.enableSearch}
          pageSize={field.listProps?.pageSize ?? 5}
          // searchTitle={field.listProps?.searchTitle}
          data={data}
          totalItems={totalElements}
          getDataRequest={(pageNo, pageSize, searchField) => {
            const [endPoint, selectors] = getContentPath(
              field.listProps?.listEndPoint
            );
            createListRequest({
              endPoint: endPoint,
              action: (data) => {
                setTotalElements(ResponseTotalElements(data));
                selectors
                  ?.filter((sel) => sel && sel != "")
                  .map((sel) => (data = data[sel]));
                //////////////////
                addDropDownKeys(formik, data);
                //////////////////
                return data?.map((item) =>
                  createDropdownLabelValue({
                    item,
                    labelSelector: field.listProps?.labelSelector,
                    valueSelector: field.listProps?.valueSelector,
                  })
                );
              },
            })({
              params: {
                ...field.listProps?.params,
                ...RequestedPaginationParams(pageNo, pageSize, searchField),
              },
              dataAction: field.listProps?.dataAction,
              setData: setData,
            });
          }}
          onChange={(e) => {
            formik?.setFieldValue(field.selector, e?.value);
            formik?.setFieldValue(field.labelSelector, e?.label);
            if (onChange) onChange(e);
          }}
        />
      ) : (
        <DataSelect
          key={key}
          name={name ?? field.selector}
          id={id ?? field.selector}
          label={field.name}
          placeholder={field.placeholder}
          isClearable={field.validation?.isValid("")}
          defaultValue={
            formik?.values[field.selector]
              ? {
                  // ? formik?.values["_" + field.selector + "_"] ?? {
                  value: formik?.values[field.selector],
                  label: formik?.values[field.labelSelector],
                }
              : field?.defaultValue
            // ??
            // field.value ??
            // field.defaultValue
          }
          isDisabled={disable}
          enableAdd={field.listProps?.enableAdd}
          addTitle={field.listProps?.addTitle}
          addComponent={field.listProps?.addComponent}
          enableSearch={field.listProps?.enableSearch}
          // searchTitle={field.listProps?.searchTitle}
          data={data}
          onChange={(e) => {
            formik?.setFieldValue(field.selector, e?.value);
            formik?.setFieldValue(field.labelSelector, e?.label);
            if (onChange) onChange(e);
          }}
        />
      )}
      {/* </AvGroup> */}
    </>
  );
};

// function initLookupListByIdFieldData  ({
//   field,
//   formik,
//   setData,
//   forceUpdate,
// }) {
//   const [
//     breaker,
//     endPoint,
//     selectors,
//     params,
//     dataAction,
//     valueSelector,
//     labelSelector,
//     data,
//     id,
//   ] = getLookupAvailablePath({ field, formik });

//   if (endPoint) {
//     createGetRequest({
//       breaker,
//       endPoint,
//       action: (data) => {
//         selectors.map((sel) => (data = data[sel] ?? data));
//         // setTotalElements(data.totalElements);
//         return data.map((item) =>
//           createDropdownLabelValue({
//             item,
//             labelSelector,
//             valueSelector,
//           })
//         );
//       },
//     })({
//       params,
//       id: id ?? "",
//       dataAction,
//       setData,
//       onSuccess: forceUpdate,
//       onFailure: () => {
//         setData((d) => data);
//       },
//     });
//   }
// };
// function getLookupAvailablePath({ field, formik }) {
//   const {
//     breaker,
//     endPoint,
//     params,
//     dataAction,
//     valueSelector,
//     labelSelector,
//     data,
//     id,
//   } = lookup({
//     field,
//     formik,
//     lookupFun: ({ lookupValue, formikValue, obj }) => {
//       if (!lookupValue || lookupValue === formikValue) {
//         return {
//           breaker: obj.breaker,
//           endPoint: obj.listEndPoint,
//           params: obj.params,
//           dataAction: obj.dataAction,

//           valueSelector: obj.valueSelector,
//           labelSelector: obj.labelSelector,

//           data: obj.data,

//           id: formikValue,
//         };
//       } else {
//         return undefined;
//       }
//     },
//   });

//   let selectors = [];
//   let endPointPath = undefined;
//   if (endPoint) {
//     [endPointPath, selectors] = getContentPath(endPoint);
//   }
//   return [
//     breaker,
//     endPointPath,
//     selectors,
//     params,
//     dataAction,
//     valueSelector,
//     labelSelector,
//     data,
//     id,
//   ];
// }

const createDropdownLabelValue = ({ item, valueSelector, labelSelector }) => {
  const itemObj = {};
  if (typeof valueSelector === "function") {
    itemObj["value"] = valueSelector(item);
  } else {
    itemObj["value"] = item[valueSelector];
  }
  if (typeof labelSelector === "function") {
    itemObj["label"] = labelSelector(item);
  } else {
    itemObj["label"] = item[labelSelector];
  }
  return itemObj;
};
// function getRequest(field) {
//   if (field.listProps?.listEndPoint) {
//     const [endPoint, selectors] = getContentPath(field.listProps?.listEndPoint);

//     return createListRequest({
//       endPoint,
//       action: (data) => {
//         let modefiedData = data;
//         selectors.map((sel) => (modefiedData = modefiedData[sel] ?? d));
//         return modefiedData.map((item) => ({
//           value: item[field.listProps?.valueSelector],
//           label: item[field.listProps?.labelSelector],
//         }));
//       },
//     });
//   }
//   return () => {};
// }
