import { Validator } from "../../../classes/validator";
import { ValidationField } from ".";

export const Email = ({ id, disable, field, formik }) => {
  let validation = new Validator("أدخل عنوان البريد الإلكتروني").email();
  if (field.required) validation = validation.required();

  return (
    <ValidationField
      id={id}
      type="email"
      errorMessage="بريد إلكتروني خاطئ"
      disable={disable}
      field={field}
      formik={formik}
      validation={validation}
    />
  );
};
