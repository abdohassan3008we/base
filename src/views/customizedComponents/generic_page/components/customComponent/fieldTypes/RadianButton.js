import { useState } from "react";
import { CustomInput, Input, Label } from "reactstrap";

export const RadianGroup = ({ id, onChange, disable, field, formik }) => {
  const [key, setKey] = useState(0);
  const forceUpdate = () => {
    setKey((k) => k + 1);
  };

  return (
    <>
      <Label for={field.value}>{field.name}</Label>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        {field.groupProps?.children?.map((btn) => (
          <>
            <RadianButton
              id={id}
              onChange={(params) => {
                setRadianChoice({
                  value: btn.value,
                  field: field,
                  formik: formik,
                });
                forceUpdate();
                onchange && onChange(params);
              }}
              disable={disable}
              buttonObj={btn}
              formik={formik}
            />
          </>
        ))}
      </div>
    </>
  );
};
export const RadianButton = ({ id, onChange, disable, buttonObj, formik }) => {
  return (
    <>
      <CustomInput
        key={buttonObj.value}
        type="radio"
        id={id ?? buttonObj.value}
        disabled={disable || buttonObj.disable}
        label={buttonObj.label}
        checked={buttonObj.check}
        style={{ cursor: "pointer" }}
        onChange={(params) => {
          // onChange({ group: buttonObj.group, buttonValue: buttonObj.value })
          onChange(params);
        }}
        onClick={(params) => {
          // onChange({ group: buttonObj.group, buttonValue: buttonObj.value })
          onChange(params);
        }}
        // className="mr-1"
      />
    </>
  );
};

const setRadianChoice = ({ field, formik, value }) => {
  field.groupProps?.children
    .map((child) => {
      child.check = false;
      return child;
    })
    .filter((child) => child.value === value)
    .map((child) => (child.check = true));
  formik?.setFieldValue(field.selector, value);
};
