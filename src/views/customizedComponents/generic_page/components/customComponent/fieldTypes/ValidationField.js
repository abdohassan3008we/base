import { AvGroup, AvField, AvForm } from "availity-reactstrap-validation-safe";
import { lookup } from "..";

import { Validator } from "../../../classes/validator";
import { useTranslation } from "react-i18next"

export const ValidationField = ({
  id,
  type,
  errorMessage,
  validation = new Validator(),
  disable,
  field,
  formik,
  style,
  onChange,
}) => {
  const { t } = useTranslation()

  if (!field.validation) {
    field["validation"] = validation;
  } else {
    field["validation"] =
      typeof field.validation === "function"
        ? field.validation(formik.values)
        : field.validation;
  }
  if (field.required && field.validation?.isValid("")) {
    field["validation"] = field["validation"].required(field.errorMessage);
  }

  // const lookUpValidation = lookup({
  //   field,
  //   formik,
  //   lookupFun: ({ lookupValue, formikValue, obj }) => {
  //     let validation = undefined;
  //     if (!lookupValue || lookupValue === formikValue) {
  //       validation =
  //         typeof obj.validation === "function"
  //           ? obj.validation(formik.values)
  //           : obj.validation;
  //     }
  //     return validation;
  //   },
  // });
  return (
    <>
      <AvForm>
        <AvField
          errorMessage={t(`${field.errorMessage}`) ?? t(errorMessage)}
          label={field.name}
          disabled={disable}
          dir={field.rtl ? "rtl" : field.ltr ? "ltr" : "start"}
          type={type}
          name={field.selector ?? field.name ?? "field"}
          id={field.selector ?? field.name}
          autoComplete="off"
          placeholder={field.placeholder}
          value={
            field["_value_"] ??
            field.value ??
            formik?.values[field.selector] ??
            field["_defaultValue_"] ??
            field.defaultValue ??
            ""
          }
          validate={
            // lookUpValidation && Object.keys(lookUpValidation).length
            //   ? lookUpValidation.getValidate()
            //   :
            field.validation?.getValidate()
          }
          // validate={field.validation?.getValidate()}
          style={{ ...style, ...field.style }}
          onChange={(_, value) => {
            formik?.setFieldValue(field.selector, value);
            onChange && onChange(value);
          }}
        />
      </AvForm>
    </>
  );
};
