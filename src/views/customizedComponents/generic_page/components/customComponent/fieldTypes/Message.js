import { Label } from "reactstrap";

export const Message = ({ field, formik, style }) => {
  return (
    <>
      <center>
        <Label style={{ ...style, ...field.style }}>
          {field["_value_"] ??
            field.value ??
            formik?.values[field.selector] ??
            field["_defaultValue_"] ??
            field.defaultValue ??
            ""}
        </Label>
      </center>
    </>
  );
};
export const ErrorMessage = ({ field, formik }) => {
  return Message({ field, formik, style: { color: "#FF6060" } });
};
export const WarningMessage = ({ field, formik }) => {
  return Message({ field, formik, style: { color: "#ff9f43" } });
};
export const SuccessMessage = ({ field, formik }) => {
  return Message({ field, formik, style: { color: "#28c76f" } });
};
