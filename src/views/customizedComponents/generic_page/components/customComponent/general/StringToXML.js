export const Space = ({ num = 1 }) => {
  if (num < 1) return <></>;
  return (
    <>
      &nbsp;
      <Space num={num - 1} />
    </>
  );
};
export const Tab = ({ num = 1 }) => {
  if (num < 1) return <></>;
  return (
    <>
      &nbsp; &nbsp; &nbsp; &nbsp;
      <Tab num={num - 1} />
    </>
  );
};
export const NewLine = ({ num = 1 }) => {
  if (num < 1) return <></>;
  return (
    <>
      <br />
      <NewLine num={num - 1} />
    </>
  );
};
export const StX = ({ text = "" }) => {
  text = text
    .replaceAll(" ", "&nbsp;")
    .replaceAll("\\t", "&nbsp;&nbsp;&nbsp;&nbsp;")
    .replaceAll("\\n", "<br/>");

  return (
    <span dangerouslySetInnerHTML={{ __html: text }}></span>
  );

  // return new DOMParser().parseFromString(text, "text/xml");
};
