import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { Share } from "react-feather";
import { useTranslation } from "react-i18next";

export const DropdownButton = ({
  title = "",
  icon = () => {},
  items,
  disabled,
}) => {
  const { t } = useTranslation();
  return (
    <UncontrolledButtonDropdown style={{ margin: "10px" }}>
      <DropdownToggle disabled={disabled} color="secondary" caret outline>
        {icon({ size: 15 })}
        <span className="align-middle ml-50"> {t(title)}</span>
      </DropdownToggle>

      <DropdownMenu right>
        {items.map((item, index) => {
          return item ? (
            <DropdownItem
              key={item.name + "_DropdownItem_" + index}
              className="w-100"
              onClick={() => item.action()}
            >
              {item.icon}
              <span className="align-middle ml-50">{item.name}</span>
            </DropdownItem>
          ) : null;
        })}
      </DropdownMenu>
    </UncontrolledButtonDropdown>
  );
};
