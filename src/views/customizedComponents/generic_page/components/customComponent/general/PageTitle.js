import { useEffect } from "react";
import { Helmet } from "react-helmet";
import app from "@src/configs/app_data";
import { useTranslation } from "react-i18next";

/**
 * Updates document title with the given one
 *
 * @param {String} title
 * @returns {JSX.Element}
 * @author Mohamed Ashraf (EMAM)
 */
export function PageTitle({ title }) {
  const { t } = useTranslation();
  useEffect(() => {
    return (_) => {
      document.title = t("app_name");
    };
  }, []);

  return (
    <Helmet>
      <title>
        {t(title)} | {t("app_name")}
      </title>
    </Helmet>
  );
}
