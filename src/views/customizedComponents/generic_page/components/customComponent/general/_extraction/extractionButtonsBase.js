import { GoDesktopDownload } from "react-icons/go";
import { useTranslation } from "react-i18next";
import { DropdownButton } from "../DropDownButton";

export const ExtractionButtonBase = ({ items, disabled }) => {
  const { t } = useTranslation();
  return (
    <DropdownButton
      title="Extraction"
      icon={(params) => <GoDesktopDownload {...params} />}
      items={items}
      disabled={disabled}
    />
  );
};
