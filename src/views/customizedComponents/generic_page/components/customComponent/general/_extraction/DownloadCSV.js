const convertArrayOfObjectsToCSV = (data, tableOptions) => {
  const columns = tableOptions.headers;
  const columnDelimiter = ",";
  const lineDelimiter = "\n";
  const keys = Object.keys(data[0]); 
  const headers = [];

  if (columns) {
    columns.map(function (col, i) {
      
      const key = keys.find((key) => col.selector === key);
      if (key) {
        headers.push(col);
      }
    });
  } else {
    keys.map((k) => headers.push({ name: k, selector: k }));
  }

  let result = "";
  result += headers.map((header) => header.name).join(columnDelimiter);
  result += lineDelimiter;

  data.map((item) => {
    headers.map((header, index) => {
      if (index > 0) result += columnDelimiter;

      result += item[header.selector];
    });
    result += lineDelimiter;
  });
  return result;
};
// ** Downloads CSV
export const downloadCSV = (data, tableOptions) => {
  if (data && data.length && data.length > 0 && tableOptions) {
    const link = document.createElement("a");
    let csv = convertArrayOfObjectsToCSV(data, tableOptions);
    if (csv === null) return;
    if (!csv.match(/^data:text\/csv/i)) {
      csv = `data:text/csv;charset=utf-8,${`\ufeff${csv}`}`;
    }
    link.setAttribute("href", encodeURI(csv));
    link.setAttribute("download", `${tableOptions.title}.csv`);
    link.click();
  }
};
