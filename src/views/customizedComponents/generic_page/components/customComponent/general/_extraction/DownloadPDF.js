import jsPDF from "jspdf";
import "jspdf-autotable";
import autoTable from "jspdf-autotable";
// import { font } from "./Amiri-Regular-normal";

export const downloadPDF = (data, tableOptions) => {
  if (data && data.length && data.length > 0 && tableOptions) {
    const columns = tableOptions.headers;
    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape
    const doc = new jsPDF(orientation, unit, size);
    doc.addFont("Amiri-Regular-normal.ttf", "Amiri-Regular", "normal");
    doc.setFont("Amiri-Regular");
    doc.setFontSize(10);

    const keys = Object.keys(data[0]);
    const titles = [];
    let headers = [];

    if (columns) {
      columns.forEach((col) => {
        const key = keys.find((key) => col.selector === key);
        if (key) {
          titles.push(col.name);
        }
      });
      headers = [...titles.reverse()];
    } else {
      headers = [...keys];
    }
    const body = data.map((item) => {
      const tempArray = [];
      columns.forEach(function (col, i) {
        const key = keys.find((key) => col.selector === key);
        if (key) {
          const d =
            item[key] === true ? "√" : item[key] === false ? "x" : item[key];
          tempArray.push(d);
        }
      });
      return tempArray.reverse();
    });
    const style = {
      // head: headers,
      // body: body,
      // startY: 15,

      // margin: 15,
      // styles: {
      cellPadding: 5,
      halign: "center",
      font: "Amiri-Regular",
      fontStyle: "normal",
      wordwrap: "normal",
      // },
    };
    // doc.autoTable(headers, body, style);
    autoTable(doc, {
      headStyles: {
        font: "times",
        halign: "center",
        wordwrap: "normal",
      },
      bodyStyles: {
        font: "cairo",
        halign: "center",
        wordwrap: "normal",
      },
      // margin: { top: 10 },
      body: body,
      head: [headers],
    });
    doc.save(`${tableOptions.title}.pdf`);
  }
};
