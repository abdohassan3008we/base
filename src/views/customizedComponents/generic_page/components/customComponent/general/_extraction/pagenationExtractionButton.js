import { GoDesktopDownload } from "react-icons/go";
import { RiFileExcel2Line } from "react-icons/ri";
import { BsFileEarmarkPdf, BsFiletypeCsv } from "react-icons/bs";
import { TbReportAnalytics } from "react-icons/tb";
import {
  downloadCSV,
  downloadPDF,
  downloadExcel,
  downloadExampleExcel,
} from ".";
import { useEffect, useState } from "react";
import { ExtractionButtonBase } from "./extractionButtonsBase";
import { DropdownButton } from "../../../..";

export const PaginationExtractionButton = ({
  fileName,
  headers,

  filter,
  recordsNo,
  extractFunction,

  disabled,
  showReportOption,
  generateReport,
}) => {
  const requestDataSize = 50;
  const [key, setKey] = useState(0);
  const [data, setData] = useState([]);
  const [report, setReport] = useState("");

  const appendData = (data) => {
    setData((d) => [...d, ...data]);
    setKey((k) => k + 1);
  };
  const resetDownloadAction = () => {
    setKey((k) => 0);
    setData((d) => []);
  };

  useEffect(() => {
    if (key >= recordsNo / requestDataSize) {
      switch (report) {
        default:
        case "PDF":
          downloadPDF(data, { title: fileName, headers });
          break;
        case "EXCEL":
          downloadExcel(data, { title: fileName, headers });
          break;
        case "CSV":
          downloadCSV(data, { title: fileName, headers });
          break;
        case "REPORT":
          generateReport(data, { title: fileName, headers });
          break;
      }
      resetDownloadAction();
    }
  }, [key]);
  return (
    <ExtractionButtonBase
      disabled={disabled}
      items={[
        showReportOption && {
          name: "REPORT",
          icon: <TbReportAnalytics size={15} />,
          action: () => {
            setReport("REPORT");
            extractFunction({
              filter,
              appendData,
              requestDataSize,
            });
          },
        },
        {
          name: "PDF",
          icon: <BsFileEarmarkPdf size={15} />,
          action: () => {
            setReport("PDF");
            extractFunction({
              filter,
              appendData,
              requestDataSize,
            });
          },
        },
        {
          name: "EXCEL",
          icon: <RiFileExcel2Line size={15} />,
          action: () => {
            setReport("EXCEL");
            extractFunction({
              filter,
              appendData,
              requestDataSize,
            });
          },
        },
        {
          name: "CSV",
          icon: <BsFiletypeCsv size={15} />,
          action: () => {
            setReport("CSV");
            extractFunction({
              filter,
              appendData,
              requestDataSize,
            });
          },
        },
      ]}
    />
  );
};
