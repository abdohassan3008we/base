import { FileText, File, Clipboard, Grid } from "react-feather";
import {
  downloadCSV,
  downloadPDF,
  downloadExcel,
  downloadExampleExcel,
} from ".";
import { ExtractionButtonBase } from "./extractionButtonsBase";

export const ExtractionButton = ({
  fileName,
  headers,
  data,
  showReportOption,
  generateReport,
  disabled,
}) => {
  return (
    <ExtractionButtonBase
      disabled={disabled}
      items={[
        showReportOption && {
          name: "REPORT",
          icon: <Clipboard size={15} />,
          action: () => generateReport(data, { title: fileName, headers }),
        },
        {
          name: "PDF",
          icon: <File size={15} />,
          action: () => downloadPDF(data, { title: fileName, headers }),
        },
        {
          name: "EXCEL",
          icon: <Grid size={15} />,
          action: () => downloadExcel(data, { title: fileName, headers }),
        },
        {
          name: "CSV",
          icon: <FileText size={15} />,
          action: () => downloadCSV(data, { title: fileName, headers }),
        },
        {
          name: "Example EXCEL",
          icon: <Grid size={15} />,
          action: () => downloadExampleExcel([], { title: fileName, headers }),
        },
      ]}
    />
  );
};
