import FileSaver from "file-saver";
import XLSX from "sheetjs-style";

export const downloadExcel = (data, tableOptions) => {
  data = initHeadersInData(data, tableOptions.headers);
  saveExcelData(data);
};
const initHeadersInData = (data, columns) => {
  const headers = [];
  if (data && data.length > 0) {
    const keys = Object.keys(data[0]);
    if (columns) {
      columns.forEach((col) => {
        const key = keys.find((key) => col.selector === key);
        if (key) {
          headers.push(col);
        }
      });
    } else {
      keys.map((k) => headers.push({ name: k, selector: k }));
    }
  } else {
    if (columns) {
      headers.push(...columns);
    }
  }
  const changedData = data.map((_) => ({}));
  data.map((item, index) => {
    headers.map((header) => {
      changedData[index] = {
        ...changedData[index],
        [header.name]:
          item[header.selector] === true
            ? "✓"
            : item[header.selector] === false
            ? "✗"
            : item[header.selector],
      };
    });
  });
  changedData.push({});
  headers.map((header) => {
    changedData[changedData.length - 1] = {
      ...changedData[changedData.length - 1],
      [header.name]: null,
    };
  });

  return changedData;
};

export const downloadExampleExcel = (data, tableOptions) => {
  console.log(data);
  console.log(tableOptions);
  data = addSelectors(data, tableOptions.headers);
  saveExcelData(data, tableOptions.title);
};
const addSelectors = (data, headers) => {
  const obj = {};
  headers
    .filter((header) => header.selector)
    .map((header) => (obj[header.selector] = header.name));
  data = [obj, ...data];
  return data;
};

const saveExcelData = (data, title) => {
  const fileType =
    "application/vnd.openxmlformats-officedocument.spreedsheetml.sheet;charset=UTF-8";
  const ws = XLSX.utils.json_to_sheet(data);
  const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
  const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
  const excelData = new Blob([excelBuffer], { type: fileType });
  FileSaver.saveAs(excelData, (title ?? Date.now()) + ".xlsx");
};
