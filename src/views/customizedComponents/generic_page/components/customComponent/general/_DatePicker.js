import Flatpickr from "react-flatpickr";
import "@styles/react/libs/flatpickr/flatpickr.scss";
import { useEffect, useState } from "react";
import dateFormat from "dateformat";
// import ReactDatePicker from "react-datepicker";

/**
 * @returns {JSX.Element}
 * @author Mohamed Ashraf (EMAM)
 */
export function DatePicker({
  name,
  enableTime = false,
  onChange = (_) => {},
  defaultValue,
  placeholder = undefined,
  options = {},
  disabled = false,
  time24 = true,
  format = undefined,
  required = false,
  // onError = "من فضلك ادخل التاريخ",
}) {
  format = format ?? (enableTime ? "yyyy-mm-dd hh:MM:ss" : "yyyy-mm-dd");
  const [key, setKey] = useState(1);
  const forceUpdate = () => {
    setKey((key) => key + 1);
  };

  useEffect(() => {
    forceUpdate();
  }, [defaultValue]);

  return (
    <>
      <Flatpickr
        key={key}
        id={name}
        name={name}
        className="date-picker"
        placeholder={placeholder}
        onChange={(date) => onChange(dateFormat(date, format))}
        // value={defaultValue ?? new Date().toString()}
        value={defaultValue}
        options={{
          enableTime: enableTime,
          // minDate: new Date() - 1,
          // dateFormat: "Y-m-d H:i",
          // dateFormat: "yy-m-d h:m",
          minuteIncrement: 1,
          defaultHour: new Date().getHours(),
          defaultMinute: new Date().getMinutes(),
          allowInput: true,
          altFormat: enableTime ? "Y/m/d H:i" : "Y/m/d",
          // dateFormat: "yy-m-d h:m",
          altInput: true,
          dir: "start",
          time_24hr: time24,
          ...options,
        }}
        disabled={disabled}
      />
    </>
  );
}
