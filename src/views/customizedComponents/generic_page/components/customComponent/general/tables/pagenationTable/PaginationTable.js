import React, { useState, useEffect } from "react";
import DataTable from "react-data-table-component";
import { ChevronDown } from "react-feather";
import { CgSortAz } from "react-icons/cg";
import { BiMessageSquareAdd } from "react-icons/bi";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CustomInput,
  Input,
  Label,
  Row,
  Spinner,
} from "reactstrap";
// ** Styles
import "@styles/react/libs/tables/react-dataTable-component.scss";
import { PaginationExtractionButton } from "../../_extraction";
import ServerSidePagination from "./ServerSidePagination";
import { useTranslation } from "react-i18next";
import { DropdownButton } from "../../DropDownButton";

/**
 * Table listing data with server side pagination
 *
 * @param {Array} columns
 * @param {Array} data
 * @param {Number} totalElements total number of elements
 * @param {Function} onPagination on pagination change (page or perPage change)
 * @param {Boolean} progress indicator for whether loading has finished or not
 * @param {Boolean} showAddBtn whether to show add button or not
 * @param {Function} handleAddButton
 * @param {String} addBtnUrl add button URL if add form is a separate page
 * @param {String} addButtonText
 * @param {Boolean} showSearch
 * @param {Integer} refresherKey a key to re-render table on change
 * @returns {JSX.Element}
 * @author Mohamed Abdul-Fattah
 */
export function PaginationTable({
  tableTitle,
  columns,
  data,
  extractFunction,
  totalRecords,
  onPagination = () => {},

  isLoading = false,

  enableAdd = true,
  addButtonText = "Add",
  handleAddButton = () => {},
  addBtnUrl,

  enableSearch = true,
  enableExtraction = true,
  extractionTableHeaders,

  refresherKey = 1,
  buttons = [],
  buttonsParams,
  buttonModelHandlers,

  showAllSwitch,
  showAllSwitchTitle,
  customSwitchHandle,

  rowDetailsComponent,
}) {
  /*
  
      return enableAdd ? (
      addBtnUrl ? (
        <>
          <Link to={addBtnUrl}>
            <Button.Ripple color="primary">{t(addButtonText)}</Button.Ripple>
          </Link>
        </>
      ) : (
        <>
          <Button.Ripple color="primary" onClick={() => handleAddButton()}>
            {addButtonText}
          </Button.Ripple>
        </>
      )
    ) : (
      <></>
    );
    */
  buttons = [
    {
      name: addButtonText,
      disable: !enableAdd,
      // group: "add",

      onClick: handleAddButton,
      url: addBtnUrl,

      //  btn: (params) => <Btn {...params} />, // apply the page by btn table header
      btn: (params) => (
        <Button.Ripple color="primary" onClick={params.onClick}>
          {params.title}
        </Button.Ripple>
      ),
    },
    ...buttons,
  ];
  const enabledButtons = buttons.reverse().filter((button) => {
    let disable = button.disable;
    if (typeof button.disable === "function") {
      disable = button.disable(data);
    }
    return !disable;
  });

  const [perPage, setPerPage] = useState(5);
  const [currentPage, setCurrentPage] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");
  const [key, setKey] = useState(0);
  const { t } = useTranslation();

  const updatePerPage = (num) => {
    setPerPage(parseInt(num, 10));
    setKey((k) => k + 1);
  };

  useEffect(() => {
    setCurrentPage((p) => 0);
    onPagination(0, perPage, searchTerm); // first request
  }, [searchTerm]);

  useEffect(() => {
    // to reduce the number of requests
    // no addition request with the first request
    if (key === 0) {
      setKey((k) => k + 1);
    } else {
      onPagination(currentPage, perPage, searchTerm);
    }
  }, [currentPage, perPage, refresherKey]);

  const searchBar = () => (
    <div className="d-flex align-items-center mb-sm-0 mb-1 mr-1">
      <Label className="mb-0" for="search-invoice">
        {t("Search")}
      </Label>
      <Input
        disabled={!enableSearch}
        id="search-invoice"
        className="ml-50 w-100"
        type="text"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
        autoComplete="off"
      />
    </div>
  );

  const handleShowAllSwitch = () =>
    showAllSwitch && (
      <div style={{ display: "flex", paddingRight: "10px" }}>
        <label style={{ paddingLeft: "5px", margin: "auto" }}>
          {showAllSwitchTitle}
        </label>
        <div style={{ margin: "auto" }}>
          <CustomInput
            type="switch"
            id="showAllData"
            name="showAllData"
            onChange={(e) => customSwitchHandle(e)}
          />
        </div>
      </div>
    );

  const extractionBtn = () => {
    return (
      <PaginationExtractionButton
        disabled={!enableExtraction}
        fileName={(tableTitle ?? "") + "_" + Date.now()}
        headers={extractionTableHeaders ?? columns}
        extractFunction={extractFunction}
        filter={searchTerm}
        recordsNo={totalRecords}
      />
    );
  };
  const RowDetailsComponent = rowDetailsComponent ?? null;
  return (
    <Card>
      <Row className="justify-content-end mx-0 ml-1">
        <div className="invoice-list-table-header w-100 mr-1 ml-50 mt-2 mb-75 mr-2">
          <div className="d-flex" style={{ justifyContent: "space-between" }}>
            <>{searchBar()}</>
            <>{handleShowAllSwitch()}</>
            <div>
              {handleButtons({
                buttons: enabledButtons,
                buttonsParams,
                translate: t,
                handleModels: buttonModelHandlers,
              })}
              {extractionBtn()}
            </div>
          </div>
        </div>
      </Row>
      <DataTable
        key={key}
        noHeader
        responsive
        expandableRows={RowDetailsComponent != null}
        expandableRowsComponent={
          RowDetailsComponent ? <RowDetailsComponent /> : <></>
        }
        className="react-dataTable"
        sortIcon={<CgSortAz size={10} />}
        columns={columns}
        data={data}
        pagination
        paginationPerPage={perPage}
        paginationComponent={() => (
          <ServerSidePagination
            // ALLdata={data}
            totalRecords={totalRecords}
            perPage={perPage}
            setPerPage={updatePerPage}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            // onPagination={onPagination}
          />
        )}
        noDataComponent={
          <p style={{ className: "react-dataTable", margin: "1em" }}>
            {t("No Data Found")}
          </p>
        }
        progressPending={isLoading}
        progressComponent={
          <div
            className="react-datatable-loader"
            style={{
              width: "100%",
              height: "100%",
              textAlign: "center",
              padding: "2%",
            }}
          >
            {t("Loading") + "....."}
            <Spinner />
          </div>
        }
      />
    </Card>
  );
}

const handleButtons = ({ buttons, buttonsParams, translate, handleModels }) => {
  // {
  //  name: "add",
  //  disable: false,
  //  // disable: () => {},
  //  /*
  //  group: "group id",
  //  */
  //  onClick: (params) => {},

  //  url: "/link/",

  //  /*
  //  btn: (params) => <Btn {...params} />, // apply the page by btn table header
  //  */
  // }

  const groupedButtons = {};
  buttons
    .filter((btn) => btn.group)
    .map((btn) => {
      if (groupedButtons[btn.group]) {
        groupedButtons[btn.group] = [...groupedButtons[btn.group], btn];
      } else {
        groupedButtons[btn.group] = [btn];
      }
    });

  const unGroupedButtons = buttons.filter((btn) => !btn.group);

  const buttonsComponents = (
    <>
      {unGroupedButtons.map((button, index) => {
        return tableHeaderButton({
          button,
          id: index,
          buttonsParams,
          translate,
          handleModel: () => handleModels(button.name),
        });
      })}
      {Object.keys(groupedButtons).map((key, index) => {
        const gBtns = groupedButtons[key];
        if (gBtns.length === 1) {
          return tableHeaderButton({
            button: gBtns[0],
            id: index,
            buttonsParams,
            translate,
            handleModel: () => handleModels(gBtns[0].name),
          });
        } else if (gBtns.length > 1) {
          const items = gBtns.map((btn) => ({
            name: btn.name,
            icon: btn.btn({
              title: translate(btn.name),
              ...buttonsParams,
              // onClick: gBtns[0].onClick,
            }),
            action: btn.onClick,
          }));
          return (
            <DropdownButton
              title={gBtns[0].group}
              icon={(params) => <BiMessageSquareAdd {...params} />}
              items={items}
            />
          );
        } else {
          return <></>;
        }
      })}
    </>
  );

  return buttonsComponents;
};

const tableHeaderButton = ({
  button,
  id,
  buttonsParams,
  translate,
  handleModel,
}) => {
  return button.url ? (
    <>
      <Link key={"Link_" + button.name + "_" + id} to={button.url}>
        {button.btn({
          title: translate(button.name),
          ...buttonsParams,
          onClick: button.onClick,
        })}
      </Link>
      &nbsp; &nbsp;
    </>
  ) : (
    <>
      {button.btn({
        title: translate(button.name),
        ...buttonsParams,
        onClick: (e) => {
          button.onClick && button.onClick(e);
          handleModel();
        },
      })}
      &nbsp; &nbsp;
    </>
  );
};
