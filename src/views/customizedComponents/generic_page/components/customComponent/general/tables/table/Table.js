import React, { useState, useEffect } from "react";
import DataTable from "react-data-table-component";
import { ChevronDown } from "react-feather";
import TableHeader from "./TableHeader";
import BootstrapCheckbox from "./BootstrapCheckbox";
import Pagination from "./Pagination";
import { Card, Row, Spinner } from "reactstrap";
// ** Styles
import "@styles/react/libs/tables/react-dataTable-component.scss";
import { useTranslation } from "react-i18next";

/**
 * Generic list table with pagination and search input
 *
 * @param {Array} columns
 * @param {Array} data
 * @param {String} tableTitle
 * @param {Array} searchFields search keys for filtration. E.g. [ "cityCode", "cityName" ]
 * @param {String} addingButton navigate to the given URL if not null
 * @param {boolean} showAddBtn toggle table add button
 * @param {Function} handleAddModal
 * @param {Boolean} showExtractBtn
 * @param {boolean} showSearch toggle table search field
 * @param {String} addButtonText
 * @param {React.Component} customBtns add custom buttons after the add button
 * @param {boolean} progress a state determines whether the data is loaded or still in progress
 * @param {boolean} selectableRows decides whether to add a checkbox for row selections or not
 * @param {Array} buttons
 * @param {Boolean} showAllSwitch
 * @param {String} showAllSwitchTitle
 * @param {Function} customSwitchHandle
 * @param {Boolean} showReportOption
 * @param {Function} generateReport
 * @param {Function} onSelectedRowsChange
 * @param {Function} selectableRowSelected
 * @param {Function} onClick
 * @param {Object} customStyles
 * @returns {React.Component}
 * @author Mohamed Ashraf (EMAM)
 */
export function Table({
  columns,
  data,
  tableTitle,
  searchFields = [],
  addingButton = null,
  showAddBtn = true,
  handleAddModal = () => {},
  showExtractBtn = false,
  showSearch = true,
  addButtonText = "إضافة",
  progress = false, // TODO change progress to isLoading
  selectableRows = false,
  buttons = [],
  showAllSwitch = false,
  showAllSwitchTitle = "",
  customSwitchHandle = () => {},
  showReportOption = false,
  generateReport = () => {},
  onSelectedRowsChange = () => {},
  selectableRowSelected = () => {},
  extractionTableHeaders,
  onClick = () => {},
  customStyles = {},
}) {
  const [searchTerm, setSearchTerm] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [filteredData, setFilteredData] = useState([]);
  const [key, setKey] = useState(1);
  const { t } = useTranslation();
  const [searchInFields, setSearchInFields] = useState(
    searchFields ??
      columns.filter((col) => col.selector).map((col) => col.selector)
  );

  // ** Function to handle filter
  const handleFilter = (value) => {
    setSearchTerm(value);

    setCurrentPage(1);
    if (value && value.length) {
      setFilteredData(
        data?.filter((item) => {
          for (const field of searchInFields) {
            if (
              getElement(item, field) &&
              getElement(item, field)
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase())
            ) {
              return true;
            }
          }
          return false;
        })
      );
    }
  };

  const getElement = (item, nameOfField) => {
    if (nameOfField.includes(".")) {
      const nameParts = nameOfField.split(".");
      const firstElement = nameParts[0];
      const secondElement = nameParts[1];
      return item[firstElement] ? item[firstElement][secondElement] : undefined;
    }
    return item[nameOfField];
  };

  useEffect(() => {
    handleFilter(searchTerm);
  }, [data]);

  return (
    <Card>
      <Row className="justify-content-end mx-0">
        <TableHeader
          columns={columns}
          data={searchTerm?.length ? filteredData : data}
          tableTitle={tableTitle}
          handleAddModal={handleAddModal}
          rowsPerPage={rowsPerPage}
          searchTerm={searchTerm}
          handleFilter={handleFilter}
          addingButton={addingButton}
          showAddBtn={showAddBtn}
          addButtonText={addButtonText}
          showExtractBtn={
            showExtractBtn &&
            data &&
            data.length &&
            data.length > 0 &&
            (searchTerm.length === 0 ||
              (searchTerm.length > 0 && filteredData.length > 0))
          }
          extractionTableHeaders={extractionTableHeaders}
          showSearch={
            (showSearch && data && data.length && data.length > 0) != 0
          }
          buttons={buttons}
          showAllSwitch={showAllSwitch}
          showAllSwitchTitle={showAllSwitchTitle}
          customSwitchHandle={customSwitchHandle}
          showReportOption={showReportOption}
          generateReport={generateReport}
        />
      </Row>

      <DataTable
        key={key}
        noHeader
        responsive
        pagination
        columns={columns}
        paginationPerPage={rowsPerPage}
        className="react-dataTable"
        sortIcon={<ChevronDown size={10} />}
        paginationDefaultPage={currentPage}
        data={searchTerm.length ? filteredData : data}
        selectableRowsComponent={BootstrapCheckbox}
        onRowClicked={onClick}
        progressPending={progress}
        selectableRows={selectableRows}
        selectableRowsHighlight
        selectableRowSelected={selectableRowSelected}
        onSelectedRowsChange={onSelectedRowsChange}
        customStyles={customStyles}
        noDataComponent={
          <p style={{ className: "react-dataTable", margin: "1em" }}>
            {t("No Data Found")}
          </p>
        }
        progressComponent={
          <div
            className="react-datatable-loader"
            style={{
              width: "100%",
              height: "100%",
              textAlign: "center",
              padding: "2%",
            }}
          >
            {t("Loading") + "....."}
            <Spinner />
          </div>
        }
        paginationComponent={() => (
          <Pagination
            currentPage={currentPage}
            data={searchTerm.length ? filteredData : data}
            setCurrentPage={setCurrentPage}
            rowsPerPage={rowsPerPage}
            setRowsPerPage={setRowsPerPage}
            // Force update table to rerender the data on rows per page change
            forceUpdate={(_) => setKey((key) => key + 1)}
          />
        )}
      />
    </Card>
  );
}
