import { Input, Label, Button, CustomInput } from "reactstrap";
import { ExtractionButton } from "../../_extraction";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

const TableHeader = ({
  handleAddModal,
  handleFilter,
  searchTerm,
  tableTitle,
  columns,
  data,
  addingButton,
  showAddBtn = true,
  showExtractBtn = true,
  extractionTableHeaders,
  showReportOption = false,
  generateReport = () => {},
  showSearch = true,
  addButtonText = "إضافة",
  showAllSwitch = false,
  showAllSwitchTitle = "",
  customSwitchHandle = null,
  buttons = [],
}) => {
  const { t } = useTranslation();
  const handleExtractButton = () => {
    return (
      <ExtractionButton
        disabled={!showExtractBtn}
        fileName={(tableTitle ?? "") + "_" + Date.now()}
        headers={extractionTableHeaders ?? columns}
        data={data}
        showReportOption={showReportOption}
        generateReport={generateReport}
      />
    );
  };
  const handleButtonRipple = () => {
    return (
      showAddBtn &&
      (addingButton ? (
        <>
          <Link to={addingButton}>
            <Button.Ripple color="primary">{addButtonText}</Button.Ripple>
          </Link>
        </>
      ) : (
        <>
          <Button.Ripple color="primary" onClick={() => handleAddModal()}>
            {addButtonText}
          </Button.Ripple>
        </>
      ))
    );
  };

  const handleSearchBar = () => {
    return (
      <>
        <Label className="mb-0" for="search-invoice">
          {t("Search")}
        </Label>
        <Input
          id="search-invoice"
          className="ml-50 w-100"
          type="text"
          disabled={!showSearch}
          value={searchTerm}
          onChange={(e) => {
            handleFilter(e.target.value);
          }}
          autoComplete="off"
        />
      </>
    );
  };
  const handleShowAllSwitch = () => {
    return (
      showAllSwitch && (
        <div style={{ display: "flex", paddingRight: "10px" }}>
          <label style={{ paddingLeft: "5px", margin: "auto" }}>
            {showAllSwitchTitle}
          </label>
          <div style={{ margin: "auto" }}>
            <CustomInput
              type="switch"
              id="showAllData"
              name="showAllData"
              onChange={(e) => customSwitchHandle(e)}
            />
          </div>
        </div>
      )
    );
  };

  const handleButtons = () => {
    return buttons.map((button, index) => {
      if ("url" in button)
        return (
          <>
            <Link key={index} to={button.url}>
              <Button.Ripple color={button.color || "primary"}>
                {button.title}
              </Button.Ripple>
            </Link>
            &nbsp; &nbsp;
          </>
        );

      if ("handler" in button)
        return (
          <>
            <Button.Ripple
              color={button.color || "primary"}
              onClick={button.handler}
            >
              {button.title}
            </Button.Ripple>
            &nbsp; &nbsp;
          </>
        );
    });

    // return (
    //   Buttons.map((Button, index) => <Button key={index} />)
    // )
  };

  return (
    <div className="invoice-list-table-header w-100 mr-1 ml-50 mt-2 mb-75">
      <div className="d-flex" style={{ justifyContent: "space-between" }}>
        <div className="d-flex align-items-center mb-sm-0 mb-1 mr-1">
          {handleSearchBar()}
          {handleShowAllSwitch()}
        </div>

        <div>
          {handleButtons()}
          {handleButtonRipple()}
          {handleExtractButton()}
        </div>
      </div>
    </div>
  );
};

export default TableHeader;
