// ** React Imports
import React from "react";
import { X } from "react-feather";
import { Modal, ModalHeader, ModalBody } from "reactstrap";

export const CustomModel = ({
  open,
  handleModel,
  title,
  body,
  maxWidth = "80%",
  minWidth = "10%",
}) => {
  const CloseBtn = (
    <X
      className="cursor-pointer"
      style={{ color: "#cc5555" }}
      size={20}
      onClick={handleModel}
    />
  );

  const cardTitle = title && <h2 style={{ textAlign: "center" }}> {title} </h2>;

  return (
    <Modal
      isOpen={open}
      toggle={handleModel}
      style={{ minWidth: minWidth, maxWidth: maxWidth }}
    >
      <ModalHeader
        className="mb-3"
        toggle={handleModel}
        close={CloseBtn}
        tag="div"
      >
        {cardTitle}
      </ModalHeader>
      <ModalBody className="flex-grow-1">
        <div style={{ padding: "5px" }}>{body}</div>
      </ModalBody>
    </Modal>
  );
};
