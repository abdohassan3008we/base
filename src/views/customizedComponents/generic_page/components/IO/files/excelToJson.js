import * as XLSX from "xlsx";

export const excel_to_JSON = ({ file }) => {
  return new Promise((resolve) => {
    const reader = new FileReader();
    reader.onload = (e) => {
      const binaryString = e.target.result;

      const workbook = XLSX.read(binaryString, { type: "binary" });
      const worksheet = workbook.Sheets[workbook.SheetNames[0]];
      const data = XLSX.utils.sheet_to_csv(worksheet, { header: 1 });

      const rows = data.split("\n");
      const headersParams = rows[0].split(",");
      const dataRows = rows.splice(1);
      const objects = [];
      for (let i = 0; i < dataRows.length; i++) {
        if (dataRows[i].includes(",")) {
          const rowData = dataRows[i].split(",");
          const rowObj = {};
          rowData.map((value, index) => {
            rowObj[headersParams[index]] = value;
          });

          objects.push(rowObj);
        }
      }
      resolve(objects);
    };
    reader.readAsBinaryString(file);
  });
};
