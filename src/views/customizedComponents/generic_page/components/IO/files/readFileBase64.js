export const readFileBase64 = ({ file, setData }) => {
  const reader = new FileReader();

  reader.onloadend = () => setData(reader.result);

  reader.readAsDataURL(file);
};
