import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const MySwal = withReactContent(Swal);

export const ConfirmCancelPopup = (warningMsg, confirmButton, CloseMsg) => {
  return MySwal.fire({
    text: warningMsg,
    icon: "question",
    showCancelButton: true,
    cancelButtonText: CloseMsg ?? "إلغاء",
    confirmButtonText: confirmButton ?? "حسنا",
    customClass: {
      confirmButton: "btn btn-primary",
      cancelButton: "btn btn-danger ml-1",
    },
    buttonsStyling: false,
  });
};
