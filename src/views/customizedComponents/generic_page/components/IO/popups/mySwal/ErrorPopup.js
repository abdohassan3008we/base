import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import "@styles/base/plugins/extensions/ext-component-sweet-alerts.scss";
const MySwal = withReactContent(Swal);

export const ErrorPopup = (errorMessage, confirmMsg, timer) => {
  MySwal.fire({
    html: `<strong>${errorMessage?.replaceAll("\n", "<br/>")}</strong>`,
    icon: "error",
    timer: timer ?? 2500,
    timerProgressBar: true,
    confirmButtonText: confirmMsg ?? "حسنا",
    showConfirmButton: false,
    customClass: {
      confirmButton: "btn btn-primary",
    },
    buttonsStyling: false,
    width: "auto",
    position: "bottom-end",
    // willOpen() {
    //   Swal.showLoading();
    //   timerInterval = setInterval(() => {
    //     Swal.getContent().querySelector("strong").textContent =
    //       parseInt(Swal.getTimerLeft() / 1000) + 1;
    //   }, 100);
    // },
    // willClose() {
    //   clearInterval(timerInterval);
    // },
  });
};
