import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import "@styles/base/plugins/extensions/ext-component-sweet-alerts.scss";
const MySwal = withReactContent(Swal);
export const SuccessPopup = ({
  rtl = localStorage.getItem("lang") == "ar",
  successMsg = "",
  timer = 2000,
}) => {
  MySwal.fire({
    html: `<strong>${successMsg
      .replaceAll("\n", "<br/>")
      .replaceAll(" ", "&nbsp;")
      .replaceAll("\t", "&nbsp;&nbsp;&nbsp;&nbsp;")}</strong>`,
    timer: timer,
    timerProgressBar: true,
    icon: "success",
    showConfirmButton: false,
    width: "auto",
    position: rtl ? "bottom-start" : "bottom-end",
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });
};
