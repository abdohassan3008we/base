export {
    ConfirmCancelPopup,
    ErrorPopup,
    InputPopup,
    SuccessPopup,
  } from "./mySwal";
  