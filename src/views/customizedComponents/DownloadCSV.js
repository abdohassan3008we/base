const convertArrayOfObjectsToCSV = (data, tableOptions) => {
    const columns = tableOptions.columns
    const columnDelimiter = ','
    const lineDelimiter = '\n'
    const keys = Object.keys(data[0])

    const titles = []
    keys.forEach(function (key, i) {
        const column = columns.find(col => col.selector === key)
        if (column) {
            titles.push(column.name)
        } else if (key.includes('Desc')) {
            titles.push('الوصف')
        } else if (key.includes('active')) {
            titles.push('منشط')
        } else if (key.includes('remarks')) {
            titles.push('ملاحظات')
        } else {
            titles.push(key)
        }
    })
    let result = ''
    result += titles.join(columnDelimiter)
    result += lineDelimiter

    data.forEach(item => {
        let ctr = 0
        keys.forEach(key => {
            if (ctr > 0) result += columnDelimiter

            result += item[key]

            ctr++
        })
        result += lineDelimiter
    })
    return result
}
// ** Downloads CSV
const downloadCSV = (data, tableOptions) => {
    if (data && data.length && data.length > 0 && tableOptions) {
        const link = document.createElement('a')
        let csv = convertArrayOfObjectsToCSV(data, tableOptions)
        if (csv === null) return
        if (!csv.match(/^data:text\/csv/i)) {
            csv = `data:text/csv;charset=utf-8,${`\ufeff${csv}`}`
        }
        link.setAttribute('href', encodeURI(csv))
        link.setAttribute('download', `${tableOptions.title}.csv`)    
    link.click()
}
}
export default downloadCSV