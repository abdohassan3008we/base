import QRCodeReact from 'qrcode.react'

/**
 * Display a QR code with the given data
 *
 * @param {String} data
 * @param {Number} size QR code size
 * @param {Number} imageSize
 * @param {Boolean} includeMargin
 * @returns {JSX.Element}
 * @author Mohamed Abdul-Fattah
 */
export default function QRCode({ data, size = 45, onClick = (_) => {}, imageSize = 10, includeMargin = true }) {
  return <QRCodeReact
    size={size}
    value={data}
    onClick={onClick}
    includeMargin={includeMargin}
    className='cursor-pointer'
    imageSettings={{
      src: '/static/media/logo2.48793bf7.png',
      excavate: true,
      width: imageSize,
      height: imageSize,
    }}
  />
}
