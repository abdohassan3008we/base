import React, { useEffect, useState } from 'react';
import { Treebeard, decorators } from 'react-treebeard';
import { useSkin } from '@hooks/useSkin'

/**
 * Generates a tree structure much like editors file browser.
 * On component mount, `dispatcher` function is called with no arguments to fetch the root node.
 *
 * @param {Function} dispatcher an API caller to fetch the next node('s)
 * @param {Object} store Redux store related to dispatcher function to help in request loading detection and data fetching
 * @param {Function} onNodeSelection an optional function to be triggered on a tree node selection
 * @returns {React.Component}
 * @author Mohamed Abdul-Fattah
 */
const Tree = ({ dispatcher, store, onNodeSelection = (_) => { } }) => {
  const [tree, setTree] = useState({});
  const [cursor, setCursor] = useState(false);
  const [skin, _] = useSkin()

  const treeStyle = {
    tree: {
      base: {
        borderRadius: "7px",
        listStyle: 'none',
        backgroundColor: skin === 'dark' ? 'rgb(22, 29, 49)' : "#F1EDE2",
        margin: 0,
        padding: "8px 0",
        color: skin === 'dark' ? '#9DA5AB' : "#555",
        fontFamily: '"Cairo"',
        fontSize: '14px',
      },
      node: {
        activeLink: {
          background: skin === 'dark' ? "#31363F" : "#E5D6B0"
        },
        toggle: {
          base: {
            position: 'relative',
            display: 'inline-block',
            verticalAlign: 'top',
            marginLeft: '5px',
            height: '24px',
            width: '24px'
          },
          wrapper: {
            position: 'absolute',
            top: '50%',
            left: '50%',
            margin: '-11px 0 0 -7px',
            height: '14px'
          },
          height: 14,
          width: 14,
          arrow: {
            fill: skin === 'dark' ? '#9DA5AB' : "#555",
            strokeWidth: 0
          }
        },
        header: {
          base: {
            display: 'inline-block',
            verticalAlign: 'top',
            color: 'rgb(208, 210, 214)'
          },
        },
      }
    }
  };

  useEffect(() => {
    // Get the root node
    dispatcher()
  }, [])

  // Update tree nodes
  useEffect(() => {
    if (cursor) {
      // There is a selected node
      // Append each child to its parent node
      setSelectedNodeChildren(store.data)
      // Disable loading phrase
      cursor.loading = false
      tree.loading = false
      // Re-render the tree nodes with the new children
      setTree(Object.assign({}, tree))
    } else {
      // No selected node
      let root = undefined
      if (store.data.length > 0) {
        // Assuming that locations tree has only one root
        const node = store.data[0]
        root = {
          id: node.locationId,
          isRoot: true,
          name: node.locationFieldName,
          children: node.hasChild ? [0] : null,
        }
      } else {
        // There is a problem with loading the tree
        root = {
          id: 0,
          isRoot: true,
          name: 'لا يوجد احداثيات',
        }
      }
      setTree(Object.assign({}, root))
    }
  }, [store.data])

  /**
   * Set selected node fetched children nodes
   *
   * @param {Array} children
   */
  const setSelectedNodeChildren = (children) => {
    if (cursor.children) {
      cursor.children.splice(0, cursor.children.length)
      for (const node of children) {
        cursor.children.push({
          id: node.locationId,
          name: node.locationFieldName,
          children: node.hasChild ? [0] : null,
        })
      }
    }
  }

  decorators.Loading = () => <small className="text-primary">جارٍ التحميل...</small>;
  decorators.Header = ({ node }) => {
    return (
      <>
        {!node.children?.length && (
          <div className="css-1ahpufi" style={{ transform: "rotateZ(0deg)" }}>
            <div className="css-9psjkc" />
          </div>
        )}
        <div style={{ display: "inline-block" }} className="css-2e3cfr">
          <div className="css-0">{node.name}</div>
        </div>
      </>
    )
  }

  /**
   * Handles on node selection
   *
   * @param {Object} node
   * @param {Number} toggled
   */
  const onToggle = (node, toggled) => {
    // API loading...
    node.loading = true;

    if (cursor) {
      cursor.active = false;
    }
    node.active = true;
    if (node.children) {
      node.toggled = toggled;
    }
    setCursor(node);
    if (!node.isRoot) tree.active = false;
    // Re-render tree nodes to draw the loading phrase
    setTree(Object.assign({}, tree))
    onNodeSelection(node.id)
    // Fetch selected tree node children
    if (node.children !== null) { dispatcher(node.id) }
  }

  return Object.keys(tree).length > 0 ? (
    <Treebeard data={tree} onToggle={onToggle} style={treeStyle} decorators={decorators} />
  ) : (
    <small className="text-primary">جارٍ التحميل...</small>
  )
}

export default Tree
