// ** React Imports
import React, { Fragment, useState, forwardRef } from 'react'


// ** Third Party Components
import DataTable from 'react-data-table-component'
import {
  ChevronDown,
  Share,
  Printer,
  FileText,
  File,
  Plus,
  Edit,
  Trash
} from 'react-feather'
import {
  Card,
  CardHeader,
  CardTitle,
  Button,
  Input,
  Label,
  Row,
  Col,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap'

//** import custom components 
import CustomPagination  from '@src/views/customizedComponents/CustomPagination'
import downloadCSV  from '@src/views/customizedComponents/DownloadCSV'
import DownloadExcel  from '@src/views/customizedComponents/DownloadExcel'
import ConfirmCancelPopup  from '@src/views/customizedComponents/ConfirmCancelPopup'

// ** Store & Actions
import { useDispatch } from 'react-redux'
import { deleteResource, updateSingleResource} from "@src/views/customizedComponents/Actions"


const BootstrapCheckbox = forwardRef(({ onClick, ...rest }, ref) => (
  <div className='custom-control custom-checkbox'>
    <input type='checkbox' className='custom-control-input' ref={ref} {...rest} />
    <label className='custom-control-label' onClick={onClick} />
  </div>
))

const GeneralTable = ({k, Table, data, meta, search, setIsUpdate, handleModal}) => {
    const Code = `${k}Code`

    // ** States
  const [currentPage, setCurrentPage] = useState(0)

  // ** Store Vars
  const dispatch = useDispatch()

  // ** handle delete
  const handelDelete = (e, row) => {
    e.preventDefault()
    ConfirmCancelPopup("هل تريد الحذف ", "حذف")
    .then(out => {
      if (out.isConfirmed) {
        dispatch(
          deleteResource(meta.path, meta.deleteType, row[`${Code}`])
        )
      }
    })
  }
      
  // ** handle update
  const handelUpdate = (e, row) => {
    e.preventDefault()
    setIsUpdate(true)
    dispatch(
      updateSingleResource(meta.updateType, row)
    )
      handleModal()
  }

  const actions = {
    name: 'Actions',
    allowOverflow: true,
    cell: row => {
      return (
        <div className='d-flex'>
            <Trash size={15} onClick={ (e) => handelDelete(e, row)} style={{marginLeft: "30px"}} />
            <Edit size={15} onClick={(e) => handelUpdate(e, row)} />
      
        </div>
      )
    }
  }

  const table =  Object.assign({}, Table)
  table.columns.push(actions)
    

  // ** Function to handle Pagination
  const pagination = () => {
    return  <CustomPagination 
      currentPage={currentPage}
      searchValue={search.value}
      filteredData={search.filterd}
      data={data}
      setCurrentPage={setCurrentPage}
      />
  }

  return (
    <Fragment>
      <Card>
        
        <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
          <CardTitle tag='h4'>{table.title}</CardTitle>

          <div className='d-flex mt-md-0 mt-1'>
              <UncontrolledButtonDropdown>
                <DropdownToggle color='secondary' caret outline>
                  <Share size={15} />
                  <span className='align-middle ml-50'>إستخراج</span>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem className='w-100'>
                    <Printer size={15} />
                    <span className='align-middle ml-50'>طباعة</span>
                  </DropdownItem>
                  {/* custom export csv sheet */}
                  <DropdownItem className='w-100' onClick={() => downloadCSV(data)}>
                    <FileText size={15} />
                    <span className='align-middle ml-50'>CSV</span>
                  </DropdownItem>
                  {/* custom export Excel sheet */}
                  <DownloadExcel columns={table.columns} data={data} filename={table.exportFileName} />
                  <DropdownItem className='w-100'>
                    <File size={15} />
                    <span className='align-middle ml-50'>PDF</span>
                  </DropdownItem>
                 
                </DropdownMenu>
              </UncontrolledButtonDropdown>
              <Button className='ml-2' color='primary' onClick={ (e) => { setIsUpdate(false); handleModal(e) }}>
                <Plus size={15} />
                <span className='align-middle ml-50'>{table.addButtonTitle}</span>
              </Button>
            </div>


        </CardHeader>
        
        <Row className='justify-content-end mx-0'>
          <Col className='d-flex align-items-center justify-content-end mt-1' md='6' sm='12'>
            <Label className='mr-1' for='search-input'>
              بحث...
            </Label>
            <Input
              className='dataTable-filter mb-50'
              type='text'
              bsSize='sm'
              id='search-input'
              value={search.value}
              onChange={search.handleFilterFunc}
            />
          </Col>
        </Row>

        <DataTable
          noHeader
          pagination
          selectableRows
          columns={Table.columns}
          paginationPerPage={10}
          className='react-dataTable'
          sortIcon={<ChevronDown size={10} />}
          paginationDefaultPage={currentPage + 1}
          paginationComponent={pagination}
          data={search.value.length ? search.filterd : data}
          selectableRowsComponent={BootstrapCheckbox}
        />

      </Card>

    </Fragment>
  )
}

export default GeneralTable
