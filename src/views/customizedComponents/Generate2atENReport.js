import jsPDF from "jspdf"
import "jspdf-autotable"
import { font } from "./Amiri-Regular-normal"
import { logo } from "./logobase64"
import moment from 'moment-timezone';
moment.locale('ar_SA');

const drawPageHeader = (doc) => {
  let title;
  //Report Title
  doc.setFontSize(12)
  title = ' نموذج اضافة 2 أ ت'
  doc.text(title || "", 380, 35);

  //Right Title
  doc.setFontSize(8)
  title = 'وزارة الدفاع'
  doc.text(title || "", 750, 25);
  title = ' - إدارة نظم المعلومات'
  doc.text(title || "", 740, 45);
  title = '(SC)'
  doc.text(title || "", 720, 45);

  //add MMC logo here
  var img = new Image()
  img.src = logo
  doc.addImage(img, 'png', 55, 10, 30, 25)
  //Left Title
  doc.setFontSize(8)
  title = 'نموذج رقم'
  doc.text(title || "", 65, 45);
  title = '2)'
  doc.text(title || "", 55, 45);
  title = '(أت'
  doc.text(title || "", 40, 45);

  //draw line
  doc.setLineWidth(0.5);
  doc.line(40, 55, 800, 55);

}

const drawHeaderTables = (doc, data) => {
  const INITIAL_X_AXIS = 40
  const INITIAL_Y_AXIS_FOR_TABLE = 65
  const INITIAL_Y_AXIS_FOR_STRING = 75
  let str;

  doc.setFontSize(9)


  // LEFT TABLE
  doc.setDrawColor(0);
  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE, 40, 40);
  str = `ختم \n الوحدة`
  doc.text(str, INITIAL_X_AXIS + 20, INITIAL_Y_AXIS_FOR_STRING + 5, { align: "center" })


  doc.rect(INITIAL_X_AXIS + 40, INITIAL_Y_AXIS_FOR_TABLE, 80, 20);
  str = `القسم`
  doc.text(str, INITIAL_X_AXIS + 80, INITIAL_Y_AXIS_FOR_STRING, { align: "center" })

  doc.rect(INITIAL_X_AXIS + 120, INITIAL_Y_AXIS_FOR_TABLE, 80, 20);
  str = `المجموعة`
  doc.text(str, INITIAL_X_AXIS + 160, INITIAL_Y_AXIS_FOR_STRING, { align: "center" })

  doc.rect(INITIAL_X_AXIS + 200, INITIAL_Y_AXIS_FOR_TABLE, 100, 20);
  str = `الوحدة`
  doc.text(str, INITIAL_X_AXIS + 250, INITIAL_Y_AXIS_FOR_STRING, { align: "center" })


  doc.rect(INITIAL_X_AXIS + 40, INITIAL_Y_AXIS_FOR_TABLE + 20, 80, 20);
  str = ' '
  doc.text(str, INITIAL_X_AXIS + 80, INITIAL_Y_AXIS_FOR_STRING + 20, { align: "center" })

  doc.rect(INITIAL_X_AXIS + 120, INITIAL_Y_AXIS_FOR_TABLE + 20, 80, 20);
  str = data?.responsGroupName||'  '
  doc.text(str, INITIAL_X_AXIS + 160, INITIAL_Y_AXIS_FOR_STRING + 20, { align: "center" })

  doc.rect(INITIAL_X_AXIS + 200, INITIAL_Y_AXIS_FOR_TABLE + 20, 100, 20);
  str = data?.receivingSiteName||'  '
  doc.text(str, INITIAL_X_AXIS + 250, INITIAL_Y_AXIS_FOR_STRING + 20, { align: "center" })


  // RIGHT TABLE
  doc.rect(INITIAL_X_AXIS + 540 + 40, INITIAL_Y_AXIS_FOR_TABLE, 80, 20);
  str = `التاريخ`
  doc.text(str, INITIAL_X_AXIS + 620, INITIAL_Y_AXIS_FOR_STRING, { align: "center" })

  doc.rect(INITIAL_X_AXIS + 540 + 120, INITIAL_Y_AXIS_FOR_TABLE, 100, 20);
  str = `رقم مستند الإضافة   `
  doc.text(str, INITIAL_X_AXIS + 710, INITIAL_Y_AXIS_FOR_STRING, { align: "center" })


  doc.rect(INITIAL_X_AXIS + 540 + 40, INITIAL_Y_AXIS_FOR_TABLE + 20, 80, 20);
  str = data?.form2atDate.toString()
  doc.text(str, INITIAL_X_AXIS + 620, INITIAL_Y_AXIS_FOR_STRING + 20, { align: "center" })

  doc.rect(INITIAL_X_AXIS + 540 + 120, INITIAL_Y_AXIS_FOR_TABLE + 20, 100, 20);
  str = data?.form2atNum.toString()
  doc.text(str, INITIAL_X_AXIS + 710, INITIAL_Y_AXIS_FOR_STRING + 20, { align: "center" })
}



const drawOrderData = (doc, data) => {

  const INITIAL_X_AXIS = 800
  const INITIAL_Y_AXIS = 120

  let str;
  doc.setFontSize(9)


  // FIRST COLUMN
  str = ' : وارد من'
  doc.text(str || "", INITIAL_X_AXIS, INITIAL_Y_AXIS, { align: 'right' });
  str = ' :  الوحدة المستفيدة'
  doc.text(str || "", INITIAL_X_AXIS, INITIAL_Y_AXIS + 20, { align: 'right' });
  str = ' : رقم العقد  '
  doc.text(str || "", INITIAL_X_AXIS, INITIAL_Y_AXIS + 40, { align: 'right' });
  str = ' :كيفية النقل'
  doc.text(str || "", INITIAL_X_AXIS, INITIAL_Y_AXIS + 60, { align: 'right' });
  
  
  // VALUE OF FIRST COLUMN
  doc.text(data.issuingSiteName || "", INITIAL_X_AXIS - 40, INITIAL_Y_AXIS, { align: 'right' });
  doc.text(data.beneficiarySiteName || "", INITIAL_X_AXIS - 60, INITIAL_Y_AXIS + 20, { align: 'right' });
  doc.text(data.relatedFormNum || "", INITIAL_X_AXIS - 60, INITIAL_Y_AXIS + 40, { align: 'right' });
  doc.text(data.transportationName || "", INITIAL_X_AXIS - 40, INITIAL_Y_AXIS + 60, { align: 'right' });
   

  // SECOND COLUMN
  str = ' : بجهة '
  doc.text(str || "", INITIAL_X_AXIS * (6/8), INITIAL_Y_AXIS, { align: 'right' });
  str = `: بجهة `
  doc.text(str || "", INITIAL_X_AXIS * (6/8), INITIAL_Y_AXIS + 20, { align: 'right' });
  str = ' : تاريخ العقد    '
  doc.text(str || "", INITIAL_X_AXIS * (6/8), INITIAL_Y_AXIS + 40, { align: 'right' });
  str = ' : ملاحظات'
  doc.text(str || "", INITIAL_X_AXIS * (6/8), INITIAL_Y_AXIS + 60, { align: 'right' });

  // VALUE OF SECOND COLUMN
  doc.text(data.issuingAreaName, INITIAL_X_AXIS * (6/8) - 50, INITIAL_Y_AXIS, { align: 'right' });
  doc.text(data.beneficiaryAreaName || "", INITIAL_X_AXIS * (6/8) - 50, INITIAL_Y_AXIS + 20, { align: 'right' });
  doc.text(data.relatedFormApproveDate || "", INITIAL_X_AXIS * (6/8) - 90, INITIAL_Y_AXIS + 40, { align: 'right' });
  doc.text(data.remarks || "لا يوجد", INITIAL_X_AXIS * (6/8) - 90, INITIAL_Y_AXIS + 60, { align: 'right' });


  // THIRD COLUMN
  str = ' : وارد الى'
  doc.text(str || "", INITIAL_X_AXIS * (4/8), INITIAL_Y_AXIS, { align: 'right' });
  str = ' : اسباب الصرف'
  doc.text(str || "", INITIAL_X_AXIS * (4/8), INITIAL_Y_AXIS + 20, { align: 'right' });
  str = ' : رقم لجنة الفحص'
  doc.text(str || "", INITIAL_X_AXIS * (4/8), INITIAL_Y_AXIS + 40, { align: 'right' });
  
  // VALUE OF THIRD COLUMN
  doc.text(data.receivingSiteName || "", INITIAL_X_AXIS * (4/8) - 60, INITIAL_Y_AXIS, { align: 'right' });
  doc.text(data.issueReasonName || "", INITIAL_X_AXIS * (4/8) - 80, INITIAL_Y_AXIS + 20, { align: 'right' });
  doc.text(data.commInspectNum || " ", INITIAL_X_AXIS  * (4/8)- 40, INITIAL_Y_AXIS + 40, { align: 'right' });
   
 // THIRD COLUMN
 str = ' : بجهة'
 doc.text(str || "", INITIAL_X_AXIS * (2/8), INITIAL_Y_AXIS, { align: 'right' });
 str = ' : الغرض من الصرف'
 doc.text(str || "", INITIAL_X_AXIS * (2/8), INITIAL_Y_AXIS + 20, { align: 'right' });
 str = ' : تاريخ لجنة الفحص'
 doc.text(str || "", INITIAL_X_AXIS * (2/8), INITIAL_Y_AXIS + 40, { align: 'right' });

 // VALUE OF THIRD COLUMN
 doc.text(data.beneficiaryAreaName || "", INITIAL_X_AXIS * (2/8) - 60, INITIAL_Y_AXIS, { align: 'right' });
 doc.text(`${data.issuePurposeName || "" }` , INITIAL_X_AXIS * (2/8) - 80, INITIAL_Y_AXIS + 20, { align: 'right' });
 doc.text(data.commInspectDate || " ", INITIAL_X_AXIS  * (2 /8)- 40, INITIAL_Y_AXIS + 40, { align: 'right' });



}



const drawItemsTable = (doc, items) => {
  const headers = [[
    "امر\nالتوريد",
    "رقم\nالعينة",
    "حالة\nالصنف",
    "وحدة\nالصرف",
    'الكمية\nالمصدق بها',
    "الكمية\nالمنصرفة",
    "سعر\nالصنف",
    "اجمالي\nالسعر",
    "الكمية\nالمستحقة",
    "تاريخ صرف\nالمستحق",
    "رقم /اللوط\nالمسلسل", 
    "رقم دفتر\nالعهدة",
    "رقم الصفحة",
    "تم\ الاضافة",
  ].reverse()]

  const data = []
  items.forEach(item => {
    data.push([
      '',
      item.partNumber,
      item.itemConditionName,
      item.itemUiName,
      item.approvedQty,
      '',
      item.unitPrice,
      '',
      item.dueOutQty,
      item.dueOutIssueDate,
      item.itemLotSerialNum,
      '',
      item.daftarNum,
      item.pageNum,
      item.pulledFlag == '0'? "لا":"نعم"
    ].reverse())
    data.push([
      { content: `${item.fscCode||' '}-${item.niinCode||' '}`, colSpan: 5, styles: { halign: 'center' } },
      { content: item.itemName||' ', colSpan: 5, styles: { halign: 'center' } },
      { content: item.allLocationName||' ', colSpan: 5, styles: { halign: 'center' } },
    ].reverse())
  })

  const content = {
    startY: 240,
    head: headers,
    body: data,
    headStyles :{fillColor : [142, 142, 142]}, 
    styles: { halign: 'center', font: "Amiri-Regular", fontStyle: "normal" },
    columnStyles: {
      13: { minCellWidth: 42 },
      12: { cellWidth: 40 },
      11: { minCellWidth: 43 },
      10: { minCellWidth: 40 },
      // 10: { minCellWidth: 40 },
      8: { cellWidth: 58 },
     7 :{ minCellWidth: 41 },
      6: { minCellWidth: 36 },
      5: { minCellWidth: 53 },
      // 5: { minCellWidth: 40 },
      //4: { minCellWidth: 50 },
      3: { minCellWidth: 50 },
      // 2: { minCellWidth: 40 },
      // 1: { minCellWidth: 40 },
      0: { minCellWidth: 40 },
    }
  }
  doc.autoTable(content)
}
const drawFooterTables = (doc, data, { receivingSiteLeader, responsGroupLeader, responsPerson, ohdaPerson}) => {
  let INITIAL_X_AXIS = 40
  const INITIAL_Y_AXIS_FOR_TABLE = 510
  const INITIAL_Y_AXIS_FOR_STRING = 525
  let str;
 

  doc.setFontSize(9)

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE, 160, 20);
  str = `قائد ` + data?.receivingSiteName||' '
  doc.text(str, INITIAL_X_AXIS + 80, INITIAL_Y_AXIS_FOR_STRING, { align: "center" })

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE + 20, 160, 20);
  str = `${receivingSiteLeader.titleName ||' '} / ${receivingSiteLeader.personName||' '}`
  doc.text(str, INITIAL_X_AXIS + 80, INITIAL_Y_AXIS_FOR_STRING + 18, { align: "center" })


  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE + 40, 160, 20);
  str = `(                                            ) التوقيع`
  doc.text(str, INITIAL_X_AXIS + 150, INITIAL_Y_AXIS_FOR_STRING + 35, { align: "right" })

  INITIAL_X_AXIS += 170

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE, 140, 20);
  str = `قائد ` + data?.responsGroupName ||'  '
  doc.text(str, INITIAL_X_AXIS + 70, INITIAL_Y_AXIS_FOR_STRING, { align: "center" })

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE + 20, 140, 20);
  str = str = `${responsGroupLeader?.titleName||' '} / ${responsGroupLeader?.personName||' '}`
  doc.text(str, INITIAL_X_AXIS + 70, INITIAL_Y_AXIS_FOR_STRING + 18, { align: "center" })

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE + 40, 140, 20);
  str = `(                                   ) التوقيع`
  doc.text(str, INITIAL_X_AXIS + 130, INITIAL_Y_AXIS_FOR_STRING + 35, { align: "right" })

  INITIAL_X_AXIS += 150

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE, 140, 20);
  str = `ضابط العهدة`
  doc.text(str, INITIAL_X_AXIS + 70, INITIAL_Y_AXIS_FOR_STRING, { align: "center" })

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE + 20, 140, 20);
  str = str = `${ohdaPerson?.titleName||''} / ${ohdaPerson?.personName||' '}`
  doc.text(str, INITIAL_X_AXIS + 70, INITIAL_Y_AXIS_FOR_STRING + 18, { align: "center" })

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE + 40, 140, 20);
  str = `(                                   ) التوقيع`
  doc.text(str, INITIAL_X_AXIS + 130, INITIAL_Y_AXIS_FOR_STRING + 35, { align: "right" })

  INITIAL_X_AXIS += 150

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE, 140, 20);
  str = `مكتب الشطب`
  doc.text(str, INITIAL_X_AXIS + 70, INITIAL_Y_AXIS_FOR_STRING, { align: "center" })

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE + 20, 140, 40);
  str = `/`
  doc.text(str, INITIAL_X_AXIS + 70, INITIAL_Y_AXIS_FOR_STRING + 18, { align: "center" })

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE + 40, 140, 20);
  str = `(                                   ) التوقيع`
  doc.text(str, INITIAL_X_AXIS + 130, INITIAL_Y_AXIS_FOR_STRING + 35, { align: "right" })

  INITIAL_X_AXIS += 150

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE, 140, 20);
  str = `المستلم`
  doc.text(str, INITIAL_X_AXIS + 70, INITIAL_Y_AXIS_FOR_STRING, { align: "center" })

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE + 20, 140, 40);
  str = `${responsPerson.titleName||' '} / ${responsPerson.personName||' '}`
  doc.text(str, INITIAL_X_AXIS + 70, INITIAL_Y_AXIS_FOR_STRING + 18, { align: "center" })

  doc.rect(INITIAL_X_AXIS, INITIAL_Y_AXIS_FOR_TABLE + 40, 140, 20);
  str = `(                                   ) التوقيع`
  doc.text(str, INITIAL_X_AXIS + 130, INITIAL_Y_AXIS_FOR_STRING + 35, { align: "right" })


}

const generate2atEnReport = (data, items, { receivingSiteLeader, responsGroupLeader, responsPerson, ohdaPerson }) => {

  const unit = "pt"
  const size = "A4" // Use A1, A2, A3 or A4
  const orientation = "landscape" // portrait or landscape
  const doc = new jsPDF(orientation, unit, size)
  doc.addFont('Amiri-Regular-normal.ttf', 'Amiri-Regular', 'normal')
  doc.setFont('Amiri-Regular')

  drawPageHeader(doc);
  drawHeaderTables(doc, data);
  drawOrderData(doc, data);
  drawItemsTable(doc, items);
  drawFooterTables(doc, data, { receivingSiteLeader, responsGroupLeader, responsPerson, ohdaPerson });
  window.open(doc.output('bloburl'));
  // doc.save(`نموذج اضافة.pdf`)
}
export default generate2atEnReport;
