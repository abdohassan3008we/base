import {
  Input,
  Label,
  Button,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  CustomInput
} from "reactstrap";
import {
  Share,
  FileText,
  File,
  Clipboard
} from "react-feather";
import downloadCSV from "@src/views/customizedComponents/DownloadCSV";
import exportPDF from "@src/views/customizedComponents/DownloadPDF";
import DownloadExcel from "@src/views/customizedComponents/DownloadExcel";
import { Link } from "react-router-dom";

const TableHeader = ({
  handleAddModal,
  handleFilter,
  searchTerm,
  tableTitle,
  columns,
  data,
  addingButton,
  showAddBtn = true,
  showExtractBtn = true,
  showReportOption = false,
  generateReport = () => {},
  showSearch = true,
  addButtonText = "إضافة",
  customBtns = null,
  showAllSwitch = false,
  showAllSwitchTitle = '',
  customSwitchHandle = null,
  buttons = []
}) => {
  const handleButtonRipple = () => {
    return showAddBtn && (
      addingButton ? (
        <>
          <Link to={addingButton}>
            <Button.Ripple color="primary">{addButtonText}</Button.Ripple>
          </Link>
          &nbsp;
          &nbsp;
          {customBtns}
        </>
      ) : (
        <>
          <Button.Ripple
            color="primary"
            onClick={() => handleAddModal()}>
            {addButtonText}
          </Button.Ripple>
          &nbsp;
          &nbsp;
          {customBtns}
        </>
      )
    )
  }

  const handleShowAllSwitch = () => {

    return showAllSwitch && (
      (
        <div style={{ display: "flex", paddingRight: "10px" }}>
          <label style={{ paddingLeft: "5px", margin: "auto" }}> {showAllSwitchTitle} </label>
          <div style={{ margin: "auto" }}>
            <CustomInput
              type="switch"
              id="showAllData"
              name="showAllData"
              onChange={(e) => customSwitchHandle(e)}
            />
          </div>

        </div>
      )
    )
  }

  const handleButtons = () => {
    return (
      buttons.map((button, index) => {
        if ('url' in button) return (
          <>
            <Link key={index} to={button.url}>
              <Button.Ripple color={button.color || "primary"}>{button.title}</Button.Ripple>
            </Link>
            &nbsp;
            &nbsp;
          </>

        )

        if ('handler' in button) return (
          <>
            <Button.Ripple
              color={button.color || "primary"}
              onClick={button.handler}>
              {button.title}
            </Button.Ripple>
            &nbsp;
            &nbsp;
          </>
        )
      })
    )

    // return (
    //   Buttons.map((Button, index) => <Button key={index} />)
    // )
  }

  const handleExtractBtn = () => {
    return showExtractBtn ? (
      <UncontrolledButtonDropdown style={{ margin: "10px" }}>
        <DropdownToggle color="secondary" caret outline>
          <Share size={15} />
          <span className="align-middle ml-50">إستخراج</span>
        </DropdownToggle>
        <DropdownMenu right>

          {/* custom export csv sheet */}
          <DropdownItem className="w-100" onClick={() => downloadCSV(data, { title: tableTitle, columns })}>
            <FileText size={15} />
            <span className="align-middle ml-50">CSV</span>
          </DropdownItem>
          {/* custom export Excel sheet */}
          <DownloadExcel columns={columns} data={data} filename={tableTitle} />
          <DropdownItem className="w-100" onClick={() => exportPDF(data, { title: tableTitle, columns })} >
            <File size={15} />
            <span className="align-middle ml-50">PDF</span>
          </DropdownItem>
          {showReportOption ?
            <DropdownItem className="w-100" onClick={_ => {
              generateReport()
            }} >
              <Clipboard size={15} />
              <span className="align-middle ml-50">Report</span>
            </DropdownItem> : <></>
          }
        </DropdownMenu>
      </UncontrolledButtonDropdown>
    ) : (
      null
    );
  }

  return (
    <div className="invoice-list-table-header w-100 mr-1 ml-50 mt-2 mb-75">
      <div className="d-flex" style={{ justifyContent: "space-between" }}>
        {showSearch && (
          <>
            <div className="d-flex align-items-center mb-sm-0 mb-1 mr-1">
              <Label className="mb-0" for="search-invoice">
                بحث
              </Label>
              <Input
                id="search-invoice"
                className="ml-50 w-100"
                type="text"
                defaultValue={searchTerm}
                onChange={e => handleFilter(e.target.value)}
                autoComplete="off"
              />
              {handleShowAllSwitch()}
            </div>

          </>
        )}
        <div>

          {handleButtons()}
          {handleButtonRipple()}
          {handleExtractBtn()}
        </div>
      </div>
    </div>
  )
}

export default TableHeader;
