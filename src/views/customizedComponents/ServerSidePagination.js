import { useTranslation } from "react-i18next"
import ReactPaginate from "react-paginate"
import { Label, Input } from 'reactstrap'

/**
 * @param {Number} totalRecords
 * @param {Function} onPagination
 * @param {Number} perPage
 * @param {Function} setPerPage
 * @param {Number} currentPage
 * @param {Function} setCurrentPage
 * @returns {JSX.Element}
 * @author Mohamed Abdul-Fattah
 */
export default function ServerSidePagination({
  totalRecords,
  onPagination,
  perPage,
  setPerPage,
  currentPage,
  setCurrentPage,
}) {
  const count = Math.ceil(totalRecords / perPage)
  const { t } = useTranslation()
  const handlePageChange = (rowsPerPage, page) => {
    setPerPage(rowsPerPage)
    setCurrentPage(page + 1)
    onPagination(page, rowsPerPage)
  }

  return (
    <div className="d-flex sc-fzoNJl dGqwdB rdt_Pagination" style={{ paddingRight: "1em" }}>
      <ReactPaginate
        previousLabel=""
        nextLabel=""
        pageCount={count || 1}
        activeClassName="active"
        forcePage={currentPage !== 0 ? currentPage - 1 : 0}
        pageRangeDisplayed={2}
        onPageChange={({ selected }) => { handlePageChange(perPage, selected) }}
        pageClassName="page-item"
        nextLinkClassName="page-link"
        nextClassName="page-item next"
        previousClassName="page-item prev"
        previousLinkClassName="page-link"
        pageLinkClassName="page-link"
        containerClassName="pagination react-paginate justify-content-start my-2 pr-1"
      />
      <div className="d-flex align-items-center w-100">
        <Label for="rows-per-page">{t("Show")}</Label>
        <Input
          className="form-control mx-50"
          type="select"
          id="rows-per-page"
          value={perPage}
          onChange={e => { handlePageChange(e.target.value, 0) }}
          style={{
            direction: "ltr",
            width: "5rem",
            padding: "0 0.8rem",
            backgroundPosition: "calc(100% - 3px) 11px, calc(100% - 20px) 13px, 100% 0"
          }}
        >
          <option value={5}>5</option>
          <option value={10}>10</option>
          <option value={25}>25</option>
          <option value={50}>50</option>
        </Input>
        <Label for="rows-per-page">{t("Rows")}</Label>
      </div>
    </div>
  )
}
