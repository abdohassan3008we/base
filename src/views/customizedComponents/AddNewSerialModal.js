import {
  Button,
  Card,
  CardBody,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row
} from 'reactstrap';
import React, { useEffect } from 'react';
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
import Modal from './Modal';
import DatePicker from './DatePicker';
import { addItemLotSerialdRequestAsync } from '../../redux/admin/itemLotSerial/thunk';
import { SERIAL } from '../../redux/ref/serialType/types';

const ModalForm = ({ catalogItemId, handleModal, serialTypeCode, onSuccess, onFailure }) => {
  const dispatch = useDispatch()
  const formik = useFormik({
    initialValues: {
      active: true,
      catalogItemId: catalogItemId,
      expireDate: null,
      itemLotSerialNum: null,
      productionDate: null,
      remarks: null,
    },
    enableReinitialize: true,
    onSubmit: values => {
      dispatch(addItemLotSerialdRequestAsync({
        values,
        onSuccess: ({ itemLotSerialId, itemLotSerialNum }) => {
          handleModal()
          onSuccess({ value: itemLotSerialId, label: values.itemLotSerialNum })
          formik.resetForm()
        },
        onFailure,
      }))
    },
  })

  // Reset form on unmount
  useEffect(() => formik.resetForm, [])

  return (
    <Form onSubmit={formik.handleSubmit}>
      <Card>
        <CardBody>
          <Row>
            <Col md={4}>
              <FormGroup>
                <Label for='productionDate'>تاريخ الانتاج</Label>
                <DatePicker
                  name='productionDate'
                  onChange={date => formik.setFieldValue('productionDate', date)}
                />
              </FormGroup>
            </Col>
            <Col md={4}>
              <FormGroup>
                <Label for='expireDate'>تاريخ انتهاء العمر المخزنى</Label>
                <DatePicker
                  name='expireDate'
                  onChange={date => formik.setFieldValue('expireDate', date)}
                />
              </FormGroup>
            </Col>
            <Col md={4}>
              <FormGroup>
                <Label for='itemLotSerialNum'>{serialTypeCode === SERIAL ? 'السيريال' : 'اللوط'}</Label>
                <Input
                  className='form-control'
                  id='itemLotSerialNum'
                  { ...formik.getFieldProps('itemLotSerialNum') }
                />
              </FormGroup>
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Row>
        <Col md={6} style={{ textAlign: 'left' }}>
          <Button type='submit' color='primary'>حفظ</Button>
        </Col>
        <Col md={6}>
          <Button type='button' color='secondary' onClick={handleModal}>إلغاء</Button>
        </Col>
      </Row>
    </Form>
  )
}

/**
 * Create new serial/LOT modal form
 *
 * @param {Boolean} open indicates whether the modal should open or dismiss
 * @param {Function} handleModal
 * @param {Number} catalogItemId
 * @param {String} serialTypeCode
 * @param {Function} onSuccess on serial creation success
 * @param {Function} onFailure on serial creation failure
 * @returns {JSX.Element}
 * @author Mohamed Abdul-Fattah
 */
export default function AddNewSerialModal({
  open,
  handleModal,
  catalogItemId,
  serialTypeCode,
  onSuccess=(_) => {},
  onFailure=(_) => {},
}) {
  return (
    <Modal
      open={open}
      title={serialTypeCode === SERIAL ? 'أضف سيريال جديد' : 'أضف لوت جديد'}
      handleModal={handleModal}
      body={
        <ModalForm
          catalogItemId={catalogItemId}
          onSuccess={onSuccess}
          onFailure={onFailure}
          handleModal={handleModal}
          serialTypeCode={serialTypeCode}
        />
      }
    />
  )
}
