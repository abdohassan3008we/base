import { Label, Input } from "reactstrap";
import ReactPaginate from "react-paginate";
import React from "react";
import { useTranslation } from "react-i18next";

/**
 * Custom pagination component for Table component
 *
 * @param {[Object]} data
 * @param {Number} currentPage
 * @param {Function} setCurrentPage
 * @param {Number} rowsPerPage
 * @param {Function} setRowsPerPage
 * @param {Function} forceUpdate force update Table component to re-draw table data on rows per page change
 * @returns {JSX.Element}
 * @author Mohamed Abdul-Fattah
 */
const Pagination = ({
  data,
  currentPage,
  setCurrentPage,
  rowsPerPage,
  setRowsPerPage,
  forceUpdate,
}) => {
  const count = Math.ceil(data.length / rowsPerPage);

  /**
   * Update the current page with the selected page
   *
   * @param {Object} page
   */
  const handlePagination = (page) => {
    setCurrentPage(page.selected + 1);
  };
  const { t } = useTranslation();
  /**
   * Update selected rows per page
   *
   * @param {String} rowsNum
   */
  const handleRowsPerPage = (rowsNum) => {
    setRowsPerPage(parseInt(rowsNum));
    setCurrentPage(1);
    // Force update table to rerender the data on rows per page change
    forceUpdate();
  };

  return (
    <div
      className="d-flex sc-fzoNJl dGqwdB rdt_Pagination"
      style={{ paddingRight: "1em" }}
    >
      {" "}
      {/* theme classes to update UI dark and light modes */}
      <ReactPaginate
        previousLabel=""
        nextLabel=""
        pageCount={count || 1}
        activeClassName="active"
        forcePage={currentPage !== 0 ? currentPage - 1 : 0}
        pageRangeDisplayed={2}
        onPageChange={handlePagination}
        pageClassName="page-item"
        nextLinkClassName="page-link"
        nextClassName="page-item next"
        previousClassName="page-item prev"
        previousLinkClassName="page-link"
        pageLinkClassName="page-link"
        containerClassName="pagination react-paginate justify-content-start my-2 pr-1"
      />
      <div className="d-flex align-items-center w-100">
        <Label for="rows-per-page">{t("Show")}</Label>
        <Input
          className="form-control mx-50"
          type="select"
          id="rows-per-page"
          value={rowsPerPage}
          onChange={(e) => handleRowsPerPage(e.target.value)}
          style={{
            direction: "ltr",
            width: "5rem",
            padding: "0 0.8rem",
            backgroundPosition:
              "calc(100% - 3px) 11px, calc(100% - 20px) 13px, 100% 0",
          }}
        >
          <option value={5}>5</option>
          <option value={10}>10</option>
          <option value={25}>25</option>
          <option value={50}>50</option>
        </Input>
        <Label for="rows-per-page">{t("Rows")}</Label>
      </div>
    </div>
  );
};

export default Pagination;
