import React from "react"
import ReactExport from "react-export-excel"
import {
    DropdownItem
} from 'reactstrap'

import {
    Grid
} from 'react-feather'

const ExcelFile = ReactExport.ExcelFile
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn


const customButton = (
    <DropdownItem className='w-100'>
        <Grid size={15} />
        <span className='align-middle ml-50'>Excel</span>
    </DropdownItem>
)

// ** React Compnent to download Excel sheet
const DownloadExcel = ({columns, data, filename}) => {
    const cols = columns.slice(0, -1)

    return (
        <ExcelFile filename={filename} element={customButton}>
             <ExcelSheet data={data} name={filename}>
               {cols.map((col, i) => (
                   <ExcelColumn key={i} label={col.name} value={col.selector}/>
                ))}
            </ExcelSheet>
        </ExcelFile>
    )
}

export default DownloadExcel
