import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import '@styles/base/plugins/extensions/ext-component-sweet-alerts.scss'
const MySwal = withReactContent(Swal)

const ErrorPopup = (errorMessage) => {

    MySwal.fire({
        html: `<strong>${errorMessage}</strong>`,
        icon: 'error',
        customClass: {
            confirmButton: 'btn btn-primary'
        },
        buttonsStyling: false,
        confirmButtonText: "حسنا",
        position: "bottom-end",
    })
}

export default ErrorPopup
