import React, { useState, useEffect } from "react"
import DataTable from "react-data-table-component"
import { ChevronDown } from "react-feather"
import { Link } from "react-router-dom";
import { Button, Card, Input, Label, Row, Spinner } from "reactstrap"
import ServerSidePagination from "./ServerSidePagination"
// ** Styles
import "@styles/react/libs/tables/react-dataTable-component.scss";
import { useTranslation } from "react-i18next";

/**
 * Table listing data with server side pagination
 *
 * @param {Array} columns
 * @param {Array} data
 * @param {Number} totalRecords total number of elements
 * @param {Function} onPagination on pagination change (page or perPage change)
 * @param {Boolean} progress indicator for whether loading has finished or not
 * @param {Boolean} showAddBtn whether to show add button or not
 * @param {Function} handleAddButton 
 * @param {String} addBtnUrl add button URL if add form is a separate page
 * @param {String} addButtonText
 * @param {Boolean} showSearch
 * @param {Integer} refresherKey a key to re-render table on change
 * @returns {JSX.Element}
 * @author Mohamed Abdul-Fattah
 */


export default function ServerSideTable(


  {
    columns,
    data,
    totalRecords,
    onPagination=()=>{},
    progress = false,
    showAddBtn = true,
    handleAddButton = () => { },
    addBtnUrl,
    showSearch = true,
    refresherKey = 1
  }) {
  const [perPage, setPerPage] = useState(5)
  const [currentPage, setCurrentPage] = useState(0)
  const [searchTerm, setSearchTerm] = useState('')
  const [key, setKey] = useState(1)
  const { t } = useTranslation()

  const updatePerPage = (num) => {
    setPerPage(parseInt(num, 10))
    setKey(k => k + 1)
  }

  useEffect(() => {
    onPagination(currentPage, perPage, searchTerm)
  }, [currentPage, perPage, searchTerm, refresherKey])

  const addButton = () =>  (
    showAddBtn?(  addBtnUrl ? (
      <div>
        <Link to={addBtnUrl}>
          <Button.Ripple color="primary">{t("Add")}</Button.Ripple>
        </Link>
      </div>
    ) : (
      <div>
        <Button.Ripple
          color="primary"
          onClick={() => handleAddButton()}>
          {t("Add")}
        </Button.Ripple>
      </div>
    )):null
  )

  return (
    <Card>
      <Row className='justify-content-end mx-0 ml-1'>
        <div className="invoice-list-table-header w-100 mr-1 ml-50 mt-2 mb-75 mr-2">
          <div className="d-flex" style={{ justifyContent: "space-between" }}>
            {showSearch && (
              <div className="d-flex align-items-center mb-sm-0 mb-1 mr-1">
                <Label className="mb-0" for="search-invoice">
                  {t("Search")}
                </Label>
                <Input
                  id="search-invoice"
                  className="ml-50 w-100"
                  type="text"
                  value={searchTerm}
                  onChange={e => {
                    if (e.target.value[0] == '{' && e.target.value[e.target.value.length - 1] == '}') {
                      let obj = JSON.parse(e.target.value)
                      let approvalNum = obj.approvalNum
                      setSearchTerm(approvalNum)
                    } else {
                      setSearchTerm(e.target.value)
                    }
                  }
                  }
                  autoComplete="off"
                />
              </div>
            )}
            {addButton()}
          </div>
        </div>
      </Row>
      <DataTable
        key={key}
        noHeader
        responsive
        className="react-dataTable"
        sortIcon={<ChevronDown size={10} />}
        columns={columns}
        data={data}
        pagination
        paginationPerPage={perPage}
        paginationComponent={() => (
          <ServerSidePagination
            totalRecords={totalRecords}
            perPage={perPage}
            setPerPage={updatePerPage}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            onPagination={onPagination}
          />
        )}
        noDataComponent={(
          <p style={{ className: "react-dataTable", margin: "1em" }}> {t("No Data Found")}</p>
        )}
        progressPending={progress}
        progressComponent={(
          <div className="react-datatable-loader" style={{ width: "100%", height: "100%", textAlign: "center", padding: "2%" }}>
            {t("Loading ......")}
            &nbsp;
            &nbsp;
            <Spinner />
          </div>
        )}
      />
    </Card>
  )
}
