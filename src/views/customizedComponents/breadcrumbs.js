import { Fragment } from 'react'
import { Breadcrumb, BreadcrumbItem } from 'reactstrap'
import { Link } from 'react-router-dom'

const BreadcrumbsDefault = ({ list }) => {

  if(list) return (
    <Fragment>
      <Breadcrumb style={{ marginBottom: '20px' }}>
        {
          list.map((item, index) => {
            if (item.link) return (
              <BreadcrumbItem key={index} >
                <h5>
                  <Link key={index} to={item.link} > {item.name} </Link>
                </h5>
              </BreadcrumbItem>)
          })
        }
        <BreadcrumbItem active>
            <h5> {list[list.length - 1].name} </h5>
        </BreadcrumbItem>
      </Breadcrumb>
    </Fragment>
  ); else return null
}
export default BreadcrumbsDefault
