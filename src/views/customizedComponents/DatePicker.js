import Flatpickr from 'react-flatpickr';
import '@styles/react/libs/flatpickr/flatpickr.scss';

/**
 * Displays a human friendly date picker.
 *
 * @param {String} name HTML field name
 * @param {Function} onChange on data change handler
 * @param {String} defaultValue
 * @param {Object} options styling options according to flatpicker styling documentation
 * @param {Boolean} disabled indicates whether the HTML input is disabled or not?
 * @returns {JSX.Element}
 * @author Mohamed Abdul-Fattah
 */
export default function DatePicker({
  name,
  onChange=(_) => {},
  defaultValue=undefined,
  options={},
  disabled=false
}) {
  return (
    <Flatpickr
      id={name}
      className='date-picker'
      name={name}
      value={defaultValue}
      onChange={(_, date) => onChange(date,"yyyy-mm-dd")}
      options={{
         altFormat: "d/m/Y",
        dateFormat: "Y-m-d", 
        altInput: true, ...options }}
      disabled={disabled}
    />
  );
}
