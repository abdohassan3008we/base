import { useEffect } from "react";
import { Helmet } from "react-helmet";
import app from "@src/configs/app_data";
import { useTranslation } from "react-i18next";

/**
 * Updates document title with the given one
 *
 * @param {String} title
 * @returns {JSX.Element}
 * @author Mohamed Abdul-Fattah
 */

export default function PageTitle({ title }) {
  const { t } = useTranslation();
  useEffect(() => {
    return (_) => {
      document.title = t(localStorage.getItem("app_name"));
    };
  }, []);

  return (
    <Helmet>
      <title>
        {title} | {t(localStorage.getItem("app_name"))}
      </title>
    </Helmet>
  );
}
