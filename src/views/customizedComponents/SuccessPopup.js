import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import '@styles/base/plugins/extensions/ext-component-sweet-alerts.scss'
const MySwal = withReactContent(Swal)
const SuccessPopup = (successMsg) => {

let timerInterval
MySwal.fire({
        // title: 'Good job!',
        // text: 'تم الحفظ بنجاح',
        html: `<strong>${successMsg}</strong>`,
        timer: 2000,
        icon: 'success',
        confirmButtonText: "حسنا",
        customClass: {
            confirmButton: 'btn btn-primary',
        },
        buttonsStyling: false,
        position: "bottom-end",
        width: "auto",
        willOpen() {
        // Swal.showLoading()
        // timerInterval = setInterval(function () {
            // Swal.getContent().querySelector('strong').textContent = Swal.getTimerLeft()
        // }, 100)
        },
        willClose() {
        clearInterval(timerInterval)
        }
    })
}

export default SuccessPopup
