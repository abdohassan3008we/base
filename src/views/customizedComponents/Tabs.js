import React, { Fragment, useEffect, useState } from "react"
import { TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap"

/**
 * Display tabs with the given components 
 *
 * @param {[Object]} items
 * @returns {React.Component}
 * @example <Tabs items=[{ name: "Lorem Ipsum", num: 1, component: <List /> }] />
 */
const Tabs = ({ items }) => {
  const [active, setActive] = useState(1)

  useEffect(() => {
    if (items.length > 0) {
      setActive(items[0].num)
    }
  }, [])

  const toggle = tab => {
    if (active !== tab) {
      setActive(tab)
    }
  }

  return (
    <Fragment>
      <Nav tabs>
        {
          items.map(item => {
            return (
              <NavItem key={item.num}>
                <NavLink
                  active={active === item.num}
                  onClick={() => {
                    toggle(item.num)
                  }}
                >
                  {item.name}
                </NavLink>
              </NavItem>
            )
          })
        }
      </Nav>
      <TabContent className="py-50" activeTab={active}>
        {
          items.map(item => {
            return (
              <TabPane tabId={item.num} key={item.num}>
                {item.component}
              </TabPane>
            );
          })
        }
      </TabContent>
    </Fragment>
  )
}

export default Tabs
