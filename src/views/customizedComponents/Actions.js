import axios from "axios";
import ErrorPopup from "@src/views/customizedComponents/ErrorPopup";
import SuccessPopup from "@src/views/customizedComponents/SuccessPopup";

const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;

// ** Get Table Data
export const getData = (path, action_type, body = null, token = null) => {
  return async (dispatch) => {
    await axios
      .get(`${REACT_APP_API_HOST}/${path}`)
      .then((response) => {
        // if (response.status === 200) {
        //     dispatch({
        //         type: action_type,
        //         payload: response.data
        //     })
        // }
      })
      .catch((error) => {
        ErrorPopup("حدث شئ خطأ");
      });
  };
};

// ** Create A new Resource
export const createNewResource = (
  path,
  action_type,
  body,
  toggelModal,
  token = null
) => {
  return async (dispatch) => {
    await axios
      .post(`${REACT_APP_API_HOST}/${path}`, body, {
        headers: {
          // 'Authorization': 'my secret token'
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        if (response.status === 201) {
          dispatch({
            type: action_type,
            payload: response.data,
          });
          toggelModal();
          SuccessPopup("تم الحفظ بنجاح");
        }
      })
      .catch((error) => {
        if (error.response.status === 302) {
          ErrorPopup("تم تسجيل هذا الكود من قبل");
        } else {
          ErrorPopup("البيانات غير صحيحة");
        }
      });
  };
};

// ** Delete Resource
export const deleteResource = (path, action_type, resourceId, token = null) => {
  return async (dispatch) => {
    await axios
      .delete(`${REACT_APP_API_HOST}/${path}${resourceId}`)
      .then((response) => {
        if (response.status === 204) {
          dispatch({
            type: action_type,
            payload: resourceId,
          });
          SuccessPopup("تم الحذف بنجاح");
        }
      })
      .catch((error) => {
        ErrorPopup("تعذر الحذف");
      });
  };
};

export const updateSingleResource = (action_type, resource) => {
  return (dispatch) => {
    dispatch({
      type: action_type,
      payload: resource,
    });
  };
};

export const updateResource = (
  path,
  action_type,
  resourceId,
  resource,
  toggelModal,
  token = null
) => {
  return async (dispatch) => {
    await axios
      .put(`${REACT_APP_API_HOST}/${path}${resourceId}`, resource)
      .then((response) => {
        if (response.status === 204) {
          dispatch({
            type: action_type,
            payload: resource,
          });
          toggelModal();
          SuccessPopup("تم التعديل بنجاح");
        }
      })
      .catch((error) => {
        ErrorPopup("فشل التعديل");
      });
  };
};
