import React from 'react'
import ReactPaginate from 'react-paginate'

// ** Custom Pagination
const CustomPagination = ({currentPage, searchValue, filteredData, data, setCurrentPage}) => {

    // ** Function to handle Pagination
    const handlePagination = page => {
        setCurrentPage(page.selected)
    }


    return (
        <ReactPaginate
          previousLabel=''
          nextLabel='' 
          forcePage={currentPage}
          onPageChange={page => handlePagination(page)}
          pageCount={searchValue.length ? filteredData.length / 10 : data.length / 10 || 1}
          breakLabel='...'
          pageRangeDisplayed={2}
          marginPagesDisplayed={2}
          activeClassName='active'
          pageClassName='page-item'
          breakClassName='page-item'
          breakLinkClassName='page-link'
          nextLinkClassName='page-link'
          nextClassName='page-item next'
          previousClassName='page-item prev'
          previousLinkClassName='page-link'
          pageLinkClassName='page-link'
          containerClassName='pagination react-paginate separated-pagination pagination-sm justify-content-end pr-1 mt-1'
        />
      )
}

  export default CustomPagination