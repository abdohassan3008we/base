import { BlobProvider } from "@react-pdf/renderer";

/**
 * Render an open in a new tab link for the given PDF document
 *
 * @param {JSX.Element} document
 * @param {String} downloadText
 * @returns {JSX.Element}
 * @author Mohamed Abdul-Fattah
 */
export default function ({ document, downloadText }) {
  return (
    <BlobProvider document={document}>
      {({ url }) => (
        <a className="btn btn-primary" href={url} target="_blank" rel="noopener noreferrer">
          {downloadText}
        </a>
      )}
    </BlobProvider>
  )
}
