import jsPDF from "jspdf"
import "jspdf-autotable"
import { font } from "./Amiri-Regular-normal"

const exportPDF = (data, tableOptions) => {
    if (data && data.length && data.length > 0 && tableOptions) {
        const columns = tableOptions.columns
        const unit = "pt"
        const size = "A4" // Use A1, A2, A3 or A4
        const orientation = "portrait" // portrait or landscape
        const doc = new jsPDF(orientation, unit, size)
        doc.addFont('Amiri-Regular-normal.ttf', 'Amiri-Regular', 'normal')
        doc.setFont('Amiri-Regular')
        doc.setFontSize(10)

        const keys = Object.keys(data[0])
        const titles = []
        let headers
        if (columns) {
            keys.forEach(function (key, i) {
                const column = columns.find(col => col.selector === key)
                if (column) {
                    titles.push(column.name)
                } else if (key.includes('Desc')) {
                    titles.push('الوصف')
                } else if (key.includes('active')) {
                    titles.push('منشط')
                } else if (key.includes('remarks')) {
                    titles.push('ملاحظات')
                }
            })
            headers = [titles.reverse()]
        } else {
            headers = [keys]
        }
        const temp = data.map(item => {
            const tempArray = []
            keys.forEach(function (key, i) {
                const column = columns.find(col => col.selector === key)
                if (column || key.includes('Desc') || key.includes('active') || key.includes('remarks')) {
                    tempArray.push(item[key])
                }
            })
            return tempArray.reverse()
        })
        const content = {
            startY: 10,
            head: headers,
            body: temp,
            styles: { halign: 'center', font: "Amiri-Regular", fontStyle: "normal" }
        }
        doc.autoTable(content)
        doc.save(`${tableOptions.title}.pdf`)
    }
}

export default exportPDF