import React from 'react'
import useTimer from '../../utility/hooks/useTimer'

/**
 * Display a timer counter in a human redeable form
 * from the given date or now if no date is specified
 *
 * @param {Date} timestamp
 * @param {Object} style custom styles
 * @param {String} className custom HTML classes
 * @returns {React.Component}
 */
const CardTime = ({ timestamp, style = {}, className = "" }) => {
  let { hours, minutes, seconds, days } = useTimer(timestamp);
  hours = hours + 24 * days
  if (hours < 10) hours = '0' + hours
  if (minutes < 10) minutes = '0' + minutes
  if (seconds < 10) seconds = '0' + seconds

  return <span dir='ltr' className={className} style={style}>{hours} h {minutes} m {seconds} s</span>
}

export default CardTime;
