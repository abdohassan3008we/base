import React, { useState, useEffect } from "react";
import {
    Button
} from "reactstrap";
import ConfirmCancelPopup from "@src/views/customizedComponents/ConfirmCancelPopup";

import proImage from './add_user_profile.png';
const UploadAndDisplayImage = (loadedImage = null) => {
    const [selectedImage, setSelectedImage] = useState(null);
    const [enabledImage, setEnabledImage] = useState(null);
    useEffect(() => {
        if((loadedImage && loadedImage['loadedImage'] === null) || !loadedImage || loadedImage === null){
            setEnabledImage(null);
        } else if(loadedImage !== undefined && loadedImage !== null) {
            setSelectedImage(loadedImage['loadedImage']);
            setEnabledImage(loadedImage['loadedImage']);
            sessionStorage.setItem('personForm-UploadedImageBase64', loadedImage['loadedImage']);
        }
    }, [])



    const handleFileInputChange = e => {
        let file = e.target.files[0];
        getBase64(file)
            .then(result => {
                file["base64"] = result;
                setSelectedImage(file);
            })
            .catch(err => {
            });
        setSelectedImage(e.target.files[0]);
    };

    const getBase64 = file => {
        return new Promise(resolve => {
            let baseURL = "";
            // Make new FileReader
            let reader = new FileReader();
            // Convert the file to base64 text
            reader.readAsDataURL(file);
            // on reader load somthing...
            reader.onload = () => {
                baseURL = reader.result;
                resolve(baseURL);
                sessionStorage.setItem('personForm-UploadedImageBase64', baseURL);
            };
        });
    };

    return (
        <div>
            <h2>أضف صورة للحساب</h2>
            <br />
            {!selectedImage && (
                <div>
                    <label >
                        <img width={"160px"} src={proImage} />
                        <input id="file-input" style={{ display: "none" }} name="myImage" id="myImage" type="file" accept="image/jpeg, image/jpg, image/png"
                            onChange={(event) => {
                                handleFileInputChange(event);
                            }} />
                    </label>
                </div>
            )}
            {selectedImage && enabledImage === null && (
                <div>
                    <img id="my-image" width={"180px"} src={URL.createObjectURL(selectedImage)} />
                </div>
            )}
            {selectedImage && enabledImage !== null && (
                <div>
                    <img id="my-image" width={"180px"} src={enabledImage} />
                </div>
            )}
            <br />
            {selectedImage && (
                <Button style={{
                    position: "relative",
                    right: "30%"
                }} color="primary" type="submit" onClick={() => {
                    ConfirmCancelPopup("هل تريد الحذف ", "حذف").then((out) => {
                        if (out.isConfirmed) {
                            setSelectedImage(null);
                            setEnabledImage(null);
                            sessionStorage.removeItem('personForm-UploadedImageBase64');
                        }
                      });
                }}>
                    ازالة
                </Button>

            )}
            <br />
            <br />
        </div>
    );
};

export default UploadAndDisplayImage;
