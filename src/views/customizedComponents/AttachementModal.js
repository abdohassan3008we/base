// ** React Imports
import React, { useEffect, useState } from "react";
import Table from "@src/views/customizedComponents/Table";
import { Eye } from "react-feather";
import { PlusCircle } from "react-feather";
import { Download } from "react-feather";
import { X } from "react-feather";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  Row,
  Col,
  Container,
  Card,
  CardHeader,
  Form,
  CardTitle,
  CardBody,
  CustomInput,
  FormGroup,
  Input,

} from "reactstrap";
import { useFormik } from "formik";
import { useParams } from "react-router";

// ** Store & Actions
import { useDispatch, useSelector } from "react-redux";
import { listattachTypeRequestAsync } from "@src/redux/ref/attachType/thunk";
import { searchAttachmentRequestAsync } from "@src/redux/admin/attachement/thunk";
import { addAttachmentRequestAsync } from "@src/redux/admin/attachement/thunk";
import ErrorPopup from "@src/views/customizedComponents/ErrorPopup";
import CustomDropDown from "@src/views/customizedComponents/CustomDropDownFormGroup";
import proImage from './attachedFile.png';
import addImage from './addAttachement.png';
import { updateNumberUploadButton } from '@src/redux/reducers/navbar/actionCreators'


const AttachmentModal = ({ open, handleModal, formId, formTypeCode }) => {
  const dispatch = useDispatch();
  const { id } = useParams();
  // ** Custom close btn
  const CloseBtn = (
    <X className="cursor-pointer" size={15} onClick={handleModal} />
  );
  const attachmentStore = useSelector((state) => state.AdminAttachmentReducer);
  const attachTypeStore = useSelector((state) => state.RefattachTypeReducer);

  const [attachmentState, setAttachmentState] = useState(null);
  // On file select (from the pop up)
  const onFileChange = (event) => {
    // Update the state
    if (event.target.files[0]) {
      event.target.files[0]['typeAr'] =
        (event.target.files[0].name.split('.').pop().toLowerCase() === 'jpg' ||
          event.target.files[0].name.split('.').pop().toLowerCase() === 'png' ||
          event.target.files[0].name.split('.').pop().toLowerCase() === 'jpeg') ? "صورة" :
          event.target.files[0].name.split('.').pop().toLowerCase() === 'pdf' ? "PDF" :
            (event.target.files[0].name.split('.').pop().toLowerCase() === 'xls' ||
              event.target.files[0].name.split('.').pop().toLowerCase() === 'csv' ||
              event.target.files[0].name.split('.').pop().toLowerCase() === 'xlsx') ? "جدول بيانات" :
              event.target.files[0].name.split('.').pop().toLowerCase() === 'pptx' ? "عرض تقديمي" :
                (event.target.files[0].name.split('.').pop().toLowerCase() === 'doc' ||
                  event.target.files[0].name.split('.').pop().toLowerCase() === 'docx') ? "ملف وورد" :
                  event.target.files[0].name.split('.').pop().toLowerCase() === 'txt' ? "ملف نص" : "";
    }
    setAttachmentState(event.target.files[0]);
  };

  useEffect(() => {
    attachmentStore.searchData = [];
    setAttachmentState(null);
    dispatch(searchAttachmentRequestAsync({
      formId: formId ? formId : id,
      formTypeCode: formTypeCode,
    }));
    dispatch(listattachTypeRequestAsync());
    return () => {
      attachmentStore.searchData = [];
      setAttachmentState(null);
    }
  }, []);

  useEffect(() => {
    dispatch(updateNumberUploadButton(attachmentStore.searchData.length));
  }, [attachmentStore.searchData])

  const formik = useFormik({
    initialValues: {
      active: true,
      versionNo: 0,
      uploadSiteId: parseInt(JSON.parse(localStorage.getItem("userData")).siteId),
      uploadUserId: parseInt(JSON.parse(localStorage.getItem("userData")).userId),
      formId: formId ? formId : id,
      formTypeCode: formTypeCode,
      uploadDate: new Date().toISOString().slice(0, 10),

      attachTypeCode: "",
      attachmentDesc: "",
      remarks: "",
      fileEncoding: "",
      fileContent: "",
      fileExtension: "",
      fileName: "",
    },
    onSubmit: (values) => {
      if (attachmentState) {
        values["fileExtension"] = attachmentState.name.split('.').pop();

        if (
          values["fileExtension"].toLowerCase() !== "png" &&
          values["fileExtension"].toLowerCase() !== "jpeg" &&
          values["fileExtension"].toLowerCase() !== "jpg" &&
          values["fileExtension"].toLowerCase() !== "xls" &&
          values["fileExtension"].toLowerCase() !== "xlsx" &&
          values["fileExtension"].toLowerCase() !== "csv" &&
          values["fileExtension"].toLowerCase() !== "pdf" &&
          values["fileExtension"].toLowerCase() !== "pptx" &&
          values["fileExtension"].toLowerCase() !== "doc" &&
          values["fileExtension"].toLowerCase() !== "docx" &&
          values["fileExtension"].toLowerCase() !== "txt"
        ) {
          ErrorPopup("نوع الملف غير مدعوم");
          setAttachmentState(null);
          return;
        }
        values["fileName"] = attachmentState.name;
        var reader = new FileReader();
        reader.readAsDataURL(attachmentState);
        reader.onload = function () {
          let [first, ...rest] = reader.result.split(',')
          rest = rest.join(',')
          values["fileContent"] = rest;
          values["fileEncoding"] = first;
          dispatch(addAttachmentRequestAsync(values));
          setAttachmentState(null);
        };
      } else {
        ErrorPopup("من فضلك اختر ملف قبل الرفع");
      }
    },
  });

  const fileData = () => {
    if (attachmentState) {
      return (
        <div style={{ marginBottom: "20px" }}>
          <h2>بيانات الملف</h2>
          <Row>
            <Col sm={6}>
              <p>اسم الملف  : </p>
              <p style={{ color: "#B8922B" }}>{attachmentState.name}</p>
            </Col>
            <Col sm={6}>
              <p>نوع الملف  : </p>
              <p style={{ color: "#B8922B" }}>{attachmentState.typeAr}</p>
            </Col>
          </Row>
        </div>
      );
    } else {
      return (
        <div>
        </div>
      );
    }
  };
  // to show attachements as tiles
  const handleAttachments = () => {
    return <> {
      attachmentStore.searchData && attachmentStore.searchData.length > 0 ? <div>
        <Row style={{ border: '2px solid #B8922B', borderRadius: '20px', padding: "10px" }}>
          {attachmentStore.searchData.map(function (object, i) {
            return <Col style={{ textAlign: "center" }} sm={2}>
              <a href={object.fileEncoding + ',' + object.fileContent} download={object.fileName}>
                <div style={{ display: "table-caption", textAlign: "center" }}>
                  <img width={"65px"} src={proImage} />
                  <Label>{object.fileName}</Label>
                </div>
              </a>
            </Col>;
          })}
        </Row>
      </div> : <label>
        Loading...
      </label>
    } </>
  }
  const download = (row) => {
    let a = document.createElement('a');
    a.href = row.fileEncoding + ',' + row.fileContent;
    a.download = row.fileName;
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
  }
  const handleAttachments2 = () => {
    const columns = [
      {
        name: 'اسم الملف',
        selector: 'fileName',
        sortable: true,
        minWidth: "300px",
      },
      {
        name: 'نوع الملف',
        selector: 'fileExtension',
        sortable: true,
        cell: (row) => (
          row.fileExtension.toLowerCase() === 'jpg' ||
            row.fileExtension.toLowerCase() === 'png' ||
            row.fileExtension.toLowerCase() === 'jpeg' ? <label>صورة</label> :
            row.fileExtension.toLowerCase() === 'pdf' ? <label>PDF</label> :
              row.fileExtension.toLowerCase() === 'xls' ||
                row.fileExtension.toLowerCase() === 'csv' ||
                row.fileExtension.toLowerCase()=== 'xlsx' ? <label>جدول بيانات</label> :
                row.fileExtension.toLowerCase() === 'pptx' ? <label>عرض تقديمي</label> :
                  row.fileExtension.toLowerCase() === 'doc' ||
                    row.fileExtension.toLowerCase() === 'docx' ? <label>ملف وورد</label> :
                    row.fileExtension.toLowerCase() === 'txt' ? <label>ملف نص</label> : <></>
        )
      },
      {
        name: 'تاريخ الإضافة',
        selector: 'uploadDate',
        sortable: true,
        minWidth: "120px",
      },
      {
        name: 'وصف الملف',
        selector: 'attachmentDesc',
        sortable: true,
      },
      {
        name: 'ملاحظات',
        selector: 'remarks',
        sortable: true,
      },
      {
        name: 'عرض',
        cell: (row) => (
          row.fileExtension.toLowerCase() === 'jpg' ||
            row.fileExtension.toLowerCase() === 'png' ||
            row.fileExtension.toLowerCase() === 'jpeg' ||
            row.fileExtension.toLowerCase() === 'pdf'
              ? (
                <div>
                  <Eye size={14} className='mr-50 cursor-pointer' onClick={_ => showFrame(row)} />
                </div>
              ) : <></>
        )
      },
      {
        name: 'تحميل',
        cell: (row) => (
          <div>
            <Download size={14} className='mr-50 cursor-pointer' onClick={_ => download(row)} />
          </div>
        )
      }
    ];
    return <Table
      columns={columns}
      data={attachmentStore.searchData}
      showAddBtn={false}
      showExtractBtn={false}
      showSearch={false}
      progress={attachmentStore.isLoading}

    />
  }

  const showFrame = (row) => {
    if (row.fileExtension === 'pdf') {
      let pdfWindow = window.open("")
      pdfWindow.document.write(
        "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
        encodeURI(row.fileContent) + "'></iframe>"
      )
    } else {
      let base64URL = `data:image/${row.fileExtension};base64,${row.fileContent}`;
      let imgWin = window.open("");
      imgWin.document.write('<iframe src="' + base64URL  + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>');
    }
  }

  return (
    <Modal isOpen={open} toggle={handleModal} style={{ maxWidth: "70%" }}>
      <input hidden name="screenId" defaultValue="ATTACHEMENT_MODAL_01" />

      <ModalHeader
        className="mb-3"
        toggle={handleModal}
        close={CloseBtn}
        tag="div"
      >
      </ModalHeader>
      <ModalBody className="flex-grow-1">
        <Card>
          <CardHeader>
            <CardTitle
              tag="h4"
              style={{ margin: "auto", padding: "0", fontSize: "200%" }}
            >
              المرفقات
            </CardTitle>
          </CardHeader>
          <CardBody>
            <hr />
            <Form onSubmit={formik.handleSubmit}>
              <Container>
                <Row style={{ border: '2px solid #B8922B', borderRadius: '20px', padding: "10px 0px", marginBottom: "20px" }} >
                  <Col sm={12}  >
                    <div style={{ textAlign: "center" }}>
                      <label style={{
                        cursor: "pointer",
                        width: "100%"
                      }} >
                        <PlusCircle size={50} className='mr-50' style={{ color: "#B8922B" }} />
                        <input
                          style={{ display: "none" }}
                          type="file"
                          onChange={onFileChange}
                          accept=".jpeg,
                                  .jpg,
                                  .png,
                                  .xls,
                                  .xlsx,
                                  .csv,
                                  .pdf,
                                  .pptx,
                                  .doc,
                                  .docx,
                                  .txt,
                                  "/>

                      </label>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12}>
                    {fileData()}
                  </Col>
                  {/*  <Col sm={4}>
                    <CustomDropDown
                      arabicIdentifier='نوع الملف'
                      keyName='attachType'
                      isAdd={true}
                      data={attachTypeStore.data}
                      formik={formik}
                    >
                    </CustomDropDown>
                  </Col>*/}
                </Row>
                <Row>
                  <Col sm={6}>
                    <FormGroup>
                      <Label for="attachmentDesc">وصف الملف</Label>
                      <Input
                        type="textarea"
                        name="attachmentDesc"
                        id="attachmentDesc"
                        placeholder="أدخل وصف الملف"
                        onChange={formik.handleChange}
                        style={{ minHeight: "70px", maxHeight: "100px" }}
                      />
                    </FormGroup>
                  </Col>
                  <Col sm={6}>
                    <FormGroup>
                      <Label for="remarks">ملاحظات</Label>
                      <Input
                        type="textarea"
                        name="remarks"
                        id="remarks"
                        placeholder="أدخل بعض الملاحظات"
                        onChange={formik.handleChange}
                        style={{ minHeight: "70px", maxHeight: "100px" }}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <div style={{ textAlign: "end" }}>
                  <Row>
                    <Col sm='12'>
                      <FormGroup >
                        <Button.Ripple color='primary' type="submit">رفع</Button.Ripple>
                      </FormGroup>
                    </Col>
                  </Row>
                </div>
              </Container>
            </Form>
            <hr />
          </CardBody>
        </Card>
        <Card>
          <CardBody>
            {handleAttachments2()}
          </CardBody>
        </Card>
      </ModalBody>
    </Modal>
  );
};

export default AttachmentModal;
