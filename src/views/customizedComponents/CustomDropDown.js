import React from 'react'
import { AvGroup, AvField } from "availity-reactstrap-validation-safe";
import {
  Label,
} from "reactstrap";
const CustomDropDown = ({ keyName,value, arabicIdentifier, isAdd, data, selectedChoice = undefined, parentCallbackFunction, isValidate = true }) => {

  let code = keyName + 'Code';
  let nameArabic = keyName + 'NameAr';
  let name = keyName + 'Name';
  let errorMsg = 'من فضلك إختر ' + arabicIdentifier;
  let selectedItem;
  if (selectedChoice) {
    let obj = data.find(e => e[code] === selectedChoice[code]);
    let label = obj ? obj[nameArabic] : selectedChoice[name] ? selectedChoice[name] : undefined;
    if (label) {
      selectedItem = {
        value: selectedChoice[code],
        label: label ? label : item[name]
      }
    }
  }
  return (
    <>
    {isValidate ? <AvGroup>
      <Label for={code}>{arabicIdentifier}</Label>
      <AvField
        value={value}
        type='select'
        name={code}
        id={code}
        validate={{
           required: { value: true, errorMessage: errorMsg }
        }}
        onChange={e => parentCallbackFunction ? parentCallbackFunction() : undefined }
      >
        {isAdd || !selectedItem ? <option defaultValue disabled></option> : <option defaultValue value={selectedItem.value} >{selectedItem.label}</option>}
        {isAdd || !selectedItem ? data.map((item) => <option key={item[code]} value={item[code]}>{item[nameArabic]}</option>) : data.filter((item) => item[code] !== selectedItem.value).map((item) => <option value={item[code]}>{item[nameArabic]}</option>)}
      </AvField>
    </AvGroup> :

    <AvGroup>
      <Label for={code}>{arabicIdentifier}</Label>
      <AvField
        value={value}
        type='select'
        name={code}
        id={code}
        onChange={e => parentCallbackFunction ? parentCallbackFunction() : undefined }
      >
        {isAdd || !selectedItem ? <option defaultValue disabled></option> : <option defaultValue value={selectedItem.value} >{selectedItem.label}</option>}
        {isAdd || !selectedItem ? data.map((item) => <option key={item[code]} value={item[code]}>{item[nameArabic]}</option>) : data.filter((item) => item[code] !== selectedItem.value).map((item) => <option value={item[code]}>{item[nameArabic]}</option>)}
      </AvField>
    </AvGroup>}
</>
  )
}

export default CustomDropDown
