import React from 'react'
import {
  Label,
  FormGroup
} from "reactstrap";
import Input from 'reactstrap/lib/Input';
const CustomDropDown = ({ 
  keyName,
  value, 
  arabicIdentifier, 
  isAdd, 
  data, 
  selectedChoice = undefined,
  parentCallbackFunction,
  formik = undefined,
  idInfo = undefined,
}) => {

  let code = keyName + 'Code';
  let nameArabic = keyName + 'NameAr';
  let name = keyName + 'Name';
  let errorMsg = 'من فضلك إختر ' + arabicIdentifier;
  let selectedItem;
  if (
    selectedChoice // 👈 null and undefined check
    && Object.keys(selectedChoice).length !== 0
  ) {
    let obj = data.find(e => e[code] === selectedChoice[code]);
    let label = obj ? obj[nameArabic] : selectedChoice[name] ? selectedChoice[name] : undefined;
    if (label) {
      selectedItem = {
        value: selectedChoice[code],
        label: label ? label : item[name]
      }
    }
  }
  return (
    <FormGroup>
      <Label for={code}>{arabicIdentifier}</Label>
      <Input
        value={value}
        type='select'
        name={code}
        id={idInfo ? idInfo : code}
        validate={{
          required: { value: true, errorMessage: errorMsg }
        }}
        onChange={e => {
          parentCallbackFunction ? parentCallbackFunction() : undefined;
          formik ? formik.handleChange(e) : undefined;
        }}
      >
          {isAdd || !selectedItem ? <option defaultValue disabled selected></option> : <option defaultValue selected value={selectedItem.value} >{selectedItem.label}</option>}
          {isAdd || !selectedItem ? data.map((item) => <option key={item[code]} value={item[code]}>{item[nameArabic]}</option>) : data.filter((item) => item[code] !== selectedItem.value).map((item) => <option value={item[code]}>{item[nameArabic]}</option>)}
      </Input>
    </FormGroup>

  )
}

export default CustomDropDown
