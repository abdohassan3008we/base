import React from 'react'
import { Spinner } from "reactstrap";


const CustomizedLoading = () => {
  return (
    <div>
      جارٍ التحميل...
        &nbsp;
        &nbsp;
        <Spinner />
    </div>
  )
}

export default CustomizedLoading
