import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { getAvailableNextStatusRequestAsync } from "../../redux/admin/grantedFormAction/thunk";
import { Trash2 } from "react-feather";


export default function dropdownActions({ formTypeCode, currentStatusCode, deleteObj, handler = () => { }, onSuccess = () => { }, onFailure = () => { } }) {
  const dispatch = useDispatch();
  const [actions, setActions] = useState([])

  useEffect(() => {
    dispatch(getAvailableNextStatusRequestAsync(
      {
        formTypeCode,
        currentStatusCode
      }, {
      onSuccess: res => { setActions(res) }
    }
    ))
  }, [])

  const deleteAction = actions.find(action => action.formActionCode == "DELETE")

  return (
    <>
      {
        deleteAction ? (
          <Trash2
            color="#6C0303"
            size={20}
            className='mr-50 cursor-pointer'
            onClick={_ => handler(deleteObj)}
          />
        ) : null
      }
    </>
  );
}
