import jsPDF from "jspdf"
import "jspdf-autotable"
import { font } from "./Amiri-Regular-normal"
import { logo } from "./logobase64"
import moment from 'moment-timezone';
moment.locale('ar_SA');

const drawPageHeader = (doc) => {
  let title;
  //Report Title
  doc.setFontSize(12)
  title = 'اذن شغل'
  doc.text(title || "", 380, 35);

  //Right Title
  doc.setFontSize(8)
  title = 'وزارة الدفاع'
  doc.text(title || "", 750, 25);
  title = ' - إدارة نظم المعلومات'
  doc.text(title || "", 740, 45);
  title = '(SC)'
  doc.text(title || "", 720, 45);

  //add MMC logo here
  var img = new Image()
  img.src = logo
  doc.addImage(img, 'png', 55, 10, 30, 25)
  //Left Title
  doc.setFontSize(8)
  title = 'نموذج رقم'
  doc.text(title || "", 65, 45);
  title = '3)'
  doc.text(title || "", 55, 45);
  title = '(أت'
  doc.text(title || "", 40, 45);

  //draw line
  doc.setLineWidth(0.5);
  doc.line(40, 55, 800, 55);

}

const drawHeaderTables = (doc, data) => {
  let title;
  let string;
  doc.setFontSize(9)

  //Left Table
  doc.setDrawColor(0);
  doc.rect(40, 65, 230, 20);

  doc.rect(40, 85, 90, 20);
  doc.rect(40, 105, 90, 20);

  doc.rect(130, 85, 140, 20);
  doc.rect(130, 105, 140, 20);

  title = 'إذن التشغيل/التصليح'
  doc.text(title || "", 190, 75, { align: 'right' });

  title = 'رقم'
  doc.text(title || "", 205, 95, { align: 'right' });
  title = data.form3atDetailNum
  doc.text(title || "", 205, 116, { align: 'center' });

  title = 'التاريخ'
  doc.text(title || "", 95, 95, { align: 'right' });
  title = data.form3atDetailDate ? moment(new Date(data.form3atDetailDate)).format("D-M-YYYY") : ""
  doc.text(title || "", 95, 116, { align: 'center' });

  //Middle Table
  doc.setDrawColor(0);
  doc.rect(290, 65, 230, 20);

  doc.rect(290, 85, 90, 20);
  doc.rect(290, 105, 90, 20);

  doc.rect(380, 85, 140, 20);
  doc.rect(380, 105, 140, 20);

  title = 'التصديق'
  doc.text(title || "", 415, 75, { align: 'right' });
  title = 'رقم'
  doc.text(title || "", 455, 95, { align: 'right' });
  title = data.wmsForm3atMasterDto?.form3atMasterNum
  doc.text(title || "", 455, 116, { align: 'center' });

  title = 'التاريخ'
  doc.text(title || "", 345, 95, { align: 'right' });
  title = data.wmsForm3atMasterDto?.form3atApproveDate ? moment(new Date(data.wmsForm3atMasterDto?.form3atApproveDate)).format("D-M-YYYY") : ""
  doc.text(title || "", 345, 116, { align: 'center' });


  //Right Table
  doc.setDrawColor(0);
  doc.rect(540, 65, 80, 20);
  doc.rect(540, 85, 80, 20);
  doc.rect(540, 105, 80, 20);

  doc.rect(620, 65, 130, 20);
  doc.rect(620, 85, 130, 20);
  doc.rect(620, 105, 130, 20);

  doc.rect(750, 85, 50, 20);
  doc.rect(750, 105, 50, 20);

  title = 'التاريخ'
  doc.text(title || "", 585, 75, { align: 'right' });
  title = data.mokaysaDate ? moment(new Date(data.mokaysaDate)).format("D-M-YYYY") : ""
  doc.text(title || "", 585, 95, { align: 'center' });
  title = data.inspectDate ? moment(new Date(data.inspectDate)).format("D-M-YYYY") : ""
  doc.text(title || "", 585, 116, { align: 'center' });

  title = 'رقم'
  doc.text(title || "", 690, 75, { align: 'right' });
  title = data.mokaysaNum || ""
  doc.text(title || "", 690, 95, { align: 'center' });
  title = data.inspectNum || ""
  doc.text(title || "", 690, 116, { align: 'center' });


  title = 'التفتيش'
  doc.text(title || "", 787, 116, { align: 'right' });
  title = 'المقايسة'
  doc.text(title || "", 787, 95, { align: 'right' });

}

const drawOrderData = (doc, data) => {
  let title;
  doc.setFontSize(9)
  //[First column]
  title = ' : السلاح الإداري المختص'
  doc.text(title || "", 800, 150, { align: 'right' });
  title = data.wmsForm3atMasterDto?.tbSiteName
  doc.text(title || "", 700, 150, { align: 'right' });



  title = ' : اسم الوحدة'
  doc.text(title || "", 800, 170, { align: 'right' });
  title = data.wmsForm3atMasterDto?.requestSiteName
  doc.text(title || "", 750, 170, { align: 'right' });

  title = ' : رقم المتسبب'
  doc.text(title || "", 800, 190, { align: 'right' });
  title = data.responsPhoneNum || ""
  doc.text(title || "", 750, 190, { align: 'right' });
  title = ' : الرتبة'
  doc.text(title || "", 800, 210, { align: 'right' });
  title = data.responsTitleName || ""
  doc.text(title || "", 770, 210, { align: 'right' });
  //[third Column] DATA

  title = ' : محطة عسكرية'
  doc.text(title || "", 640, 170, { align: 'right' });
  title = data.wmsForm3atMasterDto.requestAreaName
  doc.text(title || "", 590, 170, { align: 'right' });


  title = ' : اسم'
  doc.text(title || "", 640, 210, { align: 'right' });
  title = data.responsName || ""
  doc.text(title || "", 615, 210, { align: 'right' });

  //[fifth column]
  title = ' :  مقدم الى الورشة'
  doc.text(title || "", 480, 150, { align: 'right' });
  title = data.wmsForm3atMasterDto?.wsSiteName
  doc.text(title || "", 420, 150, { align: 'right' });


  title = ' : بجهة'
  doc.text(title || "", 480, 170, { align: 'right' });
  title = data.wmsForm3atMasterDto.wsAreaName
  doc.text(title || "", 450, 170, { align: 'right' });

  title = ' : رقم الملف'
  doc.text(title || "", 480, 190, { align: 'right' });

  title = ' : العام المالي'
  doc.text(title || "", 480, 210, { align: 'right' });
  title = data.wmsForm3atMasterDto.financialYearName
  doc.text(title || "", 440, 210, { align: 'right' });

  //[seventh Column]  Data
  title = ' :  قسم الإصلاح المختص'
  doc.text(title || "", 320, 150, { align: 'right' });
  title = data.repairGroupName
  doc.text(title || "", 240, 150, { align: 'right' });


  title = ' : تاريخ ابتداء العمل'
  doc.text(title || "", 320, 170, { align: 'right' });
  title = data.form3atDetailDate ? moment(new Date(data.form3atDetailDate)).format("D-M-YYYY") : ""
  // title = data.form3atDetailDate
  doc.text(title || "", 260, 170, { align: 'right' });


  title = ' : قرار الخصم'
  doc.text(title || "", 320, 190, { align: 'right' });
  title = ' : ملاحظات'
  doc.text(title || "", 320, 210, { align: 'right' });
  //[8th Column]  Data
  title = ' : تاريخ انتهاء العمل'
  doc.text(title || "", 160, 170, { align: 'right' });
  title = ' : التكلفة المالية'
  doc.text(title || "", 160, 190, { align: 'right' });

}

const drawItemsTable = (doc, data) => {
  let title;
  doc.setFontSize(9)
  // TABLE PHASE
  const headers = [[
    'كود الصنف',
    'رقم العينة',
    'اسم الصنف',
    'وحدة الصرف',
    'الكمية',
    'حالة الصنف',
    'رقم اللوط/المسلسل',
  ].reverse()]
3
  const temp = [[
    `${data.fscCode}-${data.niinCode}`,
    `${data.partNumber}`,
    data.itemName,
    data.itemUiName,
    data.itemQty,
    data.itemConditionName,
    data.itemLotSerialNum,
  ].reverse()];

  const content = {
    startY: 240,
    head: headers,
    body: temp,
    styles: { halign: 'center', font: "Amiri-Regular", fontStyle: "normal" }
  }
  doc.setFontSize(9)
  doc.autoTable(content)
}



const drawLeftFooter = (doc) => {
  let title;
  doc.setFontSize(9)
  title = ' ...................................................... إلى القائد'
  doc.text(title || "", 220, 470, { align: 'right' });

  title = 'انتهاء العمل و أن تكاليف الانتاج الصحيح/الخصم على المتسبب'
  doc.text(title || "", 220, 495, { align: 'right' });

  title = 'كما هو موضح بعاليه و موافاتنا بشهادة الخصم'
  doc.text(title || "", 220, 520, { align: 'right' });

  title = ' .....................................  توقيع مدير الأعمال'
  doc.text(title || "", 220, 545, { align: 'right' });

  title = ' ................................. توقيع قائد الورشة'
  doc.text(title || "", 220, 570, { align: 'right' });

  title = ' ...................  التاريخ '
  doc.text(title || "", 100, 570, { align: 'right' });
}

const drawFooterTables = (doc, data, persons) => {
  let storeBoss = persons.storeBoss
  let workshopBoss = persons.workshopBoss

  let workshopLeaderData = {
    title: (workshopBoss?.titleName || ""),
    name: workshopBoss?.personName
  }
    
  let warehouseLeaderData = {
    title: (storeBoss?.titleName || ""),
    name: storeBoss?.personName
  }

  let title;

  //Left Table
  doc.setDrawColor(0);
  doc.rect(240, 470, 70, 30); // empty square   
  doc.rect(240, 500, 70, 30); // empty square   
  doc.rect(240, 530, 70, 30); // empty square   
  doc.rect(310, 470, 90, 30); // empty square   
  doc.rect(310, 500, 90, 30); // empty square  
  doc.rect(310, 530, 90, 30); // empty square  
  doc.rect(400, 500, 40, 30); // empty square  
  doc.rect(400, 530, 40, 30); // empty square  



  doc.setFontSize(7)
  title = 'مكتب الاجراءات/المندوبين الفنيين'
  doc.text(title || "", 396, 485, { align: 'right' });
  doc.setFontSize(9)
  title = 'يعتمد'
  doc.text(title || "", 282, 485, { align: 'right' });
  title = 'الوظيفة'
  doc.text(title || "", 285, 515, { align: 'right' });
  title = 'الاسم'
  doc.text(title || "", 430, 517, { align: 'right' });
  title = 'التوقيع'
  doc.text(title || "", 430, 545, { align: 'right' });



  //STAMP
  doc.setDrawColor(0);
  doc.rect(460, 480, 60, 60); // empty square

  doc.setFontSize(9)
  title = 'خاتم الوحدة'
  doc.text(title || "", 507, 500, { align: 'right' });
  title = 'ذو التاريخ'
  doc.text(title || "", 505, 520, { align: 'right' });

  //Right Table
  doc.setDrawColor(0);
  doc.rect(540, 470, 70, 30); // empty square   
  doc.rect(540, 500, 70, 30); // empty square   
  doc.rect(540, 530, 70, 30); // empty square   
  doc.rect(610, 470, 70, 30); // empty square   
  doc.rect(610, 500, 70, 30); // empty square   
  doc.rect(610, 530, 70, 30); // empty square   
  doc.rect(680, 470, 70, 30); // empty square   
  doc.rect(680, 500, 70, 30); // empty square 
  doc.rect(680, 530, 70, 30); // empty square 
  doc.rect(750, 500, 50, 30); // empty square 
  doc.rect(750, 530, 50, 30); // empty square 


  doc.setFontSize(9)
  title = 'المتسبب'
  doc.text(title || "", 725, 485, { align: 'center' });
  title = data.responsName ? data.responsName : ""
  doc.text(title || "", 725, 517, { align: 'center' });

  title = 'الضابط الفني'
  doc.text(title || "", 650, 485, { align: 'center' });
  title = workshopLeaderData.name
  doc.text(title || "", 650, 517, { align: 'center' });

  title = 'القائد'
  doc.text(title || "", 580, 485, { align: 'center' });
  title = warehouseLeaderData.name
  doc.text(title || "", 580, 517, { align: 'center' });

  title = 'الاسم'
  doc.text(title || "", 785, 517, { align: 'right' });
  title = 'التوقيع'
  doc.text(title || "", 785, 545, { align: 'right' });

}


const generate3atReport = (data, persons) => {

  if (data) {
    const unit = "pt"
    const size = "A4" // Use A1, A2, A3 or A4
    const orientation = "landscape" // portrait or landscape
    const doc = new jsPDF(orientation, unit, size)
    doc.addFont('Amiri-Regular-normal.ttf', 'Amiri-Regular', 'normal')
    doc.setFont('Amiri-Regular')

    drawPageHeader(doc, data);
    drawHeaderTables(doc, data);
    drawOrderData(doc, data);
    drawItemsTable(doc, data);
    drawLeftFooter(doc, data);
    drawFooterTables(doc, data, persons);
    //to print
    //doc.autoPrint({variant: 'non-conform'})
    //window.open(doc.output('bloburl'));
    //to save
    doc.save(`اذن شغل.pdf`)
  }
}
export default generate3atReport;
