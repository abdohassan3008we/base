import axios from "axios";
import ErrorPopup from "@src/views/customizedComponents/ErrorPopup";
import {
  Button,
  Label,
  Row,
  Col,
} from "reactstrap";
import React, { Component } from "react";
import proImage from './attachedFile.png';
import addImage from './addAttachement.png';
class App extends Component {
  state = {
    // Initially, no file is selected
    attachments: [],
    selectedFile: null,
  };

  // On file select (from the pop up)
  onFileChange = (event) => {
    // Update the state
    this.setState({ selectedFile: event.target.files[0] });
  };

  // On file upload (click the upload button)
  onFileUpload = () => {
    let scope = this;
    if (this.state.selectedFile != undefined) {
      // Create an object of formData
      const formData = new FormData();

      // Update the formData object
      formData.append(
        "myFile",
        this.state.selectedFile,
        this.state.selectedFile.name
      );
      var reader = new FileReader();

      reader.readAsDataURL(this.state.selectedFile);
      reader.onload = function () {
          scope.state.attachments.push(
          {
            data: reader.result,
            name: scope.state.selectedFile.name,
            type: scope.state.selectedFile.type
          });
           scope.setState({ attachments: scope.state.attachments, selectedFile: undefined });
      };
    } else {
      ErrorPopup("من فضلك اختر ملف قبل الرفع");
    }
  };

  //return a promise that resolves with a File instance, badse64 to File -not needed now-
  urltoFile = (url, filename, mimeType) => {
    return (fetch(url)
      .then(function (res) { return res.arrayBuffer(); })
      .then(function (buf) { return new File([buf], filename, { type: mimeType }); })
    );
  }



  // File content to be displayed after
  // file upload is complete
  fileData = () => {
    if (this.state.selectedFile) {
      return (
        <div>
          <h2>بيانات الملف</h2>

          <p>اسم الملف  : {this.state.selectedFile.name}</p>

          <p>نوع الملف  : {this.state.selectedFile.type}</p>

        </div>
      );
    } else {
      return (
        <div>
        </div>
      );
    }
  };

  handleAttachments = () => {
    return <Row>
      {this.state.attachments.map(function (object, i) {
        return <Col> <a href={object.data} download={object.name}>
          <div style={{display:"table-caption", textAlign: "center"}}>
            <img width={"65px"} src={proImage} />
            <Label>{object.name}</Label>
          </div>
        </a>
        </Col>;
      })}
    </Row>
  }

  render() {

    return (
      <div>
        <h1>إضافة ملحقات</h1>
        <div>
          <label >
            <img width={"65px"} src={addImage} style={{ cursor: "pointer" }} />
            <input
              style={{ display: "none" }}
              type="file"
              onChange={this.onFileChange}
              accept="
          .jpeg,
          .jpg,
          .png,
          .xls,
          .xlsx,
          .pdf,
          .pptx,
          .doc,
          .docx,
          .txt,
          "/>

          </label>
          <Button color="primary" onClick={this.onFileUpload}
            style={{ borderRadius:"50px",
          height:"65px"}}
          >رفع</Button>

        </div>
        {this.fileData()}
        {this.handleAttachments()}

      </div>
    );
  }
}

export default App;
