// ** React Imports
import React from "react";
import { X } from "react-feather";
import {
  Modal,
  ModalHeader,
  ModalBody
} from "reactstrap";

const CustomModal = ({ 
    open,
    handleModal,
    title,
    body,
    maxWidth="50%"
    }) => {

    const CloseBtn = (
        <X className="cursor-pointer" size={15} onClick={handleModal} />
    );
  
    const cardTitle = (
        title && (
            <h4
              style={{ textAlign:"center", fontSize: "200%" }}
            >
                {title}
            <hr />
            </h4>)
    )

  return (
    <Modal isOpen={open} toggle={handleModal} style={{ maxWidth: maxWidth }}>

      <ModalHeader
        className="mb-3"
        toggle={handleModal}
        close={CloseBtn}
        tag="div" 
      >
      </ModalHeader>
      <ModalBody className="flex-grow-1">

          {cardTitle}
 
          <div style={{padding: "8px"}}>
            {body}
          </div>

      </ModalBody>
    </Modal>
  );
};

export default CustomModal;
