import jsPDF from "jspdf"
import "jspdf-autotable"
import { logo } from "./logobase64"
import moment from 'moment-timezone';
moment.locale('ar_SA');

let start = 0
const drawPageHeader = (doc) => {
  let title;

  //Report Title
  doc.setFontSize(12)
  title = 'مقايسة'
  doc.text(title || "", 380, 35);

  //Right Title
  doc.setFontSize(8)
  title = 'وزارة الدفاع'
  doc.text(title || "", 750, 25);
  title = ' - إدارة نظم المعلومات'
  doc.text(title || "", 740, 45);
  title = '(SC)'
  doc.text(title || "", 720, 45);

  //add MMC logo here
  var img = new Image()
  img.src = logo
  doc.addImage(img, 'png', 55, 10, 30, 25)
  //Left Title
  doc.setFontSize(8)
  title = 'نموذج رقم'
  doc.text(title || "", 65, 45);
  title = '3)'
  doc.text(title || "", 55, 45);
  title = '(أت'
  doc.text(title || "", 40, 45);

  //draw line
  doc.setLineWidth(0.5);
  doc.line(40, 55, 800, 55);

}

const drawHeaderTables = (doc, _, form3atDetail) => {
  let title;
  doc.setFontSize(9)

  //Left Table
  doc.setDrawColor(0);
  doc.rect(40, 65, 230, 20);

  doc.rect(40, 85, 90, 20);
  doc.rect(40, 105, 90, 20);

  doc.rect(130, 85, 140, 20);
  doc.rect(130, 105, 140, 20);

  title = 'إذن التشغيل/التصليح'
  doc.text(title || "", 190, 75, { align: 'right' });

  title = 'رقم'
  doc.text(title || "", 205, 95, { align: 'right' });
  title = form3atDetail.form3atDetailNum
  doc.text(title || "", 205, 116, { align: 'center' });

  title = 'التاريخ'
  doc.text(title || "", 95, 95, { align: 'right' });
  title = moment(new Date(form3atDetail.form3atDetailDate)).format("D-M-YYYY")
  doc.text(title || "", 95, 116, { align: 'center' });

  //Middle Table
  doc.setDrawColor(0);
  doc.rect(290, 65, 230, 20);

  doc.rect(290, 85, 90, 20);
  doc.rect(290, 105, 90, 20);

  doc.rect(380, 85, 140, 20);
  doc.rect(380, 105, 140, 20);

  title = 'التصديق'
  doc.text(title || "", 415, 75, { align: 'right' });
  title = 'رقم'
  doc.text(title || "", 455, 95, { align: 'right' });
  title = form3atDetail.wmsForm3atMasterDto?.form3atMasterNum
  doc.text(title || "", 455, 116, { align: 'center' });

  title = 'التاريخ'
  doc.text(title || "", 345, 95, { align: 'right' });
  title = moment(new Date(form3atDetail.wmsForm3atMasterDto?.form3atApproveDate)).format("D-M-YYYY")
  doc.text(title || "", 345, 116, { align: 'center' });


  //Right Table
  doc.setDrawColor(0);
  doc.rect(540, 65, 230, 20);

  doc.rect(540, 85, 90, 20);
  doc.rect(540, 105, 90, 20);

  doc.rect(630, 85, 140, 20);
  doc.rect(630, 105, 140, 20);

  title = 'المقايسة'
  doc.text(title || "", 665, 75, { align: 'right' });
  title = 'رقم'
  doc.text(title || "", 705, 95, { align: 'right' });
  title = form3atDetail.mokaysaNum
  doc.text(title || "", 705, 116, { align: 'center' });

  title = 'التاريخ'
  doc.text(title || "", 595, 95, { align: 'right' });
  title = moment(new Date(form3atDetail.mokaysaDate)).format("D-M-YYYY")
  doc.text(title || "", 595, 116, { align: 'center' });

  // form3at detail data
  title = `اسم المعدة : ${form3atDetail.itemName}`
  doc.text(title || '', 700, 150, { align: 'center' });

  title = `${form3atDetail.fscCode}-${form3atDetail.niinCode} : كود المعدة`
  doc.text(title || '', 500, 150, { align: 'center' })

  title = `${form3atDetail.partNumber} : رقم العينة`
  doc.text(title || '', 300, 150, { align: 'center' })

  title = `${form3atDetail.itemLotSerialNum} : رقم اللوط/المسلسل`
  doc.text(title || '', 150, 150, { align: 'center' })
}

const drawItemsTable = (doc, data, _) => {
  doc.setFontSize(9)
  // TABLE PHASE
  const headers = [[
    'م',
    'كود الصنف',
    'رقم العينة',
    'اسم الصنف',
    'وحدة الصرف',
    'الكمية',
    'حالة الصنف',
    'نوع بند المقايسة',
    'حالة الطلب'
  ].reverse()]

  const temp = data.map((item) => {
    return [
      ++start,
      item.nsn,
      item.partNumber,
      item.itemName,
      item.itemUiName,
      item.itemQty,
      item.itemConditionName,
      item.mokaysaTypeName,
      item.mokaysaItemStatusName,
    ].reverse();
  })

  const content = {
    startY: 170,
    head: headers,
    body: temp,
    styles: { halign: 'center', font: "Amiri-Regular", fontStyle: "normal" },
    columnStyles: {
      0: { minCellWidth: 50 },
      1: { minCellWidth: 50 },
      2: { minCellWidth: 40 },
      3: { minCellWidth: 40 },
      4: { minCellWidth: 40 },
      5: {},
      6: {},
      7: {},
    },
  }
  doc.setFontSize(9)
  doc.autoTable(content)
}

const drawFooterTables = (doc, _, form3atDetail, persons) => {
  let title;

  let storeBoss = persons.storeBoss
  let workshopBoss = persons.workshopBoss

  let repairPersonData = {
    title: form3atDetail.repairUserTitle || "",
    name: form3atDetail.repairUserName || "",
  }

  let workshopLeaderData = {
    title: (workshopBoss?.titleName || ""),
    name: workshopBoss?.personName
  }

  let warehouseLeaderData = {
    title: (storeBoss?.titleName || ""),
    name: storeBoss.personName
  }

  const INITIAL_X = 500
  const DIFF_BETWEEN_TWO_LINES = 15

  // line 1
  doc.setFontSize(12)
  title = "/ التوقيع"
  doc.text(title || "", 800, INITIAL_X + (0 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });
  doc.text(title || "", 500, INITIAL_X + (0 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });
  doc.text(title || "", 200, INITIAL_X + (0 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });

  // line 1
  title = "/ الرتبة"
  doc.text(title || "", 800, INITIAL_X + (1 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });
  title = repairPersonData.title
  doc.text(title || "", 765, INITIAL_X + (1 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });


  title = "/ الرتبة"
  doc.text(title || "", 500, INITIAL_X + (1 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });
  title = workshopLeaderData.title
  doc.text(title || "", 465, INITIAL_X + (1 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });

  title = "/ الرتبة"
  doc.text(title || "", 200, INITIAL_X + (1 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });
  title = warehouseLeaderData.title
  doc.text(title || "", 165, INITIAL_X + (1 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });

  // line 2
  title = "/ الاسم"
  doc.text(title || "", 800, INITIAL_X + (2 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });
  title = repairPersonData.name
  doc.text(title || "", 765, INITIAL_X + (2 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });


  title = "/ الاسم"
  doc.text(title || "", 500, INITIAL_X + (2 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });
  title = workshopLeaderData.name
  doc.text(title || "", 465, INITIAL_X + (2 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });

  title = "/ الاسم"
  doc.text(title || "", 200, INITIAL_X + (2 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });
  title = warehouseLeaderData.name
  doc.text(title || "", 165, INITIAL_X + (2 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });

  // line 3
  title = "القائم بالاصلاح"
  doc.text(title || "", 800, INITIAL_X + (3 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });

  title = "قائد الورشة"
  doc.text(title || "", 500, INITIAL_X + (3 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });

  title = "قائد المستودع الرئيسى لذخيرة م.ع"
  doc.text(title || "", 200, INITIAL_X + (3 * DIFF_BETWEEN_TWO_LINES), { align: 'right' });

}

const callFunctions = (doc, data, form3atDetail, persons, newPage) => {
  if(newPage) doc.addPage()
  drawPageHeader(doc, data, form3atDetail);
  drawHeaderTables(doc, data, form3atDetail);
  drawItemsTable(doc, data, form3atDetail);
  drawFooterTables(doc, data, form3atDetail, persons);
}

const generate3atReport = (data, form3atDetail, persons) => {
  if (data) {
    const unit = "pt"
    const size = "A4" // Use A1, A2, A3 or A4
    const orientation = "landscape" // portrait or landscape
    const doc = new jsPDF(orientation, unit, size)
    doc.addFont('Amiri-Regular-normal.ttf', 'Amiri-Regular', 'normal')
    doc.setFont('Amiri-Regular')

    let tenRows = []
    let newPage = false
    for (let row of data) {
      tenRows.push(row)
      if (tenRows.length == 10) {
        callFunctions(doc, tenRows, form3atDetail, persons, newPage)
        newPage = true
        tenRows = []
      }
    }
    if(tenRows.length > 0) callFunctions(doc, tenRows, form3atDetail, persons, newPage)

    //to print
    //doc.autoPrint({variant: 'non-conform'})
    //window.open(doc.output('bloburl'));
    //to save
    doc.save(`مقايسة.pdf`)
  }
}
export default generate3atReport;
