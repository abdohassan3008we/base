import { useEffect, useState } from "react";
import ReactSelect, { components } from "react-select";
import { selectThemeColors } from "@utils";

/**
 * Search from the given dropdown options
 *
 * @param {Function} onChange
 * @param {String} name HTML name
 * @param {String} id HTML ID
 * @param {Array} options [{ value: 'value', label: 'Label' }]
 * @param {String|Number} defaultValue selected default value
 * @param {Boolean} isLoading
 * @param {Boolean} isClearable
 * @param {Object} customComponents
 * @returns {JSX.Element}
 * @author Mohamed Abdul-Fattah
 */
export default function Select({
  onChange,
  name = "",
  id = "",
  options = [],
  defaultValue = undefined,
  isLoading = false,
  isClearable = true,
  customComponents = {},
  disabled = false,
}) {
  const [key, setKey] = useState(1);
  const forceUpdate = () => {
    setKey((key) => key + 1);
  };

  useEffect(() => {
    forceUpdate();
  }, [defaultValue, options]);

  /**
   * Customize the selected option
   *
   * @param {Object} e
   * @returns {JSX.Element}
   */
  const SingleValue = ({ children, ...props }) => (
    <components.SingleValue {...props}>
      <span title={children}>{children}</span>
    </components.SingleValue>
  );

  return (
    <ReactSelect
      key={key}
      menuShouldScrollIntoView
      placeholder=""
      isRtl
      menuPlacement="auto"
      menuPortalTarget={document.body}
      styles={{ menuPortal: (base) => ({ ...base, zIndex: 9999 }) }}
      defaultValue={options?.find((o) => o.value === defaultValue)}
      isLoading={isLoading}
      loadingMessage={(_) => "جارٍ التحميل.."}
      noOptionsMessage={(_) => "ﻻ توجد نتائج"}
      theme={selectThemeColors}
      className="react-select"
      classNamePrefix="select"
      name={name}
      id={id}
      isDisabled={disabled}
      options={options}
      isClearable={isClearable}
      onChange={onChange}
      components={{ SingleValue, ...customComponents }}
    />
  );
}
