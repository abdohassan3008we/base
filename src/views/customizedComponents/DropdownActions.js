import React, { useEffect, useState } from "react";
import * as RStrap from 'reactstrap'
import { useDispatch } from "react-redux";
import * as actionsHelpers from "../../utility/processes/formActions"
import { getAvailableNextStatusRequestAsync } from "../../redux/admin/grantedFormAction/thunk";

const getDefaultAction = (action, formId, dispatch, onSuccess, onFailure) => {
  const defaultActionWithHandler = actionsHelpers.appendHandlerToActions(action, formId, dispatch, {onSuccess, onFailure})[0];

  const defaultAction = (
    <RStrap.Button
      key={0}
      size="sm"
      color="primary"
      onClick={defaultActionWithHandler.handler}
    >
      {defaultActionWithHandler.title}
    </RStrap.Button>
  )

  return defaultAction
}

const getDropdownActions = (actions, formId, dispatch, onSuccess, onFailure) => {
  const dropdownActionWithHandler = actionsHelpers.appendHandlerToActions(actions, formId, dispatch, {onSuccess, onFailure});

  const dropdownActions = dropdownActionWithHandler.map((action, index) => {
    return (
      <>
        <RStrap.DropdownItem
          tag='a'
          key={index}
          size="sm"
          color="primary"
          onClick={action.handler}
        >
          {action.title}
        </RStrap.DropdownItem>
      </>
    )
  })

  return dropdownActions
}

export default function DropdownActions({ formTypeCode, currentStatusCode, formId, onSuccess = () => {}, onFailure = () => {} }) {
  const dispatch = useDispatch();
  const [actions, setActions] = useState([])

  useEffect(() => {
    dispatch(getAvailableNextStatusRequestAsync(
      {
        formTypeCode,
        currentStatusCode,
      }, {
      onSuccess: res => { setActions(res) }
    }
    ))
  }, [formTypeCode, currentStatusCode])

  const defaultAction = actions.find(a => a.defaultAction)
          ? actions.filter(a => a.defaultAction)
          : actions.length > 0 ? [ actions[0] ] : []
  const nonDefaultActions = actions.filter(a => a !== defaultAction[0])
  const dropdownActions = getDropdownActions(nonDefaultActions, formId, dispatch, onSuccess, onFailure)

  return (
    <>
      <RStrap.UncontrolledButtonDropdown>
        {
          defaultAction.length > 0 ?
            getDefaultAction(defaultAction, formId, dispatch, onSuccess, onFailure) :
            null
        }
        {
          dropdownActions.length > 0 ? (
            <>
              <RStrap.DropdownToggle className='dropdown-toggle-split' color='primary' caret></RStrap.DropdownToggle>
              <RStrap.DropdownMenu>{dropdownActions}</RStrap.DropdownMenu>
            </>
          ) : null
        }
      </RStrap.UncontrolledButtonDropdown>
    </>
  );
}
