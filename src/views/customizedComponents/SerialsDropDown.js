import { useEffect, useState } from "react"
import { components } from "react-select"
import { Button } from "reactstrap"
import { Plus } from "react-feather"
import Select from "./Select"
import AddNewSerialModal from "./AddNewSerialModal"
import { SERIAL } from "../../redux/ref/serialType/types"

/**
 * Generates a dropdown select for the given serials and a create new serial button
 *
 * @param {Number} catalogItemId
 * @param {String} serialTypeCode
 * @param {Array} serials
 * @param {String} defaultValue
 * @param {Function} onChange
 * @param {Boolean} isLoading
 * @returns {JSX.Element}
 * @author Mohamed Abdul-Fattah
 */
export default function SerialsDropDown({
  catalogItemId,
  serialTypeCode,
  serials,
  defaultValue,
  onChange,
  isLoading = false,
}) {
  const [openSerialModal, setOpenSerialModal] = useState(false)
  const [options, setOptions] = useState([])
  const [selected, setSelected] = useState(null)
  const title = serialTypeCode === SERIAL ? 'أضف سيريال' : 'أضف لوت'

  useEffect(() => { setOptions(serials) }, [serials])

  useEffect(() => { setSelected(parseInt(defaultValue, 10)) }, [defaultValue])

  const toggleAddNewSerialModal = () => { setOpenSerialModal(prevToggle => !prevToggle) }

  /**
   * Custom select option component for react-select
   *
   * @param {[Object]} data array of data to be displayed as options
   * @returns {JSX.Element}
   */
  const OptionComponent = ({ data, ...props }) => {
    if (data.type === 'button') {
      return (
        <Button
          className='text-left rounded-0 w-100'
          outline
          color='success'
          onClick={_ => toggleAddNewSerialModal()}
        >
          <Plus size={14} /><span className='align-middle ml-50'>{data.label}</span>
        </Button>
      );
    } else {
      return <components.Option {...props}>{data.label}</components.Option>;
    }
  }

  return (
    <>
      <Select
        defaultValue={selected}
        options={[ ...options, { type: 'button', value: 'ADD_NEW', label: title } ]}
        onChange={s => {
          setSelected(s?.value)
          onChange(s || { value: '', label: '' })
        }}
        isClearable
        isLoading={isLoading}
        customComponents={{
          Option: OptionComponent
        }}
      />
      <AddNewSerialModal
        open={openSerialModal}
        handleModal={toggleAddNewSerialModal}
        catalogItemId={catalogItemId}
        serialTypeCode={serialTypeCode}
        onSuccess={s => {
          setOptions(state => [ ...state, s ])
          setSelected(s?.value)
          onChange(s || { value: '', label: '' })
        }}
      />
    </>
  )
}
