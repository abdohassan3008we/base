import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
const MySwal = withReactContent(Swal)

const ConfirmCancelPopup = (warningMsg, confirmButton,closeButton) => {
 
    return MySwal.fire({
      text: warningMsg,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: closeButton,
      confirmButtonText: confirmButton,
      customClass: {
        confirmButton: 'btn btn-primary',
        cancelButton: 'btn btn-danger ml-1'
      },
      buttonsStyling: false,
      position: "bottom-end",
    })
  }

  export default ConfirmCancelPopup
