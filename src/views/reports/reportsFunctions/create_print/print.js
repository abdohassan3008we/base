import React, { useRef } from "react";
import { useReactToPrint } from "react-to-print";
import { Button } from "reactstrap";

export const types = {
  landscape: "landscape",
  portrait: "portrait",
};
export const Print = ({
  Document,
  title,
  pageLayout = types.landscape,
  // pageLayout = "l", // "landscape" | "vertical" | "l" | "v" | "portrait" | "horizontal" | "p" | "h"

  documentDownloadName = "report_" + Date.now(),
  showCard = false,
  scale = 1,
  width,
  height,
}) => {
  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
    documentTitle: `${documentDownloadName}.pdf`,
    copyStyles: true,
  });

  const styles = getPrintCardStyles({
    width: width,
    height: height,
    scale: scale,
    showCard: showCard,
  });

  return (
    <div style={styles.containerStyle}>
      <div style={styles.titleStyle}>{showCard ? <h3>{title}</h3> : <></>}</div>

      <div style={styles.modelStyle}>
        <Document ref={componentRef} />
      </div>

      <Button style={styles.btnStyle} color="primary" onClick={handlePrint}>
        {showCard ? "عرض" : title}
      </Button>
    </div>
  );
};

function getPrintCardStyles({ width, height, scale, showCard }) {
  const scaleWidth = scale * 240;
  const scaleHeight = scale * 375;

  const containerW = width ?? `${scaleWidth}px`;
  const titleW = "100%";
  const modelW = "100%";
  const btnW = "100%";

  const containerH =
    height ?? showCard ? `${scaleHeight}px` : `${(12.5 / 100) * scaleHeight}px`;
  const titleH = showCard ? "12.5%" : "0%";
  const modelH = showCard ? "75%" : "0%";
  const btnH = showCard ? "12.5%" : "100%";

  const containerStyle = {
    textAlign: "center",
    width: containerW,
    height: containerH,
  };
  const titleStyle = {
    margin: "auto",
    textAlign: "center",
    borderRadius: "20px 20px 0px 0px",
    backgroundColor: "#125478",
    width: titleW,
    height: titleH,
    padding: showCard ? "4% 0" : "",
  };
  const modelStyle = {
    margin: "auto",
    backgroundColor: "#ffffff",
    width: modelW,
    height: modelH,
    zoom: showCard ? "20%" : ".0001%",
    display: showCard ? "" : "none",
  };
  const btnStyle = {
    margin: "auto",
    display: "block",
    textAlign: "center",
    width: btnW,
    height: btnH,
    borderRadius: showCard ? "0px 0px 25px 25px" : "10px",
  };
  return {
    containerStyle,
    titleStyle,
    modelStyle,
    btnStyle,
  };
}
