import React from "react";
import "../../reportsFunctions/design/styles/styles.css";
import { reportBody, reportFooter, reportHeader } from "../design/styles";

export const CreateReport =
  ({
    data = (params) => {}, // returns object with data that needed to create HTML age
    HTML = (data) => {}, // returns HTML code
    HeaderComponent,
    FooterComponent,
    width = 90,
    height = 90,
    moveDown = 0,
    moveLeft = 0,
    landscape = false,
  }) =>
  ({ header = {}, body = {}, footer = {} } = {}) =>
    React.forwardRef((_, ref) => {
      // const d = Redesign(data({ header, body, footer }));
      const d = data({ header, body, footer });
      const Report = () => HTML(d.body);
      // moveDown = moveDown + (landscape ? 7 : 0);
      // moveLeft = moveLeft + (landscape ? 7 : 0);
      const sizeStyle = landscape
        ? {
            width: `${width * 1.414}%`,
            height: `${height * 0.707}%`,
            overflow: "hidden",
            transform: "rotate(-90deg)",
          }
        : {
            width: `${width}%`,
            height: `${height}%`,
          };
      return (
        <div
          ref={ref}
          style={{
            direction: "rtl",
            textAlign: "right",
            marginRight: `calc(${(100 - width) / 2 + moveLeft}%)`,
            marginLeft: `calc(${(100 - width) / 2 - moveLeft}%)`,
            marginTop: `calc(${(100 - height) / 2 + moveDown}%)`,
            marginBottom: `calc(${(100 - height) / 2 - moveDown}%)`,
            ...sizeStyle,
          }}
        >
          <div className={reportHeader}>
            {HeaderComponent ? <HeaderComponent {...d.header} /> : <></>}
          </div>
          <div className={reportBody}>
            <Report />
          </div>
          <div className={reportFooter}>
            {FooterComponent ? <FooterComponent {...d.footer} /> : <></>}
          </div>
        </div>
      );
    });
