const convertNumbers = (data) => {
    return (data + "").replaceAll("0", "٠")
        .replaceAll("1", "١")
        .replaceAll("2", "٢")
        .replaceAll("3", "٣")
        .replaceAll("4", "٤")
        .replaceAll("5", "٥")
        .replaceAll("6", "٦")
        .replaceAll("7", "٧")
        .replaceAll("8", "٨")
        .replaceAll("9", "٩")
}

export default function ConvertDataNumbers(data) {
    if (data) {
        // if the data is an object
        if (typeof data === "object") {
            Object.keys(data).map(key => {
                data[key] = ConvertDataNumbers(data[key])
            })
        }
        // otherwise
        else {
            data = convertNumbers(data)
        }
    }
    return data
}