export {
    reportHeader,
    reportTitle,
    reportBody,
    reportFooter,
    reportBold,
} from './tagsIds'
