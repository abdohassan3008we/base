import { posOfTitleCode } from "../../../../utility/Utils";

export const sortMembersByRole =
    (data) =>
        data.sort((member1, member2) => {
            if (member1.memberJobCode === "COMMHED" || posOfTitleCode(member1.memberTitleCode) > posOfTitleCode(member2.memberTitleCode)) return -1;
            if (member2.memberJobCode === "COMMHED" || posOfTitleCode(member1.memberTitleCode) < posOfTitleCode(member2.memberTitleCode)) return 1;
            return 0;
        })