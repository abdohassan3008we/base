import { reportBold } from "../design/styles";

export const MembersSigneture = ({
  members = [[]],
  reverseOrder = false,
  width = "100%",
  marginRight = "0%",
  marginLeft = "0%",
  bold = false,
} = {}) => {
  let roleTextAlign = "right";
  if (members.length === 0) {
    return <></>;
    // }
    // else if (members.length === 1) {
    //   keys = Object.keys(members[0]);
  } else if (members.length > 1) {
    // keys = Object.keys(members[0]);
    roleTextAlign = "center";
    // } else {
    //   keys = Object.keys(members);
    //   members = [members];
  }
  members = reverseOrder ? members.reverse() : members;
  console.log(members);
  return (
    <>
      <div id={bold ? reportBold : ""}>
        <table
          style={{
            width: width,
            borderCollapse: "collapse",
            border: "none",
            borderWidth:"2px",
            marginRight: `calc(${marginRight})`,
            marginLeft: `calc(${marginLeft})`,
          }}
        >
          <tbody>
            <tr>
              {members?.map((_) => (
                <td style={{ border: "none rgb(0, 0, 0)", textAlign: "right" }}>
                  /التوقيع
                </td>
              ))}
            </tr>
            {bold ? <br /> : <></>}
            <tr>
              {members?.map((member) => (
                <td style={{ border: "none rgb(0, 0, 0)", textAlign: "right" }}>
                  {typeof member == "object" && member.length
                    ? member.map((m, index) => {
                        return (
                          <>
                            {m}
                            <br />
                            {bold ? <br /> : <></>}
                          </>
                        );
                      })
                    : member}
                </td>
              ))}
            </tr>
          </tbody>
        </table>
      </div>
    </>
  );
};
