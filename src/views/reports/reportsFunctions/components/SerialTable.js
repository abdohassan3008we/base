import convertDataNumbers from "../design/convertNumbers"

export const SerialTable = (
    {
        columns = null,
        data,
        hideSerial,
        hideHeader
    } = {}
) => {
    if (!columns) {
        columns = []
        if (data.length > 0)
            columns = Object.keys(data[0])
    }

    return <>
        <table style={{ width: '100%', borderCollapse: 'collapse', border: 'solid' }}>
            {hideHeader ? <></> :
                <thead>
                    <tr>
                        {hideSerial ? <></> : <th style={{ textAlign: 'center', border: 'solid' }}>م</th>}
                        {
                            columns?.map(col =>
                                <th style={{ textAlign: 'center', border: 'solid' }}>{col}</th>
                            )
                        }
                    </tr>
                </thead>
            }
            <tbody>
                {
                    data?.map((item, index) =>
                        <tr>
                            {
                                hideSerial ? <></> : <td style={{ textAlign: 'center', border: 'solid' }}>{convertDataNumbers(index + 1)}</td>
                            }
                            {
                                columns.map(col =>
                                    <td style={{ textAlign: 'center', border: 'solid rgb(150,150,150)' }}>{item[col]}</td>
                                )
                            }
                        </tr>
                    )
                }
            </tbody>
        </table>
    </>

}