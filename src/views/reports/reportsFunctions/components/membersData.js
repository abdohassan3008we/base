import convertDataNumbers from "../design/convertNumbers"

export const MembersData = (
    {
        members = [],
        reverseOrder = false,
        hideSerial = true,
        width = "100%",
        marginRight = "0%",
        marginLeft = "0%",
    } = {}
) => {

    members = reverseOrder ? members.reverse() : members
    return <>
        <table style={{ width: width, borderCollapse: 'collapse', border: 'none', marginRight: `calc(${marginRight})`, marginLeft: `calc(${marginLeft})` }}>
            <tbody>
                {
                    members.map((member, row) => {
                        const keys = Object.keys(member)
                        return <tr>
                            {
                                hideSerial ? <></> : <td style={{ border: 'none', textAlign: 'right' }}>{convertDataNumbers(row + 1)}- </td>
                            }
                            <td style={{ border: 'none', textAlign: 'right' }}>
                                {member[keys[0]]} / {member[keys[1]]}
                            </td>
                            {
                                keys.map((key, col) => {
                                    return (col < 2) ? <></> :
                                        <td style={{ border: 'none', textAlign: 'right' }}>
                                            {member[key]}
                                        </td>
                                })
                            }
                        </tr>
                    })
                }
            </tbody>
        </table>
    </>
}