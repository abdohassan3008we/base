export const GetData = ({ header = {}, body = {}, footer = {} } = {}) => {
  header = {
    ...header,
  };
  body = {
    ...body,
    from: 2010,
    to: 2011,
  };
  footer = {
    ...footer,
  };
  return {
    header,
    body,
    footer,
  };
};
