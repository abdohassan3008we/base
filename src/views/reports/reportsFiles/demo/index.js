import { CreateReport } from "../../reportsFunctions";
import { DefaultHeader } from "../../reportsFunctions/components";
import { GetData } from "./data";
import { CreateHTML } from "./html";

export const report = CreateReport({
  data: GetData,
  HTML: CreateHTML,
  // HeaderComponent: DefaultHeader,
});
