import { MembersData, MembersSigneture } from "../../reportsFunctions";
import { SerialTable } from "../../reportsFunctions/components/SerialTable";
import { reportTitle } from "../../reportsFunctions/design/styles";
import "../../reportsFunctions/design/styles/styles.css";

export const CreateHTML = (data) => {
  return <HtmlComponent data={data} />;
};

const HtmlComponent = ({ data }) => (
  <div dir="ltr">
    <Header fromYear={data.from} toYear={data.to} />
    <Table />
  </div>
);

const Header = ({ fromYear = 2023, toYear = 2024 }) => (
  <div>
    <table
      style={{
        borderCollapse: "collapse",
        width: "100%",
        height: "119.062px",
        borderStyle: "hidden",
        float: "right",
      }}
      border={0}
    >
      <tbody>
        <tr style={{ height: "50.6424px" }}>
          <td
            style={{
              width: "27.3807%",
              height: "50.6424px",
              textAlign: "center",
              borderStyle: "hidden",
            }}
          >
            <section>
              <p dir="rtl" style={{ textAlign: "center" }}>
                <span style={{ fontSize: "11pt" }}>تابع مرفق (1)</span>
              </p>
            </section>
          </td>
          <td
            dir="rtl"
            style={{
              height: "69.3056px",
              width: "37.4306%",
              textAlign: "center",
              borderStyle: "hidden",
            }}
            rowSpan={2}
          >
            مدخلات
          </td>
          <td
            dir="rtl"
            style={{
              height: "69.3056px",
              width: "35.0719%",
              textAlign: "right",
              borderStyle: "hidden",
            }}
            rowSpan={2}
          >
            <span style={{ textDecoration: "underline" }}>
              الوحدة الفرعية / الفرعية الصغرى
            </span>{" "}
            : <span style={{ textDecoration: "underline" }}>.......</span>&nbsp;
          </td>
        </tr>
        <tr style={{ height: "18.6632px" }}>
          <td
            dir="rtl"
            style={{
              width: "27.3807%",
              textAlign: "center",
              height: "18.6632px",
              borderStyle: "hidden",
            }}
          >
            &nbsp; &nbsp;نموذج رقم (2)
          </td>
        </tr>
        <tr style={{ height: "49.7569px" }}>
          <td
            style={{
              textAlign: "center",
              borderStyle: "hidden",
              width: "99.8832%",
              height: "49.7569px",
            }}
            colSpan={3}
          >
            <p dir="rtl" style={{ textAlign: "center" }}>
              خطة الإستخدام للمعدات ذات الجنزير للتدريب على (القتال - المعاونة)
              خلال العام التدريبي&nbsp; {fromYear} / {toYear}
            </p>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
);

const Table = () => {
  return (
    <>
      <div>
        <table
          style={{
            borderCollapse: "collapse",
            width: "99.9507%",
            height: "258.594px",
          }}
          border={1}
        >
          <tbody>
            <tr style={{ height: "135.816px" }}>
              <td style={{ height: "135.816px", width: "37.727%" }} colSpan={7}>
                توزيع مسافات الإستخدام المخططة للفترة التدريبية الأولى
              </td>
              <td
                style={{
                  height: "207.309px",
                  width: "19.3338%",
                  transform: "rotate(-90deg)",
                }}
                rowSpan={2}
              >
                نوع التدريب
              </td>
              <td
                style={{ height: "207.309px", width: "8.88308%" }}
                rowSpan={2}
              >
                إجمالي مسافات الإستخدام المخططة للعام التدريبي
              </td>
              <td style={{ width: "12.1228%", height: "135.816px" }}>
                قراءة بدء العام لعداد كم/ميل
              </td>
              <td
                style={{ width: "5.53886%", height: "207.309px" }}
                rowSpan={2}
              >
                النوع
              </td>
              <td
                style={{ width: "5.43436%", height: "207.309px" }}
                rowSpan={2}
              >
                رقم الجيش
              </td>
              <td
                style={{ width: "8.98759%", height: "207.309px" }}
                rowSpan={2}
              >
                وحدة فرعية / فرعية صغرى
              </td>
              <td
                style={{ width: "1.98563%", height: "207.309px" }}
                rowSpan={2}
              >
                م
              </td>
            </tr>
            <tr style={{ height: "71.4931px" }}>
              <td
                style={{
                  width: "7.00196%",
                  height: "71.4931px",
                  transform: "rotate(-90deg)",
                }}
              >
                الإجمالي للفترة
              </td>
              <td
                style={{
                  width: "5.43436%",
                  height: "71.4931px",
                  transform: "rotate(-90deg)",
                }}
              >
                الشهر6
              </td>
              <td
                style={{
                  width: "5.43436%",
                  height: "71.4931px",
                  transform: "rotate(-90deg)",
                }}
              >
                الشهر5
              </td>
              <td
                style={{
                  width: "5.43436%",
                  height: "71.4931px",
                  transform: "rotate(-90deg)",
                }}
              >
                الشهر4
              </td>
              <td
                style={{
                  width: "5.43436%",
                  height: "71.4931px",
                  transform: "rotate(-90deg)",
                }}
              >
                الشهر3
              </td>
              <td
                style={{
                  width: "4.49379%",
                  height: "71.4931px",
                  transform: "rotate(-90deg)",
                }}
              >
                الشهر2
              </td>
              <td
                style={{
                  width: "4.49379%",
                  height: "71.4931px",
                  transform: "rotate(-90deg)",
                }}
              >
                الشهر1
              </td>
              <td style={{ width: "12.1228%", height: "71.4931px" }}>
                قراءة بدء العام لعداد الساعة الموتورية
              </td>
            </tr>
            <tr style={{ height: "33.5069px" }}>
              <td style={{ width: "7.00196%", height: "33.5069px" }}>&nbsp;</td>
              <td style={{ width: "5.43436%", height: "33.5069px" }}>&nbsp;</td>
              <td style={{ width: "5.43436%", height: "33.5069px" }}>&nbsp;</td>
              <td style={{ width: "5.43436%", height: "33.5069px" }}>&nbsp;</td>
              <td style={{ width: "5.43436%", height: "33.5069px" }}>&nbsp;</td>
              <td style={{ width: "4.49379%", height: "33.5069px" }}>&nbsp;</td>
              <td style={{ width: "4.49379%", height: "33.5069px" }}>&nbsp;</td>
              <td dir="rtl" style={{ width: "19.3338%", height: "33.5069px" }}>
                &nbsp;
              </td>
              <td style={{ width: "8.88308%", height: "33.5069px" }}>&nbsp;</td>
              <td style={{ width: "12.1228%", height: "33.5069px" }}>&nbsp;</td>
              <td style={{ width: "5.53886%", height: "33.5069px" }}>&nbsp;</td>
              <td style={{ width: "5.43436%", height: "33.5069px" }}>&nbsp;</td>
              <td style={{ width: "8.98759%", height: "33.5069px" }}>&nbsp;</td>
              <td style={{ width: "1.98563%", height: "33.5069px" }}>1</td>
            </tr>
            <tr style={{ height: "17.7778px" }}>
              <td style={{ width: "7.00196%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "5.43436%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "5.43436%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "5.43436%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "5.43436%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "4.49379%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "4.49379%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "19.3338%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "8.88308%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "12.1228%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "5.53886%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "5.43436%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "8.98759%", height: "17.7778px" }}>&nbsp;</td>
              <td style={{ width: "1.98563%", height: "17.7778px" }}>2</td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  );
};
