import React from "react";
import "./styles.css";

export const MainHeader = ({
  // const siteName=localStorage.getItem("S")
  data,
}) => {
  const toYear = data?.to ?? new Date().getFullYear() + 1;
  const fromYear = data?.from ?? new Date().getFullYear();
  const siteName = data?.siteName;

  return (
    <table
      style={{
        borderCollapse: "collapse",
        width: "100%",
        height: "119.062px",
        borderStyle: "hidden",
        float: "right",
      }}
      // className="header-table"
      border={0}
    >
      <tbody>
        <tr style={{ height: "50.6424px" }}>
          <td
            style={{
              width: "27.3807%",
              height: "50.6424px",
              textAlign: "center",
              borderStyle: "hidden",
            }}
          >
            <section>
              <p dir="rtl" style={{ textAlign: "center" }}>
                <span style={{ fontSize: "11pt" }}>تابع مرفق (1)</span>
              </p>
            </section>
          </td>
          <td
            dir="rtl"
            style={{
              height: "69.3056px",
              width: "37.4306%",
              textAlign: "center",
              borderStyle: "hidden",
            }}
            rowSpan={2}
          >
            مدخلات
          </td>
          <td
            dir="rtl"
            style={{
              height: "69.3056px",
              width: "35.0719%",
              textAlign: "right",
              borderStyle: "hidden",
            }}
            rowSpan={2}
          >
            <span style={{ textDecoration: "underline" }}>
              الوحدة الفرعية / الفرعية الصغرى
            </span>{" "}
            : <span style={{ textDecoration: "underline" }}>{siteName}</span>
            &nbsp;
          </td>
        </tr>
        <tr style={{ height: "18.6632px" }}>
          <td
            dir="rtl"
            style={{
              width: "27.3807%",
              textAlign: "center",
              height: "18.6632px",
              borderStyle: "hidden",
            }}
          >
            &nbsp; &nbsp;نموذج رقم (2)
          </td>
        </tr>
        <tr style={{ height: "49.7569px" }}>
          <td
            style={{
              textAlign: "center",
              borderStyle: "hidden",
              width: "99.8832%",
              height: "49.7569px",
            }}
            colSpan={3}
          >
            <p dir="rtl" style={{ textAlign: "center" }}>
              خطة الإستخدام للمعدات ذات الجنزير للتدريب على (القتال - المعاونة)
              خلال العام التدريبي {fromYear} /{toYear}
            </p>
          </td>
        </tr>
      </tbody>
    </table>
  );
};

MainHeader.defaultProps = {
  data: {
    from: new Date().getFullYear(),
    to: new Date().getFullYear() + 1,
    siteName: "",
  },
};
