export const TableHeaderRow = () => {
  return (
    <>
      <tr>
        <th
          style={{
            textAlign: "center",
          }}
          colSpan={7}
        >
          توزيع مسافات الإستخدام المخططة للفترة التدريبية الثالثة
        </th>
        <th
          style={{
            textAlign: "center",
          }}
          colSpan={7}
        >
          توزيع مسافات الإستخدام المخططة للفترة التدريبية الثانيه
        </th>
        <th
          style={{
            textAlign: "center",
          }}
          colSpan={7}
        >
          توزيع مسافات الإستخدام المخططة للفترة التدريبية الأولى
        </th>
        <th
          // style={{
          //   height: "135px",
          //   width: "30%",
          //   textAlign: "center",
          //   transform: "rotate(-90deg)",
          // }}
          rowSpan={2}
        >
          نوع التدريب
        </th>
        <th rowSpan={2}>إجمالي مسافات الإستخدام المخططة للعام التدريبي</th>
        <th>قراءة بدء العام لعداد كم/ميل</th>
        <th rowSpan={2}>النوع</th>
        <th rowSpan={2}>رقم الجيش</th>
        <th rowSpan={2}>وحدة فرعية / فرعية صغرى</th>
        <th rowSpan={2}>م</th>
      </tr>

      <tr style={{ height: "87.4826px" }}>
        <Period from={13} to={18} />
        <Period from={7} to={12} />
        <Period from={1} to={6} />

        <th style={{ width: "11.8435%", height: "87.4826px" }}>
          قراءة بدء العام لعداد الساعة الموتورية
        </th>
      </tr>
    </>
  );
};

const Period = ({ from, to }) => {
  const times = to - from + 1;

  return (
    <>
      <th
        style={{
          width: "6%",
          height: "85px",
          transform: "rotate(-90deg)",
        }}
      >
        الإجمالي للفترة
      </th>
      {new Array(times).fill(0).map((_, index) => {
        console.log(index + from);
        return (
          <th
            style={{
              width: "5%",
              height: "85px",
              transform: "rotate(-90deg)",
            }}
          >
            الشهر{to - index}
          </th>
        );
      })}
    </>
  );
};
