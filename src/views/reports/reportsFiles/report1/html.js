import { MembersSigneture } from "../../reportsFunctions";
import { MainHeader } from "./MainHeader";
import { TableHeaderRow } from "./TableHeaderRow";
import { SubjectRow } from "./subject";
// import Header from "./header";

export const CreateHTML = (data) => {
  return <HtmlComponent data={data} />;
};

const HtmlComponent = ({ data }) => (
  <div dir="ltr">
    <MainHeader data={data.header} />
    <Table data={data.AsnafSerialandAsnafRate} />
  </div>
);
const Table = ({ data, style }) => {
  const maxPagesItemsCount = 11;
  const pagesItemsCount = 10;
  const pagesCount = Math.ceil(data.length / pagesItemsCount);
  return (
    <>
      {new Array(pagesCount).fill(0).map((_, pageNo) => {
        return (
          <div>
              <table
                style={{
                  borderCollapse: "collapse",
                  // height: "300px",
                  textAlign: "center",
                  ...style,
                }}
                border={3}
              >
                <thead>
                  <TableHeaderRow />
                </thead>

                <tbody>
                  {data?.map((row, index) => {
                    if (
                      pageNo * pagesItemsCount <= index &&
                      index < pageNo * pagesItemsCount + pagesItemsCount
                    )
                      return <SubjectRow row={row} id={index + 1} />;
                  })}
                </tbody>
              </table>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <MembersSigneture
              bold
              marginRight="5%"
              members={["مستخدم الوحدة الفرعية/الفرعية الصغرى"]}
            />
            <div
              style={{
                // background: "#f00",
                height: `${
                  (maxPagesItemsCount - pagesItemsCount) * 200 + 350
                }px`,
              }}
            ></div>
          </div>
        );
      })}
    </>
  );
};
