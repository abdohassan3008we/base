import { CreateReport } from "../../../reportsFunctions";
import { GetData } from "./data";
import { CreateHTML } from "../html";

export const report = CreateReport({
  data: GetData,
  HTML: CreateHTML,

  // landscape: true,
  // moveDown: 17,
  moveLeft: 4,
  // width: 80,
  // height: 90,
});
