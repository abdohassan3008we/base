import { useEffect, useState } from "react";
import { createListRequest } from "../../../../../utility/services/requests";

export const GetData = ({ header = {}, body = {}, footer = {} } = {}) => {
  const [data, setData] = useState([]);
  const [carData, setcarData] = useState([]);

  useEffect(() => {
    createListRequest({
      endPoint: "UsgAsnafSerial/GetAll?PageNumber=0&PageSize=0",
    })({
      dataAction: (data) => data?.result,
      setData: setData,
    });
  }, []);

  const AllData = [];
  const AllAsnafSerial = [];
  data?.map((row) => {
    if (row?.mainUsageTypeId === 1) {
      AllAsnafSerial.push({
        armyNum: row?.armyNum,
        nomenclature: row?.nomenclature,
        serialNum: row?.serialNum,
        asnafSerialId: row?.asnafSerialId,
      });
    }
  });

  useEffect(() => {
    AllAsnafSerial.map((row) =>
      createListRequest({
        endPoint: `/PlannedReport/PlannedReportCollapsForThisYear?AsnafSerialId=${row.asnafSerialId}&MainTypeUsageId=1`,
      })({
        dataAction: (carData) => carData,
        setData: (data) => {
          setcarData((prevCarDistance) => [...prevCarDistance, data]);
        },
      })
    );
  }, [data]);

  carData.map((row, index) => {
    row.map((rows, counter) => {
      if (counter == 0) {
        AllData.push({
          counterReading: rows?.counterReading,
          motorClock: rows?.motorClock,
          rateId: rows?.rateId,
          totalPlannedValue: rows?.totalPlannedValue,
          asnafSerialId: rows?.asnafSerialId,
          month: [...row],
        });
      }
    });
  });
  const uniquesubUsageTypeNamesandAsnafSerialIds = carData.map((subArray) => {
    const subUsageTypeNames = new Set(
      subArray.map((obj) => obj.subUsageTypeName)
    );
    const asnafSerialIds = new Set(subArray.map((obj) => obj.asnafSerialId));
    return {
      subUsageTypeNames: [...subUsageTypeNames],
      asnafSerialIds: [...asnafSerialIds][0],
    };
  });
  console.log(carData);
  const combinedAllDataAndAllAsnafSerial = AllData.concat(AllAsnafSerial);
  var AsnafSerialandAsnafRate = combinedAllDataAndAllAsnafSerial.reduce(
    (acc, obj) => {
      const index = acc.findIndex(
        (item) => item.asnafSerialId === obj.asnafSerialId
      );
      if (index !== -1) {
        acc[index] = { ...acc[index], ...obj };
      } else {
        acc.push(obj);
      }
      return acc;
    },
    []
  );
  console.log(AsnafSerialandAsnafRate);
  header = {
    ...header,
    siteName: JSON.parse(localStorage.getItem("userData")).siteName,
    // to:
    // from:
  };
  body = {
    ...body,
    AsnafSerialandAsnafRate,
  };
  footer = {
    ...footer,
  };
  return {
    header,
    body,
    footer,
  };
};
