export const SubjectRow = ({ row, id }) => {
  const subCount = row?.month?.length
  return (
    <>
      {row?.month?.map((sub, index) => {
        if (index === 0) {
          return (
            <>
              <tr>
                <SubSubjectTypeValues data={sub} />
                <td rowSpan={subCount+1}>{row.totalPlannedValue}</td>
                <td rowSpan={subCount-1}>{row.counterReading}</td>
                <td
                  // style={{ height: "89.6008px", width: "5.46624%" }}
                  rowSpan={subCount+1}
                >
                  <p>{row.nomenclature}</p>
                </td>
                <td
                  // style={{ height: "89.6008px", width: "5.46624%" }}
                  rowSpan={subCount+1}
                >
                  <p>{row.armyNum}</p>
                </td>
                <td
                  style={{ height: "89.6008px", width: "8.70549%" }}
                  rowSpan={subCount+1}
                >
                  <p>{row.serialNum}</p>
                </td>
                <td
                  style={{ height: "89.6008px", width: "1.5184%" }}
                  rowSpan={subCount+1}
                >
                  {id}
                </td>
              </tr>
            </>
          );
        }
        if (index == 2) {
          return (
            <>
              <tr>
                <SubSubjectTypeValues data={sub} />
                <td rowSpan={2}>{row.motorClock}</td>
              </tr>
            </>
          );
        }
        return (
          <>
            <tr>
              <SubSubjectTypeValues data={sub} />
            </tr>
          </>
        );
      })}

      <tr
        style={{
          borderBottomStyle: "solid",
          borderBottomWidth: "4px",
        }}
      >
        {row?.month
          ?.filter((_, index) => index === 0)
          ?.map((sub, i) => {
            return (
              <>
                <TotalSubValues data={sub} />
              </>
            );
          })}
      </tr>
    </>
  );
};

const SubSubjectTypeValues = ({ data }) => {
  return (
    <>
      <td
        style={{
          borderLeftStyle: "solid",
          borderLeftWidth: "3px",
          borderRightStyle: "solid",
          borderRightWidth: "3px",
        }}
      >
        {data?.totalPannedValueForSubThirdPeriod || 0}
      </td>
      <td>{data?.totalPannedValueForSub18Month || 0}</td>
      <td>{data?.totalPannedValueForSub17Month || 0}</td>
      <td>{data?.totalPannedValueForSub16Month || 0}</td>
      <td>{data?.totalPannedValueForSub15Month || 0}</td>
      <td>{data?.totalPannedValueForSub14Month || 0}</td>
      <td>{data?.totalPannedValueForSub13Month || 0}</td>
      <td
        style={{
          borderLeftStyle: "solid",
          borderLeftWidth: "3px",
          borderRightStyle: "solid",
          borderRightWidth: "3px",
        }}
      >
        {data?.totalPannedValueForSubSecondPeriod || 0}
      </td>
      <td>{data?.totalPannedValueForSub12Month || 0}</td>
      <td>{data?.totalPannedValueForSub11Month || 0}</td>
      <td>{data?.totalPannedValueForSub10Month || 0}</td>
      <td>{data?.totalPannedValueForSub9Month || 0}</td>
      <td>{data?.totalPannedValueForSub8Month || 0}</td>
      <td>{data?.totalPannedValueForSub7Month || 0}</td>
      <td
        style={{
          borderLeftStyle: "solid",
          borderLeftWidth: "3px",
          borderRightStyle: "solid",
          borderRightWidth: "3px",
        }}
      >
        {data?.totalPannedValueForSubFirstPeriod || 0}
      </td>
      <td>{data?.totalPannedValueForSub6Month || 0}</td>
      <td>{data?.totalPannedValueForSub5Month || 0}</td>
      <td>{data?.totalPannedValueForSub4Month || 0}</td>
      <td>{data?.totalPannedValueForSub3Month || 0}</td>
      <td>{data?.totalPannedValueForSub2Month || 0}</td>
      <td>{data?.totalPannedValueForSub1Month || 0}</td>
      <td>{data?.subUsageTypeName}</td>
    </>
  );
};

const TotalSubValues = ({ data }) => {
  return (
    <>
      <td
        style={{
          borderLeftStyle: "solid",
          borderLeftWidth: "3px",
          borderRightStyle: "solid",
          borderRightWidth: "3px",
        }}
      >
        {data?.totalPannedValueForThirdPeriod || 0}
      </td>
      <td>{data?.totalPannedValueForThirdPeriodMonth18 || 0}</td>
      <td>{data?.totalPannedValueForThirdPeriodMonth17 || 0}</td>
      <td>{data?.totalPannedValueForThirdPeriodMonth16 || 0}</td>
      <td>{data?.totalPannedValueForThirdPeriodMonth15 || 0}</td>
      <td>{data?.totalPannedValueForThirdPeriodMonth14 || 0}</td>
      <td>{data?.totalPannedValueForThirdPeriodMonth13 || 0}</td>
      <td
        style={{
          borderLeftStyle: "solid",
          borderLeftWidth: "3px",
          borderRightStyle: "solid",
          borderRightWidth: "3px",
        }}
      >
        {data?.totalPannedValueForSecondPeriod || 0}
      </td>
      <td>{data?.totalPannedValueForSecondPeriodMonth12 || 0}</td>
      <td>{data?.totalPannedValueForSecondPeriodMonth11 || 0}</td>
      <td>{data?.totalPannedValueForSecondPeriodMonth10 || 0}</td>
      <td>{data?.totalPannedValueForSecondPeriodMonth9 || 0}</td>
      <td>{data?.totalPannedValueForSecondPeriodMonth8 || 0}</td>
      <td>{data?.totalPannedValueForSecondPeriodMonth7 || 0}</td>
      <td
        style={{
          borderLeftStyle: "solid",
          borderLeftWidth: "3px",
          borderRightStyle: "solid",
          borderRightWidth: "3px",
        }}
      >
        {data?.totalPannedValueForFirstPeriod || 0}
      </td>
      <td>{data?.totalPannedValueForFirstPeriodMonth6 || 0}</td>
      <td>{data?.totalPannedValueForFirstPeriodMonth5 || 0}</td>
      <td>{data?.totalPannedValueForFirstPeriodMonth4 || 0}</td>
      <td>{data?.totalPannedValueForFirstPeriodMonth3 || 0}</td>
      <td>{data?.totalPannedValueForFirstPeriodMonth2 || 0}</td>
      <td>{data?.totalPannedValueForFirstPeriodMonth1 || 0}</td>
      <td>{"الإجمالي"}</td>
    </>
  );
};
